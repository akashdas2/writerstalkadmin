-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2022 at 11:19 PM
-- Server version: 5.7.36-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `writers_talk`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `uuid`, `name`, `email`, `paypal_email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '089285cc-a2ed-4d44-b484-5f82c197bab5', 'Admin', 'admin@admin.com', 'writerstalkclub@gmail.com', '2021-08-05 04:20:26', '$2y$10$p9poD3CkQ4l6P0ilyc3I/uClVoHi.q.YdiQlC0TgSTdIDCoTwyyB2', '3rEXDsiskK0hIT5zaEjsViSPR9Q2lTp06QEkGa1Fkk8xiwANw52n4kNn906D', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_posts`
--

CREATE TABLE `admin_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tip_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `published_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_posts`
--

INSERT INTO `admin_posts` (`id`, `uuid`, `user_id`, `tip_type`, `file`, `thumbnail`, `file_type`, `title`, `description`, `status`, `published_at`, `created_at`, `updated_at`) VALUES
(9, '3fd51cdd-2798-44ab-ab80-b14835919611', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/rIeMZK1641509894.mp4', NULL, 'video', 'Distractions - Adam Donahue', '<p>instagram.com</p>', 1, NULL, '2022-01-07 05:58:17', '2022-01-08 08:17:47'),
(10, '7bd352a2-bf4b-4a95-9c4b-d50fec84a16b', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/gk43KY1642793151.mp4', NULL, 'video', 'Your Why - Alee Anderson', '<p>heyyoungwriter.com</p>', 1, NULL, '2022-01-22 02:25:55', '2022-01-22 02:30:25'),
(11, 'dd55459c-4b59-49f9-a107-5cdff619cd4a', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/RlqZ7Z1642800517.mp4', NULL, 'video', 'Writing Your Memoir - Alice Sullivan', '<p><a href=\"https://alicesullivan.com/\" target=\"_blank\">https://alicesullivan.com</a></p>', 1, NULL, '2022-01-22 04:28:37', '2022-01-22 07:36:22'),
(13, 'dbf33662-d752-421f-b6b0-5e40e81b0e65', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/BuBHgE1642810437.mp4', NULL, 'video', 'Self Publishing - Allen Taylor', '<p>authorallentaylor.com</p>', 1, NULL, '2022-01-22 07:13:57', '2022-01-22 07:17:49'),
(14, '9b206849-eadb-4355-8f74-76b5c15653d0', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/PGPEHI1642812690.mp4', NULL, 'video', 'Descriptions - Allison Reid', '<p><a href=\"https://allisondreid.com/\" target=\"_blank\">https://allisondreid.com</a></p>', 1, NULL, '2022-01-22 07:51:30', '2022-01-22 07:52:58'),
(15, '90b2efcf-753a-49c6-a7e3-f577146f4c45', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/jp15Z01642904705.mp4', NULL, 'video', 'Is Your Idea Big Enough? - Ane Mulligan', '<p><a href=\"http://www.anemulligan.com/\" target=\"_blank\">anemulligan.com</a></p>', 1, NULL, '2022-01-23 09:25:05', '2022-01-23 09:26:52'),
(16, '272a3f26-a247-4b00-a26f-a963df1d1cf1', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/Rljljr1642904946.mp4', NULL, 'video', 'Writing Space - Ann Worrall Griffiths', '<p><a href=\"http://anngriffiths.com/\" target=\"_blank\">anngriffiths.com</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>#coffeeshop #quiet #music</p>', 1, NULL, '2022-01-23 09:29:06', '2022-01-23 09:35:42'),
(17, '3fe3275f-fd2a-4215-8f11-5a8ec7ad37ba', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/atQwSM1642905425.mp4', NULL, 'video', 'Novellas - Anna Augustine', '<p>instagram.com/anna_augustine_author</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>#structure</p>', 1, NULL, '2022-01-23 09:37:05', '2022-01-23 09:40:14'),
(18, '537ca9e7-8450-4f22-bb60-09513c082156', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/emUbjj1642908653.mp4', NULL, 'video', 'Hybrid Publishing - Athena Dean Holtz', '<p><a href=\"http://www.shewritesforhim.com/\" target=\"_blank\">www.shewritesforhim.com</a>&nbsp;<a href=\"http://www.redemptiompress.com/\" target=\"_blank\">www.redemptiompress.com</a>&nbsp;</p>', 1, NULL, '2022-01-23 10:30:53', '2022-01-23 10:38:29'),
(19, '3a12ce80-9f87-4a6e-aa40-7a19035ffc6a', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/D1m24o1642913612.mp4', NULL, 'video', 'Endorsements - Ben Cooper', '<p><a href=\"mailto:cooperville@atlanticbb.net\" target=\"_blank\">cooperville@atlanticbb.net</a></p>', 1, NULL, '2022-01-23 11:53:32', '2022-01-23 14:01:45'),
(20, '5d796100-09ad-47f9-9156-dedbed9b6b79', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/yRSSRe1642920863.mp4', NULL, 'video', 'Character Author - Betsy St Amant', '<p><a href=\"http://www.betsystamant/\" target=\"_blank\">https://betsystamant.com</a></p>', 1, NULL, '2022-01-23 13:54:23', '2022-01-23 14:02:40'),
(21, '41755cc3-2a8c-41cf-8b00-e82ccae9be83', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/T7fXEy1642923381.mp4', NULL, 'video', 'Mysterious Characters - Bill Myers', '<p><a href=\"http://www.billmyers.com/\" target=\"_blank\">www.billmyers.com</a></p>', 1, NULL, '2022-01-23 14:36:21', '2022-01-23 14:37:58'),
(22, '7457b2fa-0948-44d0-afc9-0f30f47fcf6d', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/jgfTkz1643037771.mp4', NULL, 'video', 'Being Real - Bogdan Yakubets', '<p>https://www.youtube.com/channel/UC54cfMkmXqLjxgdqd00CY-g -&nbsp;https://www.youtube.com/channel/UC4HX32XqFsn2AZRkVrPxorA</p>', 1, NULL, '2022-01-24 22:22:51', '2022-01-24 22:55:20'),
(23, '086a4708-f2d0-43f0-8e02-73c9e63fab73', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/FilWbX1643037820.mp4', NULL, 'video', 'Your Client\'s Story - Brian Kennard', '<p>You can reach Brian at bk@briankannard.com</p>', 1, NULL, '2022-01-24 22:23:40', '2022-01-24 22:51:41'),
(24, '73e761a1-10a5-4fe9-8e54-d5d856002066', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/UFGCDJ1643037902.mp4', NULL, 'video', 'Heroic Characters - Bryan Davis', '<p>http://daviscrossing.com</p>', 1, NULL, '2022-01-24 22:25:03', '2022-01-24 22:49:42'),
(25, '11e4e7f2-c623-4791-8690-fb11ff93df66', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/wjQP0G1643038100.mp4', NULL, 'video', 'Writers Block - Chelsea Bobulski', '<p><a href=\"https://www.instagram.com/chelseabobulski/?hl=en\" target=\"_blank\">https://www.instagram.com/chelseabobulski</a></p>', 1, NULL, '2022-01-24 22:28:20', '2022-01-24 22:47:39'),
(26, 'fd3b7052-d35b-4e81-b108-f575745293ed', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/FneW4Y1643038263.mp4', NULL, 'video', 'Who\'s Directing - Chris Dowling', '<p>https://www.instagram.com/chris_dowling_director</p>', 1, NULL, '2022-01-24 22:31:03', '2022-01-24 22:45:38'),
(27, '72e73d6b-663c-4822-8f14-63acbeba3253', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/MFQxdU1643038547.mp4', NULL, 'video', 'Write a Synopsis - Chris Jones', '<p><a href=\"http://chrisjonesink.com/\" target=\"_blank\">chrisjonesink.com</a></p>', 1, NULL, '2022-01-24 22:35:47', '2022-01-24 22:40:57'),
(28, '0e45e4bc-0d0f-4174-9f5b-7888a97c4d16', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/sTa25w1643038599.mp4', NULL, 'video', 'Well-Rounded Characters - Christina Suzann Nelson', '<p>http://www.christinasuzannnelson.com</p>', 1, NULL, '2022-01-24 22:36:39', '2022-01-25 02:35:39'),
(29, 'f4444b4f-1666-49e4-8363-4cdbdaaed33c', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/2WnD7V1643041888.mp4', NULL, 'video', 'Stay On Topic - Christina Varvel', '<p><a href=\"http://www.christinavarvel.com/\" target=\"_blank\">www.christinavarvel.com</a></p>', 1, NULL, '2022-01-24 23:31:28', '2022-01-25 02:35:16'),
(30, '8a539545-b783-46c5-94ba-aa7ab1b6df21', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/zZelf11643041981.mp4', NULL, 'video', 'Pacing - Chuck Black', '<p>&nbsp;<a href=\"http://www.chuckblack.com/\" target=\"_blank\">www.ChuckBlack.com</a></p>', 1, NULL, '2022-01-24 23:33:01', '2022-01-25 02:32:56'),
(31, '24eef9fd-9259-4d9e-84fd-31a687ee1cf5', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/Omrdt21643042079.mp4', NULL, 'video', 'Cliffhangers - Cindy Sproles', '<p>https://www.facebook.com/cindy.sproles</p>', 1, NULL, '2022-01-24 23:34:39', '2022-01-25 02:31:40'),
(32, '9b325175-6ca9-4943-b024-123623bbdd86', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/vl5zZg1643052350.mp4', NULL, 'video', 'Maximum Capacity - CJ West', '<p>https://www.amazon.com/gp/product/B003P9XAXW/ref=dbs_a_def_rwt_bibl_vppi_i1</p>', 1, NULL, '2022-01-25 02:25:50', '2022-01-25 02:29:26'),
(33, '16b631cb-69c4-4334-8e84-771e999ff5de', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/FTzYc61643069857.mp4', NULL, 'video', '1st vs. 2nd Draft - Claudia Suzanne', '<p>https://ghostwritingcentral.com</p>', 1, NULL, '2022-01-25 07:17:37', '2022-01-25 13:23:10'),
(34, '9ec734a6-17a6-4384-853d-41a74729b570', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/lLuAil1643120945.mp4', NULL, 'video', NULL, NULL, 1, NULL, '2022-01-25 21:29:06', '2022-01-25 21:29:06'),
(35, 'c995c39a-2a78-438c-8424-e4607827ac38', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/qHWAAi1643121277.mp4', NULL, 'video', 'Demo Post 2', '<p>Hello</p>', 1, NULL, '2022-01-25 21:34:39', '2022-01-25 21:34:39'),
(36, 'b2492f55-023e-424f-a6d7-4d606fe56372', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/KHEfCI1643121569.mp4', 'uploads/videos/thumbnails/lpMYq61643121569.mp4', 'video', 'Demo post', '<p>Show info</p>', 1, NULL, '2022-01-25 21:39:32', '2022-01-25 21:39:32'),
(37, 'cf94f6fa-5bc7-4430-a811-ff17234eafca', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/awK42X1643121828.mp4', 'uploads/videos/thumbnails/Mrayft1643121828.png', 'video', 'Ha ha ha', '<p>Hello world</p>', 1, NULL, '2022-01-25 21:43:51', '2022-01-25 21:43:51'),
(38, '1d74a3de-1536-4fca-87f9-49b6bb314ee7', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'pro', 'uploads/videos/sGTHP21643124293.mp4', 'uploads/videos/thumbnails/LjGCos1643124293.png', 'video', 'Testing', '<p>Testing</p>', 1, '2026-01-24 22:49:00', '2022-01-25 22:24:53', '2022-01-25 23:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `admin_post_tags`
--

CREATE TABLE `admin_post_tags` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `post_id` varchar(255) NOT NULL,
  `tag_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_post_tags`
--

INSERT INTO `admin_post_tags` (`id`, `uuid`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(9, 'b0f0d52b-b0ac-4449-abd5-429ab3a07339', '3fd51cdd-2798-44ab-ab80-b14835919611', '63712081-137f-408f-9387-c021c3cad8c0', '2022-01-07 05:58:17', '2022-01-07 05:58:17'),
(10, '8d818809-7fe6-4e58-8ede-8da828838e3c', '7bd352a2-bf4b-4a95-9c4b-d50fec84a16b', '2b21fbe1-9935-4173-9fb8-05617eeacf3f', '2022-01-22 02:25:55', '2022-01-22 02:25:55'),
(12, 'd94542da-cea4-4fd1-acfb-ff45303df334', 'dbf33662-d752-421f-b6b0-5e40e81b0e65', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-22 07:13:57', '2022-01-22 07:13:57'),
(13, '586301ac-0cf1-467d-a6f8-913ce5b431f2', '9b206849-eadb-4355-8f74-76b5c15653d0', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-22 07:51:30', '2022-01-22 07:51:30'),
(14, '66a763f2-4bdd-45c7-b5c7-d2b782646094', '90b2efcf-753a-49c6-a7e3-f577146f4c45', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-23 09:25:05', '2022-01-23 09:25:05'),
(15, '9ba29034-71a5-495b-8311-2dbbe73f6197', '272a3f26-a247-4b00-a26f-a963df1d1cf1', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-23 09:29:06', '2022-01-23 09:29:06'),
(16, 'dbdd2421-d53a-4e19-a109-0d5a1cf7cdea', '3fe3275f-fd2a-4215-8f11-5a8ec7ad37ba', '63712081-137f-408f-9387-c021c3cad8c0', '2022-01-23 09:37:05', '2022-01-23 09:37:05'),
(17, 'b845f78d-c1b1-4a0d-9d3f-607c00340f27', '537ca9e7-8450-4f22-bb60-09513c082156', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-23 10:30:53', '2022-01-23 10:30:53'),
(18, 'f9983107-ea07-49fa-ad85-d955337d40d4', '3a12ce80-9f87-4a6e-aa40-7a19035ffc6a', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-23 11:53:32', '2022-01-23 11:53:32'),
(19, 'cb013578-4b08-4863-941b-739ec37209dd', '5d796100-09ad-47f9-9156-dedbed9b6b79', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-23 13:54:23', '2022-01-23 13:54:23'),
(20, '92a4d1a5-8ec8-491c-8134-bb74cb2777af', '41755cc3-2a8c-41cf-8b00-e82ccae9be83', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-23 14:36:21', '2022-01-23 14:36:21'),
(21, '5c9f534d-1ebc-4a1c-a28e-1e985e13ab29', '7457b2fa-0948-44d0-afc9-0f30f47fcf6d', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 22:22:51', '2022-01-24 22:22:51'),
(22, 'ab131734-4707-4627-9882-075b90cbfaec', '086a4708-f2d0-43f0-8e02-73c9e63fab73', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 22:23:40', '2022-01-24 22:23:40'),
(23, 'ea624768-b106-4168-a616-5b44549a9cfa', '73e761a1-10a5-4fe9-8e54-d5d856002066', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-24 22:25:03', '2022-01-24 22:25:03'),
(24, '4d679228-d8e8-4e40-989a-0dfa891cbbc2', '11e4e7f2-c623-4791-8690-fb11ff93df66', '2ba6c8ed-d617-495d-85ad-7f2624e5c380', '2022-01-24 22:28:20', '2022-01-24 22:28:20'),
(25, '6c3585c9-1d20-4425-9574-136deb992ed6', 'fd3b7052-d35b-4e81-b108-f575745293ed', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 22:31:03', '2022-01-24 22:31:03'),
(26, '222eef45-c59e-4d86-ae32-5c14d4f654a5', '72e73d6b-663c-4822-8f14-63acbeba3253', '63712081-137f-408f-9387-c021c3cad8c0', '2022-01-24 22:35:47', '2022-01-24 22:35:47'),
(27, 'ffb4bf62-c180-4fa1-860e-6f8862bb9428', '0e45e4bc-0d0f-4174-9f5b-7888a97c4d16', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-24 22:36:39', '2022-01-24 22:36:39'),
(28, '7c84d356-cf1f-4013-82d0-9cab361d7c97', 'f4444b4f-1666-49e4-8363-4cdbdaaed33c', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 23:31:28', '2022-01-24 23:31:28'),
(29, 'c3b9f319-d6a3-43ba-b9f3-60456ae0f3c4', '8a539545-b783-46c5-94ba-aa7ab1b6df21', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 23:33:01', '2022-01-24 23:33:01'),
(30, '1074a7ca-01aa-425f-8ecc-60b898abd5ac', '24eef9fd-9259-4d9e-84fd-31a687ee1cf5', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-24 23:34:39', '2022-01-24 23:34:39'),
(31, '8bacb071-25c8-4eb3-9c17-2fe5a6c5ed61', '9b325175-6ca9-4943-b024-123623bbdd86', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-25 02:25:50', '2022-01-25 02:25:50'),
(32, '7c911fd0-266b-44ec-bc05-2a411cf54ac7', '16b631cb-69c4-4334-8e84-771e999ff5de', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-25 07:17:37', '2022-01-25 07:17:37'),
(33, '95ff694b-ea01-4fc5-9a23-5630158606ab', '9ec734a6-17a6-4384-853d-41a74729b570', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-25 21:29:06', '2022-01-25 21:29:06'),
(34, '60ac0bd7-c751-4fca-a75f-180b3f8cb5c0', '9ec734a6-17a6-4384-853d-41a74729b570', '2b21fbe1-9935-4173-9fb8-05617eeacf3f', '2022-01-25 21:29:06', '2022-01-25 21:29:06'),
(35, '27c537a9-4c05-4675-bf50-520021bd60a5', 'c995c39a-2a78-438c-8424-e4607827ac38', '2b21fbe1-9935-4173-9fb8-05617eeacf3f', '2022-01-25 21:34:39', '2022-01-25 21:34:39'),
(36, '191a2c29-e434-4961-ab9d-483f0f5e269f', 'c995c39a-2a78-438c-8424-e4607827ac38', 'b53d9e4f-b5a0-4190-81ac-fa53719b85d3', '2022-01-25 21:34:39', '2022-01-25 21:34:39'),
(37, '2b73e0a8-ff6e-461d-bd8f-5dabe54a108a', 'b2492f55-023e-424f-a6d7-4d606fe56372', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-25 21:39:32', '2022-01-25 21:39:32'),
(38, '82d2c616-4adb-4ac1-89ca-01bca708ab7b', 'b2492f55-023e-424f-a6d7-4d606fe56372', 'b53d9e4f-b5a0-4190-81ac-fa53719b85d3', '2022-01-25 21:39:32', '2022-01-25 21:39:32'),
(39, '3d9f9dd3-56e4-4fb3-990d-c5e354faeb22', 'cf94f6fa-5bc7-4430-a811-ff17234eafca', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-25 21:43:51', '2022-01-25 21:43:51'),
(40, 'a8ec3f4c-81c7-470e-b192-b6cf73781861', 'cf94f6fa-5bc7-4430-a811-ff17234eafca', '63712081-137f-408f-9387-c021c3cad8c0', '2022-01-25 21:43:51', '2022-01-25 21:43:51'),
(41, 'db08ca14-20e1-4817-92e5-37194073a89f', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', '63712081-137f-408f-9387-c021c3cad8c0', '2022-01-25 22:24:53', '2022-01-25 22:24:53'),
(42, 'e889fc1b-9af0-45fa-a0b9-affc2774ab92', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', 'ce2e52cb-63ff-4fb9-8e1e-a380f57fb8da', '2022-01-25 22:45:19', '2022-01-25 22:45:19'),
(43, 'da16b655-72ec-47a1-9735-fab7037c6276', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', '2b21fbe1-9935-4173-9fb8-05617eeacf3f', '2022-01-25 22:47:55', '2022-01-25 22:47:55'),
(44, '62651216-d4c3-460e-a38f-1801018bec64', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', 'b53d9e4f-b5a0-4190-81ac-fa53719b85d3', '2022-01-25 22:55:22', '2022-01-25 22:55:22'),
(45, 'e96eaa6b-e5bb-4427-9840-5cdc78436101', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', '2ba6c8ed-d617-495d-85ad-7f2624e5c380', '2022-01-25 22:55:22', '2022-01-25 22:55:22'),
(46, '11bc7b95-c538-40ea-afb7-e45ad11ed873', '1d74a3de-1536-4fca-87f9-49b6bb314ee7', 'dde52863-fe63-47be-a14f-c5636d8128d5', '2022-01-25 23:43:31', '2022-01-25 23:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `admin_tips`
--

CREATE TABLE `admin_tips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_tips`
--

INSERT INTO `admin_tips` (`id`, `uuid`, `user_id`, `file`, `file_type`, `created_at`, `updated_at`) VALUES
(1, 'f5bed7fe-e56f-42d2-bd36-70d306b37c38', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'uploads/videos/9Cb2pT1641043547.jpg', 'image', '2022-01-01 20:25:47', '2022-01-01 20:25:47'),
(2, 'c462200c-f717-4e81-8ff2-91436ce9dd31', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'uploads/videos/5IRGhZ1641043690.jpg', 'image', '2022-01-01 20:28:10', '2022-01-01 20:28:10'),
(3, '9a65e779-1bee-4e6e-9326-38d062ccc025', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'uploads/videos/wUHS791641043704.jpg', 'image', '2022-01-01 20:28:24', '2022-01-01 20:28:24'),
(4, '2318c064-587f-4102-953c-2252055cb695', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'uploads/videos/ebohCP1641043719.jpg', 'image', '2022-01-01 20:28:39', '2022-01-01 20:28:39'),
(5, 'a040b529-4807-40d6-8fbc-38c41ee66a16', '089285cc-a2ed-4d44-b484-5f82c197bab5', 'uploads/videos/2KROcC1641043734.jpg', 'image', '2022-01-01 20:28:54', '2022-01-01 20:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentable_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `uuid`, `user_id`, `post_id`, `parent_id`, `post_type`, `comment`, `comment_type`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`) VALUES
(1, '4d3e6435-e5b5-4090-a12d-a77687c63687', '64', '8c5ed9d1-6b37-408a-abfe-a3b9fe50f7ea', NULL, 'admin', 'gzgsgzggz', NULL, '1', 'App\\Models\\AdminPost', '2022-01-01 19:58:50', '2022-01-01 19:58:50'),
(2, '75066779-8a8a-4029-afa4-82bd0bbc7487', '64', '8c5ed9d1-6b37-408a-abfe-a3b9fe50f7ea', NULL, 'admin', 'hshshs', NULL, '1', 'App\\Models\\AdminPost', '2022-01-01 19:58:57', '2022-01-01 19:58:57'),
(3, '65f849b3-3386-4060-89c4-c26e9e018f5f', '64', '8c5ed9d1-6b37-408a-abfe-a3b9fe50f7ea', '75066779-8a8a-4029-afa4-82bd0bbc7487', 'admin', 'bbzzbz', NULL, '1', 'App\\Models\\AdminPost', '2022-01-01 19:59:01', '2022-01-01 19:59:01'),
(4, 'f586ba16-af5f-4b44-a7b6-de898c2a16a6', '64', '58fc7cf4-1e57-4d84-a35f-a79668289968', NULL, 'user', 'bzbzzh', NULL, '1', 'App\\Models\\Post', '2022-01-01 19:59:19', '2022-01-01 19:59:19'),
(5, '55fab8e9-7dc6-42af-999e-6b0f02a3d46d', '64', '17a185d6-e21c-4a74-9a12-4c807f5aca34', NULL, 'user', 'bbbbb', 'praise', '1', 'App\\Models\\Highlight', '2022-01-01 20:01:42', '2022-01-01 20:01:42'),
(6, '80a89ecc-68b9-4ccd-989a-6a9031765819', '9', '3c5647c5-9987-4efc-ab15-d3e9b180fc37', NULL, 'user', 'Nice pic', NULL, '2', 'App\\Models\\Post', '2022-01-01 21:09:53', '2022-01-01 21:09:53'),
(7, 'd19134e7-460c-47fb-b93c-da672149e605', '9', 'e2386077-c6aa-4db5-bbc8-fe0b626ec278', NULL, 'admin', 'azaz', NULL, '7', 'App\\Models\\AdminPost', '2022-01-01 21:15:12', '2022-01-01 21:15:12'),
(8, '7f495d1d-7bf9-4305-ae5d-c934d90454d1', '9', 'e2386077-c6aa-4db5-bbc8-fe0b626ec278', NULL, 'admin', 'azaz', NULL, '7', 'App\\Models\\AdminPost', '2022-01-01 21:15:20', '2022-01-01 21:15:20'),
(9, 'ODU2NA==', '66', '17a185d6-e21c-4a74-9a12-4c807f5aca34', NULL, 'user', 'qweryui', 'critique', '1', 'App\\Models\\Highlight', '2022-01-02 05:54:41', '2022-01-02 05:54:41'),
(10, '4bc51d69-bbe0-4f1e-9297-8a6d2f6e3d77', '66', '6bae9553-f25b-4b7c-9dc7-8eab88920159', NULL, 'admin', 'wow', NULL, '8', 'App\\Models\\AdminPost', '2022-01-07 06:13:41', '2022-01-07 06:13:41'),
(11, '46fc32fd-3373-4474-bb02-b17cd09f0f00', '66', '6bae9553-f25b-4b7c-9dc7-8eab88920159', NULL, 'admin', 'weigh', NULL, '8', 'App\\Models\\AdminPost', '2022-01-07 06:13:49', '2022-01-07 06:13:49'),
(12, '269202da-26f6-45b3-9e41-8625212b6b94', '66', '3fd51cdd-2798-44ab-ab80-b14835919611', NULL, 'admin', 'wert', NULL, '9', 'App\\Models\\AdminPost', '2022-01-07 06:14:48', '2022-01-07 06:14:48'),
(13, 'bc16ec3d-c27e-41f5-9a7f-ed0bcdbb8eca', '71', '3fd51cdd-2798-44ab-ab80-b14835919611', '269202da-26f6-45b3-9e41-8625212b6b94', 'admin', 'Where’s your profile pic and name “wert”?', NULL, '9', 'App\\Models\\AdminPost', '2022-01-07 06:15:26', '2022-01-07 06:15:26'),
(14, '32aa6d72-3e51-4a21-8452-676cdfa247b3', '71', '3fd51cdd-2798-44ab-ab80-b14835919611', NULL, 'admin', '😉', NULL, '9', 'App\\Models\\AdminPost', '2022-01-07 06:16:27', '2022-01-07 06:16:27'),
(15, '3742eb94-3d46-4660-a466-c8a7302fef81', '71', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', 'I love the concept and the way Ryan is a person of integrity.', 'praise', '6', 'App\\Models\\Highlight', '2022-01-07 13:41:03', '2022-01-07 13:41:03'),
(16, 'f3ef6a4c-e9c6-47da-a018-c8cd1810531f', '69', '9ce613e1-1f30-400d-bc88-8709eb0a5aca', NULL, 'user', 'ggvvbbbb', 'critique', '3', 'App\\Models\\Highlight', '2022-01-07 18:07:11', '2022-01-07 18:07:11'),
(17, '81236ce6-59d8-452c-9ad0-e8a4b6777c58', '74', 'c3a21431-fa61-4baf-b4e3-af00cc775983', NULL, 'user', 'Iohahavabaj', 'critique', '2', 'App\\Models\\Highlight', '2022-01-07 23:41:13', '2022-01-07 23:41:13'),
(18, 'ODk1OQ==', '9', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', '23456789jhjol', 'critique', '1', 'App\\Models\\Highlight', '2022-01-08 05:10:00', '2022-01-08 05:10:00'),
(19, 'ec74bf37-5e99-45b3-99b9-c932e3bcf16d', '77', '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', NULL, 'user', 'Does someone die in this movie? Because if so, it’s super obvious', 'critique', '14', 'App\\Models\\Highlight', '2022-01-10 21:40:28', '2022-01-10 21:40:28'),
(20, '1c1122ed-c9c4-471d-b928-9b4f6a9130f1', '77', '3fd51cdd-2798-44ab-ab80-b14835919611', '32aa6d72-3e51-4a21-8452-676cdfa247b3', 'admin', 'Hey! I know you', NULL, '9', 'App\\Models\\AdminPost', '2022-01-10 21:59:12', '2022-01-10 21:59:12'),
(21, '26b63f5c-005c-4ae5-8653-d6d884a22ac4', '77', '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', NULL, 'user', 'Chris is my favorite character', 'praise', '14', 'App\\Models\\Highlight', '2022-01-11 01:59:00', '2022-01-11 01:59:00'),
(22, 'MzE2NA==', '9', '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', NULL, 'user', 'Yay!', '', '1', 'App\\Models\\Highlight', '2022-01-11 01:59:35', '2022-01-11 01:59:35'),
(23, 'a4f2d4a2-9823-4dad-a681-440962bf1a5f', '79', '132babfc-60b8-4698-a813-0637178d3676', NULL, 'user', 'nice work', NULL, '13', 'App\\Models\\Post', '2022-01-12 15:30:50', '2022-01-12 15:30:50'),
(24, '620f9d75-f123-4f30-9cee-a16380e492c5', '79', '132babfc-60b8-4698-a813-0637178d3676', 'a4f2d4a2-9823-4dad-a681-440962bf1a5f', 'user', 'thanks', NULL, '13', 'App\\Models\\Post', '2022-01-12 15:31:05', '2022-01-12 15:31:05'),
(25, '9e4f7b9d-cbec-4e34-be0e-daa36c5d035d', '79', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', NULL, 'user', 'nice 🔥', 'praise', '19', 'App\\Models\\Highlight', '2022-01-12 15:38:51', '2022-01-12 15:38:51'),
(26, '598db276-210e-47cb-8ced-1c4a1fadff40', '79', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', NULL, 'user', 'work', 'praise', '19', 'App\\Models\\Highlight', '2022-01-12 15:39:18', '2022-01-12 15:39:18'),
(27, '04b0ffec-3de9-4509-9416-5f371b68ce5d', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'okkk', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:43:55', '2022-01-12 15:43:55'),
(28, 'b9a5276c-cf3d-4c76-a239-039a49472027', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'okjdjd', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:44:07', '2022-01-12 15:44:07'),
(29, '4eaaf82e-a6ff-40b2-adde-1bba3f5b2097', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'hdhdhd', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:44:09', '2022-01-12 15:44:09'),
(30, '7e2583cc-b0c2-43e7-84d0-55f4cfa61348', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'bdbdbd', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:44:11', '2022-01-12 15:44:11'),
(31, 'a67d3617-ea1a-4e90-a38c-2842c45d291b', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'hhdhdh', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:44:14', '2022-01-12 15:44:14'),
(32, 'ed397bf1-1f9a-4278-9f4a-95c338dcb077', '82', '5c2258f1-10a1-4897-9bd3-3f852c199337', NULL, 'user', 'bdbdb', NULL, '17', 'App\\Models\\Post', '2022-01-12 15:44:16', '2022-01-12 15:44:16'),
(33, 'ab9e3f1d-d4ea-4e7a-b04c-2f0abbfc208b', '83', '9ec5becd-9924-4304-ad6a-7dec8908c36f', NULL, 'user', 'nice🔥', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:04:40', '2022-01-12 16:04:40'),
(34, 'b52ec6e3-b132-4cfc-b416-44cdd9d247d3', '83', '9ec5becd-9924-4304-ad6a-7dec8908c36f', 'ab9e3f1d-d4ea-4e7a-b04c-2f0abbfc208b', 'user', 'exactly', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:05:50', '2022-01-12 16:05:50'),
(35, '87e21e1b-dae0-41ba-bf3d-9cace77c679f', '82', '9ec5becd-9924-4304-ad6a-7dec8908c36f', 'ab9e3f1d-d4ea-4e7a-b04c-2f0abbfc208b', 'user', 'Yup', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:06:29', '2022-01-12 16:06:29'),
(36, 'ff1d21e7-9fe6-4ac8-b713-bbbf16c27934', '82', '9ec5becd-9924-4304-ad6a-7dec8908c36f', NULL, 'user', 'Nice work', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:06:51', '2022-01-12 16:06:51'),
(37, 'edb38bc6-2819-446c-b552-7d1867e0985c', '83', '9ec5becd-9924-4304-ad6a-7dec8908c36f', NULL, 'user', 'thsnks', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:07:15', '2022-01-12 16:07:15'),
(38, '86b06eaf-7d2d-41e1-9d3f-28b7c4cdbe3b', '82', '9ec5becd-9924-4304-ad6a-7dec8908c36f', NULL, 'user', 'How are you', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:07:32', '2022-01-12 16:07:32'),
(39, '9acd8272-cba3-40ec-bddf-0accbbd519a3', '83', '9ec5becd-9924-4304-ad6a-7dec8908c36f', NULL, 'user', 'im good what about you', NULL, '18', 'App\\Models\\Post', '2022-01-12 16:07:48', '2022-01-12 16:07:48'),
(40, 'f14c5468-dcb1-4143-af72-c04aac75a81f', '83', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', NULL, 'user', 'yo 🔥', 'praise', '21', 'App\\Models\\Highlight', '2022-01-12 16:33:36', '2022-01-12 16:33:36'),
(41, '419a6eb2-e761-4c36-961e-2dfaf7b0a4af', '83', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', NULL, 'user', 'nice work', 'praise', '21', 'App\\Models\\Highlight', '2022-01-12 16:33:43', '2022-01-12 16:33:43'),
(42, '84edf130-4645-49d8-bd1e-34d03d90eadd', '83', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', NULL, 'user', 'why not', 'praise', '21', 'App\\Models\\Highlight', '2022-01-12 16:33:49', '2022-01-12 16:33:49'),
(43, 'ed6b0363-340d-4c44-9189-8080646fc3c0', '83', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', NULL, 'user', '👍', 'praise', '21', 'App\\Models\\Highlight', '2022-01-12 16:34:05', '2022-01-12 16:34:05'),
(44, '904eb259-08a0-42df-b9b6-8c8af8a72d9d', '79', '8efe5912-347f-447f-83d5-be105ec37ed8', NULL, 'user', 'Hi', NULL, '16', 'App\\Models\\Post', '2022-01-13 16:41:43', '2022-01-13 16:41:43'),
(45, '214b54ec-dca6-41dc-9261-07fd6781f492', '79', '8efe5912-347f-447f-83d5-be105ec37ed8', '904eb259-08a0-42df-b9b6-8c8af8a72d9d', 'user', 'Sorry', NULL, '16', 'App\\Models\\Post', '2022-01-13 16:41:54', '2022-01-13 16:41:54'),
(46, '455e4d3d-04f0-4221-8802-8af12110f70a', '79', '8efe5912-347f-447f-83d5-be105ec37ed8', NULL, 'user', 'Nice', NULL, '16', 'App\\Models\\Post', '2022-01-13 16:42:14', '2022-01-13 16:42:14'),
(47, 'bf7138e9-6fdd-4778-83bf-5f690bb627ff', '79', '8efe5912-347f-447f-83d5-be105ec37ed8', NULL, 'user', 'Work', NULL, '16', 'App\\Models\\Post', '2022-01-13 16:42:33', '2022-01-13 16:42:33'),
(48, '5bf8a8bb-defb-4ace-b8c9-6b54b84e3cf3', '79', '8efe5912-347f-447f-83d5-be105ec37ed8', NULL, 'user', 'See u soon', NULL, '16', 'App\\Models\\Post', '2022-01-13 16:43:15', '2022-01-13 16:43:15'),
(49, 'ea7fc330-6dee-4ba1-adf0-cee6801cd2b2', '79', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', NULL, 'user', 'Wow', 'praise', '19', 'App\\Models\\Highlight', '2022-01-13 19:04:11', '2022-01-13 19:04:11'),
(50, 'MjQw', '90', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', 'Great Job!', 'praise', '1', 'App\\Models\\Highlight', '2022-01-18 04:01:03', '2022-01-18 04:01:03'),
(51, 'NTI0Nw==', '9', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', 'qwerty', 'critique', '1', 'App\\Models\\Highlight', '2022-01-18 04:01:16', '2022-01-18 04:01:16'),
(52, 'MzcyMA==', '90', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', 'Bad job!', 'praise', '1', 'App\\Models\\Highlight', '2022-01-18 04:01:31', '2022-01-18 04:01:31'),
(53, 'NzE4OA==', '90', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', NULL, 'user', 'Bad job!', 'critique', '1', 'App\\Models\\Highlight', '2022-01-18 04:01:54', '2022-01-18 04:01:54'),
(54, '0d2c31b5-0811-4493-bda9-d45b25945e8f', '9', '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', NULL, 'user', 'arty', NULL, '6', 'App\\Models\\Post', '2022-01-18 04:02:48', '2022-01-18 04:02:48'),
(55, '956a97f1-047d-48b4-bf0a-32d7c36ecdc2', '9', '4a46fab9-65a1-4fcf-878f-6b36e9c8ede2', NULL, 'user', 'When Life Started', NULL, '10', 'App\\Models\\Post', '2022-01-19 07:54:04', '2022-01-19 07:54:04'),
(56, 'ODE4NQ==', '102', 'cf8d8911-b89f-4912-a572-5b7e40651b15', NULL, 'user', 'Yes they do!', '', '1', 'App\\Models\\Highlight', '2022-01-22 02:40:41', '2022-01-22 02:40:41'),
(57, 'OTEyNQ==', '105', '3a81e3ad-69c9-4581-9716-975e658b15cd', NULL, 'user', 'fzefzef', '', '1', 'App\\Models\\Highlight', '2022-01-22 05:26:25', '2022-01-22 05:26:25'),
(58, 'MjAzMA==', '105', '3a81e3ad-69c9-4581-9716-975e658b15cd', NULL, 'user', 'gergerg', 'praise', '1', 'App\\Models\\Highlight', '2022-01-22 05:26:55', '2022-01-22 05:26:55'),
(59, 'MTYzMw==', '105', '3a81e3ad-69c9-4581-9716-975e658b15cd', NULL, 'user', 'gergerg', 'praise', '1', 'App\\Models\\Highlight', '2022-01-22 05:26:55', '2022-01-22 05:26:55'),
(60, 'Mjg5MA==', '105', '7cdb7992-007b-475f-bdad-e3d25f93ffed', NULL, 'user', 'ffff', 'praise', '1', 'App\\Models\\Highlight', '2022-01-22 05:27:17', '2022-01-22 05:27:17'),
(61, 'ODIyMw==', '105', '7cdb7992-007b-475f-bdad-e3d25f93ffed', NULL, 'user', 'zadazd', 'critique', '1', 'App\\Models\\Highlight', '2022-01-22 05:42:38', '2022-01-22 05:42:38'),
(62, 'b29557ba-c02f-43e5-a0aa-ad6be7d77667', '105', '4b3b3938-662e-4d29-9f85-068152e2082a', NULL, 'user', 'azdazd', NULL, '24', 'App\\Models\\Post', '2022-01-22 05:47:39', '2022-01-22 05:47:39'),
(63, '6530d056-7bf0-438c-841d-2f7bbf857f7a', '105', '4b3b3938-662e-4d29-9f85-068152e2082a', NULL, 'user', 'dddd', NULL, '24', 'App\\Models\\Post', '2022-01-22 05:48:13', '2022-01-22 05:48:13'),
(64, '44a9a212-85d2-4f9b-aa16-056bed21d14f', '105', '4b3b3938-662e-4d29-9f85-068152e2082a', '6530d056-7bf0-438c-841d-2f7bbf857f7a', 'user', '@u1', NULL, '24', 'App\\Models\\Post', '2022-01-22 05:48:19', '2022-01-22 05:48:19'),
(65, '435f0278-72d6-4593-a89d-d47361daead6', '104', '3fd51cdd-2798-44ab-ab80-b14835919611', '32aa6d72-3e51-4a21-8452-676cdfa247b3', 'admin', 'Yes', NULL, '9', 'App\\Models\\AdminPost', '2022-01-23 02:46:01', '2022-01-23 02:46:01'),
(66, 'MzQ1Mg==', '9', '90ad339c-7d77-4116-a4c4-bc85d9d72ebd', NULL, 'user', 'werty', '', '1', 'App\\Models\\Highlight', '2022-01-25 03:24:56', '2022-01-25 03:24:56');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `genres` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `uuid`, `genres`, `created_at`, `updated_at`) VALUES
(1, '8852889-88d0-44bb-89cf-00ab1820133', 'Non-Fiction', '2021-10-20 08:08:52', '2021-10-20 08:08:52'),
(2, '2042589-88d0-44bb-89cf-30a4c0133', 'Drama', '2021-10-20 08:08:52', '2021-10-20 08:08:52'),
(3, '335589-88d0-44bb-89cf-30a6c0143', 'Comedy', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(4, '526889-88d0-44bb-89cf-00ab122133', 'Fantasy', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(5, '951889-88d0-44bb-89cf-32b1820133', 'Sci-Fi', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(6, '325889-88d0-44bb-89cf-95637882033', 'Romance', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(7, '999889-88d0-44bb-89cf-9563c7033', 'Mystery', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(8, '532521-88d0-44bb-89cf-9563d33e9', 'Horror', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(9, '556f56-88d0-44bb-89cf-956c5033', 'Action', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(10, '55f6f96-88d0-44bb-89cf-956c5033', 'Historical Fiction', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(11, '358ff96-88d0-44bb-89cf-955rf033', 'Poetry', '2021-10-20 08:11:59', '2021-10-20 08:11:59'),
(12, '9413e3-88d0-44bb-89cf-6584f9w', 'Other', '2021-10-20 08:11:59', '2021-10-20 08:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `uuid`, `name`, `creator_id`, `created_at`, `updated_at`) VALUES
(1, '4c726b86-2c6e-4391-963a-b08e95a195bd', 'okk', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '2022-01-01 20:07:43', '2022-01-01 20:07:43'),
(2, 'a54f0d64-0557-4dd3-8b61-a9b8cbb70ca1', 'hkgvhh', 'b17315f9-c372-409b-bb9b-16240925bafe', '2022-01-06 22:55:10', '2022-01-06 22:55:10'),
(3, 'e63889c4-990f-4676-aa39-add326e2b828', 'oi', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '2022-01-07 23:31:34', '2022-01-07 23:31:34'),
(4, 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', 'New Group', '506500ef-9929-4a0c-903d-93cbea7bf733', '2022-01-07 23:36:52', '2022-01-07 23:36:52'),
(5, 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'Good afternoon', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '2022-01-07 23:49:27', '2022-01-07 23:49:27'),
(6, '33b8212c-3d2d-4aba-b708-56af51f52665', 'Bimasha', '506500ef-9929-4a0c-903d-93cbea7bf733', '2022-01-08 00:30:38', '2022-01-08 00:30:38'),
(7, 'c763a9d2-775a-46b1-8d1b-8d34356aa835', 'qwr5t6', '506500ef-9929-4a0c-903d-93cbea7bf733', '2022-01-08 05:14:01', '2022-01-08 05:14:01'),
(8, 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'work', '93d47f95-420d-4001-bb25-53f99c9ce00b', '2022-01-12 16:10:21', '2022-01-12 16:10:21'),
(9, 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', 'office', '93d47f95-420d-4001-bb25-53f99c9ce00b', '2022-01-12 16:11:12', '2022-01-12 16:11:12'),
(10, '5d7e2762-0389-431f-a07d-ff299a9f231b', 'home', '93d47f95-420d-4001-bb25-53f99c9ce00b', '2022-01-12 16:11:44', '2022-01-12 16:11:44'),
(11, 'afb52f02-8df7-4ada-83d9-a92bcfbc20c7', 'friends', '93d47f95-420d-4001-bb25-53f99c9ce00b', '2022-01-12 16:11:57', '2022-01-12 16:11:57'),
(12, '2cb85e07-123d-4919-928f-67c6fdf06bd8', 'school buddies', '93d47f95-420d-4001-bb25-53f99c9ce00b', '2022-01-12 16:12:20', '2022-01-12 16:12:20'),
(13, 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', 'Work from home', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '2022-01-13 19:10:17', '2022-01-13 19:10:17'),
(14, '62b7bbd3-7291-4f8c-bfc4-5e329e8f9110', 'Office task', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '2022-01-13 19:11:13', '2022-01-13 19:11:13'),
(15, '4046f09b-265d-47ce-ac56-36ca8b17be2d', 'Home gossip', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '2022-01-13 19:12:12', '2022-01-13 19:12:12'),
(16, '318ed36e-8dcf-4636-a88b-a37de2021e27', 'warty', '506500ef-9929-4a0c-903d-93cbea7bf733', '2022-01-18 02:14:03', '2022-01-18 02:14:03');

-- --------------------------------------------------------

--
-- Table structure for table `group_conversations`
--

CREATE TABLE `group_conversations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_conversations`
--

INSERT INTO `group_conversations` (`id`, `uuid`, `user_id`, `group_id`, `message`, `created_at`, `updated_at`) VALUES
(1, '85814b27-ff72-472e-b93c-3b96084c3897', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'okkk\nnbbbb\n\n\n\nhhbbb', '2022-01-01 20:08:01', '2022-01-01 20:08:01'),
(2, '5e2e15e9-5247-4429-9c3d-4ead18b87c36', '44d4d510-7070-4857-8625-a93453c25a0e', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'Hggg', '2022-01-01 20:08:27', '2022-01-01 20:08:27'),
(3, 'c73f93f3-0d3d-449e-a2ea-e17a8f711574', '44d4d510-7070-4857-8625-a93453c25a0e', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'Bbb', '2022-01-01 20:10:15', '2022-01-01 20:10:15'),
(4, '8f3e3be7-3bd0-46de-93c0-31c3b3628a67', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'ok', '2022-01-01 20:10:31', '2022-01-01 20:10:31'),
(5, '4536a8c1-bd4c-4612-bbea-ebe5612ee418', '44d4d510-7070-4857-8625-a93453c25a0e', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'Hi', '2022-01-01 20:14:57', '2022-01-01 20:14:57'),
(6, '209b73ff-a3ca-4e88-9d2d-b03fd2641463', '44d4d510-7070-4857-8625-a93453c25a0e', '4c726b86-2c6e-4391-963a-b08e95a195bd', 'Cccc', '2022-01-01 20:36:54', '2022-01-01 20:36:54'),
(7, 'f095e885-aca7-4d23-bb78-79ba22907767', '506500ef-9929-4a0c-903d-93cbea7bf733', 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', 'Azaz', '2022-01-07 23:37:00', '2022-01-07 23:37:00'),
(8, 'a78006b1-669e-4cfe-9c1e-ef2512cf6a48', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'Jdjdhdbdbdbbddbdbhxdbdhxhxbxbxbxbxbxbxbhhdhddhdhdhdhhdhddhdhdhdhhdhdhdhdhdhdhdhdhdhdhdhdbddhd', '2022-01-07 23:46:23', '2022-01-07 23:46:23'),
(9, '388b766e-9f3e-45c0-8945-c479e7d64d58', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'Best regards David David and his team lead at least one processor and pulse width modulation scheme and the post office box or Kuch meky to the interview remote', '2022-01-07 23:47:37', '2022-01-07 23:47:37'),
(10, 'bd5f8e60-b1b4-4f68-b6ec-fa714df24998', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'Bdbxjdjdjdjdjxjjxjdjxjdjdjdjdhdhxbdbbbbvbdbdbdbdbdbdbxbdbxxbbbxbxbxbxbxbxbdjxjxjdjdjdjdj', '2022-01-07 23:48:00', '2022-01-07 23:48:00'),
(11, '8d787ceb-dda7-4a3e-8ac2-3b347eaa34e5', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'Hahahaha I know that you have any questions please contact the seller and request that the day end report for this position as an alternative embodiment of the day and night at my place', '2022-01-07 23:49:05', '2022-01-07 23:49:05'),
(12, 'a9c33e33-ab42-4aea-b40d-c3941b093d28', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bxbxbxxbbddbdbdbd😀😀😀😃🤭😃🤭😃🤭😃🤭😃🤭😃🤭😃🤭', '2022-01-08 00:01:12', '2022-01-08 00:01:12'),
(13, '28c42925-855e-4ccb-9c92-8f1d59105571', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'ooooiii', '2022-01-08 00:02:14', '2022-01-08 00:02:14'),
(14, 'e4bb0040-f767-4a4e-8826-b1cf060cbc47', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'jsjdjdjdj', '2022-01-08 00:02:19', '2022-01-08 00:02:19'),
(15, '01daf74b-e0c9-43bc-8a5d-29f787e5ea29', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'hhdhdhdhdhdhdhdhdhdhdhdhdhdhhdhdhdhdhddhhhdd😘😘😘', '2022-01-08 00:02:32', '2022-01-08 00:02:32'),
(16, '562d61d6-c13d-43d2-8ff0-2ceafcdc4c85', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'hhhdhdhdhdhdhdhdhdhdhdhhddhdhhdhhdhdhdhdhdjdjdjdjdjdjddjjdhdhdhdhdhdhdhdhdh', '2022-01-08 00:02:45', '2022-01-08 00:02:45'),
(17, 'e858b2e9-8f07-4fa7-8454-9dc535c51642', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bdbdhdhhhd', '2022-01-08 00:02:52', '2022-01-08 00:02:52'),
(18, '13614c54-e22a-4405-a524-b1a5bd6cefff', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bdbdbdbdbdbdbdbbdd', '2022-01-08 00:02:56', '2022-01-08 00:02:56'),
(19, '0e0c077d-e667-4a1a-a4f0-1b1e787b6ce4', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bdbdbdbdbdbdbdbd', '2022-01-08 00:02:59', '2022-01-08 00:02:59'),
(20, '067123fa-642c-423b-adb6-0dffce081d94', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bzbzbzbxbxbxbdbdndjejdjdjdjdjddjjdjdjxhxhxhxjxjxjxjxjxjdjdjdjdkdkdkdkdkdkdkdkdkkdkdkdkdkddndnndbddbbdbdbddbbdbdbdbdbdbdbdbbdhdbdbdbhd', '2022-01-08 00:03:23', '2022-01-08 00:03:23'),
(21, 'b1fa82fe-b694-464d-a9a5-552de2b6f9b9', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'yh dhhdhdhdhdhdh', '2022-01-08 00:05:25', '2022-01-08 00:05:25'),
(22, 'a61cd915-0e08-4682-ba19-16445e55e0be', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'hdhdhddh', '2022-01-08 00:05:27', '2022-01-08 00:05:27'),
(23, 'bfd11f13-b6ad-4d5d-a8da-b3372b6138fc', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bbdbdhdhdhdjdkdkdkdkdkdkdkdkddkdkdjdjdjdjdjdjdjdjdjdjdjdjdjdjdjdjjdjdjdhdhddhdhdhdhdhdhdhdhdhhdhhdhffhfhhfhffh', '2022-01-08 00:05:38', '2022-01-08 00:05:38'),
(24, '3ae47e26-8286-4753-b11e-0246f69d2e3f', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bsbzbsbxbdbx', '2022-01-08 00:06:58', '2022-01-08 00:06:58'),
(25, 'b2a9999b-4cab-499d-8602-1871c0bdbb7d', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'hshsshdhdh', '2022-01-08 00:07:03', '2022-01-08 00:07:03'),
(26, '41639c71-77de-481c-a037-31432ffc6a64', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'e63889c4-990f-4676-aa39-add326e2b828', 'bsennsbsbddbbdbdbddbdbdbdndndjdjdkdkdkdkdkdkdkdkdkdkdkdkkdkdkdkdkdkddkdkkddkdkkkdkddk', '2022-01-08 00:07:34', '2022-01-08 00:07:34'),
(27, 'f487f643-aba5-425f-bd65-f1fb2154778f', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'bdbdbdhhddhhddndj', '2022-01-08 00:10:34', '2022-01-08 00:10:34'),
(28, '9c171918-bfa2-47d5-85c2-a0f45974160f', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'dbrfbbffhfhfhfbhff', '2022-01-08 00:10:37', '2022-01-08 00:10:37'),
(29, '9a592083-2bf0-4e55-8032-33d1e38acaec', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'brbfbfhfhfhffhhf', '2022-01-08 00:10:39', '2022-01-08 00:10:39'),
(30, '401e4d97-383c-4294-8dda-d178e1016460', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'brbfbfhfhfhffhhfbfb', '2022-01-08 00:10:40', '2022-01-08 00:10:40'),
(31, '7a733d90-24a1-4c69-ae55-c9ee7f83c714', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'brhhfhfbffbbffjfjjffjfjjfjfjkfckfkckfkkfkfkfkffjfjfjjffjfjfjfjrjrkejejejeejej', '2022-01-08 00:10:48', '2022-01-08 00:10:48'),
(32, '929f269b-2c7d-41ec-964e-8c5520c5c4ab', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'hejejejdnenenendnnendbdjdbfnfbfbfbfbfdnnddjjdndjddjdjdjdjdjdjdjdnkddnjdkfkf', '2022-01-08 00:10:56', '2022-01-08 00:10:56'),
(33, '38ffa93e-eba5-45d3-aa3f-70b21b7136b7', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'jdjdhdjdndndnddndndndndndndndnnddndndjdjdjdjdjdjdjdjfjfjfjffjjffjjffjfj', '2022-01-08 00:11:04', '2022-01-08 00:11:04'),
(34, '39072cfc-3c5d-47e5-8b60-b1c71c75eaf3', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'nxnxnddndndndndndn😃😃😃', '2022-01-08 00:11:10', '2022-01-08 00:11:10'),
(35, 'b3a2294f-c764-43e5-ba9a-73ee90de7fee', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'bdbdbdbdbdbdbdbdbdbdbdbxbxbxbxnnxbxxnnxxbxbxbnznznzbsdbbdbdhd', '2022-01-08 00:11:18', '2022-01-08 00:11:18'),
(36, '6394701f-a7e8-452e-96b8-9a200292accc', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'nndbdndndndndndndnfjfhfhffjdjjdfjjf', '2022-01-08 00:11:23', '2022-01-08 00:11:23'),
(37, '138583ce-f708-4f6e-834d-cb52d01485ee', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'nndbdnddnndndnxndndjdjdjdjdjdjdjfjfjfjddjjdxjxjxjdjjddj', '2022-01-08 00:11:29', '2022-01-08 00:11:29'),
(38, 'c91c5f33-b1c0-4007-b0d6-0c1b224eb906', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', 'we nsnssnsndndndnndndndndndbdbddj', '2022-01-08 00:11:55', '2022-01-08 00:11:55'),
(39, 'f6086210-fa96-4bbb-9534-670beb67ea41', '506500ef-9929-4a0c-903d-93cbea7bf733', 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', 'Azaz', '2022-01-08 00:29:05', '2022-01-08 00:29:05'),
(40, '5f5e33df-88da-4693-b2ac-95a08a48789d', '506500ef-9929-4a0c-903d-93cbea7bf733', '33b8212c-3d2d-4aba-b708-56af51f52665', 'azaz', '2022-01-08 00:30:56', '2022-01-08 00:30:56'),
(41, 'dbd5ea49-c09d-45e3-8493-4a0608479353', '506500ef-9929-4a0c-903d-93cbea7bf733', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', '1515', '2022-01-08 05:14:24', '2022-01-08 05:14:24'),
(42, '064c7818-2147-43cc-bba8-b8975660fe69', '506500ef-9929-4a0c-903d-93cbea7bf733', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', 'hey bib', '2022-01-08 05:15:45', '2022-01-08 05:15:45'),
(43, '13e27c45-ddd5-4803-b9ef-6d140a72fe9d', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', 'You Jeo', '2022-01-08 05:16:01', '2022-01-08 05:16:01'),
(44, '674af5d2-f962-435d-ac72-ddbfec37f309', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'djffx', '2022-01-12 16:10:39', '2022-01-12 16:10:39'),
(45, 'e4528670-42c5-4aeb-a884-451fb8d2855e', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'dgccvv', '2022-01-12 16:10:42', '2022-01-12 16:10:42'),
(46, '53db2463-310a-4666-a9a3-ed952346b67e', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'vcccc', '2022-01-12 16:10:43', '2022-01-12 16:10:43'),
(47, '66a5c2e5-03b2-43d4-80b7-5254fbd22dc5', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'cccccc', '2022-01-12 16:10:45', '2022-01-12 16:10:45'),
(48, 'af6933bb-60d7-47bf-b44f-e95f078bcb2f', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', 'sdcccvv', '2022-01-12 16:10:48', '2022-01-12 16:10:48'),
(49, 'd8f01629-3e3a-4e10-b7e4-1739f8cc4670', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', 'xvcvv', '2022-01-12 16:11:23', '2022-01-12 16:11:23'),
(50, '0d43b674-fceb-4bcb-b45c-b779c85ba5c5', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', 'cncvvv', '2022-01-12 16:11:26', '2022-01-12 16:11:26'),
(51, '8630bab9-f415-4d5c-8ee1-a2d44b656e1f', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', 'xvcccc', '2022-01-12 16:11:28', '2022-01-12 16:11:28'),
(52, '4c3eb013-c8c3-434f-ad32-228ee5c6a97e', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', 'xvvvv', '2022-01-12 16:11:31', '2022-01-12 16:11:31'),
(53, '0dab3273-be37-40f6-b052-d403df434f8f', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', 'Bdbxbx', '2022-01-13 19:10:26', '2022-01-13 19:10:26'),
(54, '53908ff3-c414-4cc1-bec2-d7b81b8aa21f', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', 'Hdhdhdh', '2022-01-13 19:10:28', '2022-01-13 19:10:28'),
(55, '0143099e-b9c7-4119-af16-27031b7ee731', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', 'Hdhdhdhhdhdhd', '2022-01-13 19:10:30', '2022-01-13 19:10:30'),
(56, '23fbbe9a-ba52-47ab-bac4-f90226c90c1f', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '62b7bbd3-7291-4f8c-bfc4-5e329e8f9110', 'Gggg', '2022-01-13 19:11:21', '2022-01-13 19:11:21'),
(57, '2664e871-7f9e-4c20-b430-e8b40b2087fb', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '4046f09b-265d-47ce-ac56-36ca8b17be2d', 'Kkk', '2022-01-13 19:12:21', '2022-01-13 19:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE `group_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_users`
--

INSERT INTO `group_users` (`id`, `uuid`, `group_id`, `user_id`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'e1e3e532-ceb5-4d43-8ba7-267ed6fdb567', '4c726b86-2c6e-4391-963a-b08e95a195bd', '44d4d510-7070-4857-8625-a93453c25a0e', 'member', '2022-01-01 20:07:43', '2022-01-01 20:07:43'),
(2, '0111ebe2-1a04-4e02-b16e-2b872eecbf59', '4c726b86-2c6e-4391-963a-b08e95a195bd', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'admin', NULL, NULL),
(3, '9a0c5b5d-688a-4162-9ffe-361ce65fbea7', 'a54f0d64-0557-4dd3-8b61-a9b8cbb70ca1', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-06 22:55:10', '2022-01-06 22:55:10'),
(4, '627d13f1-225e-40b7-89bb-fd2f5e35e565', 'a54f0d64-0557-4dd3-8b61-a9b8cbb70ca1', 'b17315f9-c372-409b-bb9b-16240925bafe', 'admin', NULL, NULL),
(5, '74929cd3-f250-49e6-b26f-1ae30f700650', 'e63889c4-990f-4676-aa39-add326e2b828', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-07 23:31:34', '2022-01-07 23:31:34'),
(6, 'c3c2c74b-e0bf-4ab2-9d59-35f923fcd3a1', 'e63889c4-990f-4676-aa39-add326e2b828', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'admin', NULL, NULL),
(7, '6490b862-a517-41ee-b1eb-d05f907495b1', 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-07 23:36:52', '2022-01-07 23:36:52'),
(8, 'd3d0f9d0-8a63-4aee-8374-7af7f3a5c71c', 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'member', '2022-01-07 23:36:52', '2022-01-07 23:36:52'),
(9, 'de501265-67e1-44df-abf9-184294263abb', 'fe4a17bd-c427-4e9c-8cb0-97e1deea4911', '506500ef-9929-4a0c-903d-93cbea7bf733', 'admin', NULL, NULL),
(10, 'b37dfbb6-5cd0-4e99-a0fe-c6430430463e', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-07 23:49:27', '2022-01-07 23:49:27'),
(11, '96fc1d2a-745c-41c6-8b1b-6870f8cc40f6', 'b8f82a33-5cba-41b8-820f-9d8677eb762a', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'admin', NULL, NULL),
(12, '4bb5aa39-8036-46f9-869f-e775fdc255f4', '33b8212c-3d2d-4aba-b708-56af51f52665', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-08 00:30:38', '2022-01-08 00:30:38'),
(13, '68acf6af-f962-4aa6-ba7e-1225d0cec375', '33b8212c-3d2d-4aba-b708-56af51f52665', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'member', '2022-01-08 00:30:38', '2022-01-08 00:30:38'),
(14, 'aabbe338-03e1-4322-96ca-6eda629035d0', '33b8212c-3d2d-4aba-b708-56af51f52665', '506500ef-9929-4a0c-903d-93cbea7bf733', 'admin', NULL, NULL),
(15, '11e7aea6-33b3-4316-a260-29aac71ebaeb', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'member', '2022-01-08 05:14:01', '2022-01-08 05:14:01'),
(16, '8dd8e841-72f9-498f-bd0c-d60cea221954', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'member', '2022-01-08 05:14:01', '2022-01-08 05:14:01'),
(17, '7bb847c5-2829-4911-b081-577c62f6179f', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'member', '2022-01-08 05:14:01', '2022-01-08 05:14:01'),
(18, 'ed037475-c6b6-4822-bf2d-72c7c7792af4', 'c763a9d2-775a-46b1-8d1b-8d34356aa835', '506500ef-9929-4a0c-903d-93cbea7bf733', 'admin', NULL, NULL),
(19, '8e37a68b-6658-403b-9df9-53617c576a04', 'a410ef26-9e6f-4301-ba32-3038162efd9a', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-12 16:10:21', '2022-01-12 16:10:21'),
(20, '4e1d5cdb-4d97-41cf-800b-234952aace1b', 'a410ef26-9e6f-4301-ba32-3038162efd9a', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'admin', NULL, NULL),
(21, '527def5c-39a9-4b06-bbfb-272878f87cd2', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-12 16:11:12', '2022-01-12 16:11:12'),
(22, 'f9387bab-47a4-43eb-932a-e0109e94e0cd', 'd17cc0ed-4e77-4f08-bfff-bb7080c6b523', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'admin', NULL, NULL),
(23, '49233ded-93ff-41ab-85ab-ec650de44003', '5d7e2762-0389-431f-a07d-ff299a9f231b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-12 16:11:44', '2022-01-12 16:11:44'),
(24, '9793ec58-fe8c-4413-8c67-c8e75dd23f18', '5d7e2762-0389-431f-a07d-ff299a9f231b', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'admin', NULL, NULL),
(25, '2d46fed9-7ad1-499e-aa56-76465bfac837', 'afb52f02-8df7-4ada-83d9-a92bcfbc20c7', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-12 16:11:57', '2022-01-12 16:11:57'),
(26, '390c0b9d-52f0-4a80-a684-3efd99c574ab', 'afb52f02-8df7-4ada-83d9-a92bcfbc20c7', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'admin', NULL, NULL),
(27, 'af6033c9-8ff2-420e-abc5-2db77dcc791a', '2cb85e07-123d-4919-928f-67c6fdf06bd8', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-12 16:12:20', '2022-01-12 16:12:20'),
(28, 'd94784cb-8681-4f51-b52c-4d8c0503d224', '2cb85e07-123d-4919-928f-67c6fdf06bd8', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'admin', NULL, NULL),
(29, '39863abe-2bac-4863-8e59-efdca8b80ab2', 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-13 19:10:17', '2022-01-13 19:10:17'),
(30, 'a13ac752-d459-4df8-8761-cff6284c5339', 'eaef5998-c2bd-4c7c-9492-69a29d2a29ac', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'admin', NULL, NULL),
(31, '28b9cf08-a793-4532-8437-db83a89e300b', '62b7bbd3-7291-4f8c-bfc4-5e329e8f9110', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-13 19:11:13', '2022-01-13 19:11:13'),
(32, 'de4504df-efb1-4084-b7fd-d82964b4be6e', '62b7bbd3-7291-4f8c-bfc4-5e329e8f9110', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'admin', NULL, NULL),
(33, '03af6a86-34f7-49d3-9504-b7ece931f1b5', '4046f09b-265d-47ce-ac56-36ca8b17be2d', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'member', '2022-01-13 19:12:12', '2022-01-13 19:12:12'),
(34, '0fe48188-e823-4dd0-9b56-3ec7dca50d8c', '4046f09b-265d-47ce-ac56-36ca8b17be2d', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'admin', NULL, NULL),
(35, '6ecb837b-b909-417c-81b8-bddd621ae177', '318ed36e-8dcf-4636-a88b-a37de2021e27', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(36, 'b6fc0b00-5376-4b7b-beda-08f047395e36', '318ed36e-8dcf-4636-a88b-a37de2021e27', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(37, '7082930f-528e-423c-b999-a59eab8f55da', '318ed36e-8dcf-4636-a88b-a37de2021e27', '001efd7c-adcf-41d0-9c24-0c0e4ab14439', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(38, '172ea1d9-41c1-45fa-a18f-c4a8c19c5185', '318ed36e-8dcf-4636-a88b-a37de2021e27', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(39, 'f27950cd-d679-49f4-bfc1-3462d8caa6e5', '318ed36e-8dcf-4636-a88b-a37de2021e27', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(40, 'cc0bfcee-d1ff-4b3d-91c0-9d399eb8e0bb', '318ed36e-8dcf-4636-a88b-a37de2021e27', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(41, 'cc9038bc-1846-45a1-8863-780e5aa879ab', '318ed36e-8dcf-4636-a88b-a37de2021e27', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(42, '5f6a5b02-adbc-4c27-8ebc-f91105870dc8', '318ed36e-8dcf-4636-a88b-a37de2021e27', '5134231b-ae8c-457a-a987-e468ee55754f', 'member', '2022-01-18 02:14:03', '2022-01-18 02:14:03'),
(43, '2540cd42-6fb6-4258-94ba-07a94cfb2e33', '318ed36e-8dcf-4636-a88b-a37de2021e27', '506500ef-9929-4a0c-903d-93cbea7bf733', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `highlights`
--

CREATE TABLE `highlights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `file_image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `highlights`
--

INSERT INTO `highlights` (`id`, `uuid`, `user_id`, `title`, `views`, `file_image`, `file`, `created_at`, `updated_at`) VALUES
(1, '17a185d6-e21c-4a74-9a12-4c807f5aca34', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'znnznzns', 0, 'uploads/highlights/highlight_img6446.jpeg', 'uploads/highlights/highlight_docs_6445.pdf', '2022-01-01 19:51:08', '2022-01-01 19:51:08'),
(2, 'c3a21431-fa61-4baf-b4e3-af00cc775983', 'b17315f9-c372-409b-bb9b-16240925bafe', 'aliiii', 0, 'uploads/highlights/highlight_img1919.jpg', 'uploads/highlights/highlight_docs_682.pdf', '2022-01-06 23:56:56', '2022-01-06 23:56:56'),
(3, '9ce613e1-1f30-400d-bc88-8709eb0a5aca', 'b17315f9-c372-409b-bb9b-16240925bafe', 'aliiii', 0, 'uploads/highlights/highlight_img9052.jpg', 'uploads/highlights/highlight_docs_3361.pdf', '2022-01-07 00:05:43', '2022-01-07 00:05:43'),
(4, 'ddccc2b0-aace-4671-8ed1-526c23cdf975', 'b17315f9-c372-409b-bb9b-16240925bafe', 'aliiii', 0, 'uploads/highlights/highlight_img7700.jpg', 'uploads/highlights/highlight_docs_7955.pdf', '2022-01-07 00:05:46', '2022-01-07 00:05:46'),
(5, '42b7009f-f968-44b5-8430-116826b40718', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'qwertyuipk', 0, 'uploads/highlights/highlight_img1316.png', 'uploads/highlights/highlight_docs_9404.pdf', '2022-01-07 07:03:50', '2022-01-07 07:03:50'),
(6, 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Story of legends', 0, 'uploads/highlights/highlight_img7957.jpeg', 'uploads/highlights/highlight_docs_1789.pdf', '2022-01-07 11:49:48', '2022-01-07 11:49:48'),
(7, '3a81e3ad-69c9-4581-9716-975e658b15cd', 'b17315f9-c372-409b-bb9b-16240925bafe', 'aliiii', 0, 'uploads/highlights/highlight_img3361.jpg', 'uploads/highlights/highlight_docs_7023.pdf', '2022-01-07 16:27:03', '2022-01-07 16:27:03'),
(8, '28c1edc3-86d1-4b81-b97c-d80268327cdc', 'b17315f9-c372-409b-bb9b-16240925bafe', 'jjjjhh', 0, 'uploads/highlights/highlight_img8920.jpeg', 'uploads/highlights/highlight_docs_7303.pdf', '2022-01-07 17:41:23', '2022-01-07 17:41:23'),
(9, 'e15e94c3-c717-4b87-9f48-255b28fca5ff', 'b17315f9-c372-409b-bb9b-16240925bafe', 'jjjjhh', 0, 'uploads/highlights/highlight_img818.jpeg', 'uploads/highlights/highlight_docs_8719.pdf', '2022-01-07 17:42:47', '2022-01-07 17:42:47'),
(10, '6c657930-ec68-458e-9719-92b3e8e5fb48', 'b17315f9-c372-409b-bb9b-16240925bafe', 'jjjjhh', 0, 'uploads/highlights/highlight_img8122.jpeg', 'uploads/highlights/highlight_docs_2508.pdf', '2022-01-07 17:43:06', '2022-01-07 17:43:06'),
(11, 'dbc36ebc-a3be-4d33-8424-c858accd2f58', 'b17315f9-c372-409b-bb9b-16240925bafe', 'newwwww', 0, 'uploads/highlights/highlight_img9580.jpeg', 'uploads/highlights/highlight_docs_3460.pdf', '2022-01-07 17:44:26', '2022-01-07 17:44:26'),
(12, '415166e6-b0cd-4eb0-94b5-9c84f836f005', 'b17315f9-c372-409b-bb9b-16240925bafe', 'newwwww', 0, 'uploads/highlights/highlight_img1541.jpeg', 'uploads/highlights/highlight_docs_664.pdf', '2022-01-07 17:44:59', '2022-01-07 17:44:59'),
(13, '7cdb7992-007b-475f-bdad-e3d25f93ffed', 'b17315f9-c372-409b-bb9b-16240925bafe', 'oklp', 0, 'uploads/highlights/highlight_img4566.jpeg', 'uploads/highlights/highlight_docs_4829.pdf', '2022-01-07 17:48:51', '2022-01-07 17:48:51'),
(14, '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', '5134231b-ae8c-457a-a987-e468ee55754f', 'Through the Ice', 0, 'uploads/highlights/highlight_img6244.jpeg', 'uploads/highlights/highlight_docs_7648.pdf', '2022-01-10 21:39:16', '2022-01-10 21:39:16'),
(15, 'b8f37100-d163-4183-a7ff-b897ae34145c', '506500ef-9929-4a0c-903d-93cbea7bf733', 'story of time', 0, 'uploads/highlights/highlight_img9109.png', 'uploads/highlights/highlight_docs_1535.pdf', '2022-01-10 22:08:21', '2022-01-10 22:08:21'),
(16, '70cadc06-7226-4ed0-9b36-e36559af79bb', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'politics', 0, 'uploads/highlights/highlight_img7507.jpeg', 'uploads/highlights/highlight_docs_2181.pdf', '2022-01-12 15:15:15', '2022-01-12 15:15:15'),
(17, '004ed94c-b1ce-413f-959d-6c7ca6af79b0', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'journal', 0, 'uploads/highlights/highlight_img7172.jpeg', 'uploads/highlights/highlight_docs_6102.pdf', '2022-01-12 15:22:52', '2022-01-12 15:22:52'),
(18, '90ad339c-7d77-4116-a4c4-bc85d9d72ebd', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Spirit', 0, 'uploads/highlights/highlight_img7204.jpeg', 'uploads/highlights/highlight_docs_7493.pdf', '2022-01-12 15:24:09', '2022-01-12 15:24:09'),
(19, 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'family', 0, 'uploads/highlights/highlight_img9745.jpeg', 'uploads/highlights/highlight_docs_5783.pdf', '2022-01-12 15:25:04', '2022-01-12 15:25:04'),
(20, '196cc6cc-9ab8-444d-a6f6-8b8e16efc484', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'famous', 0, 'uploads/highlights/highlight_img3261.jpeg', 'uploads/highlights/highlight_docs_4303.pdf', '2022-01-12 15:26:15', '2022-01-12 15:26:15'),
(21, '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'politics', 0, 'uploads/highlights/highlight_img3051.jpeg', 'uploads/highlights/highlight_docs_5134.pdf', '2022-01-12 16:33:05', '2022-01-12 16:33:05'),
(22, '2a41f91e-66d1-46fd-945d-b9dc654ad304', '506500ef-9929-4a0c-903d-93cbea7bf733', 'story of time', 0, 'uploads/highlights/highlight_img4400.png', 'uploads/highlights/highlight_docs_1834.pdf', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(23, 'cf8d8911-b89f-4912-a572-5b7e40651b15', 'a5d3853f-b227-425b-a66a-bf7aebf54868', 'Holstons Rule', 0, 'uploads/highlights/highlight_img9956.jpeg', 'uploads/highlights/highlight_docs_597.pdf', '2022-01-22 02:37:45', '2022-01-22 02:37:45'),
(24, 'f4cf2524-321a-4585-9390-96d2a3f204cd', '94fe9d1c-8246-42ac-8791-e103e2639aa2', 'Yum sum', 0, 'uploads/highlights/highlight_img212.jpeg', 'uploads/highlights/highlight_docs_4768.pdf', '2022-01-22 04:38:05', '2022-01-22 04:38:05'),
(25, '7b260181-6cc5-49aa-881b-c94f57e76b30', '94fe9d1c-8246-42ac-8791-e103e2639aa2', 'Yum sum', 0, 'uploads/highlights/highlight_img4805.jpeg', 'uploads/highlights/highlight_docs_7928.pdf', '2022-01-22 04:38:09', '2022-01-22 04:38:09'),
(26, 'ca5ad8bc-627e-40ed-a655-8748b771c116', '836579be-6efd-484b-bb32-60aaff95f67a', 'Story of Ho and Jo', 0, 'uploads/highlights/highlight_img2798.JPG', 'uploads/highlights/highlight_docs_7127.pdf', '2022-01-23 01:55:50', '2022-01-23 01:55:50'),
(27, '7617014f-cb9a-482f-8a37-626b38743785', 'cdd69661-41da-4976-8225-be9584e55d05', 'Story1', 0, 'uploads/highlights/highlight_img1168.jpeg', 'uploads/highlights/highlight_docs_7228.pdf', '2022-01-23 02:10:32', '2022-01-23 02:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `highlight_genres`
--

CREATE TABLE `highlight_genres` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `highlight_id` varchar(255) NOT NULL,
  `genre_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `highlight_genres`
--

INSERT INTO `highlight_genres` (`id`, `uuid`, `highlight_id`, `genre_id`, `created_at`, `updated_at`) VALUES
(1, '10015d25-076b-40a7-a0d4-df8ecddf3c9a', '17a185d6-e21c-4a74-9a12-4c807f5aca34', 'Drama', '2022-01-01 19:51:08', '2022-01-01 19:51:08'),
(2, '0ae67a26-2d5f-4993-a0c7-12d4f7b5cde1', '17a185d6-e21c-4a74-9a12-4c807f5aca34', 'Comedy', '2022-01-01 19:51:08', '2022-01-01 19:51:08'),
(3, 'ec67df7a-6ef0-43a8-9b9c-a24fb1a9688f', 'c3a21431-fa61-4baf-b4e3-af00cc775983', 'Romance', '2022-01-06 23:56:56', '2022-01-06 23:56:56'),
(4, '8866dfb9-9c2b-43b9-970a-5da6e50c393c', '9ce613e1-1f30-400d-bc88-8709eb0a5aca', 'Romance', '2022-01-07 00:05:43', '2022-01-07 00:05:43'),
(5, '16c3b813-f34e-4f8f-ac3d-207728fe4bbe', 'ddccc2b0-aace-4671-8ed1-526c23cdf975', 'Romance', '2022-01-07 00:05:46', '2022-01-07 00:05:46'),
(6, 'bb68dc93-cbdb-4b96-9516-c2c7599128c7', '42b7009f-f968-44b5-8430-116826b40718', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-07 07:03:50', '2022-01-07 07:03:50'),
(7, 'edf14cb1-8e34-43a9-a2af-69fdc0bce39c', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', 'Comedy', '2022-01-07 11:49:48', '2022-01-07 11:49:48'),
(8, '983b1a55-226c-4d39-97ab-96d161bfb026', '3a81e3ad-69c9-4581-9716-975e658b15cd', 'Non-Fiction', '2022-01-07 16:27:03', '2022-01-07 16:27:03'),
(9, '3d176feb-47f9-4211-85c9-6874a6e41a74', '3a81e3ad-69c9-4581-9716-975e658b15cd', 'Comedy', '2022-01-07 16:27:03', '2022-01-07 16:27:03'),
(10, '5a1b9f4f-af75-4f1c-a85c-133cddf10d87', '3a81e3ad-69c9-4581-9716-975e658b15cd', 'Other', '2022-01-07 16:27:03', '2022-01-07 16:27:03'),
(11, 'fd989901-dec0-4908-a99a-6bfc39a5de47', '7cdb7992-007b-475f-bdad-e3d25f93ffed', 'Non-Fiction', '2022-01-07 17:48:51', '2022-01-07 17:48:51'),
(12, '476caffc-68a3-4c20-a6e7-ad30f5d68d07', '7cdb7992-007b-475f-bdad-e3d25f93ffed', 'Drama', '2022-01-07 17:48:51', '2022-01-07 17:48:51'),
(13, '229f953b-8329-400d-9fd4-6e0e87f36126', '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', 'Comedy', '2022-01-10 21:39:16', '2022-01-10 21:39:16'),
(14, 'ad0b929e-e95e-4e8d-b001-9e8cb0c26c9d', 'b8f37100-d163-4183-a7ff-b897ae34145c', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-10 22:08:21', '2022-01-10 22:08:21'),
(15, '1bc02b82-f5ee-4f86-a244-10e9f538108b', 'b8f37100-d163-4183-a7ff-b897ae34145c', 'Historical Fiction', '2022-01-10 22:08:21', '2022-01-10 22:08:21'),
(16, '39990f92-bd4f-4921-8da9-f199025f91c3', 'b8f37100-d163-4183-a7ff-b897ae34145c', 'Horror', '2022-01-10 22:08:21', '2022-01-10 22:08:21'),
(17, 'e8297d7f-d9f8-4b11-a3c4-36e0731b553b', '70cadc06-7226-4ed0-9b36-e36559af79bb', 'Drama', '2022-01-12 15:15:15', '2022-01-12 15:15:15'),
(18, '0a6bf6a2-2eb7-4868-a07a-34dc11449a1d', '70cadc06-7226-4ed0-9b36-e36559af79bb', 'Comedy', '2022-01-12 15:15:15', '2022-01-12 15:15:15'),
(19, '6bca9fd0-16ca-41fb-87c1-767db5b01243', '004ed94c-b1ce-413f-959d-6c7ca6af79b0', 'Drama', '2022-01-12 15:22:52', '2022-01-12 15:22:52'),
(20, '9560cbb4-89cc-4b0d-812b-5676a7aa3f6e', '004ed94c-b1ce-413f-959d-6c7ca6af79b0', 'Comedy', '2022-01-12 15:22:52', '2022-01-12 15:22:52'),
(21, 'b3884365-4e85-414a-b3de-b6c11f3d956b', '90ad339c-7d77-4116-a4c4-bc85d9d72ebd', 'Romance', '2022-01-12 15:24:09', '2022-01-12 15:24:09'),
(22, 'd5fd54e5-8a53-41fc-8aae-af35a4a9f931', '90ad339c-7d77-4116-a4c4-bc85d9d72ebd', 'Sci-Fi', '2022-01-12 15:24:09', '2022-01-12 15:24:09'),
(23, '09b434d0-2697-4acb-a554-810c2a442d5b', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', 'Comedy', '2022-01-12 15:25:04', '2022-01-12 15:25:04'),
(24, '472f6680-6640-4477-8d40-e5ce3af55758', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', 'Fantasy', '2022-01-12 15:25:04', '2022-01-12 15:25:04'),
(25, 'fc21d623-1739-4f78-adb0-969fba39e07f', '196cc6cc-9ab8-444d-a6f6-8b8e16efc484', 'Sci-Fi', '2022-01-12 15:26:15', '2022-01-12 15:26:15'),
(26, '8fd36d86-2d4f-40ef-83ce-f219e0aebc9e', '196cc6cc-9ab8-444d-a6f6-8b8e16efc484', 'Fantasy', '2022-01-12 15:26:15', '2022-01-12 15:26:15'),
(27, 'dacf8779-54aa-407b-9d58-3c6a9ece4d2b', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', 'Drama', '2022-01-12 16:33:05', '2022-01-12 16:33:05'),
(28, '1d08fc41-d570-494a-91ec-1fff31255fa7', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', 'Comedy', '2022-01-12 16:33:05', '2022-01-12 16:33:05'),
(29, 'd280e6a2-325c-4535-9821-9727d93e3b13', '2a41f91e-66d1-46fd-945d-b9dc654ad304', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(30, '5fab5ec6-65e9-4a0a-8101-d660a444aa35', '2a41f91e-66d1-46fd-945d-b9dc654ad304', 'Historical Fiction', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(31, 'f6573593-6cd7-4f0d-88cb-bd1cd0cda514', '2a41f91e-66d1-46fd-945d-b9dc654ad304', 'Action', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(32, '692c9adb-2af1-4ed2-bce8-34a69fcbd5f7', '2a41f91e-66d1-46fd-945d-b9dc654ad304', 'Comedy', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(33, '7dc7cba5-295b-4397-9892-86dffb2225ca', '2a41f91e-66d1-46fd-945d-b9dc654ad304', 'Drama', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(34, '0838236c-46ec-460d-9656-f8e4b4008ad4', '2a41f91e-66d1-46fd-945d-b9dc654ad304', 'Fantasy', '2022-01-19 07:45:02', '2022-01-19 07:45:02'),
(35, 'b04559d4-5c5e-4a31-8ee8-d90563e9f3b6', 'cf8d8911-b89f-4912-a572-5b7e40651b15', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-22 02:37:45', '2022-01-22 02:37:45'),
(36, '41f64bf0-b041-4b42-801b-7824491051bc', 'cf8d8911-b89f-4912-a572-5b7e40651b15', 'Drama', '2022-01-22 02:37:45', '2022-01-22 02:37:45'),
(37, '178ba14b-c7b7-4ed2-a3a2-d0ae98ae225a', 'cf8d8911-b89f-4912-a572-5b7e40651b15', 'Historical Fiction', '2022-01-22 02:37:45', '2022-01-22 02:37:45'),
(38, '5d63605e-260a-499c-b545-317f8562ce6b', 'cf8d8911-b89f-4912-a572-5b7e40651b15', 'Comedy', '2022-01-22 02:37:45', '2022-01-22 02:37:45'),
(39, '77399674-cca2-43ec-b165-33f88f2795c8', 'f4cf2524-321a-4585-9390-96d2a3f204cd', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-22 04:38:05', '2022-01-22 04:38:05'),
(40, '55790405-1489-4c2c-8aff-b46aab074a0d', 'f4cf2524-321a-4585-9390-96d2a3f204cd', 'Historical Fiction', '2022-01-22 04:38:05', '2022-01-22 04:38:05'),
(41, '31c8dce7-54e3-464d-872a-4f5e1272a57e', '7b260181-6cc5-49aa-881b-c94f57e76b30', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-22 04:38:09', '2022-01-22 04:38:09'),
(42, '935eebb2-7ddf-4e58-9e14-f175782e7622', '7b260181-6cc5-49aa-881b-c94f57e76b30', 'Historical Fiction', '2022-01-22 04:38:09', '2022-01-22 04:38:09'),
(43, '21416bfc-3974-4578-84cf-30758ef1991c', 'ca5ad8bc-627e-40ed-a655-8748b771c116', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-23 01:55:50', '2022-01-23 01:55:50'),
(44, '100c2d6e-edb5-411e-8484-cad37e2f40c6', 'ca5ad8bc-627e-40ed-a655-8748b771c116', 'Comedy', '2022-01-23 01:55:50', '2022-01-23 01:55:50'),
(45, '5216deb2-9029-49a6-a099-cf54d610da0c', 'ca5ad8bc-627e-40ed-a655-8748b771c116', 'Fantasy', '2022-01-23 01:55:50', '2022-01-23 01:55:50'),
(46, '3dd27e38-9292-486e-96cc-eacd2f64aedf', 'ca5ad8bc-627e-40ed-a655-8748b771c116', 'Mystery', '2022-01-23 01:55:50', '2022-01-23 01:55:50'),
(47, '88bb8e82-aa56-4802-9fbc-5afae0a139e6', '7617014f-cb9a-482f-8a37-626b38743785', '8852889-88d0-44bb-89cf-00ab1820133', '2022-01-23 02:10:32', '2022-01-23 02:10:32'),
(48, 'fc9bf017-0502-461b-939c-16b61ebebd50', '7617014f-cb9a-482f-8a37-626b38743785', 'Action', '2022-01-23 02:10:32', '2022-01-23 02:10:32'),
(49, 'd77ac749-de12-4908-acd8-14395036b213', '7617014f-cb9a-482f-8a37-626b38743785', 'Drama', '2022-01-23 02:10:32', '2022-01-23 02:10:32'),
(50, '1b092353-6131-48c9-b6da-ea448278d9c2', '7617014f-cb9a-482f-8a37-626b38743785', 'Historical Fiction', '2022-01-23 02:10:32', '2022-01-23 02:10:32'),
(51, '583d1bb0-591a-4009-92e1-d252975677dd', '7617014f-cb9a-482f-8a37-626b38743785', 'Mystery', '2022-01-23 02:10:32', '2022-01-23 02:10:32'),
(52, '1ce46003-0b6c-4077-85d1-0e78b90cdbab', '7617014f-cb9a-482f-8a37-626b38743785', 'Other', '2022-01-23 02:10:32', '2022-01-23 02:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `highlight_hashtags`
--

CREATE TABLE `highlight_hashtags` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `highlight_id` varchar(255) NOT NULL,
  `hashtag_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `highlight_ratings`
--

CREATE TABLE `highlight_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `highlight_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `highlight_ratings`
--

INSERT INTO `highlight_ratings` (`id`, `uuid`, `ip_address`, `user_id`, `highlight_id`, `rating`, `agent`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, '88a0cf54-274a-4a57-87bf-7da24f111d44', '72.255.40.124', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '17a185d6-e21c-4a74-9a12-4c807f5aca34', 3, 'okhttp/3.14.9', NULL, NULL, '2022-01-01 20:24:44', '2022-01-01 20:24:44'),
(2, '776d7be5-61c3-4e90-a96f-26d7aaf15735', '76.178.165.0', 'af10af2d-c62f-4564-a702-cfcda4eb9461', '17a185d6-e21c-4a74-9a12-4c807f5aca34', 5, 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-02 06:28:11', '2022-01-02 06:28:11'),
(3, '46253bc4-67a3-4992-b670-79f91680c15f', '107.119.57.24', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '17a185d6-e21c-4a74-9a12-4c807f5aca34', 5, 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-07 05:44:01', '2022-01-07 05:44:01'),
(4, '4f821f7b-07a3-4090-b50d-60cb776778d8', '76.178.165.0', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'e6ea4a39-3ccb-42f7-8b58-0c1c8e9191d0', 5, 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-07 11:50:10', '2022-01-07 11:50:10'),
(5, '0ad976ea-bafe-46e8-91d3-150e78c668cd', '76.178.165.0', '5134231b-ae8c-457a-a987-e468ee55754f', '1b7bb54c-b128-4bdf-99bd-1312db6f83a8', 3, 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-10 21:40:03', '2022-01-10 21:40:03'),
(6, '5120bebb-9195-4bcb-9700-13f0824a827c', '72.255.40.124', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'dde3265f-6b9c-4014-9d22-ebe0086d4ff5', 3, 'okhttp/3.14.9', NULL, NULL, '2022-01-12 15:37:31', '2022-01-12 15:37:31'),
(7, '858d0329-9a1f-4f78-a145-a7e0e2346e15', '72.255.40.124', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5e3d2b8e-3709-439b-8e6d-bd1dbce1dd0c', 4, 'okhttp/3.14.9', NULL, NULL, '2022-01-12 16:33:18', '2022-01-12 16:33:18'),
(8, '3a4fb9b8-a156-4dd2-9312-262f73f6e6c9', '76.178.165.0', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '3a81e3ad-69c9-4581-9716-975e658b15cd', 4, 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-25 02:53:17', '2022-01-25 02:53:17');

-- --------------------------------------------------------

--
-- Table structure for table `highlight_views`
--

CREATE TABLE `highlight_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `highlight_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `uuid`, `user_id`, `post_id`, `post_type`, `created_at`, `updated_at`) VALUES
(1, '39cf52cd-3781-452c-be07-4ef010eb8477', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '8c5ed9d1-6b37-408a-abfe-a3b9fe50f7ea', 'admin', '2022-01-01 19:59:06', '2022-01-01 19:59:06'),
(2, '44bc3327-a7c8-4666-808c-e357e4c7cfce', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '58fc7cf4-1e57-4d84-a35f-a79668289968', 'user', '2022-01-01 19:59:14', '2022-01-01 19:59:14'),
(3, '711eb79d-efad-4e1d-8931-c67373d306fd', '506500ef-9929-4a0c-903d-93cbea7bf733', '3c5647c5-9987-4efc-ab15-d3e9b180fc37', 'user', '2022-01-01 21:09:42', '2022-01-01 21:09:42'),
(4, '8b186c8c-423c-4512-ad06-2c224b58c733', '506500ef-9929-4a0c-903d-93cbea7bf733', 'e2386077-c6aa-4db5-bbc8-fe0b626ec278', 'admin', '2022-01-01 21:15:16', '2022-01-01 21:15:16'),
(5, '626b629f-4ed9-4b29-83d3-1fc79934a4dd', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'e2386077-c6aa-4db5-bbc8-fe0b626ec278', 'admin', '2022-01-03 19:05:15', '2022-01-03 19:05:15'),
(6, '8aad948a-73cb-4ada-9707-33e2d1d0eeb0', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', '2cbb7ae7-53d8-406a-a32c-d4ac316f429b', 'user', '2022-01-03 21:54:10', '2022-01-03 21:54:10'),
(7, 'b22f0f03-fd24-41aa-9053-08a0d70192b3', '506500ef-9929-4a0c-903d-93cbea7bf733', 'fd706bf2-08a4-4db8-b527-2f7067f2d0fa', 'quick', '2022-01-04 01:07:24', '2022-01-04 01:07:24'),
(8, '1c3d3f16-b6c5-4546-a0ed-71e4e8fbe373', '7c577412-cdbd-46f0-b832-5316b068b2b4', '2abe5ea7-79fe-41f8-a7a8-1da90ab75d35', 'quick', '2022-01-04 18:57:22', '2022-01-04 18:57:22'),
(9, 'a32bae64-d1ed-4535-8950-801fa6d15435', 'b17315f9-c372-409b-bb9b-16240925bafe', 'a2c84ee9-a9ad-4e56-a043-44462ca478a6', 'user', '2022-01-06 15:59:51', '2022-01-06 15:59:51'),
(10, '6f058ee3-19d5-4fce-a779-be67a9aec438', 'b17315f9-c372-409b-bb9b-16240925bafe', '108d5a45-1268-4715-b14b-cfab2ab5c575', 'user', '2022-01-06 16:08:16', '2022-01-06 16:08:16'),
(11, 'fe6a0466-37cc-40cc-bd63-9da50266e723', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'e2386077-c6aa-4db5-bbc8-fe0b626ec278', 'admin', '2022-01-07 05:40:57', '2022-01-07 05:40:57'),
(12, '152f08ed-7fe0-4503-9627-bd2ae0e1d7d7', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '2abe5ea7-79fe-41f8-a7a8-1da90ab75d35', 'quick', '2022-01-07 05:41:12', '2022-01-07 05:41:12'),
(13, 'b95b58d6-0349-446c-858a-258a87df0fee', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '2f1aba2b-4be1-44e8-a579-73ea1f1bed55', 'quick', '2022-01-07 05:41:28', '2022-01-07 05:41:28'),
(14, '60f72bf4-5681-4e6c-8b02-f3bf9872bf44', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'fd706bf2-08a4-4db8-b527-2f7067f2d0fa', 'quick', '2022-01-07 05:42:02', '2022-01-07 05:42:02'),
(15, 'bde4a4a6-fe04-445f-92ab-50e47dbf8a04', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '44ce9159-f003-4583-8692-625c985f690d', 'quick', '2022-01-07 05:45:46', '2022-01-07 05:45:46'),
(16, '4ae55714-3152-4101-b9c4-479aed7e3c15', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', 'user', '2022-01-07 06:03:38', '2022-01-07 06:03:38'),
(17, '2fd59378-c75f-4bd4-bbc9-7286c9a65910', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', '6bae9553-f25b-4b7c-9dc7-8eab88920159', 'admin', '2022-01-07 06:13:29', '2022-01-07 06:13:29'),
(18, '24b17f22-def4-43a1-9302-230127a9a616', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', '3fd51cdd-2798-44ab-ab80-b14835919611', 'admin', '2022-01-07 06:14:19', '2022-01-07 06:14:19'),
(19, '434cf1b6-234a-4022-b02a-727d877c06e7', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '3fd51cdd-2798-44ab-ab80-b14835919611', 'admin', '2022-01-07 06:21:54', '2022-01-07 06:21:54'),
(20, '047cefdf-7533-4df9-8c5d-e459572258d2', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'ac38764d-c994-4c57-8d66-48bd8240d6d8', 'user', '2022-01-07 10:33:49', '2022-01-07 10:33:49'),
(21, '82cbc0cd-7087-45f8-987b-3ddab26ac01a', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '333b7e92-be3f-4152-9feb-9b061b503596', 'quick', '2022-01-07 13:45:25', '2022-01-07 13:45:25'),
(23, '6f49f665-bb06-41e3-b07b-53a8e2549e05', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '9aa33037-6384-499b-8203-c73cc5aae65f', 'quick', '2022-01-08 08:39:48', '2022-01-08 08:39:48'),
(24, 'df37f5af-ab76-43ce-aec9-5f3dab1a67fe', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '3c5647c5-9987-4efc-ab15-d3e9b180fc37', 'user', '2022-01-08 13:05:04', '2022-01-08 13:05:04'),
(25, '0c062367-8c68-4f26-ab0e-3e6fde256c31', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '8aa797d1-9fc4-4443-81e8-64bdcc1170c7', 'user', '2022-01-08 13:05:09', '2022-01-08 13:05:09'),
(27, '45f12087-d0d1-47f8-b9ab-ad2e8b5851e0', '5134231b-ae8c-457a-a987-e468ee55754f', 'e87fa9a4-9967-4eb7-b39c-dc43edbcde1e', 'user', '2022-01-11 02:01:46', '2022-01-11 02:01:46'),
(28, '00e06568-4b35-4aac-976a-db457956a17f', '5134231b-ae8c-457a-a987-e468ee55754f', '4a46fab9-65a1-4fcf-878f-6b36e9c8ede2', 'user', '2022-01-11 02:01:51', '2022-01-11 02:01:51'),
(29, '50c5135f-0160-4ed7-992b-85adeedf4eb4', '5134231b-ae8c-457a-a987-e468ee55754f', 'b2298ed4-d839-48c3-baa8-e460eeb47206', 'quick', '2022-01-11 02:09:18', '2022-01-11 02:09:18'),
(30, '1f3c27ff-c303-4737-a611-7ca0ceb0d97c', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '132babfc-60b8-4698-a813-0637178d3676', 'user', '2022-01-12 15:30:35', '2022-01-12 15:30:35'),
(31, 'f8e754a2-082e-4aad-bc20-55b3f09b9e46', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'a058ab07-5f0e-4ce9-bdb6-c798cca6bacf', 'user', '2022-01-12 15:32:11', '2022-01-12 15:32:11'),
(32, 'b93a7035-5c95-44e9-b049-31278111df62', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '5c2258f1-10a1-4897-9bd3-3f852c199337', 'user', '2022-01-12 15:43:45', '2022-01-12 15:43:45'),
(33, '5107948b-248f-4137-9deb-700442653514', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5c2258f1-10a1-4897-9bd3-3f852c199337', 'user', '2022-01-12 15:53:45', '2022-01-12 15:53:45'),
(34, '65a3de33-2609-4487-9f67-ad63fce2190b', '93d47f95-420d-4001-bb25-53f99c9ce00b', '9ec5becd-9924-4304-ad6a-7dec8908c36f', 'user', '2022-01-12 16:04:06', '2022-01-12 16:04:06'),
(35, '0a3e0d0d-4392-472d-b1f9-1b5250064202', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'f4f313b3-e592-463b-a4dd-505aaeddb8c0', 'quick', '2022-01-12 16:19:06', '2022-01-12 16:19:06'),
(36, '05b5e445-58c3-46d2-a3ec-e5ac4708ddea', '8f675b9d-652a-4294-a170-77859236089c', '105bb773-acf0-46f4-88c1-ba4f5c870c82', 'user', '2022-01-12 18:16:50', '2022-01-12 18:16:50'),
(37, 'fc143b03-311c-4346-9722-b0530e4b22d7', '8f675b9d-652a-4294-a170-77859236089c', 'd0b164a7-041d-42b7-a9a8-859eda334e63', 'user', '2022-01-12 18:17:43', '2022-01-12 18:17:43'),
(43, 'c1ea901d-8587-412c-80f4-750f322dce63', '8f675b9d-652a-4294-a170-77859236089c', '8c5ed9d1-6b37-408a-abfe-a3b9fe50f7ea', 'admin', '2022-01-13 16:15:09', '2022-01-13 16:15:09'),
(44, '4330f2b0-011e-47a1-96de-f94ab863b118', '8f675b9d-652a-4294-a170-77859236089c', 'e983e71e-a9d0-47c9-bb94-26c0acef17ba', 'admin', '2022-01-13 16:15:13', '2022-01-13 16:15:13'),
(45, 'ee77f57d-9db0-4604-b7cb-afc6dd0f0dbc', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '8efe5912-347f-447f-83d5-be105ec37ed8', 'user', '2022-01-13 16:41:29', '2022-01-13 16:41:29'),
(46, 'c99bbbb4-72e8-4da9-8abb-dc92e844dbaa', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '3fd51cdd-2798-44ab-ab80-b14835919611', 'admin', '2022-01-13 16:44:12', '2022-01-13 16:44:12'),
(47, 'dc75f15b-c61d-4fb0-834b-2299809bb017', '506500ef-9929-4a0c-903d-93cbea7bf733', '3fd51cdd-2798-44ab-ab80-b14835919611', 'admin', '2022-01-17 21:54:09', '2022-01-17 21:54:09'),
(49, 'd658f11b-41a1-4ac7-a08f-9791d76a187d', 'cdd69661-41da-4976-8225-be9584e55d05', '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', '2022-01-21 14:20:43', '2022-01-21 14:20:43'),
(51, '253aeb26-8f08-46da-90fb-ab0b47f4d5f3', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '1af0dadc-102e-4bd8-a090-263263785ef2', 'user', '2022-01-22 07:48:54', '2022-01-22 07:48:54'),
(54, 'bd31050e-f946-48c2-85f7-0cfba923f0f8', '506500ef-9929-4a0c-903d-93cbea7bf733', 'f4444b4f-1666-49e4-8363-4cdbdaaed33c', 'admin', '2022-01-25 02:38:51', '2022-01-25 02:38:51'),
(56, 'f8e91295-f6af-4437-a81e-7943e98cc440', '506500ef-9929-4a0c-903d-93cbea7bf733', '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', 'user', '2022-01-25 06:40:46', '2022-01-25 06:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(212, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(213, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(214, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(215, '2016_06_01_000004_create_oauth_clients_table', 1),
(216, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(243, '2014_10_12_100000_create_password_resets_table', 2),
(244, '2019_08_19_000000_create_failed_jobs_table', 2),
(245, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(246, '2021_07_28_120710_create_admins_table', 2),
(247, '2021_07_29_161847_create_posts_table', 2),
(248, '2021_07_30_092939_create_images_table', 2),
(249, '2021_07_30_142130_create_videos_table', 2),
(250, '2021_08_02_155329_add_api_token_to_user_table', 2),
(251, '2021_08_06_135047_create_likes_table', 3),
(253, '2021_08_09_133153_create_stories_table', 4),
(254, '2021_08_09_151102_create_highlights_table', 5),
(255, '2021_08_10_085158_create_admin_posts_table', 6),
(256, '2021_08_10_104603_create_penpals_table', 7),
(257, '2021_08_13_123043_create_user_connections_table', 8),
(258, '2021_08_13_123515_create_user_messages_table', 8),
(259, '2021_08_16_143639_create_groups_table', 9),
(260, '2021_08_16_143740_create_group_users_table', 9),
(261, '2021_08_16_143811_create_group_conversations_table', 9),
(263, '2021_08_09_103434_create_comments_table', 10),
(264, '2021_10_14_081600_create_admin_tips_table', 11),
(265, '2021_10_14_110220_create_referrals_table', 12),
(267, '2021_10_14_110825_create_referral_codes_table', 13),
(270, '2021_10_18_091110_create_highlight_views_table', 15),
(271, '2021_10_18_090034_create_highlight_ratings_table', 16),
(272, '2021_10_20_155326_create_user_profile_views_table', 17),
(274, '2014_10_12_000000_create_users_table', 18),
(275, '2021_11_06_092823_create_notifications_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('zeeshan@gmail.com', '$2y$10$gSIWe4BZsCT/VvJarEoC0.aXT2XqpLj1h45UOocTPJrOhzxC1YEsC', '2021-10-25 06:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `penpals`
--

CREATE TABLE `penpals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penpals`
--

INSERT INTO `penpals` (`id`, `uuid`, `sender_id`, `receiver_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '32660a07-f043-413d-b946-7507fee085fd', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '63370a83-3a80-483d-9d30-85a438eca10b', 'Accept', '2021-11-09 21:21:20', '2021-11-09 21:22:21'),
(2, 'ddcc14bf-7bff-47f3-8492-ed1ca7abf987', '1c574f81-ba2b-4af5-8baa-83b94dac738e', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'pending', '2021-11-17 02:38:03', '2021-11-17 02:38:03'),
(3, 'ed7cb2b3-4788-44a9-b1ff-652b7f1bd3ef', '1c574f81-ba2b-4af5-8baa-83b94dac738e', '63370a83-3a80-483d-9d30-85a438eca10b', 'pending', '2021-11-17 02:38:10', '2021-11-17 02:38:10'),
(4, 'b97272d3-1662-4e3b-bfb1-1f19b38d1b5c', '1c574f81-ba2b-4af5-8baa-83b94dac738e', '1b2f446a-0e0a-433b-a7f3-eccb5295a74a', 'pending', '2021-11-17 02:38:16', '2021-11-17 02:38:16'),
(5, '219cb3a9-7972-4105-9b88-da07d572e60f', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'e0e3636a-1034-44e2-b716-0caa1b5a1089', 'pending', '2021-11-17 02:38:40', '2021-11-17 02:38:40'),
(6, 'a37640a9-398d-4dd7-9cd3-d7d6a8715dd9', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'ed5ea7e6-5bc9-4f79-8428-6996474674dc', 'Accept', '2021-11-23 01:21:17', '2021-11-23 01:21:17'),
(7, '1e7bdc92-1e5e-443a-a1b8-0e40a9b938ae', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Accept', '2021-11-25 00:31:30', '2021-11-25 00:31:30'),
(8, '7e000f46-9eba-4e40-91d8-e3e440bb6f8f', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'Accept', '2021-11-25 00:40:58', '2021-11-25 00:40:58'),
(9, '291de7fd-8263-414c-9280-58d7c8b48b6d', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Accept', '2021-11-25 00:45:07', '2021-11-25 00:45:46'),
(10, '04fda142-adba-4c4d-b40a-830f19974df3', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '01e1c9b6-a656-4dd9-ae2a-17526be2ca9e', 'Accept', '2021-12-01 01:31:29', '2021-12-01 01:31:29'),
(11, '61ef5963-6cad-47e9-ab7d-f8aa0f3a667c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '7c8e80e4-bd89-4e99-b2e2-f09232907d7a', 'Accept', '2021-12-04 01:36:49', '2021-12-04 01:36:49'),
(12, '762af918-0af4-444b-a6f7-4bbf2fea8cf7', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '0b1b21e0-f70d-4237-bfbb-850976350e81', 'Accept', '2021-12-04 12:40:37', '2021-12-04 12:40:37'),
(13, '10567b3e-cf80-43b8-833b-fad888c143d9', 'f65f6bc3-8172-4ee1-82d8-ba3f2512c23c', '95c25fd1-3ee7-4f50-bd92-7aee87dd3c3a', 'Accept', '2021-12-08 00:05:34', '2021-12-08 00:11:10'),
(14, '7277a223-1dff-4e19-ae9a-60ac4451664e', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '15576bea-f562-41f4-b10a-932d8a910290', 'Accept', '2021-12-08 16:52:55', '2021-12-08 16:52:55'),
(15, '8812e9ef-d1ba-4145-9f05-bd2fa9593ec1', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'fcc2a2b2-eb18-43b5-9eb9-416744495bbe', 'Accept', '2021-12-10 18:30:19', '2021-12-10 18:30:19'),
(16, '2e4e1960-eba2-4a01-a98d-ea46a264b5bf', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '3e7b1e43-2691-44ae-ae75-e6e8912b2116', 'Accept', '2021-12-10 18:52:25', '2021-12-10 18:52:25'),
(17, 'a7c9ef74-f17e-4a6d-a85e-b84d3cef3a4e', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'b8803fc3-0d2c-4f09-a4c7-c7c03cf49fa4', 'Accept', '2021-12-10 18:53:54', '2021-12-10 18:53:54'),
(18, 'df62b497-cae1-43d5-8560-b0bd909fe7d5', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'fa32bdd0-4c5f-491e-92ee-af49f025b379', 'Accept', '2021-12-10 22:39:49', '2021-12-10 22:39:49'),
(19, '0efcbc3c-e588-4536-b749-028f4c29f521', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'b06f1192-a9fa-44d9-ab04-ad50a1c22b9e', 'Accept', '2021-12-10 22:41:11', '2021-12-10 22:41:11'),
(20, 'ddb1a07d-85bb-4688-abc0-ccb6b8fca322', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'e6746104-e8b8-4a34-9a6c-0fd4ad18d904', 'Accept', '2021-12-12 00:23:40', '2021-12-12 00:23:40'),
(21, 'eba06ebc-c1c9-4cf7-a324-7b5d6792eeae', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '18503b1d-7f5f-4b6a-97fa-11daf14dce7e', 'Accept', '2021-12-12 00:25:44', '2021-12-12 00:25:44'),
(22, 'aba9a8a0-00d4-45b2-9efa-40a15d69cdd4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '12f27bac-10aa-43bc-9bbc-69ad1b0b6691', 'Accept', '2021-12-12 00:31:23', '2021-12-12 00:31:23'),
(23, '3b284ef4-fd36-4b61-bb31-8c794040a200', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '59eb6b50-a32d-4ebf-bee7-faadfaf6c20e', 'Accept', '2021-12-12 00:31:42', '2021-12-12 00:31:42'),
(24, '6a5824f0-5a89-4d3d-9b9b-d70a99536e09', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'eab7018c-fec3-453b-bdc4-6d1b6925ad4e', 'Accept', '2021-12-12 00:34:02', '2021-12-12 00:34:02'),
(25, '515cb5a3-532e-447e-88d7-1b68f362f44c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '486f6f53-0134-40f2-8242-a03e0d77b4ee', 'Accept', '2021-12-12 01:08:44', '2021-12-12 01:08:44'),
(26, '607f74ee-f3fc-41fc-a1ab-1b1f09d36683', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'ec230c23-5bc1-463d-9212-bbc8e6617a3b', 'Accept', '2021-12-12 01:12:46', '2021-12-12 01:12:46'),
(27, 'df6a806b-3c52-4c60-b936-03c461ef3835', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '06dfdf0c-5d68-43ce-a4f2-08afbf808ebc', 'Accept', '2021-12-12 01:13:56', '2021-12-12 01:13:56'),
(28, '6aea5586-ff3e-464b-b2e7-0caaab4d2910', '001efd7c-adcf-41d0-9c24-0c0e4ab14439', 'afa92057-b887-4e5f-8977-eca73e795ec0', 'pending', '2021-12-12 03:59:15', '2021-12-12 03:59:15'),
(29, '5f683e66-49ad-409d-830e-84555763ffab', '001efd7c-adcf-41d0-9c24-0c0e4ab14439', '0b1b21e0-f70d-4237-bfbb-850976350e81', 'pending', '2021-12-12 03:59:27', '2021-12-12 03:59:27'),
(30, '01bdc85f-d97e-41bb-a75d-5b9f159eca15', '001efd7c-adcf-41d0-9c24-0c0e4ab14439', '506500ef-9929-4a0c-903d-93cbea7bf733', 'pending', '2021-12-12 04:24:45', '2021-12-12 04:24:45'),
(31, '2de291a0-22b1-42b2-be81-c907653a2dcc', 'ab213e73-3b2c-4c4b-864c-de2151ccf6cb', 'afa92057-b887-4e5f-8977-eca73e795ec0', 'Request Sent', '2021-12-12 04:50:10', '2021-12-12 04:50:10'),
(32, 'e81b4a6f-6cae-4bcd-aa4f-0abff2e66cec', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'a3bc87e9-d036-4df1-895f-23f5a7c58219', 'Accept', '2021-12-14 17:44:29', '2021-12-14 17:44:29'),
(33, 'a140afa1-ee59-4c18-b182-39fec999d0a5', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '351e4933-59cf-41af-a0aa-6990c1fe5a08', 'Accept', '2021-12-16 17:35:06', '2021-12-16 17:35:06'),
(34, '2d36307b-fc70-410b-b172-61eeef64ed12', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '0e4bf93f-763f-4f76-81a5-4c988b565bd6', 'Accept', '2021-12-16 18:06:58', '2021-12-16 18:06:58'),
(35, '8f165712-fd80-4541-9ef3-538016e684cf', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'ce377ae4-ede9-4f10-b741-0d36ff050593', 'Accept', '2021-12-16 18:09:27', '2021-12-16 18:09:27'),
(36, 'f482a9c0-aa24-47c7-8f40-c74a64100951', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '9e816eb8-be52-4038-8229-2536469c33be', 'Accept', '2021-12-16 18:14:22', '2021-12-16 18:14:22'),
(37, '9f6892e4-538c-4712-b9d8-16ec96d8e322', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'd3bb7079-7379-4468-91dc-4460a43ee80b', 'Accept', '2021-12-22 16:55:37', '2021-12-22 16:55:37'),
(38, '1394350c-66df-490d-bc01-cf48489a1540', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'Accept', '2021-12-23 11:33:55', '2021-12-23 11:33:55'),
(39, 'fe0dfd89-b41c-4bf2-9353-2dcfd08e3591', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', '506500ef-9929-4a0c-903d-93cbea7bf733', 'pending', '2021-12-23 11:36:14', '2021-12-23 11:36:14'),
(40, '704ead88-1bad-422a-8853-5253dc1622c4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '43f1d3b0-9d23-41b6-9acc-8756f8e69bc2', 'Accept', '2021-12-23 16:57:47', '2021-12-23 16:57:47'),
(41, 'b01a847c-0e45-49c0-9524-ed0a19a1e02b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'caaeb23d-a83b-42d4-b913-0c87fc49fb40', 'Accept', '2021-12-23 23:51:38', '2021-12-23 23:51:38'),
(42, 'e39d3187-ae05-49a0-aa30-89f54fb700eb', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2767bb62-d43d-46fa-b5f7-be8bb7c372ed', 'Accept', '2021-12-25 22:34:00', '2021-12-25 22:34:00'),
(43, 'df18b3d6-8532-40fc-8310-b78bba7c5fec', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'b4665684-c07a-40a4-b026-d6a82dd06791', 'Accept', '2021-12-25 22:34:41', '2021-12-25 22:34:41'),
(44, '59e09de5-bd2a-48f3-87e3-b19cccd11de9', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'be9fd82b-3840-44a9-bffb-93918c7c2157', 'Accept', '2021-12-27 16:41:05', '2021-12-27 16:41:05'),
(45, '269b9a51-b155-4811-ae67-f0918e346c06', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '6066f2b0-07d3-4c77-951b-340244253e30', 'Accept', '2021-12-27 21:37:54', '2021-12-27 21:37:54'),
(46, '2fdd6cda-b973-45de-97c2-c505d641cdc7', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '508f074b-9a41-4ab8-a982-83f180f86af1', 'Accept', '2021-12-27 21:55:24', '2021-12-27 21:55:24'),
(47, 'c537433a-051b-4714-9df6-f836aa8f760f', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '003c0093-675c-41f9-b0e9-09c1b830198b', 'Accept', '2021-12-28 01:14:08', '2021-12-28 01:14:08'),
(48, 'd8f53443-0f6b-451c-a242-19f7ceebaf78', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '129b33c7-73f6-4e4d-9095-51b451fbcd7a', 'Accept', '2021-12-28 02:13:29', '2021-12-28 02:13:29'),
(49, 'c197516a-73e5-48aa-abcf-0b59c278804d', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'Accept', '2021-12-28 02:30:47', '2021-12-28 02:30:47'),
(50, 'ec826380-e267-4410-a524-61882df7e629', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', '506500ef-9929-4a0c-903d-93cbea7bf733', 'pending', '2021-12-28 03:28:35', '2021-12-28 03:28:35'),
(51, '34b008fa-5cc8-4d90-8eca-a37c596e9185', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Accept', '2021-12-28 05:44:48', '2022-01-08 05:05:24'),
(52, 'e46667fa-0091-491e-afc3-a51d28e7b2a0', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'Accept', '2021-12-28 05:45:00', '2021-12-28 05:45:13'),
(53, 'de8a1479-e35b-4f2b-9f2b-636b8e1e03f3', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'cf68a928-6479-40c2-91c4-921024dc9c37', 'Accept', '2021-12-28 06:56:37', '2021-12-28 06:56:37'),
(54, 'f34edc03-4b39-437d-afd9-cd7ba74b73cb', 'cf68a928-6479-40c2-91c4-921024dc9c37', '003c0093-675c-41f9-b0e9-09c1b830198b', 'Request Sent', '2021-12-28 06:59:25', '2021-12-28 06:59:25'),
(55, '8226be09-adc8-429c-9d3c-95b8e2fd5837', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 'Accept', '2021-12-28 07:20:12', '2021-12-28 07:20:12'),
(56, '4ab57469-1250-4d85-aa54-425886b67725', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'pending', '2021-12-28 07:21:33', '2021-12-28 07:21:33'),
(57, 'fc68f8b1-fc41-4b86-8836-cd4bdcba5d34', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'e6fbf4cc-a14e-4104-b300-fdeb5d8cd54e', 'Accept', '2021-12-28 22:43:56', '2021-12-28 22:43:56'),
(58, '116d2d25-550b-439b-b64e-628e2b7ca215', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 'Accept', '2021-12-29 02:35:27', '2021-12-29 02:35:27'),
(59, '52d7b153-a9ff-4170-8db0-e7b5c8a5853f', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '05c2ba3e-d7b2-4ed1-af10-6b1769ed4b3c', 'Accept', '2021-12-29 16:20:39', '2021-12-29 16:20:39'),
(60, 'eaa31a27-f25e-4a66-94b4-4e37674a8e11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'b3aa3a02-c4e7-4d21-843e-a538d8a6ac9b', 'Accept', '2021-12-29 19:08:55', '2021-12-29 19:08:55'),
(61, 'a79e0690-cf8c-4141-8b36-381973e6bf6f', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '3f0e74e3-90cc-4826-b1cf-35f83f053816', 'Accept', '2021-12-30 14:50:03', '2021-12-30 14:50:03'),
(62, '0f5a81cb-9c1c-4774-b9c8-50b9b374b708', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'c234fb07-e63b-47ca-bc6a-ee840fd2e6df', 'Accept', '2021-12-30 14:54:26', '2021-12-30 14:54:26'),
(63, '6298d2f6-3bf0-4993-9a76-f0495d32ddd8', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'f83da394-2076-43b7-8845-e25511f93233', 'Accept', '2021-12-30 14:56:47', '2021-12-30 14:56:47'),
(64, '9f12713d-cf7c-4d87-aa86-1604247ecb5e', '3f0e74e3-90cc-4826-b1cf-35f83f053816', 'f83da394-2076-43b7-8845-e25511f93233', 'Accept', '2021-12-30 14:57:18', '2021-12-30 14:57:42'),
(65, '72ea7843-be32-4e11-9ecf-c60e595f9350', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '213d5d65-a887-41eb-88f8-604e12acc266', 'Accept', '2021-12-31 07:33:26', '2021-12-31 07:33:26'),
(66, 'a094323f-d64d-4917-9709-dbaeacc89ef2', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'f99a3837-d15d-4348-9fd4-12500122250f', 'Accept', '2021-12-31 07:41:11', '2021-12-31 07:41:11'),
(67, '505c4a61-eb6a-4476-9310-88a8f6c5a174', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Request Sent', '2021-12-31 10:16:40', '2021-12-31 10:16:40'),
(68, '111f39ec-eec2-4663-901e-da6fa8aea613', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'Request Sent', '2021-12-31 10:17:04', '2021-12-31 10:17:04'),
(69, '7182b8ba-e2cd-4a7e-9e37-2b143ae51605', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'Accept', '2021-12-31 10:18:45', '2021-12-31 10:18:45'),
(70, '7c278265-dc50-4b8e-85f8-9f4f32ee7579', 'af10af2d-c62f-4564-a702-cfcda4eb9461', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'pending', '2021-12-31 10:21:13', '2021-12-31 10:21:13'),
(71, '5a1f37e8-071e-4cc5-aac0-40f0cf6ebe91', 'af10af2d-c62f-4564-a702-cfcda4eb9461', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'pending', '2021-12-31 10:28:01', '2021-12-31 10:28:01'),
(72, '611c3c12-eeaa-408e-ba81-d5d0f0f0902d', 'af10af2d-c62f-4564-a702-cfcda4eb9461', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Request Sent', '2021-12-31 10:28:17', '2021-12-31 10:28:17'),
(73, '66b17359-3a7a-4f5f-9671-1feab5d4aea8', 'f99a3837-d15d-4348-9fd4-12500122250f', 'e0e3636a-1034-44e2-b716-0caa1b5a1089', 'pending', '2022-01-01 05:30:06', '2022-01-01 05:30:06'),
(74, '489f4134-029c-4aa3-86c6-0f838d88c5aa', 'f99a3837-d15d-4348-9fd4-12500122250f', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'pending', '2022-01-01 05:30:20', '2022-01-01 05:30:20'),
(75, 'd3dd1a19-3d24-4f6d-b08e-3785daac060b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '3aaec9e8-bfb4-4b81-bbe8-5abca169e7a6', 'Accept', '2022-01-01 17:10:23', '2022-01-01 17:10:23'),
(76, '2e5d06d0-df43-46fb-a070-21c03dc9d0b0', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '1db97194-3ab7-4e37-a08d-6aa9f5e5a94b', 'Accept', '2022-01-01 17:10:35', '2022-01-01 17:10:35'),
(77, '58b0924c-3d70-4151-891a-b8eeb8ab530f', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'pending', '2022-01-01 19:59:33', '2022-01-01 19:59:33'),
(78, 'e105e1aa-163f-40e9-a778-06db1909de0c', '44d4d510-7070-4857-8625-a93453c25a0e', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'Accept', '2022-01-01 20:05:43', '2022-01-01 20:07:05'),
(79, '7fcb8a8f-3817-48f6-acdc-cf65e54b45db', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '0af8cc95-9921-4b73-a245-52342b742e84', 'Accept', '2022-01-06 15:44:14', '2022-01-06 15:44:14'),
(80, 'b3a5a955-95c2-4c77-9648-a823638eb25c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'b17315f9-c372-409b-bb9b-16240925bafe', 'Accept', '2022-01-06 15:44:47', '2022-01-06 15:44:47'),
(81, '66b77e8e-cdf8-4b27-925b-232d99275cb6', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '71732563-d868-48e3-ae21-4b11e525ade4', 'Accept', '2022-01-06 15:47:18', '2022-01-06 15:47:18'),
(82, '8a6c068c-9117-4a77-8ecf-c8c334e65cd7', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Accept', '2022-01-07 05:39:22', '2022-01-07 05:39:22'),
(83, 'aa2ec5a9-b248-4b6e-bb55-6f239b213539', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Request Sent', '2022-01-07 05:39:48', '2022-01-07 05:39:48'),
(84, '40b22de5-8e9c-4848-a92f-c13f1765277c', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Request Sent', '2022-01-07 05:39:49', '2022-01-07 05:39:49'),
(85, '813a225a-5f31-4a53-9067-6ccf9fc74176', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'Request Sent', '2022-01-07 05:40:17', '2022-01-07 05:40:17'),
(86, '7bd88d90-f030-4222-ae9e-17d4088cd233', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '7c577412-cdbd-46f0-b832-5316b068b2b4', 'pending', '2022-01-07 05:41:16', '2022-01-07 05:41:16'),
(87, '394d14ae-d943-4206-9fe1-55a403011f08', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'pending', '2022-01-07 06:21:24', '2022-01-07 06:21:24'),
(88, '35dfe545-529a-43b5-825b-5938dc73ef95', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 'pending', '2022-01-07 11:48:12', '2022-01-07 11:48:12'),
(89, '04bca6e7-1af8-47d3-a500-513cfeabe382', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'Request Sent', '2022-01-07 13:49:41', '2022-01-07 13:49:41'),
(90, 'bf435777-b2ca-4293-80eb-64c12fc40d69', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'a00a83f9-bfa2-465e-a5e9-44b702f224a3', 'Accept', '2022-01-07 22:53:16', '2022-01-07 22:53:16'),
(91, '8f7357fa-d325-42d5-ba6b-f860f9024eb4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'babeaefa-10db-41c8-b8ca-474d2d8bbd71', 'Accept', '2022-01-07 23:26:36', '2022-01-07 23:26:36'),
(92, '9fd6c3d4-d271-4266-bf93-32d64e0b216a', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'Accept', '2022-01-07 23:30:48', '2022-01-07 23:30:48'),
(93, '54acb07d-1bdc-4f6e-a735-1c4baca1e449', '506500ef-9929-4a0c-903d-93cbea7bf733', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'Request Sent', '2022-01-08 04:01:33', '2022-01-08 04:01:33'),
(94, '4b74bc7f-8f98-4b9a-9b9c-fc39dc595e4b', '506500ef-9929-4a0c-903d-93cbea7bf733', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Accept', '2022-01-08 05:09:36', '2022-01-08 05:10:10'),
(95, '641a6c4c-cdca-4b37-87b0-5d58f6138db4', '506500ef-9929-4a0c-903d-93cbea7bf733', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 'Request Sent', '2022-01-08 05:16:55', '2022-01-08 05:16:55'),
(96, 'ffeb04af-e9fc-453b-b880-aae5481a0d09', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 'pending', '2022-01-08 13:05:49', '2022-01-08 13:05:49'),
(97, 'ea0f8d5d-bb02-4c4e-b15b-8bf3ed44cc97', '506500ef-9929-4a0c-903d-93cbea7bf733', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'Request Sent', '2022-01-09 02:16:25', '2022-01-09 02:16:25'),
(98, '5ed925c7-a45f-494c-babe-a8dde436abc1', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '5134231b-ae8c-457a-a987-e468ee55754f', 'Accept', '2022-01-10 21:34:02', '2022-01-10 21:34:02'),
(99, 'bd9c55a8-45d5-43df-a597-6f3a9059cd75', '5134231b-ae8c-457a-a987-e468ee55754f', '506500ef-9929-4a0c-903d-93cbea7bf733', 'pending', '2022-01-10 21:35:20', '2022-01-10 21:35:20'),
(100, 'b0f08d04-3b20-489f-8767-97698526939a', '5134231b-ae8c-457a-a987-e468ee55754f', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Request Sent', '2022-01-10 21:53:14', '2022-01-10 21:53:14'),
(101, '10f861b3-bb68-4243-8429-c05fece2f46b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'f88bccc2-8218-4e01-b7e9-ece986466870', 'Accept', '2022-01-10 23:06:48', '2022-01-10 23:06:48'),
(102, 'af73a9fd-3789-4d2b-9f52-af2a4d6e2e4c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Accept', '2022-01-10 23:07:02', '2022-01-10 23:07:02'),
(103, 'f0bf056b-d839-42ce-851b-bd070ef49b4e', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '8f675b9d-652a-4294-a170-77859236089c', 'Accept', '2022-01-10 23:14:50', '2022-01-10 23:14:50'),
(104, '86c691c4-66da-4c7a-b6df-1f19ec930451', '5134231b-ae8c-457a-a987-e468ee55754f', 'afa92057-b887-4e5f-8977-eca73e795ec0', 'Request Sent', '2022-01-11 01:47:38', '2022-01-11 01:47:38'),
(105, '5f3c94d7-51ad-4c30-b094-5f29781baf91', '5134231b-ae8c-457a-a987-e468ee55754f', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'Request Sent', '2022-01-11 01:47:51', '2022-01-11 01:47:51'),
(106, '4892e4cb-c991-4844-817a-db59c67510b8', '5134231b-ae8c-457a-a987-e468ee55754f', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'pending', '2022-01-11 02:03:58', '2022-01-11 02:03:58'),
(107, '2baaa08f-9bee-49c4-8aa1-938e527575d7', '5134231b-ae8c-457a-a987-e468ee55754f', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'Request Sent', '2022-01-11 02:04:02', '2022-01-11 02:04:02'),
(108, '5bfbf591-476d-4a0a-ab46-55606ff7a7bc', '5134231b-ae8c-457a-a987-e468ee55754f', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Request Sent', '2022-01-11 02:05:31', '2022-01-11 02:05:31'),
(109, '12d53e21-42c4-44b8-960f-ef3360550666', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Accept', '2022-01-12 15:41:47', '2022-01-12 15:41:47'),
(110, '80433a54-2ca4-4315-b2a5-5da05fd566eb', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Accept', '2022-01-12 15:42:20', '2022-01-13 16:41:01'),
(111, 'ab656fb2-a3e9-4110-986a-c36d7ada9fd7', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Accept', '2022-01-12 15:51:40', '2022-01-12 15:51:40'),
(112, '05e49b31-ecd0-48fb-9b29-f13632c0c237', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Accept', '2022-01-12 15:51:59', '2022-01-12 15:53:38'),
(113, '8fc52c0b-b856-41ea-b5a3-ba2a6e4831f7', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '6254a35f-9137-4725-b466-d8a680491d78', 'Accept', '2022-01-12 16:15:27', '2022-01-12 16:15:27'),
(114, '8e3ee46f-59ad-4ff2-b3ae-086aaad3cac8', '6254a35f-9137-4725-b466-d8a680491d78', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Accept', '2022-01-12 16:17:08', '2022-01-12 16:17:21'),
(115, '8c464cb1-099b-45b6-bfe8-8b63a1e73013', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '26dc0063-470b-453d-847c-dc71f16aa358', 'Accept', '2022-01-17 16:41:54', '2022-01-17 16:41:54'),
(116, 'c38e2dad-bc0f-42b8-b23e-05e6aa9c5cc1', '506500ef-9929-4a0c-903d-93cbea7bf733', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'Request Sent', '2022-01-18 02:12:26', '2022-01-18 02:12:26'),
(117, 'd0968197-d7d0-47fc-93d2-6dd81c796cb4', '506500ef-9929-4a0c-903d-93cbea7bf733', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', 'Request Sent', '2022-01-18 03:58:14', '2022-01-18 03:58:14'),
(118, '7f6f1d28-f0b1-44da-985f-aa4620abf522', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '48868441-b03e-42a5-ade0-d291c6a284ba', 'Accept', '2022-01-18 15:18:21', '2022-01-18 15:18:21'),
(119, 'b0095182-283b-4e7b-b20f-ca73704373ad', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Request Sent', '2022-01-22 00:25:53', '2022-01-22 00:25:53'),
(120, '34fdef02-c100-4f5c-952e-1ee35cf106ee', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', 'afa92057-b887-4e5f-8977-eca73e795ec0', 'Request Sent', '2022-01-22 00:27:39', '2022-01-22 00:27:39'),
(121, 'dc6b08d1-2d13-4a0b-a911-c994fd1e5eb2', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', '0b1b21e0-f70d-4237-bfbb-850976350e81', 'Request Sent', '2022-01-22 00:27:39', '2022-01-22 00:27:39'),
(122, '3e769652-5955-41f2-ae11-7c6ef5fe4c57', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', '129b33c7-73f6-4e4d-9095-51b451fbcd7a', 'Request Sent', '2022-01-22 00:27:47', '2022-01-22 00:27:47'),
(123, '74e684c5-e148-4f08-8da4-7a9f4a17ab52', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Request Sent', '2022-01-22 00:27:47', '2022-01-22 00:27:47'),
(124, '17fece9b-a1cd-41bf-ad57-6ba69b836962', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '741be022-e61e-4ba8-9e14-1a0dddea94c4', 'Accept', '2022-01-22 04:39:48', '2022-01-22 04:39:48'),
(125, '7d9ed43d-bf76-4450-b653-c88b81eb07da', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 'pending', '2022-01-22 07:44:34', '2022-01-22 07:44:34'),
(126, '20a7bdc2-578d-47c7-b452-cf0b8690073a', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'pending', '2022-01-22 07:44:41', '2022-01-22 07:44:41'),
(127, '89a645bf-2140-4164-a3dd-8b9170cae205', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '63370a83-3a80-483d-9d30-85a438eca10b', 'pending', '2022-01-22 07:44:49', '2022-01-22 07:44:49'),
(128, '2dad5259-d420-4d70-832f-73fd5b198784', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '506500ef-9929-4a0c-903d-93cbea7bf733', 'pending', '2022-01-22 07:44:57', '2022-01-22 07:44:57'),
(129, 'b641269d-6998-493c-92c4-b6b9acc5c2af', '506500ef-9929-4a0c-903d-93cbea7bf733', 'b17315f9-c372-409b-bb9b-16240925bafe', 'Request Sent', '2022-01-25 14:30:07', '2022-01-25 14:30:07'),
(130, '899682a2-1239-46df-8534-c93803e2b361', '5bd62c88-a2a4-466d-85b1-76b1c5145545', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Request Sent', '2022-01-25 16:57:53', '2022-01-25 16:57:53');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(4, 'App\\Models\\User', 13, 'writers_talk', '319eb545c231489ddb8cd0b614d2ba30573bc33841f4b4052a05fffddde8f0c3', '[\"*\"]', NULL, '2021-08-05 11:41:52', '2021-08-05 11:41:52'),
(13, 'App\\Models\\User', 19, 'writers_talk', '6d383df7d328f75454b2ef05d168ebb4c9c57157fcfcad8b484ab657b9773ecd', '[\"*\"]', NULL, '2021-08-10 04:43:11', '2021-08-10 04:43:11'),
(14, 'App\\Models\\User', 19, 'writers_talk', '355859d398dd954cf5090945a0912642ea1ec9e35ac40219f0ad970cfd60f5df', '[\"*\"]', NULL, '2021-08-10 04:43:27', '2021-08-10 04:43:27'),
(35, 'App\\Models\\User', 28, 'writers_talk', '27d5e31a8968a6bfb0d364770652556cf87ba920693e287e24f16d315049deb7', '[\"*\"]', NULL, '2021-08-10 07:58:41', '2021-08-10 07:58:41'),
(37, 'App\\Models\\User', 28, 'writers_talk', '567354dc56b012ab43fef731bc637eda897ba5d63f2b8248053ca44e5d94d14b', '[\"*\"]', NULL, '2021-08-10 08:03:13', '2021-08-10 08:03:13'),
(38, 'App\\Models\\User', 28, 'writers_talk', 'a2a9f4bd05dd163ffee12f3dbf6a060bd214a8074d594ace2f2cb925c3418ee0', '[\"*\"]', NULL, '2021-08-10 08:03:39', '2021-08-10 08:03:39'),
(39, 'App\\Models\\User', 28, 'writers_talk', 'af1eb7a48b96201201b63f9c034ef815e62b331726bf315f18d8c10d3cd51b48', '[\"*\"]', NULL, '2021-08-10 09:02:54', '2021-08-10 09:02:54'),
(40, 'App\\Models\\User', 28, 'writers_talk', '4af100db24d698d0b56b7fa5e2bcb33c07d55639ed593706b36489e1617ecf8d', '[\"*\"]', NULL, '2021-08-10 09:03:39', '2021-08-10 09:03:39'),
(45, 'App\\Models\\User', 30, 'writers_talk', '5770fa19ab6a9d9d6e80a42f2dabb348f7249aa38d4209c5b9cba572f6dbe74b', '[\"*\"]', NULL, '2021-08-10 09:09:15', '2021-08-10 09:09:15'),
(46, 'App\\Models\\User', 30, 'writers_talk', '5b579f20afcb2689eb0b6b3c7a36f19b3db6bcc162482bae05d35cb00d4ac6ab', '[\"*\"]', NULL, '2021-08-10 09:09:47', '2021-08-10 09:09:47'),
(55, 'App\\Models\\User', 32, 'writers_talk', '14a5a22cbbe3ad3c707b33b81c9bfcdc528c08d3187fdf6721b84f82613ecc71', '[\"*\"]', NULL, '2021-08-16 06:09:53', '2021-08-16 06:09:53'),
(56, 'App\\Models\\User', 32, 'writers_talk', '700cb4082a9e04015cb6d57d3116f99f4ef675decd759daff96a3bb96acf4408', '[\"*\"]', '2021-08-20 09:37:10', '2021-08-16 06:10:10', '2021-08-20 09:37:10'),
(57, 'App\\Models\\User', 34, 'writers_talk', '391a922a65fc591af08a2e9a46c852fa8c1d2ff8ed52ae3b686b3d9115e4a95e', '[\"*\"]', NULL, '2021-08-20 04:29:04', '2021-08-20 04:29:04'),
(58, 'App\\Models\\User', 34, 'writers_talk', '799ab2e2db2cd04251539fdcd220ace7479cb790a8edc99f3f92c728d800bd11', '[\"*\"]', '2021-08-24 05:06:43', '2021-08-20 04:29:24', '2021-08-24 05:06:43'),
(61, 'App\\Models\\User', 36, 'writers_talk', '64b1dff7c6c95dcb7db35746131239cbfa13e660eebc83822fe7026f4b66791d', '[\"*\"]', NULL, '2021-08-20 09:51:18', '2021-08-20 09:51:18'),
(62, 'App\\Models\\User', 36, 'writers_talk', '6508c6a785d9450a4cc3f12a008688f2aca2234454743041e4a908271d329cf4', '[\"*\"]', '2021-08-20 10:09:50', '2021-08-20 09:51:44', '2021-08-20 10:09:50'),
(63, 'App\\Models\\User', 37, 'writers_talk', 'a212bd680065a7211bd01ddaf606f652d631cb436375407f5ec97b0568e4739e', '[\"*\"]', NULL, '2021-08-23 03:18:17', '2021-08-23 03:18:17'),
(65, 'App\\Models\\User', 40, 'writers_talk', 'd414618f9853182f550d2a2fb6e111fa4d7d153d9cb3ad41ed7a78b9b858da33', '[\"*\"]', NULL, '2021-08-23 03:25:47', '2021-08-23 03:25:47'),
(67, 'App\\Models\\User', 40, 'writers_talk', '3aa5737f96e47828d26cd4693663e2bd52a2054ac0c78b3c38e10b41b9cc5810', '[\"*\"]', NULL, '2021-08-23 03:29:45', '2021-08-23 03:29:45'),
(70, 'App\\Models\\User', 42, 'writers_talk', '4056eaefb6a4969bb14bd291f7d2981eb3a365359c3686f1812a0bb895441e8b', '[\"*\"]', NULL, '2021-08-24 03:15:18', '2021-08-24 03:15:18'),
(71, 'App\\Models\\User', 42, 'writers_talk', '936d7771faed9a3fcafefdb04e7fbfa13910a309f5e0537f4e625f1c2bdca801', '[\"*\"]', '2021-08-30 03:57:23', '2021-08-24 03:15:57', '2021-08-30 03:57:23'),
(72, 'App\\Models\\User', 42, 'writers_talk', '62a5c2a451dce7cd785352fe7e28e845558cdddeeea85c5c38a85a4125c33426', '[\"*\"]', NULL, '2021-08-24 03:17:52', '2021-08-24 03:17:52'),
(79, 'App\\Models\\User', 43, 'writers_talk', '535d21bd9e3c7cec10e69117578498cc5693d4de88f3688dff21c7473ad4ec77', '[\"*\"]', NULL, '2021-08-25 04:50:49', '2021-08-25 04:50:49'),
(80, 'App\\Models\\User', 43, 'writers_talk', 'b802a77da7279ee9811dd29321b3abe7433c87fe5c93065a9d52784b44b73c38', '[\"*\"]', '2021-08-25 04:53:08', '2021-08-25 04:51:09', '2021-08-25 04:53:08'),
(81, 'App\\Models\\User', 44, 'writers_talk', '438b24f81db0fd8b3052aabb326a1be8933150eec122b7cb68ce6e8beea7039c', '[\"*\"]', NULL, '2021-08-25 04:53:46', '2021-08-25 04:53:46'),
(82, 'App\\Models\\User', 44, 'writers_talk', 'f4ca7e9dccf0bea630ff057439f9f9d91e293889aed76258af409877f4ee2bb0', '[\"*\"]', '2021-08-25 04:57:34', '2021-08-25 04:54:24', '2021-08-25 04:57:34'),
(88, 'App\\Models\\User', 47, 'writers_talk', 'e7a128debd3aee1ae263d7568485816bdf88bb069877d5b78ec4f1bf3840a0ee', '[\"*\"]', NULL, '2021-08-26 08:34:41', '2021-08-26 08:34:41'),
(89, 'App\\Models\\User', 47, 'writers_talk', 'd4311b8c883d7500998415ed899c293048e349c4e5eba40573ab30075e2577e6', '[\"*\"]', '2021-08-27 10:37:39', '2021-08-26 08:34:54', '2021-08-27 10:37:39'),
(91, 'App\\Models\\User', 48, 'writers_talk', '2a4c1f1ec20ee919607b0934cdc093153bc3eb076cf9af48d41bfdc9cd7bf2c8', '[\"*\"]', NULL, '2021-08-27 10:30:21', '2021-08-27 10:30:21'),
(92, 'App\\Models\\User', 48, 'writers_talk', '0c7d0cc8104068786090e17e68992cd493c0fd232881a838ea8b0fd34045e4a3', '[\"*\"]', NULL, '2021-08-27 10:39:00', '2021-08-27 10:39:00'),
(93, 'App\\Models\\User', 49, 'writers_talk', '8761827f33e951bc2fa45d30d71fef68dd423a4e5eb78edc7dcad0ca15344ec8', '[\"*\"]', NULL, '2021-08-30 03:17:54', '2021-08-30 03:17:54'),
(94, 'App\\Models\\User', 49, 'writers_talk', 'edc3881141c0418751a1e85815a2f644651f688f73e594eb474a8463b3749d62', '[\"*\"]', '2021-08-31 10:13:20', '2021-08-30 03:18:15', '2021-08-31 10:13:20'),
(98, 'App\\Models\\User', 51, 'writers_talk', 'a38c2e329093c2764a2e7d386a4edcb280932805ce87cd358ccb5415b42da775', '[\"*\"]', NULL, '2021-08-30 06:57:23', '2021-08-30 06:57:23'),
(99, 'App\\Models\\User', 51, 'writers_talk', 'd14b33f101a14846a689a99de280c8c00d6b74a293a29d8bd32d8511d05b12ef', '[\"*\"]', '2021-08-30 07:30:49', '2021-08-30 06:57:31', '2021-08-30 07:30:49'),
(100, 'App\\Models\\User', 52, 'writers_talk', 'b6f37ba8a12c01f69eda5ae769b665cb4dfe13994f5a6b1210c5807ece7c27ec', '[\"*\"]', NULL, '2021-08-30 09:35:18', '2021-08-30 09:35:18'),
(101, 'App\\Models\\User', 52, 'writers_talk', '8c1e0b488681398e62d4d3e67e7966394de21246fa492e51e17c12105647adeb', '[\"*\"]', '2021-08-30 09:36:12', '2021-08-30 09:35:32', '2021-08-30 09:36:12'),
(104, 'App\\Models\\User', 54, 'writers_talk', '8aafa4809196a83540b74547958fa8c273b1b3b6ba863c3daf8296034a9bb754', '[\"*\"]', NULL, '2021-08-30 10:03:29', '2021-08-30 10:03:29'),
(105, 'App\\Models\\User', 54, 'writers_talk', 'fb7c0e27697e6af0728fc87efa4b104f366dece7a631199c69930c88496a42dd', '[\"*\"]', '2021-08-30 10:03:48', '2021-08-30 10:03:35', '2021-08-30 10:03:48'),
(112, 'App\\Models\\User', 58, 'writers_talk', 'd1265529606935fdae71cac28fc85b44c5569d687cf23fb03c22b3343f370356', '[\"*\"]', NULL, '2021-08-30 11:11:11', '2021-08-30 11:11:11'),
(113, 'App\\Models\\User', 58, 'writers_talk', '883a49699e2bedf66cb5d16c3a8eb6ad94ad28e49801f7359529c16221b107f8', '[\"*\"]', '2021-08-30 11:11:44', '2021-08-30 11:11:18', '2021-08-30 11:11:44'),
(114, 'App\\Models\\User', 59, 'writers_talk', '88918d73ad2f82b807187390f93df338f010ed35acee6a79933b0288f8f332bc', '[\"*\"]', NULL, '2021-08-30 11:28:26', '2021-08-30 11:28:26'),
(115, 'App\\Models\\User', 59, 'writers_talk', 'cb06179a8b831d0832ce03cb6cb1d8d34575febb051820893552812161950486', '[\"*\"]', '2021-08-30 11:43:53', '2021-08-30 11:30:39', '2021-08-30 11:43:53'),
(119, 'App\\Models\\User', 62, 'writers_talk', '5f59a302908d47dc42d7294a8051d749ca06c5e6d4fda38d425986d52220d709', '[\"*\"]', NULL, '2021-08-31 02:49:14', '2021-08-31 02:49:14'),
(150, 'App\\Models\\User', 70, 'writers_talk', '20e6a0c59ef576bb19138580b7795cc0f2ab6a3ac526a5b5443323717906b0ba', '[\"*\"]', '2021-08-31 06:46:45', '2021-08-31 06:33:56', '2021-08-31 06:46:45'),
(151, 'App\\Models\\User', 70, 'writers_talk', 'f347367a4befedcc8a1e3fcc28d5e94f85e574e38e1db9cdade6026d9dea0d57', '[\"*\"]', '2021-08-31 06:35:59', '2021-08-31 06:35:27', '2021-08-31 06:35:59'),
(163, 'App\\Models\\User', 71, 'writers_talk', '6a97bda94417302544d51d25f9643a189b5defc5d1c4b786f5fe0bf00984800f', '[\"*\"]', '2021-08-31 08:47:02', '2021-08-31 07:07:43', '2021-08-31 08:47:02'),
(173, 'App\\Models\\User', 70, 'writers_talk', '175906fb9a281b4905b0ddf8254ccc4679f4d57a67052ba96b7ec566a7169eab', '[\"*\"]', NULL, '2021-08-31 09:35:23', '2021-08-31 09:35:23'),
(178, 'App\\Models\\User', 76, 'writers_talk', '3bafd4e18507aba6bc462997c4f24cc21209aad996499976e7aa1fd51b0cf48c', '[\"*\"]', NULL, '2021-08-31 09:52:34', '2021-08-31 09:52:34'),
(179, 'App\\Models\\User', 76, 'writers_talk', '618715df5d0a583b66a05830a37fa1204a775685a3c2be43ee40eaf5b68ae73a', '[\"*\"]', '2021-08-31 09:53:03', '2021-08-31 09:52:45', '2021-08-31 09:53:03'),
(182, 'App\\Models\\User', 76, 'writers_talk', '398db53fa61e8ad834b5549a52d645334c13e44d48897d9614eed34c259330bf', '[\"*\"]', '2021-08-31 09:57:26', '2021-08-31 09:57:24', '2021-08-31 09:57:26'),
(183, 'App\\Models\\User', 76, 'writers_talk', 'ad21e2c44d8b199868c53b1958350c4c22a1ab5e7e89fe5af319f43913f7a34d', '[\"*\"]', '2021-08-31 10:38:50', '2021-08-31 09:57:26', '2021-08-31 10:38:50'),
(186, 'App\\Models\\User', 78, 'writers_talk', 'eb5fb0d4c5c54a1d3044814567f9059b2836611f9a7cb2008ab9c0fb75e9df19', '[\"*\"]', NULL, '2021-08-31 10:12:21', '2021-08-31 10:12:21'),
(187, 'App\\Models\\User', 78, 'writers_talk', '29481ec2bd2515183c0aec8269dfc3829c6d6e3219b58aa0b532df9aaf67be95', '[\"*\"]', NULL, '2021-08-31 10:12:29', '2021-08-31 10:12:29'),
(216, 'App\\Models\\User', 59, 'writers_talk', 'e4fe01f1afdb4db9d3bb39a28c8c33d5d6e63f7351867629534d99c7aa811784', '[\"*\"]', '2021-09-02 03:56:53', '2021-09-01 07:11:52', '2021-09-02 03:56:53'),
(221, 'App\\Models\\User', 86, 'writers_talk', '2e84ad4c0b77ce94c28444aeb153cf9970d9ece3d51a8f32830ad625d863444b', '[\"*\"]', NULL, '2021-09-01 09:57:06', '2021-09-01 09:57:06'),
(225, 'App\\Models\\User', 59, 'writers_talk', '332226c1d1a582dbe75705f0d093182753233e48c6b0dca3460abc7b39031245', '[\"*\"]', '2021-09-02 03:57:45', '2021-09-02 03:13:38', '2021-09-02 03:57:45'),
(257, 'App\\Models\\User', 95, 'writers_talk', 'ee7df9a6498b9a3548322e799cf158a5f4931fa222458d9d3dbe61b2893e6a68', '[\"*\"]', '2021-09-04 06:00:38', '2021-09-03 12:17:21', '2021-09-04 06:00:38'),
(258, 'App\\Models\\User', 94, 'writers_talk', 'd25b89dbcfd57c454074960e5d1c4e9fac2e77061e97ef452add58411d4c38a7', '[\"*\"]', NULL, '2021-09-03 12:45:32', '2021-09-03 12:45:32'),
(278, 'App\\Models\\User', 97, 'writers_talk', '9f56285cf5a32465393c7131f3522fcb970534cb0a0ce718999e6fdcb27ac492', '[\"*\"]', '2021-09-04 08:06:26', '2021-09-04 06:49:30', '2021-09-04 08:06:26'),
(279, 'App\\Models\\User', 97, 'writers_talk', '10400c9d94b62e0fa2a68c93ee84aabd7441a77577db0c09595f2cf6103bfe5b', '[\"*\"]', '2021-09-04 07:15:29', '2021-09-04 06:59:05', '2021-09-04 07:15:29'),
(280, 'App\\Models\\User', 95, 'writers_talk', 'e33057e65483e017f7f0c0ff2cadd9a067a007c1b9d524aced8b8fe88d2e2ddc', '[\"*\"]', '2021-09-04 07:14:35', '2021-09-04 07:13:59', '2021-09-04 07:14:35'),
(281, 'App\\Models\\User', 97, 'writers_talk', '1e06011dfa333339f92a9f454b6581ed3442f28d0df95f35dfaba00c007cf81b', '[\"*\"]', '2021-09-04 07:16:03', '2021-09-04 07:15:43', '2021-09-04 07:16:03'),
(282, 'App\\Models\\User', 95, 'writers_talk', '2792a7fd24bdd27e8c5821d84bd07f0033d9169eff9dacf1ef052c8ed394ad84', '[\"*\"]', '2021-09-04 07:16:52', '2021-09-04 07:16:28', '2021-09-04 07:16:52'),
(283, 'App\\Models\\User', 98, 'writers_talk', '3a170b2e09ba20f9b7443c7ab9b380ccc3455b6d8adef08a2132e22dc3376e68', '[\"*\"]', '2021-09-04 09:53:40', '2021-09-04 09:36:01', '2021-09-04 09:53:40'),
(284, 'App\\Models\\User', 98, 'writers_talk', '56ffbc3c42c4c463076b8a55403c3f69fa5c89fa5183518e7d44f9ec692c110e', '[\"*\"]', '2021-09-04 09:50:00', '2021-09-04 09:48:18', '2021-09-04 09:50:00'),
(285, 'App\\Models\\User', 97, 'writers_talk', '271d7e27c111e19fb44d448e956967dca16514f4c52f1c1ced72f0b7fb3e02ec', '[\"*\"]', NULL, '2021-09-14 07:41:47', '2021-09-14 07:41:47'),
(291, 'App\\Models\\User', 100, 'writers_talk', 'f555cdcb7607da7b9da31db9028c2ad9ff86fc15fa44d1ecfd87467a913d58e6', '[\"*\"]', NULL, '2021-09-15 05:35:58', '2021-09-15 05:35:58'),
(292, 'App\\Models\\User', 100, 'writers_talk', '8998202f026678df90a32f045fe7b0881d2c29f2333a2b2732ef89e19a5b92d7', '[\"*\"]', '2021-09-15 05:40:57', '2021-09-15 05:40:37', '2021-09-15 05:40:57'),
(293, 'App\\Models\\User', 100, 'writers_talk', 'd7cfd1b0121eaf8e8ace5e443840262b2420338872f650e3569785b5618cd876', '[\"*\"]', NULL, '2021-09-15 05:45:20', '2021-09-15 05:45:20'),
(294, 'App\\Models\\User', 100, 'writers_talk', '7552c59b47a706c442d047fb7897a420a928c025b6ef306f6326deef9db6c3b0', '[\"*\"]', '2021-09-15 05:52:59', '2021-09-15 05:46:01', '2021-09-15 05:52:59'),
(295, 'App\\Models\\User', 100, 'writers_talk', '35d644a7d2680c2d0d6a51aaea29c6afaab96cdfdfb08b56083403ef650a3fb2', '[\"*\"]', '2021-09-15 06:05:19', '2021-09-15 06:05:04', '2021-09-15 06:05:19'),
(296, 'App\\Models\\User', 101, 'writers_talk', '1a2f36bdebddcd06766568610bb89c7a8de61d209b089c7ad76f3ad779843d38', '[\"*\"]', '2021-09-15 08:19:48', '2021-09-15 08:18:29', '2021-09-15 08:19:48'),
(297, 'App\\Models\\User', 103, 'writers_talk', 'b582b79c0cf5de8856353b64d9240fdcce7c67011a9df2300a46dc471f64ea28', '[\"*\"]', '2021-09-15 09:57:08', '2021-09-15 09:55:47', '2021-09-15 09:57:08'),
(298, 'App\\Models\\User', 101, 'writers_talk', 'cd80b760db4d69396e95b44cbe7780c06c3fed682441e9f11a9035cae878fd47', '[\"*\"]', '2021-09-15 10:00:38', '2021-09-15 10:00:26', '2021-09-15 10:00:38'),
(299, 'App\\Models\\User', 104, 'writers_talk', '2faec6e37cc617a11ba39be0c0537b4f0f4285b29d4cde941e2f79e7bdc1ef45', '[\"*\"]', '2021-09-15 10:12:52', '2021-09-15 10:08:53', '2021-09-15 10:12:52'),
(300, 'App\\Models\\User', 105, 'writers_talk', '2d68ab1db7a8c29e8704b381b2dab17ecc0c1d91a35ca058a2a76ad4716dc9c0', '[\"*\"]', NULL, '2021-09-15 10:13:43', '2021-09-15 10:13:43'),
(301, 'App\\Models\\User', 105, 'writers_talk', '18701fa5c42398cd3d1cae9715f6d87de6369811b08630a7106e657d9485c063', '[\"*\"]', NULL, '2021-09-15 10:14:17', '2021-09-15 10:14:17'),
(302, 'App\\Models\\User', 105, 'writers_talk', '918cda494488556becc670b73a41bbff8f49fb5cb2cf9154cf8a369a67c21d38', '[\"*\"]', NULL, '2021-09-15 10:15:54', '2021-09-15 10:15:54'),
(303, 'App\\Models\\User', 105, 'writers_talk', '7e3ce04218d2ce5a09304497b58e0fa90900c62ad7b941fd85b5e3821bc69283', '[\"*\"]', NULL, '2021-09-15 10:18:09', '2021-09-15 10:18:09'),
(304, 'App\\Models\\User', 105, 'writers_talk', 'ef29c2bd758061bf40f51814250653e883c7ec5e29fb7e36ccd80d93ecaeee4a', '[\"*\"]', NULL, '2021-09-15 10:18:31', '2021-09-15 10:18:31'),
(305, 'App\\Models\\User', 105, 'writers_talk', 'f244fcc3fc3ae03a7a946893f1a79bbc4aa56ed305b58100fa42fe28d67b38b9', '[\"*\"]', NULL, '2021-09-15 10:18:36', '2021-09-15 10:18:36'),
(306, 'App\\Models\\User', 105, 'writers_talk', 'b330cd9c98fb8ed49400eb34b0e4e8bbef82844af03ea4651c2bacc8e37175d2', '[\"*\"]', NULL, '2021-09-15 10:18:51', '2021-09-15 10:18:51'),
(307, 'App\\Models\\User', 105, 'writers_talk', '8112cc50ff3b480c439076efbc094482cb3de82ad3083919510f1a82f65e2ba4', '[\"*\"]', '2021-09-16 03:43:32', '2021-09-15 10:20:45', '2021-09-16 03:43:32'),
(308, 'App\\Models\\User', 107, 'writers_talk', '6ecbdae7f0d1df870d4a00e57934104ba8ac2eeb38d7cf9ea992580d7faab6a8', '[\"*\"]', NULL, '2021-09-15 10:25:14', '2021-09-15 10:25:14'),
(309, 'App\\Models\\User', 108, 'writers_talk', '480b2c9b0bde3861bf62829410eb0c8fd6d1cea5714f3410f5d561b476d10dce', '[\"*\"]', '2021-09-16 03:47:31', '2021-09-16 02:59:14', '2021-09-16 03:47:31'),
(310, 'App\\Models\\User', 108, 'writers_talk', 'd6945763454f003625a07fb67f54d029364d99fe20f97d1a188971d85d36c2f2', '[\"*\"]', NULL, '2021-09-16 03:44:26', '2021-09-16 03:44:26'),
(311, 'App\\Models\\User', 108, 'writers_talk', '02c3cef2acf29a8520d43a803017b80558ec1e3afa144bc54f8ca026fcbbf6e9', '[\"*\"]', '2021-09-16 04:53:38', '2021-09-16 03:44:41', '2021-09-16 04:53:38'),
(312, 'App\\Models\\User', 108, 'writers_talk', '4bb1f5145c941b0a7a5ed9ffcc9e42583ed6793146982e7fa4bb032c4836b195', '[\"*\"]', '2021-09-16 03:48:42', '2021-09-16 03:48:16', '2021-09-16 03:48:42'),
(313, 'App\\Models\\User', 108, 'writers_talk', '4f128d74b4da077c0b9aa314cd2c9f03985fac1948ad18b5483ddd056d696bdc', '[\"*\"]', '2021-09-16 05:23:32', '2021-09-16 04:56:51', '2021-09-16 05:23:32'),
(314, 'App\\Models\\User', 108, 'writers_talk', '05cd5a2a75b80eb3f4b16fb70ed7cb0e2be21d147b58703584aa6e126702445b', '[\"*\"]', '2021-09-16 05:44:33', '2021-09-16 05:22:25', '2021-09-16 05:44:33'),
(315, 'App\\Models\\User', 107, 'writers_talk', '442ece2b3ee1674f582b8988c9bb050515f7e27f4b433bbe774f3854a9e4aebc', '[\"*\"]', '2021-09-16 05:33:29', '2021-09-16 05:32:57', '2021-09-16 05:33:29'),
(316, 'App\\Models\\User', 108, 'writers_talk', '15662c97d9be6dd36de216d1614534646f9d772a3362f83d7b1cda69f67dac00', '[\"*\"]', NULL, '2021-09-16 05:33:57', '2021-09-16 05:33:57'),
(317, 'App\\Models\\User', 108, 'writers_talk', '75cfcc76b75f107adcdee8fa12efa5c104a95fbcd5e69d8c53c0bdc045211644', '[\"*\"]', '2021-09-16 05:41:53', '2021-09-16 05:34:02', '2021-09-16 05:41:53'),
(318, 'App\\Models\\User', 109, 'writers_talk', '6f0533e374d4cd47fdad3e1b3fd169a387aa47c4bc393905c9687354cbc653ef', '[\"*\"]', NULL, '2021-09-16 05:45:28', '2021-09-16 05:45:28'),
(319, 'App\\Models\\User', 109, 'writers_talk', '0af5ea0660c5d50be3f866e6dcf01387c9354551529336cb7593a5b0e385440a', '[\"*\"]', '2021-09-16 05:45:53', '2021-09-16 05:45:43', '2021-09-16 05:45:53'),
(320, 'App\\Models\\User', 109, 'writers_talk', 'be84b8c5cd1fae61a83529216fa3dc08c2e96cb8367e2f93d6cfb03943d24100', '[\"*\"]', '2021-09-16 05:58:39', '2021-09-16 05:47:26', '2021-09-16 05:58:39'),
(321, 'App\\Models\\User', 109, 'writers_talk', 'ae1fc364a43a446c3c4db87ed8496482bc8256696379352d2a9905f32df27ca5', '[\"*\"]', NULL, '2021-09-16 05:57:59', '2021-09-16 05:57:59'),
(322, 'App\\Models\\User', 109, 'writers_talk', '37675f1747d8c74be210f205610dc1bf85424ab3cc6c38099f7c612219969c4b', '[\"*\"]', '2021-09-16 06:09:21', '2021-09-16 05:59:57', '2021-09-16 06:09:21'),
(323, 'App\\Models\\User', 109, 'writers_talk', '502bf4734d50d666c52b9efdbf862326932e2243da2c4a73ffdd48127ccc65c8', '[\"*\"]', '2021-09-16 06:13:51', '2021-09-16 06:10:20', '2021-09-16 06:13:51'),
(324, 'App\\Models\\User', 109, 'writers_talk', '9baa02857b2d4f499c18448d32ab42688695a9b6e1ac391aad9efccf8464ea41', '[\"*\"]', '2021-09-16 10:06:18', '2021-09-16 06:14:51', '2021-09-16 10:06:18'),
(325, 'App\\Models\\User', 109, 'writers_talk', '00ce98594839d55755abd1922ffded30e2bc0c39ca573a313068bedf8790c9a1', '[\"*\"]', '2021-09-17 04:20:51', '2021-09-16 10:07:27', '2021-09-17 04:20:51'),
(326, 'App\\Models\\User', 110, 'writers_talk', 'b5c8c33e5dc4b910f5f91ad905016f1c83b9de103db80a655530cdb694c0a5f9', '[\"*\"]', '2021-09-17 04:31:49', '2021-09-17 04:17:57', '2021-09-17 04:31:49'),
(327, 'App\\Models\\User', 107, 'writers_talk', '1a686df527f94dafc9a9ab12a911e355d67dd051ffe485b07380c54955f3328d', '[\"*\"]', NULL, '2021-09-22 08:45:06', '2021-09-22 08:45:06'),
(328, 'App\\Models\\User', 107, 'writers_talk', 'c9aafb923deb03b7023e4800021df8c5f03801183c99def8602f3a5bf96e9c27', '[\"*\"]', NULL, '2021-09-22 08:48:24', '2021-09-22 08:48:24'),
(329, 'App\\Models\\User', 107, 'writers_talk', 'a48b605d33dc826a82d1d7c40d720bf7dca5a158b1f0f03ebd24721c644dd564', '[\"*\"]', '2021-09-22 09:58:25', '2021-09-22 08:49:11', '2021-09-22 09:58:25'),
(330, 'App\\Models\\User', 115, 'writers_talk', 'c2ded73af27e63631c46cd59aeb7563434e6190e25e4da012d97da77fcca23f2', '[\"*\"]', NULL, '2021-09-22 10:29:06', '2021-09-22 10:29:06'),
(331, 'App\\Models\\User', 116, 'writers_talk', 'b78d829218ca494555c95408ab565be7dfb962c6cddc7826a846bdc8a7159c2e', '[\"*\"]', NULL, '2021-09-22 10:39:32', '2021-09-22 10:39:32'),
(358, 'App\\Models\\User', 13, 'writers_talk', '06307e275befdb74559a94e034b17bdff0b8294af0f34c7c855ed736097fe57b', '[\"*\"]', '2021-10-18 07:25:59', '2021-10-18 03:03:07', '2021-10-18 07:25:59'),
(359, 'App\\Models\\User', 13, 'writers_talk', '5584de7bd8408b2565f0265ba8dc42d59cf56f217f8af485078b454b799ef999', '[\"*\"]', '2021-10-27 03:13:50', '2021-10-18 03:11:28', '2021-10-27 03:13:50'),
(360, 'App\\Models\\User', 13, 'writers_talk', '8163f160316b1d2911b2510df848c1bfb09ca6fc438f15289a6fb7d9ceaff4a6', '[\"*\"]', NULL, '2021-10-18 07:18:50', '2021-10-18 07:18:50'),
(394, 'App\\Models\\User', 13, 'writers_talk', 'cb4316a24ee1fa708cad00a9eb8a016a640e31ad8a734746837693f80418d824', '[\"*\"]', '2021-10-25 09:44:39', '2021-10-25 09:41:13', '2021-10-25 09:44:39'),
(398, 'App\\Models\\User', 13, 'writers_talk', 'e1969cc75ffa4fee99ea488b7fc0a49634cb470f1bfaed3a3b2efd0d498c9de8', '[\"*\"]', '2021-10-25 11:52:07', '2021-10-25 10:31:38', '2021-10-25 11:52:07'),
(401, 'App\\Models\\User', 13, 'writers_talk', 'ad6f72e7b942fe0c2d8beb18c4c8454c53a9fd91aa946499037dbf0799bed07c', '[\"*\"]', '2021-10-26 06:24:33', '2021-10-26 05:37:21', '2021-10-26 06:24:33'),
(404, 'App\\Models\\User', 13, 'writers_talk', '4592e59e5c6d1d76ea288f69681ca1cf8b85ebdd1298ad051d21772e0b706ef1', '[\"*\"]', '2021-10-27 06:55:33', '2021-10-27 03:32:37', '2021-10-27 06:55:33'),
(409, 'App\\Models\\User', 13, 'writers_talk', 'fad120c0abe1abbf35bde6e2404978b12dfb86ac330592fb30961df38ccca161', '[\"*\"]', '2021-10-28 08:34:11', '2021-10-28 07:09:47', '2021-10-28 08:34:11'),
(414, 'App\\Models\\User', 19, 'writers_talk', '1d9b302749a7f840034f2acc67b67034a797c93409726dd42b6bfad24da8b440', '[\"*\"]', '2021-11-01 09:22:24', '2021-11-01 06:46:05', '2021-11-01 09:22:24'),
(415, 'App\\Models\\User', 19, 'writers_talk', '96c69b59f591f98832d13d258997f7e8772f9774fe370f28a21e661d8769c4ec', '[\"*\"]', '2021-11-01 08:34:52', '2021-11-01 08:02:17', '2021-11-01 08:34:52'),
(418, 'App\\Models\\User', 19, 'writers_talk', 'ef64325349a9d615c3fa181ec51326435af2c7b4b265821fbe25fdfb3af13e7d', '[\"*\"]', '2021-11-01 09:22:28', '2021-11-01 09:21:10', '2021-11-01 09:22:28'),
(419, 'App\\Models\\User', 19, 'writers_talk', 'f65d385411491a8efbf9f9c4a83c52900c2be9282b4950147a34b4a148c87a38', '[\"*\"]', NULL, '2021-11-01 09:22:14', '2021-11-01 09:22:14'),
(422, 'App\\Models\\User', 20, 'writers_talk', '7516c85c66dded2d756bf1939bbf0c87d8ca8110f42602cc32df17e2c1f02cbd', '[\"*\"]', NULL, '2021-11-01 09:27:23', '2021-11-01 09:27:23'),
(428, 'App\\Models\\User', 13, 'writers_talk', '6908467be5810438194853511255514635c352f3a45448b1b67831d21962d062', '[\"*\"]', '2021-11-02 09:52:41', '2021-11-02 09:46:39', '2021-11-02 09:52:41'),
(432, 'App\\Models\\User', 13, 'writers_talk', '42566622bf758daf965a9ae060f3a885239783cd88d85ade035365ccddee04d5', '[\"*\"]', '2021-11-04 11:19:49', '2021-11-04 05:02:19', '2021-11-04 11:19:49'),
(435, 'App\\Models\\User', 13, 'writers_talk', '7e09aed38988e4ab7c6c544a6dfd4c52b137ae210dcb77a6a1c1b4811731a632', '[\"*\"]', '2021-11-05 10:04:27', '2021-11-05 05:20:27', '2021-11-05 10:04:27'),
(441, 'App\\Models\\User', 13, 'writers_talk', 'd68ed2f71c84af8252b8ce7e59daee4557b4461bd2004d357eb99b1f42ca2826', '[\"*\"]', '2021-11-08 04:12:43', '2021-11-06 02:38:41', '2021-11-08 04:12:43'),
(449, 'App\\Models\\User', 22, 'writers_talk', '84202b83ba98fa50025a0f9b800572382133a9997247fe150506257f66e14d64', '[\"*\"]', '2021-11-09 18:50:08', '2021-11-09 18:48:33', '2021-11-09 18:50:08'),
(463, 'App\\Models\\User', 1, 'writers_talk', 'b4c6983cb2748b9fac88d40678b38e94450d180b3a119f08636977c53c82537d', '[\"*\"]', '2021-11-15 17:20:28', '2021-11-09 23:40:49', '2021-11-15 17:20:28'),
(465, 'App\\Models\\User', 4, 'writers_talk', '0873b03f77d428e7cbc55e142f2c5bbcd9c4b0d7b0db117ff557afd96c9c38d7', '[\"*\"]', '2021-11-17 00:18:48', '2021-11-12 20:22:06', '2021-11-17 00:18:48'),
(466, 'App\\Models\\User', 4, 'writers_talk', '15680e4026f3e75dae6e1f72671aaadd8d8ed5d092b2ef7cf7d48518319a4743', '[\"*\"]', '2021-11-17 05:17:01', '2021-11-13 13:13:26', '2021-11-17 05:17:01'),
(467, 'App\\Models\\User', 5, 'writers_talk', '113e25919f93b869aaa20c34b947b0d186806050b292723a963b16b849323ea7', '[\"*\"]', '2021-11-13 17:35:32', '2021-11-13 17:11:55', '2021-11-13 17:35:32'),
(468, 'App\\Models\\User', 2, 'writers_talk', 'ee9d5f4aadfd901f1fe82d5215c9c7e38855190bd96d1aa73f5199923c8963ae', '[\"*\"]', '2021-11-15 17:20:56', '2021-11-15 17:20:55', '2021-11-15 17:20:56'),
(469, 'App\\Models\\User', 6, 'writers_talk', '8dff84626ad2e44357a9481bf68b028539f201521f39b28edacc789b67dbaaf6', '[\"*\"]', '2021-11-15 22:34:20', '2021-11-15 17:24:48', '2021-11-15 22:34:20'),
(470, 'App\\Models\\User', 6, 'writers_talk', 'f0d722ffafa49b09f1a3d9f61865da399e017b4c87950132696b95b2a60bb798', '[\"*\"]', '2021-11-16 17:43:08', '2021-11-16 00:42:24', '2021-11-16 17:43:08'),
(471, 'App\\Models\\User', 6, 'writers_talk', '967df01422798fa21d79eb884811250c4438d1d33d59c4b33c7b38db2a8ec890', '[\"*\"]', '2021-11-16 00:50:28', '2021-11-16 00:50:15', '2021-11-16 00:50:28'),
(472, 'App\\Models\\User', 6, 'writers_talk', '1ab0beedaefdc9a1de246229ea17965641cf2306d1cf93032285d49bd19cd1ab', '[\"*\"]', NULL, '2021-11-16 00:59:05', '2021-11-16 00:59:05'),
(473, 'App\\Models\\User', 6, 'writers_talk', '4357702eda971e895ce7ea5c8d620dce437c089e6143966cd9739a77c7fbe18b', '[\"*\"]', '2021-12-09 01:59:26', '2021-11-16 01:13:40', '2021-12-09 01:59:26'),
(476, 'App\\Models\\User', 4, 'writers_talk', '5a7f96aa8f129a51c0d5f6711c7240003b0ed0ded66d91466f998aedf12ec290', '[\"*\"]', '2021-11-21 03:25:17', '2021-11-17 05:19:18', '2021-11-21 03:25:17'),
(477, 'App\\Models\\User', 7, 'writers_talk', 'a17ac9632fee001e02cc6b40547e2674c8577107cf04f8fb9645ef0d0de21ce8', '[\"*\"]', '2021-12-28 02:28:36', '2021-11-19 21:07:22', '2021-12-28 02:28:36'),
(478, 'App\\Models\\User', 8, 'writers_talk', '21ba0ebc95f50bda91e15617854b0a0d38925f6f57668bceafbb136cec92c158', '[\"*\"]', '2021-11-23 17:38:13', '2021-11-23 01:21:50', '2021-11-23 17:38:13'),
(480, 'App\\Models\\User', 1, 'writers_talk', '896d33f04fd34ed3e9b75a8907862610d1d0ee638fc597a5aa1597d0ea673878', '[\"*\"]', '2021-11-25 00:40:44', '2021-11-25 00:40:36', '2021-11-25 00:40:44'),
(484, 'App\\Models\\User', 11, 'writers_talk', '4e7967124e9b474873bcf1b8d0c66e57c2db1929539f6500304ff6e4c10d1752', '[\"*\"]', '2021-12-02 16:48:56', '2021-12-01 01:31:46', '2021-12-02 16:48:56'),
(485, 'App\\Models\\User', 12, 'writers_talk', 'e9b3e507e98921242952c87dd9cf14a144212d80985f6951446e6a1d3aa134d8', '[\"*\"]', NULL, '2021-12-04 01:39:20', '2021-12-04 01:39:20'),
(486, 'App\\Models\\User', 12, 'writers_talk', 'f3d0beb334f489e72946dda9b1714af1c3d2d7e72bdc474253fa3fdec166fb55', '[\"*\"]', '2021-12-07 17:38:35', '2021-12-04 01:42:01', '2021-12-07 17:38:35'),
(487, 'App\\Models\\User', 13, 'writers_talk', 'e001acf7e67e5337d1ddca5b234349ac7c60756a2d84c9974731ae5858048069', '[\"*\"]', '2021-12-12 03:47:46', '2021-12-04 12:40:38', '2021-12-12 03:47:46'),
(498, 'App\\Models\\User', 14, 'writers_talk', '0eb3070fbf5b570c4eab9b2c3c5e4d7e9ea8407e5840aa31cd4f28d71b509942', '[\"*\"]', '2021-12-08 16:49:45', '2021-12-08 00:20:06', '2021-12-08 16:49:45'),
(500, 'App\\Models\\User', 14, 'writers_talk', '1567f566d65e007beb1ae31698ded2aa01354641937f86c6d3dd6a14a9bb6071', '[\"*\"]', '2021-12-10 17:49:05', '2021-12-08 18:47:29', '2021-12-10 17:49:05'),
(502, 'App\\Models\\User', 18, 'writers_talk', 'fec8ba05a3514bbdcab711694a602c53baac5d21ada924199ddae70e029d1ea0', '[\"*\"]', '2021-12-10 18:53:18', '2021-12-10 18:52:25', '2021-12-10 18:53:18'),
(503, 'App\\Models\\User', 19, 'writers_talk', 'ac7081b8bfb0d21ff14ce7588a2fa8361555f50a05370270bd6f875743f5f43b', '[\"*\"]', '2021-12-10 18:54:10', '2021-12-10 18:53:54', '2021-12-10 18:54:10'),
(504, 'App\\Models\\User', 14, 'writers_talk', '7f0834a83b3208fcddfc20ce1494301560aafea892d7476cf12443141a5a1658', '[\"*\"]', NULL, '2021-12-10 18:56:02', '2021-12-10 18:56:02'),
(505, 'App\\Models\\User', 20, 'writers_talk', 'e2d5500d4f85f8e1458b6af174a23a088f1b9318651fe30ec57d76609371ea31', '[\"*\"]', '2021-12-10 22:40:33', '2021-12-10 22:39:59', '2021-12-10 22:40:33'),
(506, 'App\\Models\\User', 21, 'writers_talk', '0daf7eb173f75e341fbf02ef24af7909ad557a25ba9e5711178d96fad2e4a0cd', '[\"*\"]', '2021-12-10 22:41:55', '2021-12-10 22:41:19', '2021-12-10 22:41:55'),
(507, 'App\\Models\\User', 22, 'writers_talk', '035af53fe5c0b8a970e92b49d51dcef750f3d3849519c7e1e825340f10814ee5', '[\"*\"]', NULL, '2021-12-12 00:23:41', '2021-12-12 00:23:41'),
(508, 'App\\Models\\User', 23, 'writers_talk', '0f21370556303c19c9d272df7384903b8a41cbbb14ea5885f8933c2475fdaba6', '[\"*\"]', '2021-12-12 00:28:08', '2021-12-12 00:25:44', '2021-12-12 00:28:08'),
(509, 'App\\Models\\User', 24, 'writers_talk', 'a0a985ef2a17e533a58c7e4b27779513b431c3290880649de58802f6304534f1', '[\"*\"]', '2021-12-12 00:31:28', '2021-12-12 00:31:23', '2021-12-12 00:31:28'),
(510, 'App\\Models\\User', 25, 'writers_talk', 'afb25a773488ee644607df55efb81362f17f9b67fbe549aeee83e6721f80fd7c', '[\"*\"]', '2021-12-12 00:33:39', '2021-12-12 00:31:42', '2021-12-12 00:33:39'),
(512, 'App\\Models\\User', 27, 'writers_talk', '1a0b38a0d9efa0c77141e09560916d29a4c8300addb3d65f43ca03e77e8fbdbc', '[\"*\"]', '2021-12-12 01:08:58', '2021-12-12 01:08:44', '2021-12-12 01:08:58'),
(513, 'App\\Models\\User', 15, 'writers_talk', 'd53d0b1e9d83e5e51684b60947788fb6aa241e338d07bf15fec8465134d269a9', '[\"*\"]', '2021-12-12 01:09:22', '2021-12-12 01:09:22', '2021-12-12 01:09:22'),
(514, 'App\\Models\\User', 12, 'writers_talk', 'b929f11f711ee67dadab6c8938b3fffd43829b9580cc89daa3c0f5bd71902dbf', '[\"*\"]', '2021-12-12 01:09:37', '2021-12-12 01:09:36', '2021-12-12 01:09:37'),
(515, 'App\\Models\\User', 12, 'writers_talk', '81f24c10d8b41e4ef23229c90d2655ca09ef0d673ad93113ba596d98fd94ddfb', '[\"*\"]', '2021-12-12 01:09:51', '2021-12-12 01:09:37', '2021-12-12 01:09:51'),
(516, 'App\\Models\\User', 28, 'writers_talk', 'aa08fa686b952d8746d092584ca8352adc23cb6038e6f888e1903a54fd0e238c', '[\"*\"]', '2021-12-12 01:12:51', '2021-12-12 01:12:46', '2021-12-12 01:12:51'),
(517, 'App\\Models\\User', 29, 'writers_talk', '3a98d8687c558e984ae0ce0ca97e8e0763c855d6e6195ec8a3827e48cc6bee47', '[\"*\"]', '2021-12-12 01:14:08', '2021-12-12 01:13:56', '2021-12-12 01:14:08'),
(518, 'App\\Models\\User', 29, 'writers_talk', '4ad9e3117de5bb0d8332ab12f5177e0f681e2c3a786f7af99bc382c46c9a01d4', '[\"*\"]', '2021-12-12 01:14:39', '2021-12-12 01:14:38', '2021-12-12 01:14:39'),
(519, 'App\\Models\\User', 29, 'writers_talk', '30df79b77ae45dc5b0591ab8af914a09c268d3978509d86b9c4ec4f48e23d478', '[\"*\"]', '2021-12-12 01:17:06', '2021-12-12 01:16:56', '2021-12-12 01:17:06'),
(520, 'App\\Models\\User', 3, 'writers_talk', 'aaf2e9d161b61c365e9a577e828f11770dfe33df6d5bb469baf195ead363942f', '[\"*\"]', '2021-12-12 01:23:10', '2021-12-12 01:22:48', '2021-12-12 01:23:10'),
(521, 'App\\Models\\User', 30, 'writers_talk', '5ed5e4ff8f67868c85b8addc5cec1c53d92619bb4b84ee4267b82d2e3de3535c', '[\"*\"]', '2021-12-12 03:52:20', '2021-12-12 03:52:07', '2021-12-12 03:52:20'),
(522, 'App\\Models\\User', 30, 'writers_talk', '310ed697771b826e48f92274bf0be90092b09f595c1e76237221647ced833bc8', '[\"*\"]', '2021-12-12 03:59:28', '2021-12-12 03:52:39', '2021-12-12 03:59:28'),
(523, 'App\\Models\\User', 30, 'writers_talk', '9725721017d952d2aa954f3c1a0dbdfcd1ea4015feb7fec590a0b08e7a3efe11', '[\"*\"]', '2021-12-12 04:25:42', '2021-12-12 04:01:24', '2021-12-12 04:25:42'),
(524, 'App\\Models\\User', 30, 'writers_talk', '272500b3fbf8ed7701bfa377fc6ce27311789f9aa4e8dbf613998df16e720c52', '[\"*\"]', '2021-12-12 04:41:18', '2021-12-12 04:37:45', '2021-12-12 04:41:18'),
(525, 'App\\Models\\User', 15, 'writers_talk', '63b38e4f9be83e24b47f83f3aeecfece143b1aeac66773f95fda56ff7ff5eee9', '[\"*\"]', '2021-12-16 17:17:16', '2021-12-16 17:17:15', '2021-12-16 17:17:16'),
(532, 'App\\Models\\User', 34, 'writers_talk', '0c1d7de750b5fd862fe9c62fc9d2037747e7cf96005cd759d360b5e4e83072c8', '[\"*\"]', '2021-12-16 18:07:08', '2021-12-16 18:06:58', '2021-12-16 18:07:08'),
(535, 'App\\Models\\User', 36, 'writers_talk', '9712f2c3c9117009e34f39cca7eea161dd91bce193f94dd955d301e20a94a64b', '[\"*\"]', '2021-12-16 18:14:28', '2021-12-16 18:14:23', '2021-12-16 18:14:28'),
(543, 'App\\Models\\User', 35, 'writers_talk', 'd90216b0d2155a53c5eef839becb9400f134554d2e2c22d99bc07e2babd745d0', '[\"*\"]', '2021-12-21 17:22:31', '2021-12-16 18:30:17', '2021-12-21 17:22:31'),
(544, 'App\\Models\\User', 37, 'writers_talk', '98a30dab8f9569965218493aecbbe3feffc0990b563dc2eb9acfd421fda23ee2', '[\"*\"]', '2021-12-23 16:39:35', '2021-12-22 16:55:51', '2021-12-23 16:39:35'),
(546, 'App\\Models\\User', 37, 'writers_talk', '70bd39579cccbebbc3820f4f532f9fc7d331e3ad1326b7e39feacbb9c7f1efcb', '[\"*\"]', '2021-12-23 16:41:14', '2021-12-23 16:40:10', '2021-12-23 16:41:14'),
(547, 'App\\Models\\User', 37, 'writers_talk', 'be84ff8118532e37823ee833ff18e747ae1361c5a595132cb305256dbc77f2a6', '[\"*\"]', '2021-12-23 16:56:50', '2021-12-23 16:56:49', '2021-12-23 16:56:50'),
(549, 'App\\Models\\User', 40, 'writers_talk', '9eb0c2add299f736e22c3f7ebe5f483d284fe13b967a7fed21339f3e59f9029a', '[\"*\"]', '2021-12-24 00:14:29', '2021-12-23 23:53:13', '2021-12-24 00:14:29'),
(550, 'App\\Models\\User', 40, 'writers_talk', '1f04268419c43dc00ccc7a4c5348e830651adb583bd29c38eb13a550b8569a7a', '[\"*\"]', '2021-12-23 23:57:49', '2021-12-23 23:57:33', '2021-12-23 23:57:49'),
(552, 'App\\Models\\User', 40, 'writers_talk', '81cb31386959b13da741a9ea66c26d3bd0816039bbf9242b4c005f96296056ba', '[\"*\"]', '2021-12-30 14:47:23', '2021-12-24 15:01:11', '2021-12-30 14:47:23'),
(553, 'App\\Models\\User', 40, 'writers_talk', '391138d450471406c0e30b96fa0d1a041fed12064be4a45004bfc779c86ecff7', '[\"*\"]', '2022-01-14 16:44:21', '2021-12-24 15:27:08', '2022-01-14 16:44:21'),
(554, 'App\\Models\\User', 40, 'writers_talk', 'f2d0ce9ba78c667c8601eb476f10b309d6b9f71c35d4e8bfaf0a59dcfd8d20e7', '[\"*\"]', '2021-12-27 16:39:36', '2021-12-24 16:54:49', '2021-12-27 16:39:36'),
(555, 'App\\Models\\User', 41, 'writers_talk', '9277b33945df9bd5f1c3d7d1b2c125575c450204ba9d7e1e4bedcb2d3750912a', '[\"*\"]', NULL, '2021-12-25 22:34:01', '2021-12-25 22:34:01'),
(556, 'App\\Models\\User', 42, 'writers_talk', '43c8411ff1533c5af763f205e161729e502a78d8e303d7d6e23c4d39c51d71cc', '[\"*\"]', NULL, '2021-12-25 22:34:42', '2021-12-25 22:34:42'),
(557, 'App\\Models\\User', 42, 'writers_talk', '3bfc00b20435f978951aeef9ea5af09147440722cd0b1e4448b4295743257e54', '[\"*\"]', '2021-12-25 22:35:04', '2021-12-25 22:35:03', '2021-12-25 22:35:04'),
(558, 'App\\Models\\User', 42, 'writers_talk', '129d436f90cdd6bdecd3bdb726efc867a1e2a368eed8440e2069a59ba00a4a0d', '[\"*\"]', '2021-12-25 22:35:07', '2021-12-25 22:35:06', '2021-12-25 22:35:07'),
(559, 'App\\Models\\User', 43, 'writers_talk', '1b76acd45ff530185aead2bc327bd7881b44627b37d626f4f6319c2e6b08bf81', '[\"*\"]', '2021-12-27 16:42:08', '2021-12-27 16:41:24', '2021-12-27 16:42:08'),
(560, 'App\\Models\\User', 43, 'writers_talk', 'a0f2975df7d7c9f709862357908065d0b80094f2b0f55222b45dd1f83626b5bd', '[\"*\"]', '2021-12-27 20:37:38', '2021-12-27 16:42:22', '2021-12-27 20:37:38'),
(561, 'App\\Models\\User', 43, 'writers_talk', '19a5be94b0b60aea6729c7c80ae0d2532c8db52f7a4d2aa02b276e8f9fbcfa0f', '[\"*\"]', '2021-12-27 19:08:37', '2021-12-27 18:08:44', '2021-12-27 19:08:37'),
(562, 'App\\Models\\User', 43, 'writers_talk', '630e979faf5a407dd03c281dd0ecbca437d7d97842483b94cf6335447f2630d4', '[\"*\"]', '2021-12-30 15:09:59', '2021-12-27 19:09:42', '2021-12-30 15:09:59'),
(563, 'App\\Models\\User', 44, 'writers_talk', 'fbc1383086e5c2c786bc2daaf3941112042f5bec12df2385d894ac73f6ace919', '[\"*\"]', '2021-12-27 21:38:58', '2021-12-27 21:37:54', '2021-12-27 21:38:58'),
(567, 'App\\Models\\User', 47, 'writers_talk', 'abb1e6c7dedf9944abdbf132e7b5e99c3135fb3aa58378bab9e6f15530c4f6d4', '[\"*\"]', '2021-12-28 02:14:29', '2021-12-28 02:13:29', '2021-12-28 02:14:29'),
(568, 'App\\Models\\User', 47, 'writers_talk', 'c1e27c19b355c8f23b39ffd95134086b33dc745e744c1281a2fe5ca64eb59474', '[\"*\"]', '2021-12-28 02:21:34', '2021-12-28 02:20:48', '2021-12-28 02:21:34'),
(572, 'App\\Models\\User', 48, 'writers_talk', '9835e098d633e45ac02354a9dac6e347ef2be6283a43e5b4296ac7c2ee650698', '[\"*\"]', '2021-12-28 07:08:31', '2021-12-28 02:30:48', '2021-12-28 07:08:31'),
(579, 'App\\Models\\User', 48, 'writers_talk', '894d9192cc9005e6cca2ee28ab51764cc7c59e53e05fe309cc74f92c19cfd4cb', '[\"*\"]', '2021-12-28 07:17:25', '2021-12-28 05:34:29', '2021-12-28 07:17:25'),
(580, 'App\\Models\\User', 46, 'writers_talk', 'b36d44bc8c73a286c9288be972aa841738fbeac257c3f8a4865e564b621fd3b0', '[\"*\"]', '2021-12-28 06:53:20', '2021-12-28 05:54:12', '2021-12-28 06:53:20'),
(581, 'App\\Models\\User', 46, 'writers_talk', '905e51e63ca91b2e583491e5b210048b825bdf2e78e901086a15e3751e1b5757', '[\"*\"]', '2021-12-28 06:53:38', '2021-12-28 05:54:54', '2021-12-28 06:53:38'),
(583, 'App\\Models\\User', 48, 'writers_talk', '90ed6c9177a35656ca49477c71b1a25f89fc2f5c4d5bd6db38961dda7b48a282', '[\"*\"]', '2021-12-28 07:08:52', '2021-12-28 07:08:51', '2021-12-28 07:08:52'),
(584, 'App\\Models\\User', 48, 'writers_talk', '7364fc2984fd9951a8ab71598a2f49cc57a6fbe59d37623538d8d7bea4e6681f', '[\"*\"]', '2021-12-28 07:08:53', '2021-12-28 07:08:52', '2021-12-28 07:08:53'),
(585, 'App\\Models\\User', 51, 'writers_talk', '21f2de9a3df60c6cbec19009760559232a19b44dac99ac47a6ac4b57ce66f197', '[\"*\"]', '2021-12-28 23:38:09', '2021-12-28 07:20:12', '2021-12-28 23:38:09'),
(587, 'App\\Models\\User', 52, 'writers_talk', '1474828aa82b8f4f5b274554c306da6ed0e82373329ecffca2867ff94ebb763c', '[\"*\"]', '2021-12-28 22:48:22', '2021-12-28 22:47:39', '2021-12-28 22:48:22'),
(588, 'App\\Models\\User', 52, 'writers_talk', '66deb22a45f16f33375c8b170d8704f3d6e154cc485aa222b6630b4241712f9c', '[\"*\"]', '2021-12-29 16:23:10', '2021-12-28 22:49:01', '2021-12-29 16:23:10'),
(590, 'App\\Models\\User', 48, 'writers_talk', '77ad1bbf346f89dd0cddfa69ef8c7a178b36c88bb5e11bc12be9cb81569a3020', '[\"*\"]', '2021-12-29 02:31:13', '2021-12-29 01:45:15', '2021-12-29 02:31:13'),
(591, 'App\\Models\\User', 47, 'writers_talk', '20d38b111b4a00526a64898c3f2357933a347fac582f5d75e6777f5f9f53d4c8', '[\"*\"]', '2021-12-29 02:31:44', '2021-12-29 02:31:43', '2021-12-29 02:31:44'),
(593, 'App\\Models\\User', 46, 'writers_talk', '04e50b30d7ee886b6bc914ede0ed45fc10750a98ca547ce7aa70c4f41a594284', '[\"*\"]', '2021-12-29 05:06:41', '2021-12-29 05:05:17', '2021-12-29 05:06:41'),
(603, 'App\\Models\\User', 46, 'writers_talk', '3d72819cf2b8a642e8af471c1a49f9988ae8b0bffdc800eac372f082a7328f10', '[\"*\"]', '2021-12-29 05:52:04', '2021-12-29 05:23:07', '2021-12-29 05:52:04'),
(604, 'App\\Models\\User', 46, 'writers_talk', '20c92da09515a034c4de010357cd12603bea5649334d8ac7d5bb679b64f601a0', '[\"*\"]', '2021-12-29 06:00:03', '2021-12-29 06:00:03', '2021-12-29 06:00:03'),
(605, 'App\\Models\\User', 46, 'writers_talk', '4296f92bc60fe47dc086ce4e21ce64c46c06a1ff9e34121420573d6188c12de5', '[\"*\"]', '2021-12-29 06:05:23', '2021-12-29 06:00:13', '2021-12-29 06:05:23'),
(606, 'App\\Models\\User', 46, 'writers_talk', 'ff8f48c532306ae735fad9a4f27ef780dd355e3e5eb990f762a1e389ebace78a', '[\"*\"]', '2021-12-29 06:05:43', '2021-12-29 06:05:42', '2021-12-29 06:05:43'),
(607, 'App\\Models\\User', 46, 'writers_talk', 'c5aed22c26209c09f9056207fee44226a4f5068227725fb78de249182c04f4d2', '[\"*\"]', '2021-12-29 06:12:47', '2021-12-29 06:11:13', '2021-12-29 06:12:47'),
(608, 'App\\Models\\User', 46, 'writers_talk', '1db837cabfe4d23c72d56708b28416c8f190e5127571b6ac1d7722de14a2effd', '[\"*\"]', '2021-12-29 06:13:33', '2021-12-29 06:13:04', '2021-12-29 06:13:33'),
(609, 'App\\Models\\User', 46, 'writers_talk', '9379077bdefec0a37cfa906334cd0c445fbc034a6641238544dc68d4e59bc1fa', '[\"*\"]', '2021-12-29 06:13:41', '2021-12-29 06:13:40', '2021-12-29 06:13:41'),
(610, 'App\\Models\\User', 46, 'writers_talk', '096ce58154df1d462de774501506567c61e144120b6e6767081751e515a7bea0', '[\"*\"]', '2021-12-29 06:14:35', '2021-12-29 06:14:08', '2021-12-29 06:14:35'),
(611, 'App\\Models\\User', 46, 'writers_talk', '774d5707fba43b11cb7cd26c659627f5d8ab0cae492867ea7d28aa0d8509be4c', '[\"*\"]', '2021-12-29 06:14:52', '2021-12-29 06:14:51', '2021-12-29 06:14:52'),
(612, 'App\\Models\\User', 46, 'writers_talk', 'b0cf1980d8897cdcb974e8a75c39c33c0a83373d6f4081b07fa749fd60163eb9', '[\"*\"]', '2021-12-29 06:17:50', '2021-12-29 06:17:20', '2021-12-29 06:17:50'),
(613, 'App\\Models\\User', 46, 'writers_talk', '80ce1eced45ddb8632dcfaab086a35a3385abaeb46e479bc55e8b34b23b1581c', '[\"*\"]', '2021-12-29 06:18:19', '2021-12-29 06:18:19', '2021-12-29 06:18:19'),
(618, 'App\\Models\\User', 54, 'writers_talk', 'a43d2eea1272a3d822acf9d5ed286b74e8a64e9e150b2623796142a600391454', '[\"*\"]', '2021-12-29 16:25:20', '2021-12-29 16:23:28', '2021-12-29 16:25:20'),
(619, 'App\\Models\\User', 54, 'writers_talk', '227e9b6e43784eecb7ed68cababf1bce7280c484f9b78a9d3a0e96cdd288fc45', '[\"*\"]', '2021-12-29 17:11:33', '2021-12-29 16:26:14', '2021-12-29 17:11:33'),
(620, 'App\\Models\\User', 54, 'writers_talk', '0853ea8c9245388889a859a53714315a62ade8447629c10db3dfa856965fadda', '[\"*\"]', '2021-12-29 18:28:31', '2021-12-29 17:12:25', '2021-12-29 18:28:31'),
(621, 'App\\Models\\User', 54, 'writers_talk', 'f619bcbc6feaf8955a8e6682bf2efee3674f83e41e103a8b2beed99df2a92232', '[\"*\"]', NULL, '2021-12-29 18:29:28', '2021-12-29 18:29:28'),
(622, 'App\\Models\\User', 54, 'writers_talk', '84553f48000e3b927af9269534ed96fab8853007b1ccd1fb424c6722dd133e24', '[\"*\"]', '2021-12-29 19:10:06', '2021-12-29 18:29:35', '2021-12-29 19:10:06'),
(629, 'App\\Models\\User', 46, 'writers_talk', 'a015c53acb02f0dc603c6b446ae95160be3c3bfece87d6733bb988eb51f4ac53', '[\"*\"]', '2021-12-30 06:24:00', '2021-12-30 06:23:59', '2021-12-30 06:24:00'),
(630, 'App\\Models\\User', 46, 'writers_talk', '8860c9abd80852b6c80930b6596cbeabc9be5e2bdc4084bbe856703246f5f399', '[\"*\"]', '2021-12-30 06:24:18', '2021-12-30 06:24:17', '2021-12-30 06:24:18'),
(633, 'App\\Models\\User', 46, 'writers_talk', '1730873c9eb2d973a0bf1887f5af49ad928cff03a6e7f6cfa8bf5e69a5b04276', '[\"*\"]', '2021-12-30 06:37:36', '2021-12-30 06:36:50', '2021-12-30 06:37:36'),
(634, 'App\\Models\\User', 46, 'writers_talk', 'fd74904e6d8aa8f01b0e53c8a5d898fdc313d531277296ca4096edd54cde2fe9', '[\"*\"]', '2021-12-30 06:40:16', '2021-12-30 06:38:28', '2021-12-30 06:40:16'),
(637, 'App\\Models\\User', 46, 'writers_talk', '17a6685d936169af9de2588584c2a8f189ac5d86afe07ed079fac5320cdbc1ce', '[\"*\"]', '2021-12-30 06:54:35', '2021-12-30 06:54:34', '2021-12-30 06:54:35'),
(638, 'App\\Models\\User', 46, 'writers_talk', '1a05377e1ac4b4eedb55dcbaee3d048b7c2459508b3305b757b169fb80b384c7', '[\"*\"]', '2021-12-30 06:54:54', '2021-12-30 06:54:53', '2021-12-30 06:54:54'),
(644, 'App\\Models\\User', 50, 'writers_talk', '8a1a66d15026ff2e88a8bdff4dd0325b0bca8f70d5d355519b6baf80eaa7eb3c', '[\"*\"]', '2021-12-30 07:16:11', '2021-12-30 07:16:10', '2021-12-30 07:16:11'),
(645, 'App\\Models\\User', 46, 'writers_talk', 'c0c8c6bba02c4154651ebca73cbf905b1ca8eb0ebe64d84dde23132d9e8fafc8', '[\"*\"]', '2021-12-30 07:16:52', '2021-12-30 07:16:52', '2021-12-30 07:16:52'),
(646, 'App\\Models\\User', 50, 'writers_talk', '8c90ea238cd53ebdd26f797f7ac8f6a1c15d80834c38804aa272cf32c6d8258c', '[\"*\"]', '2021-12-31 07:31:34', '2021-12-30 07:17:05', '2021-12-31 07:31:34'),
(647, 'App\\Models\\User', 50, 'writers_talk', '87cd86f367a4824f55fd83a5b8a0a8bc8dbff9454cea0f3277e42b968b04d1e4', '[\"*\"]', '2021-12-31 07:40:14', '2021-12-30 07:18:00', '2021-12-31 07:40:14'),
(648, 'App\\Models\\User', 50, 'writers_talk', '04121e4de4c10fa6bba3ddc534ead161179f51390adfceab616b120fa2190f96', '[\"*\"]', '2021-12-31 13:06:04', '2021-12-30 09:43:35', '2021-12-31 13:06:04'),
(651, 'App\\Models\\User', 1, 'writers_talk', 'a5e8f3abc9daae5f9923a18d925f1bdea13673f337053a4170bf9cd8b549e9f7', '[\"*\"]', '2021-12-30 14:53:57', '2021-12-30 14:53:56', '2021-12-30 14:53:57'),
(654, 'App\\Models\\User', 56, 'writers_talk', '7c98e768ab650cf8ff09e207085b27dffee543402fe00e1b4549d20a029a2a2d', '[\"*\"]', '2021-12-30 14:57:00', '2021-12-30 14:56:59', '2021-12-30 14:57:00'),
(655, 'App\\Models\\User', 56, 'writers_talk', 'd899a26ca911044ec8f3762315b4ddb5bda49ef0b02619dc95bc0f8d7f8811bc', '[\"*\"]', '2021-12-30 15:07:51', '2021-12-30 14:57:00', '2021-12-30 15:07:51'),
(656, 'App\\Models\\User', 58, 'writers_talk', '91f96e87d0a33232a25c20eb1844c074962ae975d377a71bc38dade57ccfe1ce', '[\"*\"]', '2022-01-03 19:00:28', '2021-12-30 14:57:32', '2022-01-03 19:00:28'),
(657, 'App\\Models\\User', 56, 'writers_talk', 'c245ae446154ca77c5ea2b3b8f5ae61fc85488d609d92a6ad01558d9a0bd174d', '[\"*\"]', '2021-12-30 15:21:08', '2021-12-30 15:10:26', '2021-12-30 15:21:08'),
(658, 'App\\Models\\User', 58, 'writers_talk', '15639fa8f3f7cd23d3d8395b50b29976323c9c3fe6d93284b8b3d7ea13ef653e', '[\"*\"]', '2021-12-30 15:21:25', '2021-12-30 15:10:49', '2021-12-30 15:21:25'),
(659, 'App\\Models\\User', 58, 'writers_talk', '32fc5e2cfbc718b02b3168e38fc933b57b4db881b8ed8336aab1ccd9885a7789', '[\"*\"]', '2021-12-30 15:16:06', '2021-12-30 15:15:47', '2021-12-30 15:16:06'),
(660, 'App\\Models\\User', 56, 'writers_talk', '766d393517007e9ef78b94c31d1c433e36d588dc6a8129cbf749d632882fbe49', '[\"*\"]', '2021-12-30 15:25:01', '2021-12-30 15:24:26', '2021-12-30 15:25:01'),
(661, 'App\\Models\\User', 58, 'writers_talk', '0cec87204d2b729c3d89b21c65906adc762b462e0516834fc1e7ee22b5ef0070', '[\"*\"]', NULL, '2021-12-30 15:36:22', '2021-12-30 15:36:22'),
(662, 'App\\Models\\User', 55, 'writers_talk', '8f569080e8554b6481ddf1ff2ee3031ed241bd1c4b44fcaee4a3c46ae39bdc90', '[\"*\"]', '2021-12-30 18:46:52', '2021-12-30 18:46:51', '2021-12-30 18:46:52'),
(663, 'App\\Models\\User', 55, 'writers_talk', 'b990dd845e5ba2c52a37373c3385100718846b9a5a385eda8dd4b8a9c3f959e7', '[\"*\"]', '2022-01-01 21:25:08', '2021-12-30 18:46:51', '2022-01-01 21:25:08'),
(664, 'App\\Models\\User', 50, 'writers_talk', '097003a052499410531340e630153c3bebfaf15a99387a59030282aaf7f2d1c7', '[\"*\"]', '2021-12-31 07:32:08', '2021-12-31 07:31:58', '2021-12-31 07:32:08'),
(665, 'App\\Models\\User', 59, 'writers_talk', 'dbaf2fd63481a1a3b045e7e0e985cbdd9a5b0916752439cd96763999b103781b', '[\"*\"]', NULL, '2021-12-31 07:33:27', '2021-12-31 07:33:27'),
(666, 'App\\Models\\User', 59, 'writers_talk', '70a5a6fa0c09c8b35ead2de3b3f5634bd6e4cc6cd4e58d583522a556e1961382', '[\"*\"]', '2022-01-01 02:35:28', '2021-12-31 07:36:15', '2022-01-01 02:35:28'),
(667, 'App\\Models\\User', 59, 'writers_talk', '3c5823fc52ca38a3905b9a12bab310c60e424bac046905e8b2a3912862961be5', '[\"*\"]', NULL, '2021-12-31 07:40:38', '2021-12-31 07:40:38'),
(669, 'App\\Models\\User', 61, 'writers_talk', '3697a5ca6685bd354e18ef9c0b1c772a2a971a47634960fab398b72ea5b569ea', '[\"*\"]', '2022-01-03 21:59:58', '2021-12-31 10:18:46', '2022-01-03 21:59:58'),
(688, 'App\\Models\\User', 60, 'writers_talk', '59ec9b11a1b248b9a7f94d099aa59c93c13926e433ac10ff1ec468d6490071cc', '[\"*\"]', '2022-01-01 07:23:31', '2022-01-01 07:09:36', '2022-01-01 07:23:31'),
(689, 'App\\Models\\User', 60, 'writers_talk', '0965f3c2d19719f624b7f73c52b29a3ef9deab22e7fd6af5750af04d47a383f9', '[\"*\"]', NULL, '2022-01-01 07:14:11', '2022-01-01 07:14:11'),
(690, 'App\\Models\\User', 60, 'writers_talk', 'a030fe27d379adf47b4efb8f9aa05379551e4cfe2d301980569a631cefa0e752', '[\"*\"]', '2022-01-02 19:00:13', '2022-01-01 07:14:16', '2022-01-02 19:00:13'),
(691, 'App\\Models\\User', 62, 'writers_talk', '84deadfdf9453c58e0f1554e8d075128a9a285fedd35f708f57e1508d632de54', '[\"*\"]', '2022-01-01 17:12:21', '2022-01-01 17:10:51', '2022-01-01 17:12:21'),
(692, 'App\\Models\\User', 62, 'writers_talk', '56dd650710c745ad8722242a6bdb54db3a5ee46b28be91a81f2b6049922968b7', '[\"*\"]', '2022-01-01 17:26:16', '2022-01-01 17:12:51', '2022-01-01 17:26:16'),
(693, 'App\\Models\\User', 63, 'writers_talk', '261f16858e61358c2fb15af103aa0c1f44b37974d67a0b0924cfdd412010d4bc', '[\"*\"]', '2022-01-01 19:27:39', '2022-01-01 17:23:55', '2022-01-01 19:27:39'),
(694, 'App\\Models\\User', 63, 'writers_talk', 'bef575855c833f5ab007398580cb216881dd7cbe39c6d89842c83c7c9dfb2962', '[\"*\"]', '2022-01-01 17:29:11', '2022-01-01 17:29:08', '2022-01-01 17:29:11'),
(695, 'App\\Models\\User', 63, 'writers_talk', '79342b3ded85d42764377faf42358aba41c08f560268b8887ed60db5d24537d5', '[\"*\"]', '2022-01-01 17:34:13', '2022-01-01 17:31:28', '2022-01-01 17:34:13'),
(696, 'App\\Models\\User', 62, 'writers_talk', 'f2c2cafa0df2b2c33862eda649e5c80b7428c95ebf59a960b2ef6744e5609c35', '[\"*\"]', '2022-01-01 17:35:17', '2022-01-01 17:34:59', '2022-01-01 17:35:17'),
(697, 'App\\Models\\User', 62, 'writers_talk', 'd571ba1e8776e6676c56ea1839091e28e928cc9f6d9e9500eb03f59090c6c641', '[\"*\"]', '2022-01-01 17:35:46', '2022-01-01 17:35:45', '2022-01-01 17:35:46'),
(698, 'App\\Models\\User', 62, 'writers_talk', '4c1cb6781b9c3d69f6b2d3e43424b0aac70213a56030536a71821836c0ffb47c', '[\"*\"]', '2022-01-01 17:37:57', '2022-01-01 17:37:38', '2022-01-01 17:37:57'),
(701, 'App\\Models\\User', 63, 'writers_talk', '5223a25e9b0c247b1458ed0113aaa0fc3c1f1bb20ee8661ae33629587d9b4cbd', '[\"*\"]', NULL, '2022-01-01 18:00:23', '2022-01-01 18:00:23'),
(708, 'App\\Models\\User', 65, 'writers_talk', '9a6b4c40a31c57e557fe0ba5120cd72b3f87980d32c22b4facf4c39806e52ba1', '[\"*\"]', '2022-01-13 16:39:10', '2022-01-01 20:04:33', '2022-01-13 16:39:10'),
(709, 'App\\Models\\User', 65, 'writers_talk', 'e32b09d979a13c116075f187d6bdae5e9e34d08bed940e441e811d7cc1bc0fa9', '[\"*\"]', '2022-01-01 20:28:32', '2022-01-01 20:28:00', '2022-01-01 20:28:32'),
(711, 'App\\Models\\User', 64, 'writers_talk', '65ad4cff00adee615a024436bc4de13c8db097f76ff9bf9cc152def5d9e75292', '[\"*\"]', '2022-01-03 19:06:30', '2022-01-03 19:00:47', '2022-01-03 19:06:30'),
(712, 'App\\Models\\User', 64, 'writers_talk', '478689c9e38d7fb9318f1f134c003eb67dfb24196235acdf712a12c80dd0268f', '[\"*\"]', '2022-01-07 17:07:14', '2022-01-03 19:08:09', '2022-01-07 17:07:14'),
(713, 'App\\Models\\User', 47, 'writers_talk', 'da569ab83d44e7020f997f9f7a192de0d2035ec7439f7974dc090cde39091b5c', '[\"*\"]', '2022-01-03 22:00:22', '2022-01-03 22:00:21', '2022-01-03 22:00:22'),
(714, 'App\\Models\\User', 66, 'writers_talk', 'ae2cc72466775387e7135865a55639d04d2e0969c59b8efa00407c989fa52020', '[\"*\"]', NULL, '2022-01-03 22:01:14', '2022-01-03 22:01:14');
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(715, 'App\\Models\\User', 4, 'writers_talk', 'd4638362c3cbce9ce5c6944b6ff50a19b4e5c0e663c0013a6de4c3f7afcec578', '[\"*\"]', '2022-01-03 22:01:30', '2022-01-03 22:01:29', '2022-01-03 22:01:30'),
(717, 'App\\Models\\User', 64, 'writers_talk', 'ffad01fbbdbdae6ad0504fb1b3dd02f01a84774a5ba0d6f6109dc8e426e67ffe', '[\"*\"]', '2022-01-04 18:43:01', '2022-01-04 17:14:50', '2022-01-04 18:43:01'),
(718, 'App\\Models\\User', 56, 'writers_talk', '3101c82ec38a0ae83ee044007e1e9d3fec0a1953135a432cdfca5f13fc929896', '[\"*\"]', '2022-01-04 18:47:01', '2022-01-04 18:47:00', '2022-01-04 18:47:01'),
(719, 'App\\Models\\User', 67, 'writers_talk', '47d5684950163f46d7f95bacc63d951f1eebc6e0576500a83b6e6e0e4869fb4b', '[\"*\"]', '2022-01-04 18:54:01', '2022-01-04 18:52:28', '2022-01-04 18:54:01'),
(720, 'App\\Models\\User', 67, 'writers_talk', 'e259cbed18d7fcc94b2826b48172467cef3e6e1da0904c0ec3c6b5465f23941d', '[\"*\"]', '2022-01-04 18:56:47', '2022-01-04 18:54:34', '2022-01-04 18:56:47'),
(721, 'App\\Models\\User', 67, 'writers_talk', '8e89fe57838d65f5dba15b9ee7d6056e75919ced236771245224a3986ba2eb85', '[\"*\"]', '2022-01-04 18:57:42', '2022-01-04 18:57:08', '2022-01-04 18:57:42'),
(722, 'App\\Models\\User', 67, 'writers_talk', '222efbe8c428ab0517ae23d5d1c63cb4efe7bd0a7a352ff02a07201d9e43a590', '[\"*\"]', '2022-01-04 18:58:41', '2022-01-04 18:58:07', '2022-01-04 18:58:41'),
(723, 'App\\Models\\User', 67, 'writers_talk', '8e319e6f8dc2498f95fc5407bdcb39fee9efd106eeebabf65e0aa0f6da2a02a7', '[\"*\"]', '2022-01-04 19:10:06', '2022-01-04 18:58:55', '2022-01-04 19:10:06'),
(724, 'App\\Models\\User', 64, 'writers_talk', 'a95a9f42483f0366682233047a8e18ee9739d1f37450e4cce944483e40a1109d', '[\"*\"]', '2022-01-05 20:18:40', '2022-01-05 20:18:39', '2022-01-05 20:18:40'),
(725, 'App\\Models\\User', 10, 'writers_talk', 'b292eb95a8e03d936698cf818f2a1fc54f2164b21fcc4da3442ac02734d24c70', '[\"*\"]', '2022-01-06 15:24:33', '2022-01-06 15:24:25', '2022-01-06 15:24:33'),
(728, 'App\\Models\\User', 70, 'writers_talk', '09f63ba50364014c1c477c57df37e8c3d6daff8ae401a50281cd9fa20bae3456', '[\"*\"]', '2022-01-06 15:56:59', '2022-01-06 15:47:18', '2022-01-06 15:56:59'),
(747, 'App\\Models\\User', 71, 'writers_talk', '2670085aa2ffa0a05721e496015e985a29cb683bbb8446859be8a958e2045cac', '[\"*\"]', '2022-01-10 21:32:47', '2022-01-07 05:39:23', '2022-01-10 21:32:47'),
(761, 'App\\Models\\User', 76, 'writers_talk', 'abadba086e21392fc113f751a2be22a0325c4adca0bb636de7fe44f11b07b384', '[\"*\"]', NULL, '2022-01-08 14:45:20', '2022-01-08 14:45:20'),
(762, 'App\\Models\\User', 76, 'writers_talk', '8f9d11ee1de3be3dfe1d9b84d7db8fb83fb5f360db0b7f4334ebd6e1737df64d', '[\"*\"]', NULL, '2022-01-08 14:45:47', '2022-01-08 14:45:47'),
(763, 'App\\Models\\User', 76, 'writers_talk', '770f37a9e0c6664a0aae551252b5c406258e2bf0c5c6db35b9cbfbccbafb6beb', '[\"*\"]', NULL, '2022-01-08 14:45:51', '2022-01-08 14:45:51'),
(764, 'App\\Models\\User', 76, 'writers_talk', '757375e62ecf88238607e9d071b164b918fc87611dffa632da8ea5217dc19ed8', '[\"*\"]', NULL, '2022-01-08 14:47:31', '2022-01-08 14:47:31'),
(770, 'App\\Models\\User', 78, 'writers_talk', 'a597399498b32c4a525b46dc4a8b70c755532fbed25186a35d884f0c225a086f', '[\"*\"]', NULL, '2022-01-10 23:06:48', '2022-01-10 23:06:48'),
(773, 'App\\Models\\User', 81, 'writers_talk', '72d29c20b94a27e68f75fc558ef56373d11c5043a7ae49c96c21a23f9bbde988', '[\"*\"]', '2022-01-18 01:51:34', '2022-01-11 07:17:22', '2022-01-18 01:51:34'),
(775, 'App\\Models\\User', 83, 'writers_talk', 'ffb08ac1b7f97a45f40f2b08b9af36927e48883a19c6b421a87d20e4204ae913', '[\"*\"]', '2022-01-12 16:35:23', '2022-01-12 15:52:39', '2022-01-12 16:35:23'),
(777, 'App\\Models\\User', 84, 'writers_talk', 'ee9dfe0b53322a8a62659ae954b1a83bd3a5d0a33d43fd5acc7267ea54b5fe2b', '[\"*\"]', '2022-01-12 16:17:09', '2022-01-12 16:16:16', '2022-01-12 16:17:09'),
(778, 'App\\Models\\User', 86, 'writers_talk', 'ef9e8de20a25fa2cc9b082e7b4fb37cc1db43a9797430769782e9bde8b5711a9', '[\"*\"]', '2022-01-13 12:10:57', '2022-01-13 12:07:14', '2022-01-13 12:10:57'),
(780, 'App\\Models\\User', 9, 'writers_talk', 'fc899dd5a933e2a60f38336e4c64c36fe18d7a8ec584185183fbcea871a6bc48', '[\"*\"]', '2022-01-13 15:37:57', '2022-01-13 15:37:16', '2022-01-13 15:37:57'),
(781, 'App\\Models\\User', 9, 'writers_talk', '5d716fddf9f6e90bad36cd1aa299fd7791aafc1f122a34e63d544a902a8c1b32', '[\"*\"]', '2022-01-13 18:52:54', '2022-01-13 16:18:04', '2022-01-13 18:52:54'),
(782, 'App\\Models\\User', 79, 'writers_talk', 'b23178dffa4335d17407f1e9da380ba9a722786bf87c5ed874191dacbde682d3', '[\"*\"]', '2022-01-13 19:17:04', '2022-01-13 16:40:05', '2022-01-13 19:17:04'),
(783, 'App\\Models\\User', 82, 'writers_talk', '5ff2cb1ae31b8cc74c90454998bc0102512f708afda8897ed10845d035ff3a29', '[\"*\"]', '2022-01-13 17:28:58', '2022-01-13 16:45:47', '2022-01-13 17:28:58'),
(784, 'App\\Models\\User', 82, 'writers_talk', '8908ea3df7d09dc8a24361c3cac978ae71b24623c7159cdbb4deac23d058af84', '[\"*\"]', '2022-01-13 17:31:58', '2022-01-13 17:31:40', '2022-01-13 17:31:58'),
(785, 'App\\Models\\User', 84, 'writers_talk', 'ca41d0891de8b207c5e55c4e77bdaf52ea0ca2ae5bad644c8ef90f15c1d52c80', '[\"*\"]', '2022-01-15 18:07:56', '2022-01-14 17:44:23', '2022-01-15 18:07:56'),
(786, 'App\\Models\\User', 82, 'writers_talk', '46a37fbd26f4a1a727e47b4722ba0963c40c98b1e26a8b36aa836963a2cbe4fe', '[\"*\"]', '2022-01-17 16:40:46', '2022-01-17 16:39:47', '2022-01-17 16:40:46'),
(787, 'App\\Models\\User', 87, 'writers_talk', '423e7b75c9b0586c0bb1308a17c0a17c54f9941e16579aaad9a3dd0ffa6cba45', '[\"*\"]', '2022-01-17 16:42:00', '2022-01-17 16:41:54', '2022-01-17 16:42:00'),
(788, 'App\\Models\\User', 82, 'writers_talk', 'ebb54059371c56136860763539de3f1eb3f7c30185d435e0087ffd04a7bd99be', '[\"*\"]', '2022-01-18 17:07:28', '2022-01-17 17:08:26', '2022-01-18 17:07:28'),
(790, 'App\\Models\\User', 89, 'writers_talk', 'adf6b58503d6db564a27acc4dd2e763a8bc58f99caa5a66d1ca23c84e253fdf9', '[\"*\"]', NULL, '2022-01-18 02:03:05', '2022-01-18 02:03:05'),
(791, 'App\\Models\\User', 89, 'writers_talk', 'd9cc21a1fc2f20b62a127bba04f1ae3fd81ad284ff9722a64d2af4d04b70f5d1', '[\"*\"]', NULL, '2022-01-18 02:04:49', '2022-01-18 02:04:49'),
(792, 'App\\Models\\User', 89, 'writers_talk', '0cb05f5c2014df01daf727c2b63a9e6a1ad590b115aed64c2a80ab660e6683cc', '[\"*\"]', NULL, '2022-01-18 02:04:54', '2022-01-18 02:04:54'),
(794, 'App\\Models\\User', 92, 'writers_talk', '6364649f7da1eff0766c4e3f5c92da74acb5837e316c9b981290210271117192', '[\"*\"]', NULL, '2022-01-18 15:22:50', '2022-01-18 15:22:50'),
(795, 'App\\Models\\User', 93, 'writers_talk', 'e7b87d72412e3458408595e8a6400696ec77ab14c84629b2ec127d31f4e935da', '[\"*\"]', NULL, '2022-01-18 15:23:22', '2022-01-18 15:23:22'),
(796, 'App\\Models\\User', 93, 'writers_talk', '32e186d8f952ed75df023e9ef0cf18083d2ded5b547ac14c64aaf4309927e05a', '[\"*\"]', NULL, '2022-01-18 15:24:54', '2022-01-18 15:24:54'),
(797, 'App\\Models\\User', 93, 'writers_talk', 'acf0a8b9471e07a3afde63871a770a78a8f8392313dc570c0bfce93a04e5dbbe', '[\"*\"]', NULL, '2022-01-18 15:24:57', '2022-01-18 15:24:57'),
(798, 'App\\Models\\User', 93, 'writers_talk', '824760abda59312be61bd38d1a441c7f7ff3d3f48dc98a3069c54e15ac006dd4', '[\"*\"]', NULL, '2022-01-18 15:27:26', '2022-01-18 15:27:26'),
(799, 'App\\Models\\User', 94, 'writers_talk', '6e4754c7498e1b21bf3d4505dc9b16747b760676dad096b5f9e305a74b867190', '[\"*\"]', NULL, '2022-01-18 15:30:31', '2022-01-18 15:30:31'),
(800, 'App\\Models\\User', 93, 'writers_talk', 'f8620194aeaf53d192dd0491a600698f716ab77a6f7f17ce3d30b1c4b29d0f10', '[\"*\"]', NULL, '2022-01-18 15:42:44', '2022-01-18 15:42:44'),
(801, 'App\\Models\\User', 93, 'writers_talk', '039659c057ce4d255da7ec1ef4f92f127a6a4f78e2651e998ca658d835e41677', '[\"*\"]', NULL, '2022-01-18 15:43:31', '2022-01-18 15:43:31'),
(802, 'App\\Models\\User', 82, 'writers_talk', '1da1c1629d242461bc16d03b74cb2bb576686f1bfb18b4a81f589f6df7309131', '[\"*\"]', '2022-01-18 15:44:52', '2022-01-18 15:44:51', '2022-01-18 15:44:52'),
(803, 'App\\Models\\User', 95, 'writers_talk', 'f289377508ded0c55e616fa0bbb8f2bd07aff27370e552eabc2099ff72de4998', '[\"*\"]', '2022-01-18 17:00:11', '2022-01-18 15:55:27', '2022-01-18 17:00:11'),
(804, 'App\\Models\\User', 95, 'writers_talk', '38cc681cd7b4e0ef3f23cd62e8522eaf96d68eded85dd771c0f7123ca92a45fd', '[\"*\"]', '2022-01-18 16:17:33', '2022-01-18 16:12:48', '2022-01-18 16:17:33'),
(805, 'App\\Models\\User', 82, 'writers_talk', 'acbbec7311e31f9a79caa7ec4357c29fd5f4d867cc90fa22bda41a3cdd55c809', '[\"*\"]', '2022-01-18 17:04:48', '2022-01-18 17:02:52', '2022-01-18 17:04:48'),
(806, 'App\\Models\\User', 82, 'writers_talk', '5d6e261a5b20288770732d8db936ab5457d3fc8423c80751730f51e85822c493', '[\"*\"]', '2022-01-18 17:07:52', '2022-01-18 17:07:49', '2022-01-18 17:07:52'),
(807, 'App\\Models\\User', 104, 'writers_talk', 'e76d0c72b339dfff87d9dcc76b511c0a11eac6f8e10952a5979899311ec37376', '[\"*\"]', '2022-01-25 06:41:36', '2022-01-22 04:39:49', '2022-01-25 06:41:36'),
(808, 'App\\Models\\User', 9, 'writers_talk', '0d6b0196cce54ecfbc44d54b436d87603631ee67cc479da73b1636d1ef016669', '[\"*\"]', '2022-01-24 15:32:00', '2022-01-24 15:29:47', '2022-01-24 15:32:00'),
(809, 'App\\Models\\User', 82, 'writers_talk', 'b4a56f6ab0e1b0c9662ac520bee111ed7a957acc4f9801e51598e12fd186900a', '[\"*\"]', '2022-01-24 15:57:10', '2022-01-24 15:57:08', '2022-01-24 15:57:10'),
(810, 'App\\Models\\User', 9, 'writers_talk', '29e3f705f7ec5ff36180167d0319259e77d84ffae5e28f536236678218a44a20', '[\"*\"]', '2022-01-25 13:24:34', '2022-01-25 06:43:38', '2022-01-25 13:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspend` int(11) DEFAULT '0',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `uuid`, `user_id`, `description`, `post_type`, `suspend`, `file`, `file_type`, `created_at`, `updated_at`) VALUES
(1, '58fc7cf4-1e57-4d84-a35f-a79668289968', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'Address: Street, Apt, City, Country/Region, Province, Postal code', 'user', 0, 'uploads/images/post_38601641041753.jpeg', 'image', '2022-01-01 19:55:53', '2022-01-01 19:55:53'),
(2, '3c5647c5-9987-4efc-ab15-d3e9b180fc37', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Let have a talk together', 'user', 0, 'uploads/images/post_77531641043952.jpg', 'image', '2022-01-01 20:32:32', '2022-01-01 20:32:32'),
(3, '2cbb7ae7-53d8-406a-a32c-d4ac316f429b', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'warty', 'user', 0, NULL, NULL, '2022-01-02 06:26:41', '2022-01-02 06:26:41'),
(4, 'a2c84ee9-a9ad-4e56-a043-44462ca478a6', 'b17315f9-c372-409b-bb9b-16240925bafe', 'An amount of PKR 2000.00 has been transferred from your HBL account # 247xxxxxxx5603 to MUHAMMAD SALEEM Mobilink Microfinance Bank Ltd/ Jazzcash account # 030xxxx2947 through HBL Digital Banking on 05-01-2022 08:43:24.', 'user', 0, 'uploads/images/post_57881641459578.jpeg', 'image', '2022-01-06 15:59:38', '2022-01-06 15:59:38'),
(5, '108d5a45-1268-4715-b14b-cfab2ab5c575', 'b17315f9-c372-409b-bb9b-16240925bafe', 'An amount of PKR 2000.00 has been transferred from your HBL account # 247xxxxxxx5603 to MUHAMMAD SALEEM Mobilink Microfinance Bank Ltd/ Jazzcash account # 030xxxx2947 through HBL Digital Banking on 05-01-2022 08:43:24.', 'user', 0, 'uploads/videos/post_30941641460088.mp4', 'video', '2022-01-06 16:08:08', '2022-01-06 16:08:08'),
(6, '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Motivation', 'user', 0, 'uploads/videos/post_32461641509356.mp4', 'video', '2022-01-07 05:49:16', '2022-01-07 05:49:16'),
(7, 'ac38764d-c994-4c57-8d66-48bd8240d6d8', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'Chicago writing all day in the train', 'user', 0, 'uploads/videos/post_73371641513671.mp4', 'video', '2022-01-07 07:01:11', '2022-01-07 07:01:11'),
(8, 'acffc725-fd16-4719-9cdc-18cb69e0e0fa', 'b17315f9-c372-409b-bb9b-16240925bafe', 'mjjjjjjjj', 'user', 0, 'uploads/videos/post_83511641553561.mp4', 'video', '2022-01-07 18:06:01', '2022-01-07 18:06:01'),
(9, '8aa797d1-9fc4-4443-81e8-64bdcc1170c7', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Azaz', 'user', 0, 'uploads/images/post_23011641574902.jpeg', 'image', '2022-01-08 00:01:42', '2022-01-08 00:01:42'),
(10, '4a46fab9-65a1-4fcf-878f-6b36e9c8ede2', '5134231b-ae8c-457a-a987-e468ee55754f', 'If you write your life story what would the title be?', 'user', 0, 'uploads/images/post_25581641832929.jpeg', 'image', '2022-01-10 23:42:09', '2022-01-10 23:42:09'),
(11, 'e87fa9a4-9967-4eb7-b39c-dc43edbcde1e', '5134231b-ae8c-457a-a987-e468ee55754f', 'Delusional', 'user', 0, 'uploads/videos/post_30271641833028.mp4', 'video', '2022-01-10 23:43:48', '2022-01-10 23:43:48'),
(12, 'd0b164a7-041d-42b7-a9a8-859eda334e63', '8f675b9d-652a-4294-a170-77859236089c', 'Please leave the package by the door. Thanks!', 'user', 0, 'uploads/images/post_28411641833256.jpeg', 'image', '2022-01-10 23:47:36', '2022-01-10 23:47:36'),
(13, '132babfc-60b8-4698-a813-0637178d3676', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_30391641976226.jpeg', 'image', '2022-01-12 15:30:26', '2022-01-12 15:30:26'),
(14, 'a058ab07-5f0e-4ce9-bdb6-c798cca6bacf', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_39971641976321.jpeg', 'image', '2022-01-12 15:32:01', '2022-01-12 15:32:01'),
(15, '0ac48e0b-d291-4a5b-880f-d350327a72cd', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_54201641976377.jpeg', 'image', '2022-01-12 15:32:57', '2022-01-12 15:32:57'),
(16, '8efe5912-347f-447f-83d5-be105ec37ed8', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_91181641976408.jpeg', 'image', '2022-01-12 15:33:28', '2022-01-12 15:33:28'),
(17, '5c2258f1-10a1-4897-9bd3-3f852c199337', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_24671641977017.jpeg', 'image', '2022-01-12 15:43:37', '2022-01-12 15:43:37'),
(18, '9ec5becd-9924-4304-ad6a-7dec8908c36f', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_33321641978239.jpeg', 'image', '2022-01-12 16:03:59', '2022-01-12 16:03:59'),
(19, '105bb773-acf0-46f4-88c1-ba4f5c870c82', '8f675b9d-652a-4294-a170-77859236089c', 'Two would-be lovers from different cultures are kept apart by cultural differences that make their families interfere. What are the differences and how will they overcome them to be together?', 'user', 0, 'uploads/images/post_87991641986202.jpeg', 'image', '2022-01-12 18:16:42', '2022-01-12 18:16:42'),
(20, 'acfafa74-9028-4767-887d-65fce2423bc0', 'ddb65e98-772d-4d50-be1c-640ff9d03360', 'My first post !', 'user', 0, NULL, NULL, '2022-01-13 05:27:28', '2022-01-13 05:27:28'),
(21, '129216c9-189e-4def-85dd-6f5f0409a7eb', 'cdd69661-41da-4976-8225-be9584e55d05', 'Hello world', 'user', 0, NULL, NULL, '2022-01-21 14:13:56', '2022-01-21 14:13:56'),
(22, '08f3eeed-4491-4598-9687-36ebfeadc653', 'cdd69661-41da-4976-8225-be9584e55d05', 'I a php certificate developer', 'user', 0, 'uploads/images/post_98551642749322.jpg', 'image', '2022-01-21 14:15:22', '2022-01-21 14:15:22'),
(23, 'c39d0b8b-fdd6-438f-a8fe-5f40bd448fd1', 'cdd69661-41da-4976-8225-be9584e55d05', 'What is in my mind', 'user', 0, 'uploads/videos/post_99941642750249.mp4', 'video', '2022-01-21 14:30:49', '2022-01-21 14:30:49'),
(24, '4b3b3938-662e-4d29-9f85-068152e2082a', '1fa5a314-583e-475b-ba7e-86b7618f5004', 'azdazdazd', 'user', 0, 'uploads/images/post_48891642805252.png', 'image', '2022-01-22 05:47:32', '2022-01-22 05:47:32'),
(25, '42e07436-6789-4186-96b8-76804cb263d2', '741be022-e61e-4ba8-9e14-1a0dddea94c4', 'I’m telling the world', 'user', 0, NULL, NULL, '2022-01-22 07:48:01', '2022-01-22 07:48:01'),
(26, '1af0dadc-102e-4bd8-a090-263263785ef2', '741be022-e61e-4ba8-9e14-1a0dddea94c4', 'Yay! I finally joined! Can’t wait to see what goes on here', 'user', 0, 'uploads/images/post_83951642812524.jpeg', 'image', '2022-01-22 07:48:44', '2022-01-22 07:48:44'),
(27, 'e069e9c8-e51d-4d99-aceb-05026243ca23', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Like a boss', 'user', 0, 'uploads/videos/post_35761643068042.mp4', 'video', '2022-01-25 06:47:22', '2022-01-25 06:47:22');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL,
  `promo_code` varchar(255) NOT NULL,
  `payment_option` varchar(255) DEFAULT NULL,
  `promo_text` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `promo_code`, `payment_option`, `promo_text`, `created_at`, `updated_at`) VALUES
(13, 'WT2022', 'Free forever', NULL, '2022-01-10 22:16:36', '2022-01-10 22:16:36'),
(14, 'Adam22', '$1.99 for two months', NULL, '2022-01-14 01:52:20', '2022-01-14 01:52:20'),
(15, 'Alee22', '$1.99 for two months', NULL, '2022-01-18 01:45:33', '2022-01-18 01:45:33'),
(16, 'Alice22', '$1.99 for two months', NULL, '2022-01-18 02:00:31', '2022-01-18 02:00:31'),
(17, 'Alissa22', '$1.99 for two months', NULL, '2022-01-18 02:11:05', '2022-01-18 02:11:05'),
(18, 'Allen22', '$1.99 for two months', NULL, '2022-01-18 02:14:58', '2022-01-18 02:14:58'),
(19, 'Allison22', '$1.99 for two months', NULL, '2022-01-18 02:15:13', '2022-01-18 02:15:13'),
(20, 'AmyJ22', '$1.99 for two months', NULL, '2022-01-18 02:15:38', '2022-01-18 02:16:19'),
(21, 'Amy22', '$1.99 for two months', NULL, '2022-01-18 02:15:58', '2022-01-18 02:16:10'),
(22, 'Andrea22', '$1.99 for two months', NULL, '2022-01-18 02:16:53', '2022-01-18 02:16:53'),
(23, 'AW22', '$1.99 for two months', NULL, '2022-01-18 02:17:59', '2022-01-18 02:17:59'),
(24, 'Ane22', '$1.99 for two months', NULL, '2022-01-18 02:18:14', '2022-01-18 02:18:14'),
(25, 'Ann22', '$1.99 for two months', NULL, '2022-01-18 02:18:25', '2022-01-18 02:18:25'),
(26, 'Anna22', '$1.99 for two months', NULL, '2022-01-18 02:18:34', '2022-01-18 02:18:34'),
(27, 'Athena22', '$1.99 for two months', NULL, '2022-01-18 02:18:52', '2022-01-18 02:18:52'),
(28, 'Ben22', '$1.99 for two months', NULL, '2022-01-18 02:19:06', '2022-01-18 02:19:06'),
(29, 'Betsy22', '$1.99 for two months', NULL, '2022-01-18 02:19:21', '2022-01-18 02:19:21'),
(30, 'Bill22', '$1.99 for two months', NULL, '2022-01-18 02:19:32', '2022-01-18 02:19:32'),
(31, 'Bogdan22', '$1.99 for two months', NULL, '2022-01-18 02:19:46', '2022-01-18 02:19:46'),
(32, 'Bonita22', '$1.99 for two months', NULL, '2022-01-18 02:19:57', '2022-01-18 02:19:57'),
(33, 'Brian22', '$1.99 for two months', NULL, '2022-01-18 02:20:09', '2022-01-18 02:20:09'),
(34, 'Bryan22', '$1.99 for two months', NULL, '2022-01-18 02:20:20', '2022-01-18 02:20:20'),
(35, 'Kaitlyn22', '$1.99 for two months', NULL, '2022-01-18 02:23:20', '2022-01-18 02:23:20'),
(36, 'Chelsea22', '$1.99 for two months', NULL, '2022-01-18 02:23:31', '2022-01-18 02:23:31'),
(37, 'ChrisD22', '$1.99 for two months', NULL, '2022-01-18 02:23:55', '2022-01-18 02:23:55'),
(38, 'ChrisJ22', '$1.99 for two months', NULL, '2022-01-18 02:24:06', '2022-01-18 02:24:06'),
(39, 'Christina22', '$1.99 for two months', NULL, '2022-01-18 02:24:29', '2022-01-18 02:24:29'),
(40, 'ChristinaV22', '$1.99 for two months', NULL, '2022-01-18 02:24:44', '2022-01-18 02:24:44'),
(41, 'Chuck22', '$1.99 for two months', NULL, '2022-01-18 02:24:59', '2022-01-18 02:24:59'),
(42, 'Cindy22', '$1.99 for two months', NULL, '2022-01-18 02:25:13', '2022-01-18 02:25:13'),
(43, 'CJ22', '$1.99 for two months', NULL, '2022-01-18 02:25:26', '2022-01-18 02:25:26'),
(44, 'Claudia22', '$1.99 for two months', NULL, '2022-01-18 02:25:38', '2022-01-18 02:25:38'),
(45, 'Clinton22', '$1.99 for two months', NULL, '2022-01-18 02:25:47', '2022-01-18 02:25:47'),
(46, 'Connilyn22', '$1.99 for two months', NULL, '2022-01-18 02:26:04', '2022-01-18 02:26:04'),
(47, 'Cynthia22', '$1.99 for two months', NULL, '2022-01-18 02:26:24', '2022-01-18 02:26:24'),
(48, 'CynthiaSe22', '$1.99 for two months', NULL, '2022-01-18 02:26:47', '2022-01-18 02:26:47'),
(49, 'Dan22', '$1.99 for two months', NULL, '2022-01-18 02:26:59', '2022-01-18 02:26:59'),
(50, 'Darren22', '$1.99 for two months', NULL, '2022-01-18 02:27:08', '2022-01-18 02:27:08'),
(51, 'DavidH22', '$1.99 for two months', NULL, '2022-01-18 02:27:20', '2022-01-18 02:27:20'),
(52, 'DavidM22', '$1.99 for two months', NULL, '2022-01-18 02:27:30', '2022-01-18 02:27:30'),
(53, 'DavidN22', '$1.99 for two months', NULL, '2022-01-18 02:27:40', '2022-01-18 02:27:40'),
(54, 'Deborah22', '$1.99 for two months', NULL, '2022-01-18 02:27:53', '2022-01-18 02:27:53'),
(55, 'Del22', '$1.99 for two months', NULL, '2022-01-18 02:28:03', '2022-01-18 02:28:03'),
(56, 'Diane22', '$1.99 for two months', NULL, '2022-01-18 02:28:15', '2022-01-18 02:28:15'),
(57, 'Donald22', '$1.99 for two months', NULL, '2022-01-18 02:28:25', '2022-01-18 02:28:25'),
(58, 'Donna22', '$1.99 for two months', NULL, '2022-01-18 02:28:38', '2022-01-18 02:28:38'),
(59, 'Doug22', '$1.99 for two months', NULL, '2022-01-18 02:28:49', '2022-01-18 02:28:49'),
(60, 'Edie22', '$1.99 for two months', NULL, '2022-01-18 02:29:02', '2022-01-18 02:29:22'),
(61, 'Edwina22', '$1.99 for two months', NULL, '2022-01-18 02:29:36', '2022-01-18 02:29:36'),
(62, 'Elizabeth22', '$1.99 for two months', NULL, '2022-01-18 02:30:57', '2022-01-18 02:30:57'),
(63, 'Emerald22', '$1.99 for two months', NULL, '2022-01-18 02:31:09', '2022-01-18 02:31:09'),
(64, 'Emilie22', '$1.99 for two months', NULL, '2022-01-18 02:31:39', '2022-01-18 02:31:39'),
(65, 'Erica22', '$1.99 for two months', NULL, '2022-01-18 02:31:56', '2022-01-18 02:31:56'),
(66, 'Erin22', '$1.99 for two months', NULL, '2022-01-18 02:32:05', '2022-01-18 02:32:05'),
(67, 'Eva22', '$1.99 for two months', NULL, '2022-01-18 02:32:14', '2022-01-18 02:32:14'),
(68, 'Fiona22', '$1.99 for two months', NULL, '2022-01-18 02:32:25', '2022-01-18 02:32:25'),
(69, 'GMichael22', '$1.99 for two months', NULL, '2022-01-18 02:32:41', '2022-01-18 02:32:41'),
(70, 'Grace22', '$1.99 for two months', NULL, '2022-01-18 02:32:55', '2022-01-18 02:32:55'),
(71, 'Hannah22', '$1.99 for two months', NULL, '2022-01-18 02:33:10', '2022-01-18 02:33:10'),
(72, 'Harry22', '$1.99 for two months', NULL, '2022-01-18 02:33:30', '2022-01-18 02:33:30'),
(73, 'HeidiB22', '$1.99 for two months', NULL, '2022-01-18 02:33:45', '2022-01-18 02:33:53'),
(74, 'HeidiR22', '$1.99 for two months', NULL, '2022-01-18 02:34:08', '2022-01-18 02:34:08'),
(75, 'James22', '$1.99 for two months', NULL, '2022-01-18 02:34:40', '2022-01-18 02:34:40'),
(76, 'q0k8Be', 'Free for first year', NULL, '2022-01-21 14:09:10', '2022-01-21 14:09:10'),
(77, 'Mesu22', '$1.99 for two months', NULL, '2022-01-25 08:40:05', '2022-01-25 08:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `quicks`
--

CREATE TABLE `quicks` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `user_id` varchar(191) NOT NULL,
  `file` varchar(191) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quicks`
--

INSERT INTO `quicks` (`id`, `uuid`, `user_id`, `file`, `updated_at`, `created_at`) VALUES
(1, 'fd706bf2-08a4-4db8-b527-2f7067f2d0fa', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'uploads/videos/quick_36021641078130.mp4', '2022-01-02 06:02:10', '2022-01-02 06:02:10'),
(2, 'b713dcf7-e3eb-4a14-8967-49a11bf745c3', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/videos/quick_12431641283479.mp4', '2022-01-04 15:04:39', '2022-01-04 15:04:39'),
(3, '2f1aba2b-4be1-44e8-a579-73ea1f1bed55', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/videos/quick_57771641283885.mp4', '2022-01-04 15:11:25', '2022-01-04 15:11:25'),
(4, '2abe5ea7-79fe-41f8-a7a8-1da90ab75d35', '7c577412-cdbd-46f0-b832-5316b068b2b4', 'uploads/videos/quick_73651641297346.mp4', '2022-01-04 18:55:46', '2022-01-04 18:55:46'),
(5, '44ce9159-f003-4583-8692-625c985f690d', '7c577412-cdbd-46f0-b832-5316b068b2b4', 'uploads/videos/quick_48311641297380.mp4', '2022-01-04 18:56:20', '2022-01-04 18:56:20'),
(6, '333b7e92-be3f-4152-9feb-9b061b503596', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'uploads/videos/quick_64211641537895.mp4', '2022-01-07 13:44:55', '2022-01-07 13:44:55'),
(7, '7252b06a-16af-4309-86e1-0a2a8aa5b677', 'b17315f9-c372-409b-bb9b-16240925bafe', 'uploads/videos/quick_10321641552845.mp4', '2022-01-07 17:54:05', '2022-01-07 17:54:05'),
(8, 'c5414b2c-69a7-4a13-aa0c-33cf036723c4', 'b17315f9-c372-409b-bb9b-16240925bafe', 'uploads/videos/quick_38101641552882.mp4', '2022-01-07 17:54:42', '2022-01-07 17:54:42'),
(9, '9aa33037-6384-499b-8203-c73cc5aae65f', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/videos/quick_97881641573471.mp4', '2022-01-07 23:37:51', '2022-01-07 23:37:51'),
(10, '398f9a45-3858-416f-8214-256ac84ff5b1', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/videos/quick_93491641576600.mp4', '2022-01-08 00:30:00', '2022-01-08 00:30:00'),
(11, '95399649-79c9-4fac-b6f7-b32093a90f59', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/videos/quick_52541641592922.mov', '2022-01-08 05:02:02', '2022-01-08 05:02:02'),
(12, 'b2298ed4-d839-48c3-baa8-e460eeb47206', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'uploads/videos/quick_37281641606553.mp4', '2022-01-08 08:49:13', '2022-01-08 08:49:13'),
(13, 'f4f313b3-e592-463b-a4dd-505aaeddb8c0', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'uploads/videos/quick_93851641979094.mp4', '2022-01-12 16:18:14', '2022-01-12 16:18:14');

-- --------------------------------------------------------

--
-- Table structure for table `quicks_text`
--

CREATE TABLE `quicks_text` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `text` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quicks_text`
--

INSERT INTO `quicks_text` (`id`, `uuid`, `text`, `updated_at`, `created_at`) VALUES
(1, '6ef36ff7-eae0-4ccf-9bed-605772c581f9', 'yoyo', '2022-01-08 05:17:31', '2021-11-30 07:23:32');

-- --------------------------------------------------------

--
-- Table structure for table `referral_codes`
--

CREATE TABLE `referral_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `referral_codes`
--

INSERT INTO `referral_codes` (`id`, `sender_id`, `referral_code`, `referral_text`, `created_at`, `updated_at`) VALUES
(1, '1d80578f-8a6d-487b-8d63-15f2ca90ec41', 'yjjq5', NULL, '2021-10-14 10:45:32', '2021-10-14 10:45:32'),
(2, '1d80578f-8a6d-487b-8d63-15f2ca90ec41', NULL, NULL, '2021-10-14 10:46:05', '2021-10-14 10:46:05'),
(3, '1d80578f-8a6d-487b-8d63-15f2ca90ec41', NULL, NULL, '2021-10-14 10:47:03', '2021-10-14 10:47:03'),
(4, '1d80578f-8a6d-487b-8d63-15f2ca90ec41', NULL, NULL, '2021-10-14 10:48:58', '2021-10-14 10:48:58'),
(5, '1d80578f-8a6d-487b-8d63-15f2ca90ec41', NULL, NULL, '2021-10-14 10:51:23', '2021-10-14 10:51:23'),
(6, 'afa92057-b887-4e5f-8977-eca73e795ec0', NULL, NULL, '2021-11-15 12:48:54', '2021-11-15 12:48:54'),
(7, '15576bea-f562-41f4-b10a-932d8a910290', NULL, NULL, '2021-12-08 17:55:40', '2021-12-08 17:55:40'),
(8, '003c0093-675c-41f9-b0e9-09c1b830198b', NULL, NULL, '2021-12-28 02:47:57', '2021-12-28 02:47:57'),
(9, '003c0093-675c-41f9-b0e9-09c1b830198b', NULL, NULL, '2021-12-28 06:18:27', '2021-12-28 06:18:27'),
(10, 'cf68a928-6479-40c2-91c4-921024dc9c37', NULL, NULL, '2021-12-30 08:09:12', '2021-12-30 08:09:12'),
(11, '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', NULL, NULL, '2021-12-30 10:08:54', '2021-12-30 10:08:54'),
(12, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-07 06:09:41', '2022-01-07 06:09:41'),
(13, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-08 08:44:48', '2022-01-08 08:44:48'),
(14, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-08 08:44:48', '2022-01-08 08:44:48'),
(15, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-08 08:44:48', '2022-01-08 08:44:48'),
(16, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-08 08:44:49', '2022-01-08 08:44:49'),
(17, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', NULL, NULL, '2022-01-08 13:06:13', '2022-01-08 13:06:13'),
(18, '5134231b-ae8c-457a-a987-e468ee55754f', NULL, NULL, '2022-01-10 21:36:22', '2022-01-10 21:36:22'),
(19, '5fa5150c-3c7a-4c25-83ae-01fd8a2ce385', NULL, NULL, '2022-01-13 12:10:31', '2022-01-13 12:10:31'),
(20, '5fa5150c-3c7a-4c25-83ae-01fd8a2ce385', NULL, NULL, '2022-01-13 12:10:40', '2022-01-13 12:10:40'),
(21, '5fa5150c-3c7a-4c25-83ae-01fd8a2ce385', NULL, NULL, '2022-01-13 12:10:43', '2022-01-13 12:10:43'),
(22, 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', NULL, NULL, '2022-01-22 00:24:36', '2022-01-22 00:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_height` double DEFAULT NULL,
  `video_width` double DEFAULT NULL,
  `video_x` double DEFAULT NULL,
  `video_y` double DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `text_x` double DEFAULT NULL,
  `text_y` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `uuid`, `user_id`, `file`, `post_id`, `post_type`, `file_type`, `video_height`, `video_width`, `video_x`, `video_y`, `message`, `text_x`, `text_y`, `created_at`, `updated_at`) VALUES
(1, '55cab1db-4871-498c-a8a6-a3ea436ffa40', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'uploads/stories/post_45891641041903.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-01 19:58:23', '2022-01-01 19:58:23'),
(2, '72dd5b91-71c7-4142-8197-c38ab5f25c81', '44d4d510-7070-4857-8625-a93453c25a0e', 'uploads/stories/post_35881641042303.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-01 20:05:03', '2022-01-01 20:05:03'),
(3, '2f240403-21bf-4497-9f91-3b528bcbe40d', '506500ef-9929-4a0c-903d-93cbea7bf733', 'uploads/stories/post_14011641046208.jpg', NULL, 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-01 21:10:08', '2022-01-01 21:10:08'),
(4, 'a18113fc-5680-4563-82c9-07c3019c2dc6', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'uploads/stories/post_47311641221698.mp4', NULL, 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-03 21:54:58', '2022-01-03 21:54:58'),
(5, 'bd8e926b-1c95-4e8f-8e41-067dc32b6e8f', 'b17315f9-c372-409b-bb9b-16240925bafe', 'uploads/stories/post_83951641459649.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-06 16:00:49', '2022-01-06 16:00:49'),
(6, 'fb766e29-6b76-455e-9314-f1a9c9adfd28', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'uploads/stories/post_92821641509371.mp4', NULL, NULL, 'video', 87, 86, 107, 106, NULL, 0, 0, '2022-01-07 05:49:31', '2022-01-07 05:49:31'),
(7, '086987a2-0df8-4cb4-be6b-952b8e771c62', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'uploads/stories/post_43031641513593.mp4', NULL, 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 06:59:53', '2022-01-07 06:59:53'),
(8, 'b1cdd128-67eb-4745-8b9b-f4ddbefb0183', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'uploads/stories/post_75641641513598.mp4', NULL, 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 06:59:58', '2022-01-07 06:59:58'),
(9, '9d989dd2-7a21-409f-a50d-2e2ac9305e1e', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', NULL, '2cbb7ae7-53d8-406a-a32c-d4ac316f429b', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 07:00:06', '2022-01-07 07:00:06'),
(10, 'c898f67f-749a-497b-8114-10ca8ba20fb0', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', NULL, '2cbb7ae7-53d8-406a-a32c-d4ac316f429b', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 07:00:09', '2022-01-07 07:00:09'),
(11, '92edd104-316d-46f3-93ab-8b24541f59c5', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', NULL, '2cbb7ae7-53d8-406a-a32c-d4ac316f429b', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 07:00:10', '2022-01-07 07:00:10'),
(12, '77c492df-b631-44ad-beee-0afb2d82a339', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', NULL, 'ac38764d-c994-4c57-8d66-48bd8240d6d8', 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 07:01:35', '2022-01-07 07:01:35'),
(13, 'f08d2a0d-bb41-4366-b1a8-b00599e86ee3', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', NULL, 'ac38764d-c994-4c57-8d66-48bd8240d6d8', 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-07 07:01:38', '2022-01-07 07:01:38'),
(14, '214dc078-0e62-4f83-b194-cce93afd921e', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'uploads/stories/post_42001641621937.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-08 13:05:37', '2022-01-08 13:05:37'),
(15, 'b4277425-e7ef-4a96-9b68-cb594672c7cf', '506500ef-9929-4a0c-903d-93cbea7bf733', NULL, '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 14:14:49', '2022-01-09 14:14:49'),
(16, '5583bc3f-5942-4cea-900a-96e2c731c54b', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'uploads/stories/post_31031641976471.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 15:34:31', '2022-01-12 15:34:31'),
(17, 'c8f80d73-b85a-435c-880c-6d636badf80f', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'uploads/stories/post_10501641977109.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 15:45:09', '2022-01-12 15:45:09'),
(18, '7b4c28ef-dbdd-46c6-87d3-83233948d388', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'uploads/stories/post_79231641977755.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 15:55:55', '2022-01-12 15:55:55'),
(19, '63585a23-15f8-498c-8cbb-61428db63241', '8f675b9d-652a-4294-a170-77859236089c', 'uploads/stories/post_39551641986282.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 18:18:02', '2022-01-12 18:18:02'),
(20, '36b5998d-ca34-4f91-ad0e-163ed19fc54f', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'uploads/stories/post_92381642067173.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-13 16:46:13', '2022-01-13 16:46:13'),
(21, '05a9f947-4c34-4164-b871-29c1ee143911', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'uploads/stories/post_49951642075524.jpeg', NULL, NULL, 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-13 19:05:24', '2022-01-13 19:05:24'),
(22, '15bdab6c-172e-44f6-9035-a1eea281130b', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:16:14', '2022-01-21 14:16:14'),
(23, 'ac0d6420-9c3d-4fbb-ad2e-644e4a18b573', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:17:01', '2022-01-21 14:17:01'),
(24, 'b89168b6-b62a-4607-b456-44343b14eeb5', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:17:18', '2022-01-21 14:17:18'),
(25, 'b6fa6c65-f1cd-40e3-ab83-61a8530d191b', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:17:19', '2022-01-21 14:17:19'),
(26, 'd57afef4-bcba-4fa1-9001-1836082958a5', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, '08f3eeed-4491-4598-9687-36ebfeadc653', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:17:33', '2022-01-21 14:17:33'),
(27, 'ea40dc3e-3865-4610-a8ee-e5b8b146385f', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, 'c39d0b8b-fdd6-438f-a8fe-5f40bd448fd1', 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 14:31:02', '2022-01-21 14:31:02'),
(28, '6d967cfb-8ca5-48b1-9675-75a5f1de231d', '1fa5a314-583e-475b-ba7e-86b7618f5004', NULL, '4b3b3938-662e-4d29-9f85-068152e2082a', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-22 05:48:02', '2022-01-22 05:48:02'),
(29, '5cf6fbe4-81af-43cc-90b7-75f7493c0b38', '1fa5a314-583e-475b-ba7e-86b7618f5004', NULL, '4b3b3938-662e-4d29-9f85-068152e2082a', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-22 05:48:05', '2022-01-22 05:48:05'),
(30, 'b5f63f91-d54b-4fe0-9fe9-27ff37264d03', '1fa5a314-583e-475b-ba7e-86b7618f5004', NULL, '4b3b3938-662e-4d29-9f85-068152e2082a', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-22 05:48:06', '2022-01-22 05:48:06'),
(31, '5dc790e5-140d-43b1-bfb2-5c84e74711bf', '1fa5a314-583e-475b-ba7e-86b7618f5004', NULL, '4b3b3938-662e-4d29-9f85-068152e2082a', 'user', 'image', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-22 05:48:06', '2022-01-22 05:48:06'),
(32, 'bc41aac3-0624-4db0-b07f-0623adedd118', '506500ef-9929-4a0c-903d-93cbea7bf733', NULL, '2dc79d5c-6c6b-48cc-ac8a-42cb861d0bc2', 'user', 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-23 02:37:13', '2022-01-23 02:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `tag_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `favorite_genres` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `show_top_hundered` int(11) DEFAULT '0' COMMENT '0:no, 1:yes',
  `promo_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_used` int(11) DEFAULT '0',
  `referral_used` int(11) DEFAULT '0',
  `verify_user` int(11) NOT NULL DEFAULT '0',
  `secret_key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invitation_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `favorite_genres`, `email_verified_at`, `country_code`, `contact_no`, `status`, `bio`, `image`, `views`, `show_top_hundered`, `promo_code`, `promo_used`, `referral_used`, `verify_user`, `secret_key`, `password`, `api_token`, `invitation_key`, `device_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'u1', 'u1@gmail.com', 'Romance', NULL, NULL, '454-568-351', 'active', 'fighter', 'uploads/users/user_1636460350_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6IjJ5RklpZm5TWkJpazBFNk5Sd0VmdUE9PSIsInZhbHVlIjoiTnJybDl6dUZ4TGV1OVR4R1dTclhnZz09IiwibWFjIjoiYWM3NDZhY2NiM2NiNGM2ZWYyNDgwZjFmNGY4ZWZjNmIxOTdiNDMzYmNjODU5MmZmMTNiMDI1ZjA3YjBiODBjNyIsInRhZyI6IiJ9', '$2y$10$yax0WAef7vZl7jBh/6CSxencGUUeoc/LjiI4yOLc5WrTJrA/KyoGS', NULL, NULL, NULL, NULL, '2021-11-09 18:57:11', '2022-01-01 19:59:30'),
(2, '63370a83-3a80-483d-9d30-85a438eca10b', 'u2', 'u2@gmail.com', 'Romance', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6Im11bnQzS2Y4QS81RWdqQW1TME9Uemc9PSIsInZhbHVlIjoiL1hIU0piSTJuVU83Q2h4dWVET3Zrdz09IiwibWFjIjoiOTljZDgwMzA1MGVhOWVmMDRiMWU4MjA2ZmJiYTVjNDc0OTIzMzIwMDUyMWY2MmVkNDg3MjBkOWE0NmJkZDI5YyIsInRhZyI6IiJ9', '$2y$10$/rdv.3jFz87s5qC3Em.a2.2.dggqr9q9jNHlQKvtIquyTDaiopC9O', NULL, NULL, NULL, NULL, '2021-11-09 21:21:00', '2022-01-22 05:41:23'),
(3, 'e0e3636a-1034-44e2-b716-0caa1b5a1089', 'aliii', 'ui@gmail.com', 'Drama', NULL, NULL, '096663355896', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6Ikt3Q2ttMFVnZGdlczByRElRMDNWanc9PSIsInZhbHVlIjoibXVMaE5OMjJnbGhhdjVjM1V1Z2RvUT09IiwibWFjIjoiNzZkYzYwMzFmM2Q4ZjIxMWNmZGRhZTM5NmM5ODQ2OTIwYTc2MGRiZGJjNzlmOWU2YTVlODJkYWI3NTE0MDM0MSIsInRhZyI6IiJ9', '$2y$10$hv2yYgKlpWWIlMYWVJ2NWefLcO.bdrRrcu/QnrfnObx7tEqMJmnb6', NULL, NULL, NULL, NULL, '2021-11-09 23:39:01', '2021-11-17 02:38:39'),
(4, 'afa92057-b887-4e5f-8977-eca73e795ec0', 'Elijah', 'Qwerty@gmail.com', 'Fantasy', NULL, NULL, '2087860965', 'active', 'I write generic words so you can', 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 1, 'eyJpdiI6Iis3aStSdmdvdGJpZFJ1K1hhQ0hRdnc9PSIsInZhbHVlIjoiOC9taEp6MkNnQitpdjZxMTVMbXYyZz09IiwibWFjIjoiNWRjOGUyMTE2YWVhZDQ3NmQxZWJjYTA4OTg5MmU0MDkxNThjOGFjYTYzNzE3Nzg1MzZmNGI3MDgyYmVhNTNhMCIsInRhZyI6IiJ9', '$2y$10$/icVaRHWyAWSoghp6rJxD.xSh./DAgXPyTknYHGX1shjLaT7JAPpi', NULL, NULL, NULL, NULL, '2021-11-12 20:22:05', '2021-12-12 03:59:07'),
(5, '1b2f446a-0e0a-433b-a7f3-eccb5295a74a', 'Zeeshan', 'ziishan.mughal@gmail.com', 'Comedy', NULL, NULL, '03084555546', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IndNNld2bjl5TlE2amdPak1rcG9sUGc9PSIsInZhbHVlIjoiVmEzMFE1M2paUG8zdFhXeHFURG1Scy9EMGQ5SEkzTjVpYjUzUGtWS21COD0iLCJtYWMiOiIwMWIxZTQ2Zjg0ODRiNWFmODJlNTQxZWYzYzM2Y2Q3ODk0NjE0OTIwODFlNjI5OTcxZDQyYjg4ZDJiN2RkNDFmIiwidGFnIjoiIn0=', '$2y$10$ekBzXDvp7z18ySpqS2rbFuTX2Si3F.fHEh8LhXr/DBbQsdlh/3kxe', NULL, NULL, NULL, NULL, '2021-11-13 17:11:55', '2021-11-17 02:38:15'),
(6, '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'onism', 'u3@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'xdxxff', 'uploads/users/user_1636975722_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6ImkwWXhDazdIam9raUxMMmlNOWsvNGc9PSIsInZhbHVlIjoiU2xEQ3ZEOXJmNkJuL01lWXFwSG5KQT09IiwibWFjIjoiYzg3OWNmYzNhM2QzODIyOTc3N2Q0OWYzMmZjZDMwMzA4ZjNiYjI1OWYwZGY1OGMxYTI1M2YxOGZmYzU2ODI5YiJ9', '$2y$10$DOu3u8xiBzQIuxNnJ1u87O9/hEbUKfVYRgT52lrHP8yh2KHDycs0G', NULL, NULL, NULL, NULL, '2021-11-15 17:24:32', '2022-01-03 21:55:29'),
(7, '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'Jack', 'steadfastpictures@outlook.com', 'Drama', NULL, NULL, '2087860965', 'active', 'I write meaningless words for meaningful moments', 'uploads/users/user_1637099997_.jpeg', 2, 0, NULL, 0, 0, 1, 'eyJpdiI6ImFpSFpOMVE2RWZuSXJEV0UrRjhhNGc9PSIsInZhbHVlIjoiZ3Vrb2J6Q2xGMW5XcUN5Q1BGcEk3RThRNys0blZwNnZMemtFYzdFR3Q2WT0iLCJtYWMiOiIxZjY5MGYzYzYwNTA5YWMyOWFiZjJjZmIwOTZjNTA0ODEyYjk4NzM1ZmZlNTViYmY1NDFmYzYwNGRlZTk1YzEyIiwidGFnIjoiIn0=', '$2y$10$QoQa5sCIp8.3njfj8gS9GuLJLWZk25l8MGLSrEKMKqS3xY6VQZiG6', NULL, NULL, NULL, NULL, '2021-11-17 00:19:53', '2022-01-02 06:26:16'),
(8, 'ed5ea7e6-5bc9-4f79-8428-6996474674dc', '14', '14@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImU3YlhZOFdMSDZyWHRROGdPaVl3REE9PSIsInZhbHVlIjoiQk9aZTB5ZFNlZ2JPTzJrZGt6Q0Mzdz09IiwibWFjIjoiNjFiMzJmMTJjMWU4YzE5OWVjYWU2Yjk5NjgxN2EyYTQ3MTc0Mzc0ZWRlZGFlZjY1ZWNlYzY3YzNlNWFmNmNkMSIsInRhZyI6IiJ9', '$2y$10$djnsXY9zDoSWkoHRCzZy2uLR6DDLewvqIwCoFiVhscgGCPO42eEnC', NULL, 'inv_OfWjq', NULL, NULL, '2021-11-23 01:21:17', '2021-11-23 01:21:17'),
(9, '506500ef-9929-4a0c-903d-93cbea7bf733', 'Jason Stathem', 'testing@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'warrior', 'uploads/users/user_1637775456_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6IlQ3MkVhcDFvQlJkR0duVnBjbXNSWFE9PSIsInZhbHVlIjoiTFJEVGRFSEFwNmVTdmwvOUVnNGlqQT09IiwibWFjIjoiZmZlOTU2ZjJlZjY0ZmJiYTQ5MDFjMTZkNDQ5YTEwMWU5OGUzMzBjNTgwNjI2NTM4NDcyZTNmMjc5OGExMWQzOCIsInRhZyI6IiJ9', '$2y$10$2O0zVyWmJReVH8Av3/h6eu6lCsNA84WlHZd.fYe2IwWEKj2LnMZMO', NULL, 'inv_7yJ8Y', NULL, 'PB97AH0o2BTjY3iWh2Z5IJod0LJ8t5dJw9qTypxz4PQtkJtRfQs2076z3IQU', '2021-11-25 00:31:30', '2022-01-03 21:55:53'),
(10, '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'test user 1', '11@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'fighter', 'uploads/users/user_1637775789_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6InBMdEJ2V0NTd2krRG1nb1FmNWR6UWc9PSIsInZhbHVlIjoidVhWNGt4U2J1MUVLN21FTks0Qm9odz09IiwibWFjIjoiMzgxZTViMDJlMzc1NzU5MTZjNmZhODJhYTBkM2ZlM2JjODIxNzE5ZGI3N2UwN2Y0ZWZiNDU2MmZkNGQwNzUyNyIsInRhZyI6IiJ9', '$2y$10$Pxu8Dy3aPwZdXtY39XC8u.DmzeyA1kPxjJ1bLBx5vn9uUp66TgYmu', NULL, 'inv_zULLt', NULL, NULL, '2021-11-25 00:40:58', '2022-01-07 13:50:07'),
(11, '01e1c9b6-a656-4dd9-ae2a-17526be2ca9e', 'test user', 'onism1@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImIyRHNPU1ZpeCtLZGhnbSt1VDB3eUE9PSIsInZhbHVlIjoiTTNkdjZHMWx1QXdzcEhXcWp0UTF1dz09IiwibWFjIjoiNTczNDExMjMxNjI2MGI3ZmRkYTQ2M2M4YmIyY2ExNWUzNTA5NDViMGFkMjhjZGI0NDA1ODdhZTI4ZTg3MmJmMSIsInRhZyI6IiJ9', '$2y$10$3kvYmIuQ1/Kilyyq9WbUM.V26MYmuqR2M1Y9sWjSFfrAdt0wdauSO', NULL, 'inv_1c4mV', NULL, NULL, '2021-12-01 01:31:29', '2021-12-01 01:31:29'),
(12, '7c8e80e4-bd89-4e99-b2e2-f09232907d7a', 'test user', '1@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6Iis3aFY3RXFrSHZ2SUhyRk1qRjRhUUE9PSIsInZhbHVlIjoiUWFPRTBGb1plbWFQRW1PSGkwWjdrZz09IiwibWFjIjoiODgwYmNjZGQwNjRiN2FjM2ZhNGVkOGU2ZGU2YWNiN2IzNTc0MDFlYjhlMGNjMjZhNDNlZDgwZTExOWEyMmMxZSIsInRhZyI6IiJ9', '$2y$10$D26GOFokIhmqBNWot.v8kOz/zeww00SX.KCN4YgKQMxLT8QbP.QLm', NULL, 'inv_T1LyA', NULL, NULL, '2021-12-04 01:36:49', '2022-01-05 19:32:21'),
(13, '0b1b21e0-f70d-4237-bfbb-850976350e81', 'The Elijah', 'stead@outlook.com', 'Drama', NULL, NULL, '2087860965', 'active', 'null', 'uploads/users/user_1638596654_.jpeg', 1, 1, NULL, 0, 0, 0, 'eyJpdiI6IldqaEMyQTZpNE9JdFNpbXlickY2ZFE9PSIsInZhbHVlIjoiZUlSQmo1bHlGcXFmNnZ1Y1dLMHMvdz09IiwibWFjIjoiNTJiNGExYWFhYzMyNTNjZjA3ZGM2MjFkMWEwMTk0YzAxYWQxZWUyNGU1OGRhMGFkZDJmNzEzNzQ4ODE4ZDZjOCIsInRhZyI6IiJ9', '$2y$10$MeHyVwZMUNKYgOIU8zljDOh/9lfCvLHBG1Btr8O8b.gGvUDp7XzZK', NULL, 'inv_1jmME', NULL, NULL, '2021-12-04 12:40:37', '2021-12-12 03:59:25'),
(14, '95c25fd1-3ee7-4f50-bd92-7aee87dd3c3a', 'test', '2@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'fighter', 'uploads/users/user_1638900194_.jpeg', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IjZtZEw5RUgzNkI4WTUxcFhFWWNWR1E9PSIsInZhbHVlIjoibC9xUittZ0NmbTNOTElOajZvcmU4Zz09IiwibWFjIjoiZGY0NzFmODliMDAwNGI4MDg1MGVjYWI4NmVmMmNhYTY3Y2M4MWI0MzFiOTgwN2U5NmQyNWIzMzk0OGI4NWQ5NiJ9', '$2y$10$A1zBSn92aZ2XIFv.TV.Avuua3fQILHtnHjDt7UtvP6wDr9nDCSWU6', NULL, NULL, NULL, NULL, '2021-12-07 17:40:51', '2022-01-03 12:16:05'),
(15, 'f65f6bc3-8172-4ee1-82d8-ba3f2512c23c', 'onism', '3@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6InZpRnM4a1p3ZkFWZWJhcHNjNWl5d3c9PSIsInZhbHVlIjoiUUN0NURIS2NxZi90OU9BWEVJQS8xQT09IiwibWFjIjoiZGNjYWNjZjI3ZjY4ZmQ4NGVlNTMyZDAxYjVlY2MxOWJhZDdkOTkyZWIwMzAwMTY4MTFmNDRlY2RkZGE2NjdkNSJ9', '$2y$10$qWuz7kR78orQ/ZVZf3X39eYBE6az4gXQavb1M/BngfIyhiHampGNG', NULL, NULL, NULL, NULL, '2021-12-08 00:00:58', '2021-12-08 00:11:07'),
(16, '15576bea-f562-41f4-b10a-932d8a910290', 'kalo', 'g@gmail.com', 'Drama', NULL, NULL, '03669988526', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 1, NULL, 0, 0, 0, 'eyJpdiI6IllDUUFjc1JicW1xS01aU1RKdnlXYnc9PSIsInZhbHVlIjoiY2xOSDVFQ1g3QWU3K2dMeTBIeWlpZz09IiwibWFjIjoiZDhjNDBjZTYzZTVjMDFkMmIzMDMzMzE4ZWI5MDFmMjZlYzFiMTJiNTc1N2M5N2EwMjQyOTdhNmUzOThiOTRlZCIsInRhZyI6IiJ9', '$2y$10$lnq5bW75nUlKTZngpNYH0uMZ6pXN5Rj70pKoQJEuuDNpgPdnRY6jS', NULL, 'inv_qzoO1', NULL, NULL, '2021-12-08 16:52:55', '2021-12-08 17:53:26'),
(17, 'fcc2a2b2-eb18-43b5-9eb9-416744495bbe', 'ali', 'h@gmail.com', 'Romance', NULL, NULL, '06335599638', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Ijd3U0duMkRnNTdhNnBFUlFuaGFWQUE9PSIsInZhbHVlIjoiWEF2b1NNMFgrOG1HVW5CbFNKK28vZz09IiwibWFjIjoiZjU2OWFhMTNjYTg1MjQxNjI3OTAzYmRlODNmNmJhNWYyN2ZiZDBjNjQ1NDRmN2YzZjRkZDVjM2Q2MmFhZWRiYyIsInRhZyI6IiJ9', '$2y$10$6sXnwaMtHKNusmA9Jtwu.uc89ocRH919oebBiW.42/YO6Wbiwp4.W', NULL, 'inv_bOxfe', NULL, NULL, '2021-12-10 18:30:19', '2021-12-10 18:30:19'),
(18, '3e7b1e43-2691-44ae-ae75-e6e8912b2116', 'hi', 'test@gmail.com', 'Mystery', NULL, NULL, '32112255639', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjB0YWx0T3k3RGViakh2Qzc3NGZxTnc9PSIsInZhbHVlIjoiZjgyM2hlRHJsNkxBamN4YXRRQURYUT09IiwibWFjIjoiYjZhYmQwNjhjNDFiMjljNjJkOWZkMWRkYmQ5M2JjNTc4ZmRiNzlmZDFlZGIyZTJmM2ZlNTE3MmQ0YmM0NDgxNiIsInRhZyI6IiJ9', '$2y$10$M2akrn7DKpInkp9N/k8m2ehPX.NNsjZ9HNo54Nc9YVXe1ULYws1cy', NULL, 'inv_Y9IMd', NULL, NULL, '2021-12-10 18:52:25', '2021-12-10 18:52:25'),
(19, 'b8803fc3-0d2c-4f09-a4c7-c7c03cf49fa4', 'hhh', 'hh@gmail.com', 'Mystery', NULL, NULL, '0312456985236', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IlZNbU9DWTBWem5wR2tRN2trTmU0NlE9PSIsInZhbHVlIjoiTVIrb2c5d08vNmZqU2hiOG5VOC9oZz09IiwibWFjIjoiZmUzN2ViNjY0M2RhYzQ5NmFmY2U2NWIxNTRlM2UyOGU3OTlhYjgxNWU4MWZmMjAxNzI1NzRkNjg4MzE4ZmE5YSIsInRhZyI6IiJ9', '$2y$10$I2ufq3JxPnpHmSqmVupn3ucw/i.ypzp4pXJHrN6.HPkyZUrGwULk.', NULL, 'inv_PmxN1', NULL, NULL, '2021-12-10 18:53:54', '2022-01-09 02:24:17'),
(20, 'fa32bdd0-4c5f-491e-92ee-af49f025b379', 'onism', '4@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6Iko0TnZQT3JzSnY3enlsMVh3NWZFMEE9PSIsInZhbHVlIjoiOWlnTy9WaXBXSVgvVVFkWTg1Y0JBQT09IiwibWFjIjoiZGI0MDVkZDZhNTcwNjA0MGY2NzMxMmI4ZDRjYzdkMTA5ZTUwMDNhNmNhZTg3YTdlYzk5Y2E0MmI3MTc4YjgxZSIsInRhZyI6IiJ9', '$2y$10$DOjz9IOvCtc1iP0CWaEAE.V0OSDbkmknhLvir0QQzWDHsZwgG59C2', NULL, 'inv_Ue03Q', NULL, NULL, '2021-12-10 22:39:49', '2022-01-09 02:24:17'),
(21, 'b06f1192-a9fa-44d9-ab04-ad50a1c22b9e', 'onism', '5@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImhBa2l5WnNVUnFxVTkwQ0c4dmxLVFE9PSIsInZhbHVlIjoiZ1J5UmwrRWFDMFFvYjVDZ0I0bUVUQT09IiwibWFjIjoiNGVhMDMzYzVhYzk1MGZhN2IxOGUwMTAwNGM4YjgwNWE5NzMwNWViYjNmY2NkNTBkZDg5ZTcxMzRkNjk5NzM5MCIsInRhZyI6IiJ9', '$2y$10$/Mjl9YnkiFr/VHoOPlE6Ju1moxaKa8ZGa9bkETyH9oZONWbo9brZG', NULL, 'inv_BXIKz', NULL, NULL, '2021-12-10 22:41:11', '2021-12-10 22:41:11'),
(22, 'e6746104-e8b8-4a34-9a6c-0fd4ad18d904', 'bdhddh', 'gh@gmail.com', 'Mystery', NULL, NULL, '56595989988', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InhUazBrZGxQaGdFUjVEMnRrUnQ1R2c9PSIsInZhbHVlIjoiMEtOZDJIN1RKemUwYjdLOTQ0WjM5Zz09IiwibWFjIjoiODg2YzA2YzRkZjcxNTBkZmFkZjhhODdjN2I4ZDI3ZDgzMzc5Mjg4Nzc1MjgxNjFjZWFhMWVlOTYxMzZmNzc3MSIsInRhZyI6IiJ9', '$2y$10$Udul37WMqRyS844L2EPiger1j1tdnnO1qs9nTd31JcAWfU058UsUa', NULL, 'inv_56Sfs', NULL, NULL, '2021-12-12 00:23:40', '2021-12-12 00:23:40'),
(23, '18503b1d-7f5f-4b6a-97fa-11daf14dce7e', 'hddhdhdh', 'gh@gmail.cok', 'Romance', NULL, NULL, '868686886', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlhEbHlHQzRHQ2cwQTE5U1VCOVkrWEE9PSIsInZhbHVlIjoiMXBGTFZkNndpaVkwaXM5azNDWWtlUT09IiwibWFjIjoiYmVlZTRkZjg5MDk3ZjhkYWE1ZjMxNzM3ZDg1OWZiMjU5ZGNiZTY5MzhkZjliZTBhNGUwODMyNjA1YzRiYzllNiIsInRhZyI6IiJ9', '$2y$10$baH3CEvGt.ncmyXnyson.uYxSpnXrahQDTYkwvROa0MBnLwgWsjGa', NULL, 'inv_NeYEt', NULL, NULL, '2021-12-12 00:25:44', '2021-12-12 00:25:44'),
(24, '12f27bac-10aa-43bc-9bbc-69ad1b0b6691', 'hshsshh', 'uu@gmail.com', 'Sci-Fi', NULL, NULL, '7979799494', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlhwSmdTSWlCM0NFRkx1cnpZaGJvMEE9PSIsInZhbHVlIjoiK1BIL21qY0JnaGJBanV0Qm5Fd3BHZz09IiwibWFjIjoiZTM5MDkwZGRjODUyOTdlMDkxYWZmYWU5OTdkODY4NTkyMzljMTQyODUxYmFmNTI4NWI3NTA3NDM1Nzk5NTNhOSIsInRhZyI6IiJ9', '$2y$10$iKIieaFsodG4bMVcUcTVEec7aXRpe3NpDskfBzyeboesZ.bHRp1lK', NULL, 'inv_SRDc7', NULL, NULL, '2021-12-12 00:31:23', '2021-12-12 00:31:23'),
(25, '59eb6b50-a32d-4ebf-bee7-faadfaf6c20e', 'hshsshhxhxbhd', 'uub@gmail.com', 'Sci-Fi', NULL, NULL, '7979799494', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjVkRnhvMlJzTXZrbjJVNGFveFl0RHc9PSIsInZhbHVlIjoiTFVaSHFLcEpwVyszYnFENVhwNmg3dz09IiwibWFjIjoiZjkxMjI2ZTMxNzU0ZTJkM2E3YTJhOGYyNGMyNDZkMzVkYzgzNmZlMDU1NTBiNjA4NTk4MWVkMmNhZmNiMGZhNyIsInRhZyI6IiJ9', '$2y$10$hhBEvjDkinUaslwg7Sb13uCGx9.kS35EFH6.8E5smTwwCkWHpGDP6', NULL, 'inv_XFwc0', NULL, NULL, '2021-12-12 00:31:42', '2021-12-12 00:31:42'),
(26, 'eab7018c-fec3-453b-bdc4-6d1b6925ad4e', 'zhzhhz', 'xhhx@gmail.com', 'Horror', NULL, NULL, '86866868', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 1, 'eyJpdiI6InVwNjRXMnhVYXhQU2t4cFRjRDlRTEE9PSIsInZhbHVlIjoiS05mMnNtZzhkc1VUN2psSHZjbElIdz09IiwibWFjIjoiYzQ0YWIxZmNhMzEzZjdiYTNiZDU4ZGQyNzllM2ZhZWViNDZlYjA2MzA3MDg3YzViZDBhODgxZjkyZWUyZTlhZCIsInRhZyI6IiJ9', '$2y$10$VzCXcbTh3QngyMy029mrweCR8jyzrRn1qyMBsdqag.lh7E9snLiJa', NULL, 'inv_H1FF9', NULL, NULL, '2021-12-12 00:34:02', '2022-01-08 05:18:02'),
(27, '486f6f53-0134-40f2-8242-a03e0d77b4ee', 'teat', 'yu@gmail.com', 'Mystery', NULL, NULL, '63556699853', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjZsY2pyRVhhU3k1U0tsRzJnUHNuc3c9PSIsInZhbHVlIjoiUmFwb1E2OVkvYXBseHdGcTUxTCsxdz09IiwibWFjIjoiNmQ4ODg0ZGMzNWYxZTY5MzYyMzRmMzVjNzBjYzU5NWNiZjc2NWEwYjQ5OWQ1ZmZhYTRhMzQ2YzVkOTQ4ZjY4YiIsInRhZyI6IiJ9', '$2y$10$oXb/qnwnl./aF1XWxzLU4u57Wzn5JpLzQtHUPBo5zYPf4BC/G7ih6', NULL, 'inv_lhMfA', NULL, NULL, '2021-12-12 01:08:44', '2021-12-12 01:08:44'),
(28, 'ec230c23-5bc1-463d-9212-bbc8e6617a3b', 'testtt', 'tr@gmail.com', 'Romance', NULL, NULL, '03664455963', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjhIdXpSWWdIQ2JDMHVLRjFEdHJwMGc9PSIsInZhbHVlIjoiRVFLWThrT3VaVk8zWDR4YVdWNXpJZz09IiwibWFjIjoiZmNjMDUzNGFiMTJmMTA4OGNmODEzOGRkZDQ5NTI0YmZkZThmMTk4ZDM4NDJjNzE2YzcxZWQ0NWNlYzhmNjJiMCIsInRhZyI6IiJ9', '$2y$10$vT1OaZDdUtIqF1zoGIOVn.TRHUE/QQ3gBi0su3hTXy7iUXvWn0b32', NULL, 'inv_qEaox', NULL, NULL, '2021-12-12 01:12:46', '2021-12-12 01:12:46'),
(29, '06dfdf0c-5d68-43ce-a4f2-08afbf808ebc', 'op', 'uii@gmail.com', 'Action', NULL, NULL, '03224466985', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImllRjBWUDV2SE9Fb0Z3SGNnaDByeGc9PSIsInZhbHVlIjoiVjN0WUE4ckRzbDQxRG4wS3hXUnVFUT09IiwibWFjIjoiMjU5YzFjNzJmN2Y1ZGUxMGRjMDEyMTQ5MDA3ZWE5NzZmNTJiNGM5Zjc4ZTNlZTZiNjBkNGI1YTliODg1MDVjNiIsInRhZyI6IiJ9', '$2y$10$6sw.EUXLFBKUdv0FKb5K8uVFUF4mAJEHurlAq3K6mY5cRDf/1Cxfe', NULL, 'inv_MtvEy', NULL, NULL, '2021-12-12 01:13:56', '2021-12-12 01:13:56'),
(30, '001efd7c-adcf-41d0-9c24-0c0e4ab14439', 'Me', 'Me@outlook.com', 'Mystery', NULL, NULL, '2087860965', 'active', 'this is me', 'assets/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlR1MEhEbHBmUjc2SUJ3dHNZc0F3TVE9PSIsInZhbHVlIjoiVklnOGdYeXd1ZzBUQXVGUWVSOTM5QT09IiwibWFjIjoiOTJiNzc1MjVjZGRiODY5YTIyNzU0MDg2MjFlODA1M2MxNzk5YjY3OTJkY2M0MjA5NTc1Mjk1MGJjMmQyMWNlYSJ9', '$2y$10$zoMnP0p0nZtj.74RCVC.2ePTwRyoPqOKAU/jzewZUsuj9SyRjBeuy', NULL, NULL, NULL, NULL, '2021-12-12 03:51:51', '2021-12-12 03:58:33'),
(31, 'ab213e73-3b2c-4c4b-864c-de2151ccf6cb', 'Ehj', 'joe@me.com', NULL, NULL, '1', '12087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImpIWlBaelJ6TC9FSXdvSDRhMHl5c3c9PSIsInZhbHVlIjoiajg2NDd2ZS9XSDdhcThhR0R4aFYydXQrNDJScFpkUjNFRWJhUE0xK0Q3RT0iLCJtYWMiOiJhZGNmNjU4NDdhNGIzOTM3NmVhZjdiNGY1YmI1ZDkwMmJkNmEyYTQ1YTBlZDJkZGI0OWMxMjE2ZGIxNDQwZDAxIiwidGFnIjoiIn0=', '$2y$10$tp9dnwYo2pbrkgsNb8rm1ejrvqgleQgqhMef0jSd8pGHrNAA75DRi', NULL, NULL, NULL, NULL, '2021-12-12 04:48:33', '2021-12-12 04:48:33'),
(32, 'a3bc87e9-d036-4df1-895f-23f5a7c58219', 'onism', '6@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InRrTjRGQ0hEVXYrYTF3N2Z4TE1aYnc9PSIsInZhbHVlIjoiL21LWXRiRUI5cEdIdytWNm9WYS94QT09IiwibWFjIjoiOGFmNzQ1NWVlODJmNjE5MzNiMmFlNGM3YTgxODQzMWViMjZmNjc5ZDBkOGY3MjM1MDdiYTZiYTNjYWZlOGJiYSIsInRhZyI6IiJ9', '$2y$10$4OQYs.UawfgW.UuD1Gzzr.Shi1ld5qDnPqOro0tafP08MGj9xI0hi', NULL, 'inv_gFP99', NULL, NULL, '2021-12-14 17:44:29', '2021-12-14 17:44:29'),
(33, '351e4933-59cf-41af-a0aa-6990c1fe5a08', 'onism', '62@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjMrMnZDTVJTSWZ1UERGK29IeERodEE9PSIsInZhbHVlIjoiQUZCWWhwUUdUcFBQbm1MektIWEVSZz09IiwibWFjIjoiMTQxYzgwNzFiZTMyMzk1OGYzOWM0NmYzN2ZiN2VlYzI1ZWMyOTI2OGNmMGRlOTQ3M2NmY2IxNmFjOGMzZDc0OCIsInRhZyI6IiJ9', '$2y$10$DRqW5AZgtlINTigqoojSVO/8InCjA8dfOpQr.1w0VV9yJWIre00d.', NULL, 'inv_gC2R5', NULL, NULL, '2021-12-16 17:35:06', '2021-12-16 17:35:06'),
(34, '0e4bf93f-763f-4f76-81a5-4c988b565bd6', 'xbbxbx', 'hnnh@gmail.com', 'Horror', NULL, NULL, '89898968', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImlUUjlOWEphSUI5ZWs4R0ZyZkJQWEE9PSIsInZhbHVlIjoiVnluWnlJN1NtZGhPc2lITHZCM1ZSdz09IiwibWFjIjoiNGQwZDRlY2NlZWE3MzBhN2RmMTYxZWVjNDJkMjQyM2ZkM2M4MzMyZmExMGI3YjJhZmFkYmJiODdkZTUzNDcxNyIsInRhZyI6IiJ9', '$2y$10$O8T1szdSwr5JB8t0RmapWOk1f7barOUZR66qoM6.gkhPbeE5xSfny', NULL, 'inv_yIAAH', NULL, NULL, '2021-12-16 18:06:58', '2021-12-16 18:06:58'),
(35, 'ce377ae4-ede9-4f10-b741-0d36ff050593', 'kkk', 'oi@gmail.com', 'Horror', NULL, NULL, '963344558999', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IkdsL243eFpoOTZpdjc0cmpOYnlDSVE9PSIsInZhbHVlIjoiSDM2R0p5OHoyaEVtN2ZDWFhlSDZvQT09IiwibWFjIjoiNzY0MDBkN2VlNmUxOGY3MjM4ZTgzNjcyZTNmODQ3MWJlYzAwYjkzNjExY2E4M2I2MWQ1MGU2NTliODFkNzMwZiIsInRhZyI6IiJ9', '$2y$10$bP596HkAgD9HKBtzSJJWDuMM24dXZNIeajZFvuqPUJvUOaCIGlxeq', NULL, 'inv_C0xnq', NULL, NULL, '2021-12-16 18:09:27', '2021-12-16 18:09:27'),
(36, '9e816eb8-be52-4038-8229-2536469c33be', 'ooooo', 'hj@gmail.com', 'Romance', NULL, NULL, '09663355693', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjlrTHExK1VaUWN1M21mZGI1RkZOVEE9PSIsInZhbHVlIjoid3hkYjk4VW9BNUczSURJNEZwbUtTQT09IiwibWFjIjoiMDcyMTQ2YWJkZmEzZTk3ZDBlNzE5MjAwNjA2Nzc5OWFkNWQyNDkzZTEyODQyNmY5YTNkMjdjOGRhMzY4Yzc3NCIsInRhZyI6IiJ9', '$2y$10$AXfLigx.SnSlp/ePtuxfQe5FXVB8Pl8sU2bdSsZFhZpvknUVck/dm', NULL, 'inv_zeY6Y', NULL, NULL, '2021-12-16 18:14:22', '2021-12-16 18:14:22'),
(37, 'd3bb7079-7379-4468-91dc-4460a43ee80b', 'onism', '9@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InhPMXp2WHFsQzgyTE50cDlnNHo3Vmc9PSIsInZhbHVlIjoiektDWjdtanI2aFVDM0toTytxeHhIUT09IiwibWFjIjoiZGVlOGFlOTA4ODlmMjIyMjk3YmNkZjBhMWQ4ZGRiMjkyMmMwOWQwYzk2YjgyMWEyMDUzNTMxMDA4MjhlMzRhNiIsInRhZyI6IiJ9', '$2y$10$uwCaryp4388HhPKj3VdU0uqVsTnfoRs45WzvCvdGgYd6dCTA1XLxO', NULL, 'inv_fNZjz', NULL, NULL, '2021-12-22 16:55:37', '2021-12-22 16:55:37'),
(38, '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'Elijah', 'Ej@me.com', 'Non-Fiction', NULL, NULL, '2087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IkpDLzU5SXR0SGh4clk2YU1UWWRkWWc9PSIsInZhbHVlIjoiNkplTVlBZ2hlWnk2Y1ByZllLU3VpUT09IiwibWFjIjoiMGI5OWY5MjM0NDdmNjc1N2QxNDNhMjY0NjFkNzRmNDJjYTBhYzUwN2Y4Njc4OTk3NjU1YzYzYWYzYzcwZWQzNyIsInRhZyI6IiJ9', '$2y$10$SxJNnGDGqwUH2THDf1P3k.j.ajNpaIyh1g4K8zzDf0vo.PuY0EeRK', NULL, 'inv_XpDWK', NULL, NULL, '2021-12-23 11:33:55', '2021-12-31 10:20:53'),
(39, '43f1d3b0-9d23-41b6-9acc-8756f8e69bc2', 'onism', '19@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Ik5Pd0RaOGtoNWNoWm5hektMT3FZcFE9PSIsInZhbHVlIjoiTE9POE5PS3JUZkhta3d5WVBuc1JsUT09IiwibWFjIjoiNTUzZDY3ZTQ3N2JkMTE5NDk2YzRhZmZlYTM2N2I0YjgyMTRmNmIwN2Y5N2MwZjA3YjFiZjg5NGZmNTdkZTAyYSIsInRhZyI6IiJ9', '$2y$10$PxCrqF52p2XFVDOOAI81lO387VWdEhH8LfZJ.S2BDR4VleLBkRjba', NULL, 'inv_O4YRf', NULL, NULL, '2021-12-23 16:57:47', '2021-12-23 16:57:47'),
(40, 'caaeb23d-a83b-42d4-b913-0c87fc49fb40', 'onism', '89@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IllzNEh2UUIxa3c1Ymp2Y2g2aUZSUnc9PSIsInZhbHVlIjoiR1BqV0VGeE9JdnRGVjROQURmQ3M3UT09IiwibWFjIjoiNzRmM2Y4MzMwMzVlMmE4ZTVjZTEzYWZkNzkzYTFmYWIzNjIwMzYwYTkwZDBkMDYxYjhiMThlMmI0MzZmYWE2OSIsInRhZyI6IiJ9', '$2y$10$cH3Bh2MWLdAC2J.MztMviuxRGSwTxmPWie1WzjIu/gpRf.iNQt2D6', NULL, 'inv_HHIiL', NULL, NULL, '2021-12-23 23:51:38', '2021-12-23 23:51:38'),
(41, '2767bb62-d43d-46fa-b5f7-be8bb7c372ed', 'Me', 'B@b.com', 'Romance', NULL, NULL, '2085280528', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InVYZWRTa1kzemdheUpUUUNsUjhtT2c9PSIsInZhbHVlIjoiWGFwUXNLTlhvbllpZHA5MXErNENzdz09IiwibWFjIjoiY2Q4MWZlMzVhMDI2NGRkMmQwNGVmN2U4YWY0ZDJmZTRiNjBjNWYxNDQ3YTMyZGZhOTUyMGE0MzUyZTdkMzcwMCIsInRhZyI6IiJ9', '$2y$10$Rg5c5lqR1FGH39MVUqYsH.oR8XTo0t5hrcn9x/QXvbNIyaDe4XMBm', NULL, 'inv_9PUYA', NULL, NULL, '2021-12-25 22:34:00', '2021-12-25 22:34:00'),
(42, 'b4665684-c07a-40a4-b026-d6a82dd06791', 'Me', 'B@me.com', 'Romance', NULL, NULL, '2085280528', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6Ind0ZHZXOHA4alI4bFBYeWluZ1g1S3c9PSIsInZhbHVlIjoiWGl5ZzdXUUZxcnlRU0ZlWHZDaVpLUT09IiwibWFjIjoiYmZlYjk0MTY2YjE5OTBlMTI4YzI3NWZiNWE5ZDdkOTM4M2Q4NGIwZWFlOGM0MGY3ZDJmMWQxZjk2NjYzMGM2MiIsInRhZyI6IiJ9', '$2y$10$vEjf8UGXmOVG2CEbh8Gd0.yXSnGJjIPLSU4Y2GSm3m8X2P07oB9AS', NULL, 'inv_rxEPC', NULL, NULL, '2021-12-25 22:34:41', '2022-01-22 05:41:43'),
(43, 'be9fd82b-3840-44a9-bffb-93918c7c2157', 'onism', '12@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IitSeVZXUFAvOWhXamYvTGxMeVNRUWc9PSIsInZhbHVlIjoiYTZXdXJvRjFIVUZrWFAxZThjemtJdz09IiwibWFjIjoiNTJkZTY2ZTA5Yjc2NGIxOGZlNDc5MTc5N2U4NzVmNTgzMjUwNTlmODRlNDExNDI5ZTZiODA5MmYzMjRmNzZmMiIsInRhZyI6IiJ9', '$2y$10$yzXjLMVKbkpRJ0R9CyHLz.YqjBiiuv29EIEvI0nojj1ndACBkIXFO', NULL, 'inv_r97KI', NULL, NULL, '2021-12-27 16:41:05', '2021-12-27 16:41:05'),
(44, '6066f2b0-07d3-4c77-951b-340244253e30', 'user', 'gk@gmail.com', 'Mystery', NULL, NULL, '03445566983', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IllCVVlqNCtLKzVEcmg4b0dpS2F6NUE9PSIsInZhbHVlIjoiODllZFpoMlFrSUdQVm9iU1VSclhJdz09IiwibWFjIjoiY2FjYzAyMjFkZTUzMDE5NThkN2M3Nzg0OGMwYzI1MzM5Y2QzMjQ4MjA1NGQxOGZhNDBhODhmYzk2NTNjMGZmOSIsInRhZyI6IiJ9', '$2y$10$8g0SHH094CZ9BC78x8IQb.gvVlfJ1NHAylpaK4GC9vvrTDqgAmDpG', NULL, 'inv_zNR7n', NULL, NULL, '2021-12-27 21:37:54', '2021-12-27 21:37:54'),
(45, '508f074b-9a41-4ab8-a982-83f180f86af1', 'oii', 'uji@gmail.com', 'Mystery', NULL, NULL, '03446699856', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Imw3WlhOYWM5MXFmZUhlOC8vWTc2SkE9PSIsInZhbHVlIjoiWm84UlgxQmc4QXNqQlRSL0hnQjhvQT09IiwibWFjIjoiMjY5NWNiNDdhOTQ3YzNjZmM5NTk2ZjBlZmE2ZGE0NGYxNGYzNjcxYzliZTQ5OGU4YzBiYjIwNTVlM2QzODM0MCIsInRhZyI6IiJ9', '$2y$10$UNggZ2vwQfB9kXQcqleU0OXnmDklNZero0Uv6Il4uicIsQt1Rng3G', NULL, 'inv_iQdi9', NULL, NULL, '2021-12-27 21:55:24', '2021-12-27 21:55:24'),
(46, '003c0093-675c-41f9-b0e9-09c1b830198b', 'Kevin', 'Kevin@vangelder.tdch', 'Sci-Fi', NULL, NULL, '3609037248', 'active', 'null', 'assets/imgs/user_avatar.png', 1, 1, NULL, 0, 0, 0, 'eyJpdiI6IkFJOTB1ZHR3N2NVVWgrb01VVXVlMHc9PSIsInZhbHVlIjoiYVhCTzZEcG5tZTFlbnNzQkw3MGpRUT09IiwibWFjIjoiZTExM2VmZjcxN2U5ZjVkNzdmNGY2NjUxODY5YjczZjhlMTIzNzliOTc3OGFmYjdhMjhkYmZhNzZmNWFhNjcyNSIsInRhZyI6IiJ9', '$2y$10$CwFkKB3YjRx3HIK.Ye.j0O3fRcKZV/KUELnQOXx4.FdTdk5SZuvFS', NULL, 'inv_jDifh', NULL, NULL, '2021-12-28 01:14:08', '2021-12-28 06:18:03'),
(47, '129b33c7-73f6-4e4d-9095-51b451fbcd7a', 'Elijah h', 'elijah.steadfastpictures@gmail.com', 'Sci-Fi', NULL, NULL, '2087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjhaRXZMdElSd3lneEZ6eXY5S1VVckE9PSIsInZhbHVlIjoiYlZmamJiQ0R0cVdUTWRiVkJ0RElGUT09IiwibWFjIjoiMDc0YjMwZjA1ZmZmYTU5MWRkN2EyODU4NWMxZGE3ZmJjMjVlNGM3MWM3OWJjOTU1YjIyMjE2YThiYjM2YzM3ZiIsInRhZyI6IiJ9', '$2y$10$Ms4roJozofPvi/CWmv1JUOljkJ63lO.8e01QM2JBIBCFI5BH7uBLm', NULL, 'inv_fs7XW', NULL, NULL, '2021-12-28 02:13:29', '2021-12-28 02:13:29'),
(48, '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 'QWERTY', 'Me@me.com', 'Romance', NULL, NULL, '2087860965', 'active', 'Write what you know? Know what you write.', 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IjJLcFp0azZFUEc2TEw4Mm1MblQ2d0E9PSIsInZhbHVlIjoiMUMycE83dnRCb0xpYVVRd0dpOGpIZz09IiwibWFjIjoiOGVhMmE1MzAxZjEyZGY3OThkMzI5NzY0NzVhNjY4ZGVlNmIzMTMzMGJhMDllNzZmOGM3NzIwMzZhYjMwOWM5ZiIsInRhZyI6IiJ9', '$2y$10$Xe6Gj6qJYC0IJ5zEfh.3fu1GnV1NOAwQWx6pLJttNnQIJs3nVnlo2', NULL, 'inv_zfbcu', NULL, NULL, '2021-12-28 02:30:47', '2021-12-28 05:46:27'),
(49, '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Wanda', 'me@you.com', NULL, NULL, '1', '12087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6ImMrenFpZzRnQmw0alFrNjRJVWM2a3c9PSIsInZhbHVlIjoicEhoTiswM0xueWlTV0RaS0YreU50UT09IiwibWFjIjoiNWRmMmVlNGYwMTY5MDMwNzcyYTdkY2U4ZDQ0ZTcxZGM2MGI1ZTRmZmI2NGFlNGU2MTA5YWViMWVmZTNmYjcwZCIsInRhZyI6IiJ9', '$2y$10$PiQACFacrAATao6YE8IFQuDTPfkVshVcVxlISOR5pRFCzm.uLzxuy', NULL, NULL, NULL, NULL, '2021-12-28 05:43:22', '2022-01-08 05:05:16'),
(50, 'cf68a928-6479-40c2-91c4-921024dc9c37', 'Kevin', 'Kevin@vangelder.tech', 'Sci-Fi', NULL, NULL, '3609037248', 'active', 'null', 'uploads/users/user_1640832330_.jpeg', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6Ilg1Slp5dVBSeDYzdWU5OHhSWXM3ZkE9PSIsInZhbHVlIjoiMHpHTVFNWmRPajFsR0Z6L0lKY0Q0Zz09IiwibWFjIjoiZjIwODBjYThhYTk2NjdiODA0ZDdkN2ZjZTRhZjQ5YmIxN2VmOTBiNmIxNmE4YTQ5ZTQxYTNmMzc5YjE2M2VhOSIsInRhZyI6IiJ9', '$2y$10$Cu.LhU2.N4cxaG5WuULIfey7JRhjpjIbCryxbRk4bUlnq/6Da1Xuq', NULL, 'inv_XR39s', NULL, NULL, '2021-12-28 06:56:37', '2021-12-31 06:35:23'),
(51, '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 'Ejh', 'Plm@gmail.com', 'Action', NULL, NULL, '2087860965', 'active', 'null', 'uploads/users/user_1640709301_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6Im9SdStQR3c1RWtCelZvdTVvZ2hodVE9PSIsInZhbHVlIjoieWVFcmJXTWhMU1RCdWFOb3RjMFh6UT09IiwibWFjIjoiMTg3NjdkZDdkMmM0ZDVhOWM4MWZiZjE4MTI2ODNkMjcyNjdhM2MwOWQ3OGRjZGJkZTc2YmY2MTNkMWVmNWNlZCIsInRhZyI6IiJ9', '$2y$10$t7JnixRTRvWIuPr7RkJSne0NakqGwePbdYpKpa9hqWtG1SGWEnjRq', NULL, 'inv_mOClI', NULL, NULL, '2021-12-28 07:20:12', '2022-01-08 08:13:49'),
(52, 'e6fbf4cc-a14e-4104-b300-fdeb5d8cd54e', 'onism', '78@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImYvVDVMb2FQOHBScGtpaDBVL3dlRGc9PSIsInZhbHVlIjoiZlRLN0Zaa2NRYlFyZWJHVm5IVE1wUT09IiwibWFjIjoiZjVkNzRmZjU4NWVhZmYzODViOWNiMDMwNmE2MGVlMWM1ZmZjMDlkNDM1NjIzZTBjMTZkZDc4NjJmMDc4M2YyYyIsInRhZyI6IiJ9', '$2y$10$RP4ZvtbJbAlGAgp0n4.mPuczbqVAl2FICWsSonNRFI3upsPWkpUW2', NULL, 'inv_SZMa9', NULL, NULL, '2021-12-28 22:43:56', '2021-12-28 22:43:56'),
(53, '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 'Ej je', 'Ej@outlook.com', 'Action', NULL, NULL, '2087860965', 'active', 'null', 'uploads/users/user_1640880575_.jpeg', 2, 0, NULL, 0, 0, 0, 'eyJpdiI6ImQrcEdRLzlneUkxUFprYnVybkxOWlE9PSIsInZhbHVlIjoib1lWYXgxMG1Ca2dZZzBseUVjMWRSOGNLeWx4bWpicEN0Ti9yR25jWksvVT0iLCJtYWMiOiI4MDkzYWNiNzIxMDdjODdjMmI5MTIxZDE1OTExZmI1NWI3YTMwZTU1ZmQ0OTZlMDE0MDI0MmU5NDk4OTRmM2ZiIiwidGFnIjoiIn0=', '$2y$10$y3PHHD1M8dd/GzJyeVZhwuA5sdOUgAR3diCr21o7MQqVCMVDLQYAe', NULL, 'inv_IzDxE', NULL, NULL, '2021-12-29 02:35:27', '2022-01-07 07:10:33'),
(54, '05c2ba3e-d7b2-4ed1-af10-6b1769ed4b3c', 'onism', '33@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IjRwdXprVUdPUEJQa015UldtNWIzUkE9PSIsInZhbHVlIjoiakNsUTE5UWZsemJybWlwZTdVcDJOdz09IiwibWFjIjoiMWJlNzlhNTY5ODMyMzY5NzMwMTYxNjc3ZTUwYWU1MTAxZTVhYTliNjZjMTBiNmZiNzQ2NWFjMDNjY2VhNzc5ZCIsInRhZyI6IiJ9', '$2y$10$FZdERCFbkIXwsPSkfV1fru7FA3HE.P6uS7CLON02DiMLDJspRQvtS', NULL, 'inv_6HqM5', NULL, NULL, '2021-12-29 16:20:39', '2022-01-01 07:23:31'),
(55, 'b3aa3a02-c4e7-4d21-843e-a538d8a6ac9b', 'Ali Malik', '7@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'null', 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjNQMUtLM3ZSTWVyNVhlUktpRkF5RFE9PSIsInZhbHVlIjoialVYTHFqZUJpR0ppTHMrQis4OHdPUT09IiwibWFjIjoiMmE1MjkyZmJmNjVjZDdkMWM2YWM2YWIyZGE3ZTYxNzExOTFmNThhZmJhNDA4ZWUzMGNlM2VlYWJlMTA3NGRjMSIsInRhZyI6IiJ9', '$2y$10$yax0WAef7vZl7jBh/6CSxencGUUeoc/LjiI4yOLc5WrTJrA/KyoGS', NULL, 'inv_wJ6Yw', NULL, NULL, '2021-12-29 19:08:55', '2021-12-29 22:44:48'),
(56, '3f0e74e3-90cc-4826-b1cf-35f83f053816', 'onism', '71@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InNVU3MwdmpLNHdRWWUzWkt6anVFWVE9PSIsInZhbHVlIjoiZERQV2pKTDZzR1BWaUFJd1o1bXVndz09IiwibWFjIjoiYzRkYjhkOTExMjEyZmJlYjg2MGFlZTc1MTE1ODY2Mzc2M2U3OTg1MTk0MGY2NWQ0OTViZjNmZWE4MDE2NDllMSIsInRhZyI6IiJ9', '$2y$10$tK9vSUlf28wajYbq5KQApu3muSdqNtK8WRRDyLDwWdb3L16Zr7aKK', NULL, 'inv_ffsc8', NULL, NULL, '2021-12-30 14:50:03', '2021-12-30 14:50:03'),
(57, 'c234fb07-e63b-47ca-bc6a-ee840fd2e6df', 'onism', '87@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IkRZZGdUek5aSWRFc1kvaGUvS3BZUUE9PSIsInZhbHVlIjoibUhHSncveTkxcDEyOXFlVnZKOWRJQT09IiwibWFjIjoiOGVhYTNkODIyZDRmMzI4NzAxNzU2N2JmNWI0N2E4ODk0ZmQ4MDE3ZTIyN2U4YzEzYWZkNGNjYzc3NTc1MzkzZCIsInRhZyI6IiJ9', '$2y$10$opzDBBBOocZO6Jy6Lrwb6uwdjmTF36aOhcY1RFPhjsxDYKwr3g.Sa', NULL, 'inv_GBtzt', NULL, NULL, '2021-12-30 14:54:26', '2021-12-30 14:54:26'),
(58, 'f83da394-2076-43b7-8845-e25511f93233', 'asml', '72@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IkZWNGRLWVp3Y2dTOURRZnMwQUhKQ1E9PSIsInZhbHVlIjoiZDU2Mjh3MzNSa0ZkQXRXZlA2bzV0UT09IiwibWFjIjoiYmFhNjdlMDQxNTg2YWY3YzFmMmIzYTdjNWQ3ODFkNTY1ZDFhMjdiZDFlNDIyMDFjZWU3YWMxZmFhZGViYjI5NCIsInRhZyI6IiJ9', '$2y$10$f7kKHN31DcQUzoUINmAkPeBBMg2ks8b9W6985l/EM3nWU9HB7UgS6', NULL, 'inv_Qhtnm', NULL, NULL, '2021-12-30 14:56:47', '2021-12-30 14:56:47'),
(59, '213d5d65-a887-41eb-88f8-604e12acc266', 'Kevin', 'Kevin@1.com', 'Sci-Fi', NULL, NULL, '3609037248', 'active', 'null', 'uploads/users/user_1640914188_.jpeg', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IlI2VWw2QWJITVYrOXYvWk1zY3ZFMlE9PSIsInZhbHVlIjoieVRqZ3ArZ0thYkVMd0kzUnVQK0JiQT09IiwibWFjIjoiNjlmZTZkN2M1ODgxZjhhYmYyMzk2ZjUwNjRjMGNkYTBmYzQ0MTM0YTkwOGNiNzQ0ZDQ4M2E1OGJmZGMzNGVkNCIsInRhZyI6IiJ9', '$2y$10$YU48ENekF0dDHnTcKV573uU2wxpih.d4HgTcuixkG1l2wXlrL1LF6', NULL, 'inv_t5j4e', NULL, NULL, '2021-12-31 07:33:26', '2022-01-08 13:55:41'),
(60, 'f99a3837-d15d-4348-9fd4-12500122250f', 'Kevin', 'kevin@2.com', 'Sci-Fi', NULL, NULL, '3609037248', 'active', 'null', 'uploads/users/user_1640988128_.jpeg', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjFYRlVsSW9EVFpUam5YN3pYd0VLUmc9PSIsInZhbHVlIjoiOWgxbHFsTFZDU0tteHlId2xJOWMrZz09IiwibWFjIjoiYjgyZDJkZGUyNGMxNmI4NmZkZmFjY2IyOGZiNmVkMjU2MDQxYTkwMWZjMTBiOTBmNTU4MGEzMzNmOTkyZDFlNSIsInRhZyI6IiJ9', '$2y$10$g4OYPxlUvqxqsKrhglGLUOwQXPPCQrs/d0uBvvH8PXQAAk.f/CSwS', NULL, 'inv_Vkw8z', NULL, NULL, '2021-12-31 07:41:11', '2022-01-01 05:02:08'),
(61, 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'Mark Whalberg', 'Yes@me.com', 'Other', NULL, NULL, '2087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 1, NULL, 0, 0, 0, 'eyJpdiI6Ik1NUzRlSjVWMDF4RFIwek5TSFZ1cEE9PSIsInZhbHVlIjoidnJBVVBQRFV6OVVMT1pxYUtLdXc2dz09IiwibWFjIjoiYjU2ZGFhMjhiNWFiNjViZjhkMjQ1MzdkM2U4OTEwZmJmMGQ1MzhiODEyYWM0YTE5ZjMzNTI2MDY1MTA3OGIzMiIsInRhZyI6IiJ9', '$2y$10$9KPAVYoTznwR5Gzw1.PpTuOzZWhJ52bl/mN9vNTN71i5WCY33HLp.', NULL, 'inv_tY6Bg', NULL, NULL, '2021-12-31 10:18:45', '2022-01-06 23:40:30'),
(62, '3aaec9e8-bfb4-4b81-bbe8-5abca169e7a6', 'Asml', '31@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjNRRy9HdWNORkFtbjBLS3VRVzdCSkE9PSIsInZhbHVlIjoiYmU2OEl5UEMrbk91QXVHL0xrak5QQT09IiwibWFjIjoiNGUxODI3YTg5NWZlZDdiZDk0NDNmZDY4MjU2Nzc1MDI5ZmNmZDVmZGM4OTg0M2ZjMjdjMjFlYTMzZjk5MTUzOSIsInRhZyI6IiJ9', '$2y$10$aYndij9N/Gq63Im0rNaMjezP8PoIaE0MmfX5hv2YfVbj8ke5PFrvC', NULL, 'inv_6Z4pd', NULL, NULL, '2022-01-01 17:10:23', '2022-01-01 17:10:23'),
(63, '1db97194-3ab7-4e37-a08d-6aa9f5e5a94b', 'ONISM', '32@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InVISzlsQlh3OWczODRjQ2FvKzBDTHc9PSIsInZhbHVlIjoidjQvdnA5bXZtZ0tScmRrTjNNRGZDUT09IiwibWFjIjoiZTNlYmU3YTljNTZjYjM4MDFjNjYxNTJjNjNhN2MxNWQ0NzIyMDM0NDg0NmNiZDI4NTJlOTAzZjEzNWQ5MDhiYSIsInRhZyI6IiJ9', '$2y$10$7z7cu1iKmVCphOMxqaA6.eUmukuqXH/kO1N6q4caB9DtdhsYxJCgG', NULL, 'inv_Oxqbe', NULL, NULL, '2022-01-01 17:10:35', '2022-01-01 17:10:35'),
(64, '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'Asml', 'asml@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', '899', 'uploads/users/user_1641042128_.jpeg', 1, 1, NULL, 0, 0, 0, 'eyJpdiI6ImNWemllTHBFQk1nOVRlUEdtQndEVkE9PSIsInZhbHVlIjoiOERxMkwxUThWM1h0c2dxWkdjL083UT09IiwibWFjIjoiNmU5MjMwOWU1YTVjZWM2N2ZhZGZhYzJhNjc4ZDMwNGFiNjY1YWZhMGIxMjhkNmQxMTE0YjNiN2UwNmQzY2MzOCJ9', '$2y$10$04FGT0t9DiEoEE74Ax9H3OZETclk9hsAdtFjXcQhB1q4sVnxg5DTC', NULL, NULL, NULL, NULL, '2022-01-01 17:38:50', '2022-01-03 21:56:15'),
(65, '44d4d510-7070-4857-8625-a93453c25a0e', 'new user100', 'op@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IlhBN1lJcTEyYUVYWm82ZHpNc052QXc9PSIsInZhbHVlIjoiaWpVU3NuSDZSTVdQQ3ZnbGZPbUNLZz09IiwibWFjIjoiZDQ4YjlkYTY1ZTAzNWY4NzYyZWM4ODU0NTFjNTQ0NTQ5ZDg4ZTdkMGVkNTY2MWY0YWE0YTdlNGY0NGMxNzNjZCJ9', '$2y$10$bF7ZI59rH.GUULBl2/J6D.PA3B2gqx2ExFihWip8bo2Mv01vXsAuW', NULL, NULL, NULL, NULL, '2022-01-01 20:03:27', '2022-01-01 20:07:04'),
(66, '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 'Hijile', 'writerstalkclub@gmail.com', NULL, NULL, '1', '12087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Iit1QlBtNWRwa2NRbUFFUUtDcFhlYnc9PSIsInZhbHVlIjoiaElEZFdWR0tsbSswVms3clNOb3F1UzJ2YzBhc0NEai91eXh0cnJtTDhzTT0iLCJtYWMiOiIwYTdjNGQxZDQwNDQ0M2NjNTg5YmU0ZjNhMDQxYzgyMWJkNmM5NGEyNzZmMTdiNDk2OWFhYWViNjU4NWVkNTRmIiwidGFnIjoiIn0=', '$2y$10$dZRKf22Yhugdi1sWH5u3fekmcf13inKaFZ78R51JwA1bQj3JVH8PW', NULL, NULL, NULL, NULL, '2022-01-02 05:52:51', '2022-01-02 05:52:51'),
(67, '7c577412-cdbd-46f0-b832-5316b068b2b4', 'new user100', 'test34@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IlNyOUdlcFFDaERybUxqeHBDVXpxcXc9PSIsInZhbHVlIjoienZUWGZHNUZDOXRyaW5zN1EybUdYUT09IiwibWFjIjoiZGFmYTZkZjllYzkxM2RkYjVlMDY2YmJjNWUxMGRjZGQ2NWRmMmJlNmFiYmQ4NDA1YzQ4NzQzN2M5Mzc0ZGM1NiJ9', '$2y$10$ExI8zYqxQM0aXvBD/E32tuYNLBsy1CmiJ0PXpy0yU1sypgWwmQXYC', NULL, NULL, NULL, NULL, '2022-01-04 18:52:04', '2022-01-06 15:57:59'),
(68, '0af8cc95-9921-4b73-a245-52342b742e84', 'new user100', '456@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlNHV2U3azB3WTd5cEd4VW9jQ2NzN1E9PSIsInZhbHVlIjoiK0VkenhONHNWUGpWQlA0MzBleTh1dz09IiwibWFjIjoiYmY3MTkwY2E2ZWRjODZmODdmMzk1NmNlNjBiMGEyMWU3MDBmMDkyNmQzNTVmY2E5NDhkMWQwNzk5YWUyMThkNyIsInRhZyI6IiJ9', '$2y$10$Xkj3zFRBSWhMspPxoBvmJeZH4sZvlY9bwrMq4q0xbTqvjj5ZwKt5u', NULL, 'inv_1cfMI', NULL, NULL, '2022-01-06 15:44:14', '2022-01-06 15:44:14'),
(69, 'b17315f9-c372-409b-bb9b-16240925bafe', 'new user100', '56@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IjVsai9IWmZJb0M1a0FMZjl4eWlKeWc9PSIsInZhbHVlIjoiUVNaVmI4ZWxYUmVXaXFwTjhkU3pJQT09IiwibWFjIjoiYzc5NWQ1YTE5ZGUwNzQ2YjRkZDllODBiMzIzNGM5N2I0NDE1ZGQxMDc5NDFmNDY4MmViZmI4OGUwYjk1ZjI2MCIsInRhZyI6IiJ9', '$2y$10$aaCNX1ZEn.Wi5d0IKs0Rz.svEbR0pHqRuPMEPiDBtF3KCERGjHWQ.', NULL, 'inv_g2vY1', NULL, NULL, '2022-01-06 15:44:47', '2022-01-25 14:29:41'),
(70, '71732563-d868-48e3-ae21-4b11e525ade4', 'test again', 'test789@gmail.com', 'Horror', NULL, NULL, '03669988563', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IklxeGNZR2JZTUpJRDk4dWJ5ZU1DN2c9PSIsInZhbHVlIjoiaUhhSXdXRzZPREVzVE5Gd0FhSDBPZz09IiwibWFjIjoiNDFlYWQ0NjMzYTQ1NDJmOWIwYmRkMmNiMjEyMjUxYzkxNTMwYjUyNzIxYWUxMTU0NTNkMWY3YzYyODI0NTM4NSIsInRhZyI6IiJ9', '$2y$10$c6ZV4vGbTf0pQGDujzUaDeEnaRgGVFFk./IGBPxVS4SgmP74mHteO', NULL, 'inv_hGKHO', NULL, NULL, '2022-01-06 15:47:18', '2022-01-06 15:47:18'),
(71, '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Elijah H', 'Q@me.com', 'Mystery', NULL, NULL, '2087860965', 'active', 'null', 'uploads/users/user_1641509023_.jpeg', 1, 0, NULL, 0, 0, 1, 'eyJpdiI6Ikh4NlJVMzczTTlYMk5KN1B5N05jR2c9PSIsInZhbHVlIjoiUmhCWVFJN3ZpVG1zRDFVdEllaVFrUVVIdDFONVlmcUJIQjdnUTAwdFN3az0iLCJtYWMiOiIwYjgxN2E0OGZjYzdlMWUxM2NjYTExZTI2MzhlZDY0Nzg5OWIwM2FhZDhiZjJlNGIxZDEzZDg5ZjlmNWViNmYxIiwidGFnIjoiIn0=', '$2y$10$5vxr0BCt5C8p6hnauMo3Z.roKb2HMN1UM.Id4EPQLiZdMEnxgS2VK', NULL, 'inv_X20tK', NULL, NULL, '2022-01-07 05:39:22', '2022-01-08 03:12:09'),
(72, 'a00a83f9-bfa2-465e-a5e9-44b702f224a3', 'new user100', '10201@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InVDVWpDZUhKNHMzdlVpNGVhcExibXc9PSIsInZhbHVlIjoiY2lEYUQzbmwwR29XZ0pVeTJKZXdwQT09IiwibWFjIjoiMzA2OGRiNDEyNzc5YzQ3NmM2NjUwMWQ4OGY5NTM2MTJlZTcyNzAxNDFmOGU1MTc4ZWRkYzA0NjU2MmU4ZDg3OSIsInRhZyI6IiJ9', '$2y$10$.dNkIAtKIHbBMj/7TgYzfuQpakDBpj4U72bc.0v1v8QJOMNUY7NG6', NULL, 'inv_GBgDw', NULL, NULL, '2022-01-07 22:53:16', '2022-01-07 22:53:16'),
(73, 'babeaefa-10db-41c8-b8ca-474d2d8bbd71', 'test onism', 'onsimtest@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IkNlUnVoODFIdkNFRkIzeXg2V3BUaWc9PSIsInZhbHVlIjoiWFFRZ29rb3lUY05wQTdCSTlSTkNtdz09IiwibWFjIjoiYWY2OWYyMDRjYWYxZmU0OGNhOTBiNGU2Yjk5Yzc2ZTQ0NmQ5ZDNiNzc1MzUyYWQzOGM5MjdkZDNmMzg4OWQzOSIsInRhZyI6IiJ9', '$2y$10$mKWyU/LMhyvXsJmJ0JybUepWYBgLe1J4wwNnEQIre7oDicrdYsIA2', NULL, 'inv_2yEfN', NULL, NULL, '2022-01-07 23:26:36', '2022-01-07 23:26:36'),
(74, '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 'okkk', 'uiop@gmail.com', 'Mystery', NULL, NULL, '03556699856', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InZRQmpxUy9aVGVQaG9CVy9QS3BsQmc9PSIsInZhbHVlIjoiWjlHelFMTGxtYXZvYkpBVDJIZnZQZz09IiwibWFjIjoiMjRhYjMwNDMzYzcwN2ZmYWIxYjdiNjZiOWUwMzRjN2FkMWMwNjAxNjgxZmU5NThlZDgyYmI1NWZhN2I3YjFhMSIsInRhZyI6IiJ9', '$2y$10$.aAURPhveHIV7Xa/.jOih.8e3AZ8DabR8riQn6fihoxa06WZZ9ijO', NULL, 'inv_xFHNp', NULL, NULL, '2022-01-07 23:30:48', '2022-01-07 23:30:48'),
(75, '77ef87e7-a42f-4bab-b15c-e468a16cc706', 'Yashwant patidar', 'yashwantp48@gmail.com', NULL, NULL, '91', '9109993751858', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlUxWHhHaUtnY2VRWDBKY1d3ZWJqSHc9PSIsInZhbHVlIjoiU1BsbnlkbjcrQUVvdTU4aVB3aUR0eFVJdDdPMU1Ibi9xK0swNnZKZkgxQT0iLCJtYWMiOiJmN2Q1MWY3YjgyNDk2ZGE5NTQ4M2JmNGI3MjQ5NWQwMzhlNjUyNGQxMDRmOWMwODQ1YjExZTlkNWFjMzVhZmM0IiwidGFnIjoiIn0=', '$2y$10$oV0Q1UTeJZx6nTIyTBXp1et1AqolJG7CgybKHkzev5f0xes1B8N/K', NULL, NULL, NULL, NULL, '2022-01-08 13:53:50', '2022-01-08 13:53:50'),
(76, 'c2af7023-e62f-4641-8e1a-461cc70cb262', 'shyam', 'shyam.digittrix@gmail.com', NULL, NULL, '91', '917976145039', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IlJuZDFsRE5VYkdrK2RCc0NKMXBTTXc9PSIsInZhbHVlIjoiVVdaWm1TS1g4YjY5ZC9JSEcwQWthVTkzWFF6TUNFdkp6Q1pKSk8rVDBOaz0iLCJtYWMiOiI4Nzg3ZDM3MWViMWZhNzRlZDc0YmE0MTllOTk3N2YxNTc3YThjMzAwMDZiMWY4MjhjZjM0MzJiYzg0NDQ1YTRlIiwidGFnIjoiIn0=', '$2y$10$NGklAA6muuflvYJG/c0mDuulAdmGnAaDirRCbMbde5MYnWCVRbWu2', NULL, NULL, NULL, 'goxwxvfGgLKismPBc1noDwP6ulHXOdIjVC7m6cwBMBpVjgJ8WPjckDBNX7Yj', '2022-01-08 14:34:08', '2022-01-10 20:49:21'),
(77, '5134231b-ae8c-457a-a987-e468ee55754f', 'Pog', 'Qw@me.com', 'Historical Fiction', NULL, NULL, '2087860965', 'active', 'Pogs are dogs with peppers', 'uploads/users/user_1641825448_.jpeg', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Ild4aUs4VytiU1R5Z2hzN0NjUmVZUVE9PSIsInZhbHVlIjoiUzVVU045U1NheWxQNm5zSkZrS2FjUkpHSmpyT25sS25mNGdRNmZVbWliYz0iLCJtYWMiOiJjNTYzZjJkY2JmNjk5MDJhN2JkMjA0NmNhYzI3YWRkNDAyNWQ2YWQ2ZDUxZWEzMjJkNTQ0MjU2NjYzMTlmNGVmIiwidGFnIjoiIn0=', '$2y$10$G6B6/iuTWCVCAg3nQXlPDOOaObRf1vc/xUBrS3Gg3OBp5Bgw6t/nO', NULL, 'inv_ZjckJ', NULL, NULL, '2022-01-10 21:34:01', '2022-01-10 21:39:48'),
(78, 'f88bccc2-8218-4e01-b7e9-ece986466870', 'hdhdhdh', 'yuih@gmail.com', 'Horror', NULL, NULL, '56345698563', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IktOU0RUMGtpcEN2TU5VOUkwNG1NRlE9PSIsInZhbHVlIjoidjdYY2VwZmloRWU2RFlMcFZ5bm8vUT09IiwibWFjIjoiMTJhZmQzMDc1NjQ0ODNiYzg3OWMyMTVmNTA3YTM0MGMzYmIzN2RmZGMzMTA5Y2YyN2Q5MzIwN2U4ZTg5MjU1ZiIsInRhZyI6IiJ9', '$2y$10$e5NyZdMUt/mth8dd3ofy7.5nItScD7eHBOW1n2TuIMq0gqfPHBoEK', NULL, 'inv_bvhJd', NULL, NULL, '2022-01-10 23:06:48', '2022-01-10 23:06:48'),
(79, '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'Denzel Wshington', 'yukih@gmail.com', 'Horror', NULL, NULL, '56345698563', 'active', 'writer', 'uploads/users/user_1642076174_.jpeg', 1, 1, NULL, 0, 0, 0, 'eyJpdiI6IlZoUzFZMGpmTDN5Z2pjcDQzczBaM1E9PSIsInZhbHVlIjoibE1tdk84NEFMdTRRZmJINksxQTYvdz09IiwibWFjIjoiYzQ3MGM1MDY3ZmM5ZmQ5YmRlOGExNjg0MmZkOWQxYTVkMGIzOTBkMWQ5YjdjMThlYzlmNzRjZTU4ZWUyYjg0NiIsInRhZyI6IiJ9', '$2y$10$PcmMHtvcWkvtIIlTnhQTvO4Ebw47RY7PAIlzqDkTVpFWKKbpV02bq', NULL, 'inv_0VZoO', NULL, NULL, '2022-01-10 23:07:02', '2022-01-13 19:16:14'),
(80, '8f675b9d-652a-4294-a170-77859236089c', 'uiiooooo', 'uhj@gmail.com', 'Horror', NULL, NULL, '03224499638', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlNCc3pNVGdHb0o3eG5Cc0xucWpFRnc9PSIsInZhbHVlIjoiWU96My83SjR1am1aa1Q5bG5NaGF5dz09IiwibWFjIjoiOWVmMGMxNWMyNTkzMmZhNWJhMzMyM2RiM2YxNWE0NjczNmY5YmUwNWFiZTg1MmRmNzNhMjQ0MzliOTBlMTdhNyIsInRhZyI6IiJ9', '$2y$10$HbZfE7o4lnRDitUEWb8YK.SRE4mtK9Vm.Xdb7NI1DPAFyamli0lfy', NULL, 'inv_CImvO', NULL, NULL, '2022-01-10 23:14:50', '2022-01-10 23:14:50'),
(81, '1cf15667-4bf7-405a-8ed5-b98b632608c6', 'Carson Holston', 'Ch@me.com', 'Comedy', NULL, NULL, '2087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'WT2022', 1, 0, 0, 'eyJpdiI6Im5jMThKSnl5cXJHTjZ6WEVNeXJkUVE9PSIsInZhbHVlIjoiWEtZM1piNnVKZlE2MG85aTA1ZkF5ZUsra0Y4dGxSOEJPQjZqR3o4Z1NRVT0iLCJtYWMiOiI0ZjgzOWE5NmEzYmIzYzkyODYwZTI5ZjZiZjcwNzc2MTE3NTg0MzdkNGVhOWYzYzkwNzc0NjdjYTJkZDg4ZGQzIiwidGFnIjoiIn0=', '$2y$10$B9DU87Gty5dmIFsRB3NUU.1/vcXhQ.r48sIOuDrLkrEwt81K/6nge', NULL, 'inv_28Cwh', NULL, NULL, '2022-01-11 07:17:21', '2022-01-11 07:17:21'),
(82, '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Nelson Mandela', 'nelson@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'null', 'uploads/users/user_1641976987_.jpeg', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IkJCbTJjb2RKNTM5ME5UWjNOaEJnZkE9PSIsInZhbHVlIjoiaXVyQXZiUnRuOS9rc3hUbk1SOUFpUT09IiwibWFjIjoiY2NjZjBhMzI2MTI3MDU2YTk2OWU2ZDE3MDQ4ZGJjZDg0OTQzOGNkODBkMDNkYWY2ODNlMTFjZTFkZTk2ODgyNyIsInRhZyI6IiJ9', '$2y$10$KaOpVFpSHR00mn4iNJWzCO6ZTdCENZB7q49jjBzq2m.Ug3.k2CIr.', NULL, 'inv_yk6sE', NULL, NULL, '2022-01-12 15:41:47', '2022-01-12 15:53:36'),
(83, '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Denzel', 'Abra@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', 'horror', 'uploads/users/user_1641977853_.jpeg', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlVxclFKR096RFdxd0phNG40OUw5ZFE9PSIsInZhbHVlIjoiSUdnNzZZZ2lKUEJoQW84M1k2NFcwUT09IiwibWFjIjoiZTc1NjE3YzIwYTk0YTM5NDkyMWNjMmJmMGY2ZTM0ZjFhOWE1ODE2ZmMwMTdiMzJmOWQ4N2YxYzRmNjEyZTAyYiIsInRhZyI6IiJ9', '$2y$10$bsMcmy5YlnsMPv6K.sZ7sOZVt7v9jhjXq6k4b4VVmPo4TpDuPzYWa', NULL, 'inv_nRUoo', NULL, NULL, '2022-01-12 15:51:40', '2022-01-12 15:57:33'),
(84, '6254a35f-9137-4725-b466-d8a680491d78', 'azii', '178@gmail.com', 'aa', NULL, NULL, '454-568-351', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6IkRneWFNeUgvVzg1SmNaSnIrSjdxY1E9PSIsInZhbHVlIjoiTHdLbXpiaE9KZ2t2bkk2cHVndThzUT09IiwibWFjIjoiNjk5ZDg1MmEzMzc1NzhkOGY3ZWJkNTE1NzljNDMyY2I0MDkxNGZlNWM0MzAwNjYzZmVjYjNkNTVkYTU1OWQxZiIsInRhZyI6IiJ9', '$2y$10$1APg4bugUrjLLmT9CT0RJOa70EuuLT/HJyVT4KAswzFQ.ToCxTwpK', NULL, 'inv_vxQWp', NULL, NULL, '2022-01-12 16:15:27', '2022-01-12 16:17:19'),
(85, 'ddb65e98-772d-4d50-be1c-640ff9d03360', 'Ghazi Tozri', 'tanzori@icloud.com', NULL, NULL, '216', '21654311362', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6ImpwWjFoL1c3NURhNXdKTmR2UlZqRnc9PSIsInZhbHVlIjoiMDltazZpTHAvSzVBY0dhZ0t3dGpWU3ZCWGNCNTRwNjhxK1JUUS9uNlc4MD0iLCJtYWMiOiJkNzYzZGYwMDIyMmM0MmY2NmMyZWZkZTEyYTBlMTk0MTQyNGUxYWIzNTdkNTYyMTFiMWUzMDg0MzcwOGMxMDNjIiwidGFnIjoiIn0=', '$2y$10$FP6zU6zKjSpcF189oiDFQ.dNrI8ozIMAJZGVBvtpF3QZsHn70fBSi', NULL, NULL, NULL, NULL, '2022-01-13 05:27:04', '2022-01-13 05:27:04'),
(86, '5fa5150c-3c7a-4c25-83ae-01fd8a2ce385', 'Faiz', 'lijazi@thichanthit.com', NULL, NULL, '1', '116465106465', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InFEbFBYWmNsYktIaFBvM3ZDejJKN0E9PSIsInZhbHVlIjoianVtV2F0U0I2cDJMc3VFMEYxbGs4OHlCRXRPK3M3RjRZUFlpejFmNDdRaz0iLCJtYWMiOiJhODVkZGY3YzVhMmMzY2NjZjRkOTA5Y2M5YTkwZDQ5N2Q2OTgwYWQyYjE4Y2IxMTMzM2UzNDU1OTVlY2M1YTAyIiwidGFnIjoiIn0=', '$2y$10$f2U2T6AcLNI7bGvYYQBo3eWJVgIRP.3Nb.60lbFrdHcYcYZlVG0mS', NULL, NULL, NULL, NULL, '2022-01-13 12:05:00', '2022-01-13 12:05:00'),
(87, '26dc0063-470b-453d-847c-dc71f16aa358', 'test again', 'tim@gmail.com', 'Mystery', NULL, NULL, '03669955639', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjBlNFNTWFdDdzhVSW1wTlBCVS9UQ0E9PSIsInZhbHVlIjoiQVNicnBKWHZmUE5kWUpCYmtZNmZUdz09IiwibWFjIjoiMjUyODM0MmFmYTM2NGJhMTVjNzZmNjEwMTRmYjYwNTgwOGRiNWYwMzIwNzJlNGY4NzY3ODYwZjUwMjZhZDU1MCIsInRhZyI6IiJ9', '$2y$10$.x1w3ps9QhSTdCJnRSX3keMjrh/buKRQY.PkrjVOqXaHmFpmF4SHi', NULL, 'inv_6hshj', NULL, NULL, '2022-01-17 16:41:54', '2022-01-17 16:41:54'),
(88, 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', 'Hajile', 'Hello@outlook.com', 'Mystery', NULL, NULL, '2087860965', 'suspend', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'WT2022', 1, 0, 0, 'eyJpdiI6IjFvSGgyWjdvVGFXZjFJeGZoTDdzZVE9PSIsInZhbHVlIjoiWkVGZi82VjhNVGIxUm5BcVZESzI0UGQyQ3RReHVQRzRiQTUxYjdPdEEvUT0iLCJtYWMiOiJhNWYxNWMxYThhOTZlYWZiMzgxNWZmNTVlZDNiN2ZmZjg2ZGY5ZGY4ODlmNTA4NDAzYjQ5NzNlMGI0M2NjYzdmIiwidGFnIjoiIn0=', '$2y$10$7z6vfiTjYl0SA/UTF87My.T7FwxHKhrt5mTyqIRfqMI6.yVrKeHtS', NULL, 'inv_l6A3L', NULL, NULL, '2022-01-18 01:54:57', '2022-01-25 06:50:22'),
(89, '41757d45-2bec-4e8f-89c1-617dfdb8a789', 'Hello', 'Me@yahoo.com', 'Drama', NULL, NULL, '2087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6Ik9PQlBkTE5XOXRUbjR1WndPNDFnZWc9PSIsInZhbHVlIjoiN2FxYU1QVXN3TExqbjBWeGdvU2xIZz09IiwibWFjIjoiYzZjMGQ3ZjVjM2QyMGVlYWVhMzU0N2RkMmQ2MzcwZDk4YzNmYmIyOWQyNjZhYmFkMTIxYzcwM2Y4MjZhYzJhOSIsInRhZyI6IiJ9', '$2y$10$nrG.yjtLFDN0ZmnqkNrep.bUPMKAkSTT6PbL.KiXL3fiGeU8tMsHO', NULL, 'inv_L56m4', NULL, NULL, '2022-01-18 02:03:04', '2022-01-18 02:03:04'),
(90, '7742c325-a2d5-45fd-bd09-91f6e0cd608a', 'Jacob Edwards', 'jacob@writerstalkadmin.com', NULL, NULL, '1', '19895530477', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, NULL, 0, 0, 0, 'eyJpdiI6ImNHY2Zhd1NFd2FuN0liOTc0cngxQUE9PSIsInZhbHVlIjoiLytvelR0QTlpWjNvOGgxUkdLL2NNU3hNZnZiUGtnYzYyTmNoSk00VGlWUT0iLCJtYWMiOiJiNGU1MDljNDg2MzBkOWIyNmQ0NDFhZmRjYjUzMmNiOGMyZjEwNDk4NzVkMjAwOTY5Y2IzMzBkOTI3OTQ3MjEzIiwidGFnIjoiIn0=', '$2y$10$Dd1NpqvGj8nIQz3mi9nsQeIpfSNEaZ4YwOdFgfjbndtgGirW35N1C', NULL, NULL, NULL, NULL, '2022-01-18 03:55:28', '2022-01-18 03:58:13'),
(91, '48868441-b03e-42a5-ade0-d291c6a284ba', 'Ali Saqib', 'steadfatpictures@outlook.com', 'Horror', NULL, NULL, '56465656565665', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Ikp6ZkJtVEw4L2FUU1lCS3JxQ2FicUE9PSIsInZhbHVlIjoiQkliREtMeXJPY0RHWktZZnBCNmJmZz09IiwibWFjIjoiMjNiN2FhYjYzODNlOTVmOThmMmQ3YWIyZTcwODQ5NTExMzEyMTEwYzNlNGI3MjZmNDY1YmMwOGI0M2UwZGM4MyIsInRhZyI6IiJ9', '$2y$10$tT4y7C9QRIOQ/gnA4vROyOFlS4Gf4t0rrnPJfFhR4cFuOdanANTc2', NULL, 'inv_33DQX', NULL, NULL, '2022-01-18 15:18:21', '2022-01-18 15:18:21'),
(92, 'a8a06127-fd56-4b0b-8ec9-ea6c5469c5bb', 'Mark Melody', 'testqw@gmail.cok', 'Horror', NULL, NULL, '33344645545454', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6IjNDeTMyT2Ivam5Da2ZOYXo4dU8wU2c9PSIsInZhbHVlIjoiL2JtdldrdEkwS3MxcmI4RVZMVXpmdz09IiwibWFjIjoiNjA0YWIxNGZiMGRiMjZmNzc0MTVkYzFiZDYzYWFhYjBjYjQ4MGU4YzQyZjBjZTRjNmEzNDRkZDlkZmI2ZmNmMyIsInRhZyI6IiJ9', '$2y$10$RIEDlWICTCQgSENN1FDWzeTuXcKebmDLMTRuJKr3zE3nsaczB0w8u', NULL, 'inv_oTGLk', NULL, NULL, '2022-01-18 15:22:50', '2022-01-18 15:22:50'),
(93, '8bab8021-40e6-4bf2-8a00-7ee08f92a72f', 'Mark Melody', 'teshtqw@gmail.cok', 'Horror', NULL, NULL, '33344645545454', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6IlZsaStDRnZWVkNkM045WHp4RWpHNWc9PSIsInZhbHVlIjoiS1E0L2J1MWVFdGU5cnZzUnZTZkk4Zz09IiwibWFjIjoiMGFkYTJlMDNkYzlkN2MxN2E1ZWY0ZGQ4N2QwYTlmMTRmNzc2MzY1ZTQwMTI1ZjhjMDQ1ODE3MGRmZmJhNTViZCIsInRhZyI6IiJ9', '$2y$10$YJXNZNI4UeUG0JWflu5pBecO1QDzMFEwYXSqL61w2d56nkr5FM1na', NULL, 'inv_PpWXJ', NULL, NULL, '2022-01-18 15:23:21', '2022-01-19 07:42:37'),
(94, '0994934f-d8ad-441f-91dd-868294e7002d', 'hdbdbsbs', 'yuu@gmail.com', 'Mystery', NULL, NULL, '9559946595', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6IkpQRzNOeExiQmVNYk1YOFlRSmh0dnc9PSIsInZhbHVlIjoiN3RMVE9Cc1RCdWpRcVpRdjdhZkxqUT09IiwibWFjIjoiMDJhODQxMDA5YjVlN2ZiY2FhNjE5NTM4ZGQ3OWI5ZGM3M2I5YTEyZTA5MWY3YWIzNzhkNTVmZjAyZGRiMThhOSIsInRhZyI6IiJ9', '$2y$10$l55HP44zJRMR5uzKucPHeO492/QFNuVFjLRRZtBhaUO28Lu8S.w2K', NULL, 'inv_SVBxW', NULL, NULL, '2022-01-18 15:30:30', '2022-01-18 15:30:30');
INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `favorite_genres`, `email_verified_at`, `country_code`, `contact_no`, `status`, `bio`, `image`, `views`, `show_top_hundered`, `promo_code`, `promo_used`, `referral_used`, `verify_user`, `secret_key`, `password`, `api_token`, `invitation_key`, `device_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(95, '7c6521e3-72a1-4329-89fe-8ef071b44c2e', 'test1', 'testr@gmail.com', 'Horror', NULL, NULL, '646464646464', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'WT2022', 1, 0, 0, 'eyJpdiI6Ik4xeWJWTU9BR0p5MG9XOEVqajQzZ3c9PSIsInZhbHVlIjoiNnlhOGlqTU1zdWxBNXUrY25URlJNZz09IiwibWFjIjoiNDkyNGEyZDIyNDhlYTdmMjJjMjc2YmUxZTk2NGI5NDQwZmJkMGRiYjJiNjU5NDk1Y2EwN2RiZWViOWIwNzQ3ZSIsInRhZyI6IiJ9', '$2y$10$mo35utbG8VU9Nx.1iW7EoO/FDXTNp0f4EhBhwwXw2tTt4PNpfOEOC', NULL, 'inv_ndQq0', NULL, NULL, '2022-01-18 15:55:27', '2022-01-18 15:55:27'),
(96, 'cdd69661-41da-4976-8225-be9584e55d05', 'Akash', 'niharranjandasmu@gmail.com', NULL, NULL, '880', '8801761152186', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'q0k8Be', 1, 0, 1, 'eyJpdiI6Im9hVEtPTmNuWjg1QlNSRDRlMnFqYUE9PSIsInZhbHVlIjoiY2JXWTVrc3NVK3lVd2lzVWpxNnhSdz09IiwibWFjIjoiYzllY2I5ZGE2OWNmY2FhMzM0NjhmMTk0MTQ2MzY4ZTI2YWMwNzVlMjM2MjM0OGUwNGE3ZGZhMDc1ODYyMjVhNiIsInRhZyI6IiJ9', '$2y$10$dER13T03foDirHVdw0oUTuUp/d.lb/DfqflScbK7UEOCjs8IPtjIG', NULL, NULL, NULL, NULL, '2022-01-21 14:09:58', '2022-01-21 14:33:24'),
(97, 'daa81715-0e38-47aa-a38f-8f5cb814934f', 'Me', 'writerstalk@gmail.com', NULL, NULL, '1', '12087860968', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6Im4xZDhxb0RTbUZ5cUNMeTc5d1FHM3c9PSIsInZhbHVlIjoiZG5kQXBvTjZsQ1NUaFU2TGpaL1RmNFpRc04rRGZaVWU5emxLU3VvdXFSYz0iLCJtYWMiOiI2NDYwZTU5YzIwYzBmYjhlNmEzMjdiZjg4MDY0OGE3NmExMTNkYzJjOTFiZDY5NDMwMGY5ZTg5ZjRkOWEwOTA4IiwidGFnIjoiIn0=', '$2y$10$T5BhYJ8/P7SbaJIqYHUZ1.CBqqX89TgpBl0Ksbwl3Z3sOUd5dYBMG', NULL, NULL, NULL, NULL, '2022-01-22 01:21:37', '2022-01-22 01:21:37'),
(98, '5f0deb17-7821-4ec7-ab33-22174b732ebc', 'PF OT', 'admin@admin.com', NULL, NULL, '1', '12089876543', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6IllOZFU1Q2E2WGlyLytpUmZGSGZhQUE9PSIsInZhbHVlIjoiVGNad1JQNEZvVm1LdjhNNkNhNklIUT09IiwibWFjIjoiNTY5YmUwNDNjMzQxODFjMjI1NDgzYTkyZDQ1MmU0NmY0NmE5ZDNkMzJiYWI4NDk0NmI2OGNiZmU5ZmVmZTM1YSIsInRhZyI6IiJ9', '$2y$10$xOzK6GUnOzyuTrZ.ntGZd.la0UZGvH4hNdEAlCZM1qJULMExjz2Wq', NULL, NULL, NULL, NULL, '2022-01-22 01:49:58', '2022-01-22 01:49:58'),
(101, 'ad81baa7-c62a-4be4-81db-3d97ec0d9bc1', 'Akash', 'itakashdas@gmail.com', NULL, NULL, '880', '8801761152185', 'active', NULL, 'assets/imgs/user_avatar.png', 1, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6Ild3cHVicm13ZEdDNHYxeEhXd2VTMnc9PSIsInZhbHVlIjoiY29pTTEyYWc0V0RxUWxTOVFvaWpUUT09IiwibWFjIjoiNzVlOTViN2ZlYTRiZjljMzc1NmRhMTg0MTA1NWU4ZDA3NTI5MzU0OWZiZWZjYjAzOGJhY2NjNGVjZTQ3Y2U5MCIsInRhZyI6IiJ9', '$2y$10$uQjX7Ns29/0.AEf3rbm1cuvUgMU/zf85zvgZ8jQhttpnIV69Ewfny', NULL, NULL, NULL, NULL, '2022-01-22 02:30:11', '2022-01-22 20:13:23'),
(102, 'a5d3853f-b227-425b-a66a-bf7aebf54868', 'Faith', 'faith@gmail.com', NULL, NULL, '1', '12087190276', 'suspend', NULL, 'assets/imgs/user_avatar.png', 1, 0, 'Adam22', 1, 0, 0, 'eyJpdiI6Ikw3TVQzVVdaOGRrY0pwa1JEck5NUUE9PSIsInZhbHVlIjoiczVFb21ueENiZHhpWmVwR0J5eURzQ3c4bHo1K053SHUvQndXM2RWd09jdz0iLCJtYWMiOiIwMWE2ZjUzMjQyYWY1OWM1NWRhMWNmZjg1ZjUyNzYzN2NiZjg2MDAzOTdjNzg2MTY5ZmI5NjUyNTBlODhkMTE0IiwidGFnIjoiIn0=', '$2y$10$Y29xk7Th/ROO3gc3myD6hOzKNItQOhr4NFIvJ1q62jyjB.Mm8aaaa', NULL, NULL, NULL, NULL, '2022-01-22 02:35:44', '2022-01-25 06:51:06'),
(103, '94fe9d1c-8246-42ac-8791-e103e2639aa2', 'Joe Blow', 'qwert@gmail.com', NULL, NULL, '1', '12087660985', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, 'Alee22', 1, 0, 0, 'eyJpdiI6Ik1mM28zaUlwMDdLMWpFeVB1b3Azc2c9PSIsInZhbHVlIjoiczQvU0FhRlJINWRYTHpCZGZDZm12YlVUVXZtNThkREx2VUJUWlpYbFNBYz0iLCJtYWMiOiIxODVlNWMzNGIxOTBjMmFjMzlkMjExODc0ZmMwZjllMTg1Y2NjYmNlMDljMWRjMzVmYThkOGQ2NWVhMTIyN2EzIiwidGFnIjoiIn0=', '$2y$10$PyQoLwbaEvNTu/QZUBCbquNAOBIrGz8QKeojWS4ZSyKcpEHboAKEy', NULL, NULL, NULL, NULL, '2022-01-22 04:36:59', '2022-01-22 04:36:59'),
(104, '741be022-e61e-4ba8-9e14-1a0dddea94c4', 'Eueh', 'Dudh@gmail.com', 'Fantasy', NULL, NULL, '2058643972', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjkvaWE3N21LbFVvOXY5eW5KMHFCM1E9PSIsInZhbHVlIjoickhrRTA1RDhJcitVMVpnOEZzN2lKYVJpU2hpUmF4Q2JOTGowVklzUTM1QT0iLCJtYWMiOiJlYjI1YzU2OTZhNGMwZTQ5MGU0ZGE3ZGFhNjI3MTBjNGMxZTlkNTFkMTNjNmVmMzgyZDc4NWM0NmQ1OTA0ZDcxIiwidGFnIjoiIn0=', '$2y$10$CU5w5Mx4VdmNIq8dINFymO16nbJ.M7e/4TWiSkUtIT97gnRk9iaeq', NULL, 'inv_63Aed', NULL, NULL, '2022-01-22 04:39:48', '2022-01-22 04:39:48'),
(105, '1fa5a314-583e-475b-ba7e-86b7618f5004', 'cherif amine sriret', 'cherifsriret@gmail.com', NULL, NULL, '213', '2130699042510', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6Im4wbktiU2JaMUFFdHh6VG9UTTB4cGc9PSIsInZhbHVlIjoiUS9DQTNPZXlmaUtHTStKSVBmanR5Rnp1dXgybXRGMlR6bkxwaVJyNE9Daz0iLCJtYWMiOiIxMjJhYjFiNmY4NGRmNTMyMTBlM2Q4NDMwNmY5OWMzMjY5YWY5ZGQyMTEyNmI3YWUzY2Y2OWRiZWMwYWUyMzc1IiwidGFnIjoiIn0=', '$2y$10$G3uYSmWRuBtWjD5zFEvgL.95gnTiC6F/W10Fo129RVx2RD1.JmHI.', NULL, NULL, NULL, NULL, '2022-01-22 05:10:38', '2022-01-22 05:10:38'),
(106, '836579be-6efd-484b-bb32-60aaff95f67a', 'Ho', 'poiu@gmail.com', NULL, NULL, '1', '12087895642', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IkI3eGR6ZkV1bFkyaE1ITVJzNmszYnc9PSIsInZhbHVlIjoiNGVEbkFQdG5KQ0pUTDJnWTNGeG9SLytpMmg5STVmUDUvMitmZFFpYytSbz0iLCJtYWMiOiJhNjk0YjI1NDNhYWJiZTdiYzQ3MTBhMzlhNDM1YTc4MDcyMjhjNzhhMTgwMzJiMDQ4NjU4OThkZDU4ODc3MGQwIiwidGFnIjoiIn0=', '$2y$10$2QM8VYRGPJac3OBohyUcW.c2w/hYRsPkRiXBU7Rgh8vEWhD8WFFgO', NULL, NULL, NULL, NULL, '2022-01-23 01:54:32', '2022-01-25 11:44:10'),
(107, '5bd62c88-a2a4-466d-85b1-76b1c5145545', 'Girus', 'elijah@gmail.com', NULL, NULL, '1', '12087860965', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IjErNHo2ckkyRy94NHpFMFZDMnhQVFE9PSIsInZhbHVlIjoiUW0rMWptRkNpeWROSjlCTFN1Skd5bnJ6NktPZ08vbE1DN05NUGgzTzhWZz0iLCJtYWMiOiIzZmMzZmExZDc5ZThkYTQwMGU0MDczOWZiNGE4NGI5YjJlM2QyOGM4OTI3ODc0MDI3MWViMzYzOWQ5ZDBjNzAyIiwidGFnIjoiIn0=', '$2y$10$C6WErdB0b0QA7BO0XHZS4uSD416kfQOV1lz.V2Hv2YzAQ/IlZpgQ6', NULL, NULL, NULL, NULL, '2022-01-25 16:57:24', '2022-01-25 16:57:24'),
(108, 'c3016cf9-e094-49a6-90a2-05dfe122a88b', 'user', 'farzamamjad1@gmail.com', NULL, NULL, '92', '92+923200489980', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6InJINHBEMmJldUpQVnRhSk94Z2dTQ0E9PSIsInZhbHVlIjoiSHFEMEhBU0Z0UFhBNjVzOWhvZFZZZXFkWmgxQjNsazNJeW12VHN2d21UST0iLCJtYWMiOiIwMmIzNmMzN2I3Mzc5NDU3YzdlOTg0YjYxM2Q2NGVhMTNhNWRhNjUxMGFlNWI1ODMyOWVkYmU1MjU5M2IzMjY4IiwidGFnIjoiIn0=', '$2y$10$vuGxVV6x1sangWHRdf2zLeIIWYrW24dcOrfHiz8e.rXof6iB6hw7u', NULL, NULL, NULL, NULL, '2022-01-26 03:53:21', '2022-01-26 03:53:21'),
(109, '32acb1d3-3196-4199-b8db-db5de28648d0', 'Heather', 'Holstonbythesea@msn.com', NULL, NULL, '1', '12086261751', 'active', NULL, 'assets/imgs/user_avatar.png', 0, 0, NULL, 0, 0, 0, 'eyJpdiI6IlZrbUt3QzBCaE1HeEJ2ZXdWZVFvRHc9PSIsInZhbHVlIjoiY3Nrc3FqRklERFhNQXhIODI2azFodz09IiwibWFjIjoiNjZkM2E5Y2JjNDliZTIwYWE3YWU4ZTgxY2E4YjM1NGEzMmZlN2E5MDQxZjAzM2U0M2FhYzY5M2NmMjc2NzcwMCIsInRhZyI6IiJ9', '$2y$10$.2q0DXKzLEUUH4VIjLnTDunLDq3msrSVK1CsuWSAhoU/XiMqyBKoi', NULL, NULL, NULL, NULL, '2022-02-03 04:41:44', '2022-02-03 04:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_connections`
--

CREATE TABLE `user_connections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_connections`
--

INSERT INTO `user_connections` (`id`, `uuid`, `sender_id`, `receiver_id`, `created_at`, `updated_at`) VALUES
(1, 'f698c51b-9264-4d34-ba3a-f3f47be91b21', '63370a83-3a80-483d-9d30-85a438eca10b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-11-09 21:22:57', '2021-11-09 21:22:57'),
(2, '4d2b0b65-eef5-4aee-9e8a-f67cc140d5db', '15576bea-f562-41f4-b10a-932d8a910290', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-08 16:59:57', '2021-12-08 16:59:57'),
(3, '39e801c0-f0a6-4dda-b00e-8a75e64b32e5', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-28 05:40:27', '2021-12-28 05:40:27'),
(4, '4744b8a0-44c1-4511-aa22-e0f084dc0c23', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', '2021-12-28 05:48:32', '2021-12-28 05:48:32'),
(5, '9557869d-fb16-4a76-afb8-0ba820a064cb', '003c0093-675c-41f9-b0e9-09c1b830198b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-28 05:59:10', '2021-12-28 05:59:10'),
(6, '12f259f0-0ef9-40ed-b0c4-91bb244a827c', 'cf68a928-6479-40c2-91c4-921024dc9c37', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-29 01:17:16', '2021-12-29 01:17:16'),
(7, '852d9ad3-be52-4197-b39b-3add85a74532', '05c2ba3e-d7b2-4ed1-af10-6b1769ed4b3c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-29 18:27:33', '2021-12-29 18:27:33'),
(8, '95f7b108-2986-4fde-91d0-ed27946ce8be', 'b3aa3a02-c4e7-4d21-843e-a538d8a6ac9b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-29 19:11:09', '2021-12-29 19:11:09'),
(9, '9a3fa7e7-1268-4369-8b1d-e90ca3a4cd90', 'f83da394-2076-43b7-8845-e25511f93233', '3f0e74e3-90cc-4826-b1cf-35f83f053816', '2021-12-30 14:58:17', '2021-12-30 14:58:17'),
(10, '89335964-698c-4c06-8aae-192be854abe1', '213d5d65-a887-41eb-88f8-604e12acc266', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-31 07:39:52', '2021-12-31 07:39:52'),
(11, '46fe32a9-a020-4be0-9694-82e07c9ee8aa', 'f99a3837-d15d-4348-9fd4-12500122250f', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2021-12-31 07:41:36', '2021-12-31 07:41:36'),
(12, '62f4d065-ce8d-4884-a107-bbebb828615b', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '44d4d510-7070-4857-8625-a93453c25a0e', '2022-01-01 20:07:18', '2022-01-01 20:07:18'),
(13, '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', '2022-01-01 21:28:27', '2022-01-01 21:28:27'),
(14, '9c75aea7-547f-4cc7-94eb-715160363dd0', '506500ef-9929-4a0c-903d-93cbea7bf733', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', '2022-01-01 21:29:00', '2022-01-01 21:29:00'),
(15, 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2022-01-07 05:42:26', '2022-01-07 05:42:26'),
(16, 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2022-01-07 23:31:45', '2022-01-07 23:31:45'),
(17, '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '506500ef-9929-4a0c-903d-93cbea7bf733', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '2022-01-08 05:11:47', '2022-01-08 05:11:47'),
(18, 'c4fb91dc-23a1-4cd7-9383-d7f6a24903d6', '8f675b9d-652a-4294-a170-77859236089c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2022-01-10 23:47:05', '2022-01-10 23:47:05'),
(19, '0a711498-6348-4056-a383-08b307ad8788', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '2022-01-12 15:58:50', '2022-01-12 15:58:50'),
(20, 'de3c129d-e6cf-4e3c-b2c5-2c91edb42c59', '93d47f95-420d-4001-bb25-53f99c9ce00b', '6254a35f-9137-4725-b466-d8a680491d78', '2022-01-12 16:17:29', '2022-01-12 16:17:29'),
(21, '308c0608-99cb-4d26-9c96-18751c1d8101', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '2022-01-13 16:44:27', '2022-01-13 16:44:27'),
(22, 'efeb31bd-ec16-487d-b2a5-9d049bcae86b', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', '506500ef-9929-4a0c-903d-93cbea7bf733', '2022-01-18 04:04:42', '2022-01-18 04:04:42'),
(23, '19f2d913-bb35-4fe2-b83d-a649bcd1595a', '506500ef-9929-4a0c-903d-93cbea7bf733', '5134231b-ae8c-457a-a987-e468ee55754f', '2022-01-18 04:06:07', '2022-01-18 04:06:07'),
(24, '81167f4c-0466-4ba0-9c65-38fb51b8bd14', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', '2022-01-23 03:03:33', '2022-01-23 03:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

CREATE TABLE `user_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_messages`
--

INSERT INTO `user_messages` (`id`, `uuid`, `connection_id`, `sender_id`, `receiver_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 'e9329c53-ba1c-4d4f-97b2-79ce5a386fb2', '62f4d065-ce8d-4884-a107-bbebb828615b', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '44d4d510-7070-4857-8625-a93453c25a0e', 'hjjj', '2022-01-01 20:07:18', '2022-01-01 20:07:18'),
(2, '06e0e414-d306-491d-9c38-3e1c7fa265d9', '62f4d065-ce8d-4884-a107-bbebb828615b', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', '44d4d510-7070-4857-8625-a93453c25a0e', 'ook', '2022-01-01 20:12:09', '2022-01-01 20:12:09'),
(3, '6241eb3a-ba15-48df-9f92-6d663d7ce29a', '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'Hi elijah', '2022-01-01 21:28:27', '2022-01-01 21:28:27'),
(4, 'd2a69101-e263-4d02-bfec-ed65f197146d', '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'Whatsup?', '2022-01-01 21:28:33', '2022-01-01 21:28:33'),
(5, 'd057c317-6a7f-4035-9096-9e210e6513d6', '9c75aea7-547f-4cc7-94eb-715160363dd0', '506500ef-9929-4a0c-903d-93cbea7bf733', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'Hi test', '2022-01-01 21:29:00', '2022-01-01 21:29:00'),
(6, 'cf1110cb-067a-4924-bd1f-2a50d9b89bea', '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'sxsx', '2022-01-01 21:31:47', '2022-01-01 21:31:47'),
(7, '23465a29-644a-4381-8b28-006e807f46e7', '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'Hi', '2022-01-01 21:31:54', '2022-01-01 21:31:54'),
(8, '8ad52bec-43e7-4309-af0a-223c796697d0', '9c75aea7-547f-4cc7-94eb-715160363dd0', '506500ef-9929-4a0c-903d-93cbea7bf733', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'az', '2022-01-01 21:43:56', '2022-01-01 21:43:56'),
(9, '312b4b10-5ce8-480d-9830-c9c68d2b38cc', '02332980-c42b-4156-b1a2-e0025389a073', '506500ef-9929-4a0c-903d-93cbea7bf733', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 'zaa', '2022-01-01 21:44:00', '2022-01-01 21:44:00'),
(10, '79341d1b-26a0-4f83-ba32-d464687d4c46', '9c75aea7-547f-4cc7-94eb-715160363dd0', '506500ef-9929-4a0c-903d-93cbea7bf733', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'az', '2022-01-05 16:48:24', '2022-01-05 16:48:24'),
(11, 'f05b39ff-de20-44f0-bef5-617aeedf6b01', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'My life be like', '2022-01-07 05:42:26', '2022-01-07 05:42:26'),
(12, '58ded70b-dcb3-4866-87b3-ff610f93ec8e', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Eth', '2022-01-07 06:02:32', '2022-01-07 06:02:32'),
(13, '84d726ff-b87c-4998-b598-44acf46db09c', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'B', '2022-01-07 06:02:34', '2022-01-07 06:02:34'),
(14, '147a97f8-b454-4517-a2db-53fd4347841a', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'okk', '2022-01-07 23:31:45', '2022-01-07 23:31:45'),
(15, '73f14d96-df16-4339-b69a-fcf5324960eb', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Good afternoon please find attached the following URL', '2022-01-07 23:43:48', '2022-01-07 23:43:48'),
(16, '2b9c357b-ccff-4cf0-b0b2-14be804c13cb', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'okii', '2022-01-07 23:59:49', '2022-01-07 23:59:49'),
(17, 'bf46d3ca-c393-4103-b455-115e158c8a21', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bssbbsbs', '2022-01-08 00:00:36', '2022-01-08 00:00:36'),
(18, 'de905a8f-dafe-4497-9c03-25589d40e3a2', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'hshshs', '2022-01-08 00:00:39', '2022-01-08 00:00:39'),
(19, '7c73ec29-43fe-465b-b463-523e318e239f', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'vdvvdbd', '2022-01-08 00:00:41', '2022-01-08 00:00:41'),
(20, '9fb8367f-10e2-43aa-b543-7b84a6c3773b', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'ddbdbddb', '2022-01-08 00:00:45', '2022-01-08 00:00:45'),
(21, '51a28acd-c0b7-4dff-ae51-76c523dda02d', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bdbddb', '2022-01-08 00:00:48', '2022-01-08 00:00:48'),
(22, 'd430d528-576d-437f-a29a-1f899eca3136', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bdbdbdbdbdbdbdbd', '2022-01-08 00:00:56', '2022-01-08 00:00:56'),
(23, '7d6109de-dd42-4640-9eba-c5413e325224', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bdbdbdbdd', '2022-01-08 00:01:00', '2022-01-08 00:01:00'),
(24, '9fe1f85a-633d-4a7b-8dc4-9a6fb5766dd0', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bdbdhsjsjsjsjsjsjsjsjsjsjsjdjsjsjdjkdkdndjdndnxnxnxxnxbxbhxhxhxhdhdhdhdhdhdhdbdbdbdbdhdjdjdjdjdjdjdjdhddhhdhdhdhdhdhdhdjdjdkdkdkddkkddk', '2022-01-08 00:03:57', '2022-01-08 00:03:57'),
(25, '4c96504e-5932-43c4-a514-b2593f3ba623', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'uidjjddjjd', '2022-01-08 00:05:16', '2022-01-08 00:05:16'),
(26, 'f3b2bd53-95bd-4ee7-867a-4f6bccef36b4', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'hdbdbddjndjdkdkdkdkdkddkdkdkkdkdkddkkdkdkdkdjdjdjdjffj', '2022-01-08 00:12:06', '2022-01-08 00:12:06'),
(27, 'd168bf43-9903-44df-a247-c188aa04b658', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'jjdjdjddjdjdjjdndjdjdjddjdjdjdjdjddjdjdjdjd', '2022-01-08 00:12:12', '2022-01-08 00:12:12'),
(28, '44c8eea4-18a9-479d-b98f-861f840d8252', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bdbdbdbdndndndndndndbdbdjdjd', '2022-01-08 00:12:16', '2022-01-08 00:12:16'),
(29, '3e8aaeed-81f1-4f57-91b5-d5e4bba6fe15', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'nbdhdjnjdjdksskkskdldkdjdkksksksksksks', '2022-01-08 00:12:20', '2022-01-08 00:12:20'),
(30, '025e93e7-e19d-4691-a16a-711690059f3f', 'c6d31013-ca0f-4890-90f1-6b2bac6ae53e', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'bnndjdndnddjdjkdldldndnddjjddjdhbdbdbbdbsbsbsnsjsjsjsjsjs', '2022-01-08 00:12:27', '2022-01-08 00:12:27'),
(31, '706614ff-f1f4-4a64-8ee6-643ffa55ea3b', '9c75aea7-547f-4cc7-94eb-715160363dd0', '506500ef-9929-4a0c-903d-93cbea7bf733', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'Azaz', '2022-01-08 00:31:16', '2022-01-08 00:31:16'),
(32, '6486f613-26b8-49d5-a18e-328ecfbed66c', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Way to go!', '2022-01-08 05:04:29', '2022-01-08 05:04:29'),
(33, '24f40dd3-14f4-410e-ad0b-0539887cea2a', '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '506500ef-9929-4a0c-903d-93cbea7bf733', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Hey!', '2022-01-08 05:11:47', '2022-01-08 05:11:47'),
(34, '8258d4a9-b053-420d-8a94-41ccfcb70922', '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '506500ef-9929-4a0c-903d-93cbea7bf733', 'How’s it going?', '2022-01-08 05:12:25', '2022-01-08 05:12:25'),
(35, '49ba0b2a-0483-4ef9-b7be-a807e7f2f1b7', '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Hey!', '2022-01-08 08:40:10', '2022-01-08 08:40:10'),
(36, 'd19b353f-0b50-47ef-b0ad-fc3d4b1a018a', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Weh', '2022-01-08 08:40:22', '2022-01-08 08:40:22'),
(37, '7f4d3f99-f218-4914-93ba-dc3fbdacda8e', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Web', '2022-01-08 08:40:23', '2022-01-08 08:40:23'),
(38, '8a6804dc-e3fa-47aa-8ccb-fc20578ad46c', 'd54e494e-1749-4be6-9ddb-5a057c5fa6bb', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Weh', '2022-01-08 08:40:23', '2022-01-08 08:40:23'),
(39, 'da62d195-44a6-4a7d-a5bc-103e785996fc', '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Djei', '2022-01-08 13:04:23', '2022-01-08 13:04:23'),
(40, 'd25243cb-14c0-447c-ba23-427b0d4795a4', '11a4f3d3-d5ec-4bdb-8d3e-49fead4a6f77', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Djei', '2022-01-08 13:04:26', '2022-01-08 13:04:26'),
(41, '719e2e60-8d6b-4301-9791-d0ad2750c5ab', 'c4fb91dc-23a1-4cd7-9383-d7f6a24903d6', '8f675b9d-652a-4294-a170-77859236089c', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'okkkk', '2022-01-10 23:47:05', '2022-01-10 23:47:05'),
(42, '6cf51c03-3a17-4590-b624-bf348975efd0', '0a711498-6348-4056-a383-08b307ad8788', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'hi', '2022-01-12 15:58:50', '2022-01-12 15:58:50'),
(43, 'ed7029a2-a2a7-4249-a691-a6ed6f30a1cb', '0a711498-6348-4056-a383-08b307ad8788', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Hello', '2022-01-12 16:08:30', '2022-01-12 16:08:30'),
(44, 'b4ab65a1-4009-41ce-8ead-5110fdfd0a62', '0a711498-6348-4056-a383-08b307ad8788', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'how are', '2022-01-12 16:08:45', '2022-01-12 16:08:45'),
(45, 'ec99186d-224b-4e12-8d24-0a63ca52929a', '0a711498-6348-4056-a383-08b307ad8788', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Fine you', '2022-01-12 16:08:55', '2022-01-12 16:08:55'),
(46, '2e6ec4b6-5841-4c99-bae5-181bf30eb71a', '0a711498-6348-4056-a383-08b307ad8788', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'what do you do', '2022-01-12 16:09:09', '2022-01-12 16:09:09'),
(47, 'b60a63e1-f4ec-483a-9058-41785b803dc9', '0a711498-6348-4056-a383-08b307ad8788', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Just job', '2022-01-12 16:09:19', '2022-01-12 16:09:19'),
(48, '12b3805f-b25c-4af8-9e52-398350c771f7', '0a711498-6348-4056-a383-08b307ad8788', '93d47f95-420d-4001-bb25-53f99c9ce00b', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'hows your new year???', '2022-01-12 16:09:39', '2022-01-12 16:09:39'),
(49, 'aef3909e-385a-4eb4-a732-366307ad5574', '0a711498-6348-4056-a383-08b307ad8788', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '93d47f95-420d-4001-bb25-53f99c9ce00b', 'Awesome 👍', '2022-01-12 16:09:50', '2022-01-12 16:09:50'),
(50, '7516ad90-21df-4bc1-8d78-c3d5cc20eeae', 'de3c129d-e6cf-4e3c-b2c5-2c91edb42c59', '93d47f95-420d-4001-bb25-53f99c9ce00b', '6254a35f-9137-4725-b466-d8a680491d78', 'tff', '2022-01-12 16:17:29', '2022-01-12 16:17:29'),
(51, '861772b1-0bd4-494c-8849-17b51fa46cf8', '308c0608-99cb-4d26-9c96-18751c1d8101', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Ok', '2022-01-13 16:44:27', '2022-01-13 16:44:27'),
(52, '44892f6a-f665-45f2-8f65-ab6dc24dddd1', '308c0608-99cb-4d26-9c96-18751c1d8101', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Collaboration', '2022-01-13 16:44:42', '2022-01-13 16:44:42'),
(53, 'ed45b00a-5477-44db-92e8-85601dfde084', '308c0608-99cb-4d26-9c96-18751c1d8101', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'why not', '2022-01-13 16:46:26', '2022-01-13 16:46:26'),
(54, '8711c8d6-3d73-40d2-a619-6a802b56f2b4', '308c0608-99cb-4d26-9c96-18751c1d8101', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'tell me location and time', '2022-01-13 16:46:45', '2022-01-13 16:46:45'),
(55, 'bd86d1f7-57dd-4794-a4e2-57fac242db53', '308c0608-99cb-4d26-9c96-18751c1d8101', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'Ya sure', '2022-01-13 16:46:56', '2022-01-13 16:46:56'),
(56, '44135049-14d2-451a-bf74-3a263851fb74', '308c0608-99cb-4d26-9c96-18751c1d8101', '7efe18bc-2f07-4b41-a5da-3cb251db3398', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'I’ll update you later', '2022-01-13 16:47:10', '2022-01-13 16:47:10'),
(57, '22805450-cbe0-4983-a546-39b60bd7ab3a', 'efeb31bd-ec16-487d-b2a5-9d049bcae86b', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Hey', '2022-01-18 04:04:42', '2022-01-18 04:04:42'),
(58, 'a2585c4f-9ade-48c9-87fa-d2d675d4c4c7', 'efeb31bd-ec16-487d-b2a5-9d049bcae86b', '506500ef-9929-4a0c-903d-93cbea7bf733', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', 'dfghj', '2022-01-18 04:04:51', '2022-01-18 04:04:51'),
(59, 'd7709ff5-cf5b-4471-b02c-1bfb2267fa8e', '19f2d913-bb35-4fe2-b83d-a649bcd1595a', '506500ef-9929-4a0c-903d-93cbea7bf733', '5134231b-ae8c-457a-a987-e468ee55754f', 'Hey Pog', '2022-01-18 04:06:07', '2022-01-18 04:06:07'),
(60, '9b2dac5c-615b-458c-80be-65be293ea2bd', '81167f4c-0466-4ba0-9c65-38fb51b8bd14', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Yay o', '2022-01-23 03:03:33', '2022-01-23 03:03:33'),
(61, '2cc1a0a2-08f1-4b2e-854d-dd376646112d', '81167f4c-0466-4ba0-9c65-38fb51b8bd14', '741be022-e61e-4ba8-9e14-1a0dddea94c4', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'Yay o', '2022-01-23 03:03:33', '2022-01-23 03:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_payments`
--

CREATE TABLE `user_payments` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `payment` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `pay_key` varchar(255) DEFAULT NULL,
  `promo_code` varchar(255) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_payments`
--

INSERT INTO `user_payments` (`id`, `uuid`, `user_id`, `payment`, `status`, `pay_key`, `promo_code`, `days`, `end_date`, `created_at`, `updated_at`) VALUES
(1, '7a36b5c6-d72e-4ee3-a388-b792885d60cb', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 0, 'trial', NULL, NULL, 3, '2021-11-12 11:57:11', '2021-11-09 18:57:11', '2021-11-09 18:57:11'),
(2, '28e978c9-bbe5-47fb-a194-d3e4d1d3666c', '63370a83-3a80-483d-9d30-85a438eca10b', 0, 'trial', NULL, NULL, 3, '2021-11-12 14:21:00', '2021-11-09 21:21:00', '2021-11-09 21:21:00'),
(3, '46eeaca8-2807-425e-9915-60536cbb5c04', 'e0e3636a-1034-44e2-b716-0caa1b5a1089', 0, 'trial', NULL, NULL, 3, '2021-11-12 16:39:01', '2021-11-09 23:39:01', '2021-11-09 23:39:01'),
(4, 'a5b75263-1f05-4cc0-b256-9c0e5297cd63', 'afa92057-b887-4e5f-8977-eca73e795ec0', 0, 'trial', NULL, NULL, 3, '2021-11-15 13:22:05', '2021-11-12 20:22:05', '2021-11-12 20:22:05'),
(5, '9f7b54e5-23a2-458a-9c14-65a0a94d4117', '1b2f446a-0e0a-433b-a7f3-eccb5295a74a', 0, 'trial', NULL, NULL, 3, '2021-11-16 10:11:55', '2021-11-13 17:11:55', '2021-11-13 17:11:55'),
(6, 'afc91d99-d881-4e1e-8500-76008db75d1f', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 0, 'trial', NULL, NULL, 3, '2021-11-18 10:24:32', '2021-11-15 17:24:32', '2021-11-15 17:24:32'),
(7, '18ea6e62-fe16-4ba2-a8f3-8215f3dee385', 'afa92057-b887-4e5f-8977-eca73e795ec0', 4.99, 'pending', NULL, NULL, 30, '2021-12-16 17:18:39', '2021-11-17 00:18:39', '2021-11-17 00:18:39'),
(8, '00de50ad-1a1f-4b71-b656-fc012183f6d5', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 0, 'trial', NULL, NULL, 3, '2021-11-19 17:19:53', '2021-11-17 00:19:53', '2021-11-17 00:19:53'),
(9, '2186743d-b463-4d86-9430-0f6ed7149fd6', 'ed5ea7e6-5bc9-4f79-8428-6996474674dc', 0, 'trial', NULL, NULL, 3, '2021-11-25 18:21:17', '2021-11-23 01:21:17', '2021-11-23 01:21:17'),
(10, '60f5bc75-867b-439c-91fe-2457a07618f7', '506500ef-9929-4a0c-903d-93cbea7bf733', 0, 'trial', NULL, NULL, 3, '2040-11-27 17:31:30', '2021-11-25 00:31:30', '2021-11-25 00:31:30'),
(11, 'd949f4a5-4536-4227-85e6-ddb6a28d460b', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 4.99, 'pending', NULL, NULL, 30, '2021-12-24 17:40:41', '0000-00-00 00:00:00', '2021-11-25 00:40:41'),
(12, '32de572b-faea-4a11-8736-c10ef46e8c1e', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 0, 'trial', NULL, NULL, 3, '2021-11-27 17:40:58', '2021-11-25 00:40:58', '2021-11-25 00:40:58'),
(13, '27aac846-7f7f-44f7-bcd2-4a7d65c636d1', '01e1c9b6-a656-4dd9-ae2a-17526be2ca9e', 0, 'trial', NULL, NULL, 3, '2021-12-03 18:31:29', '2021-12-01 01:31:29', '2021-12-01 01:31:29'),
(14, 'cf6ffe80-c909-4a02-91dd-0f8a20a87640', '7c8e80e4-bd89-4e99-b2e2-f09232907d7a', 0, 'trial', NULL, NULL, 3, '2021-12-06 18:36:49', '2021-12-04 01:36:49', '2021-12-04 01:36:49'),
(15, '175d8e6c-9bcf-4b99-bc8a-493f397ea4c7', '0b1b21e0-f70d-4237-bfbb-850976350e81', 0, 'trial', NULL, NULL, 3, '2021-12-07 05:40:37', '2021-12-04 12:40:37', '2021-12-04 12:40:37'),
(16, '66527d17-ae00-402d-96bd-aca6dca8bd76', '95c25fd1-3ee7-4f50-bd92-7aee87dd3c3a', 0, 'trial', NULL, NULL, 3, '2021-12-10 10:40:51', '2021-12-07 17:40:51', '2021-12-07 17:40:51'),
(17, '57d243bc-9737-4156-b919-65a1b980d2b5', 'f65f6bc3-8172-4ee1-82d8-ba3f2512c23c', 0, 'trial', NULL, NULL, 3, '2021-12-10 17:00:58', '2021-12-08 00:00:58', '2021-12-08 00:00:58'),
(18, '9b4e8a6f-1305-468a-9617-6e1de99f405b', '15576bea-f562-41f4-b10a-932d8a910290', 0, 'trial', NULL, NULL, 3, '2021-12-11 09:52:55', '2021-12-08 16:52:55', '2021-12-08 16:52:55'),
(19, '2005fdcf-80e5-4bb4-98c8-e98168d1ffec', '0b1b21e0-f70d-4237-bfbb-850976350e81', 4.99, 'pending', NULL, NULL, 30, '2022-01-08 23:17:35', '2021-12-10 06:17:35', '2021-12-10 06:17:35'),
(20, '6ede67f2-7ad7-416b-b478-7a7357d37730', '95c25fd1-3ee7-4f50-bd92-7aee87dd3c3a', 4.99, 'pending', NULL, NULL, 30, '2022-01-09 10:45:17', '2021-12-10 17:45:17', '2021-12-10 17:45:17'),
(21, 'e3f1243e-4953-4676-823d-e9296643eddc', 'fcc2a2b2-eb18-43b5-9eb9-416744495bbe', 0, 'trial', NULL, NULL, 3, '2021-12-13 11:30:19', '2021-12-10 18:30:19', '2021-12-10 18:30:19'),
(22, '4fe884b1-50c7-418a-ac6f-202b95b92f87', '3e7b1e43-2691-44ae-ae75-e6e8912b2116', 0, 'trial', NULL, NULL, 3, '2021-12-13 11:52:25', '2021-12-10 18:52:25', '2021-12-10 18:52:25'),
(23, 'd96b8d56-a36f-46b1-8f9a-cd8e24472820', 'b8803fc3-0d2c-4f09-a4c7-c7c03cf49fa4', 0, 'trial', NULL, NULL, 3, '2021-12-13 11:53:54', '2021-12-10 18:53:54', '2021-12-10 18:53:54'),
(24, '4873a24d-8e61-48df-81d0-10d5d04fb67b', 'fa32bdd0-4c5f-491e-92ee-af49f025b379', 0, 'trial', NULL, NULL, 3, '2021-12-13 15:39:49', '2021-12-10 22:39:49', '2021-12-10 22:39:49'),
(25, '326ea81f-d928-4bbf-a07a-8e3e7357e1e3', 'fa32bdd0-4c5f-491e-92ee-af49f025b379', 4.99, 'pending', NULL, NULL, 30, '2022-01-09 15:40:33', '2021-12-10 22:40:33', '2021-12-10 22:40:33'),
(26, '01b2a39a-3212-4225-aa9a-3f8414030e55', 'b06f1192-a9fa-44d9-ab04-ad50a1c22b9e', 0, 'trial', NULL, NULL, 3, '2021-12-13 15:41:11', '2021-12-10 22:41:11', '2021-12-10 22:41:11'),
(27, 'b35812b4-ffa1-4686-987f-c7dfe23c1bd8', 'b06f1192-a9fa-44d9-ab04-ad50a1c22b9e', 4.99, 'pending', NULL, NULL, 30, '2022-01-09 15:41:55', '2021-12-10 22:41:55', '2021-12-10 22:41:55'),
(28, '7f8a3238-258a-4f6a-b0ed-12841cf9eb96', 'e6746104-e8b8-4a34-9a6c-0fd4ad18d904', 0, 'trial', NULL, NULL, 3, '2021-12-14 17:23:40', '2021-12-12 00:23:40', '2021-12-12 00:23:40'),
(29, '464d5a09-b397-4cd0-a9dd-bfd8c4aa4552', '18503b1d-7f5f-4b6a-97fa-11daf14dce7e', 0, 'trial', NULL, NULL, 3, '2021-12-14 17:25:44', '2021-12-12 00:25:44', '2021-12-12 00:25:44'),
(30, 'f31f91c9-501e-4cdb-81de-7af466c69755', '12f27bac-10aa-43bc-9bbc-69ad1b0b6691', 0, 'trial', NULL, NULL, 3, '2021-12-14 17:31:23', '2021-12-12 00:31:23', '2021-12-12 00:31:23'),
(31, 'f42eac81-3c07-4a14-8e5c-553dc12b5b8c', '59eb6b50-a32d-4ebf-bee7-faadfaf6c20e', 0, 'trial', NULL, NULL, 3, '2021-12-14 17:31:42', '2021-12-12 00:31:42', '2021-12-12 00:31:42'),
(32, 'e960f4a9-4b0f-4051-820f-54569a39baf2', 'eab7018c-fec3-453b-bdc4-6d1b6925ad4e', 0, 'trial', NULL, NULL, 3, '2021-12-14 17:34:02', '2021-12-12 00:34:02', '2021-12-12 00:34:02'),
(33, '96eb44a8-35b4-4469-823f-d110c741f71f', '486f6f53-0134-40f2-8242-a03e0d77b4ee', 0, 'trial', NULL, NULL, 3, '2021-12-11 18:08:44', '2021-12-12 01:08:44', '2021-12-12 01:08:44'),
(34, '589f89df-9880-4895-a8a1-c057860e5c8a', '486f6f53-0134-40f2-8242-a03e0d77b4ee', 4.99, 'pending', NULL, NULL, 30, '2022-01-10 18:08:58', '2021-12-12 01:08:58', '2021-12-12 01:08:58'),
(35, 'edb7256b-2a89-400b-b5c5-cc434c2aa16c', 'ec230c23-5bc1-463d-9212-bbc8e6617a3b', 0, 'trial', NULL, NULL, 3, '2021-12-11 18:12:46', '2021-12-12 01:12:46', '2021-12-12 01:12:46'),
(36, '0bfec77f-965e-4655-9bf2-601805367a5d', '06dfdf0c-5d68-43ce-a4f2-08afbf808ebc', 0, 'trial', NULL, NULL, 3, '2021-12-14 18:13:56', '2021-12-12 01:13:56', '2021-12-12 01:13:56'),
(37, 'f92a6e67-ed52-4f00-b369-6b936b69db21', '001efd7c-adcf-41d0-9c24-0c0e4ab14439', 0, 'trial', NULL, NULL, 3, '2021-12-14 20:51:51', '2021-12-12 03:51:51', '2021-12-12 03:51:51'),
(38, '31ae8d66-7f02-4b1c-b1ff-30dea16f48e9', 'ab213e73-3b2c-4c4b-864c-de2151ccf6cb', 0, 'trial', NULL, NULL, 3, '2021-12-14 21:48:33', '2021-12-12 04:48:33', '2021-12-12 04:48:33'),
(39, 'f29e6b65-39be-46f8-bd52-9fd3d55ac59c', 'a3bc87e9-d036-4df1-895f-23f5a7c58219', 0, 'trial', NULL, NULL, 3, '2021-12-17 10:44:29', '2021-12-14 17:44:29', '2021-12-14 17:44:29'),
(40, '9c374a0c-e5fe-4a87-879b-f661d00350b8', '351e4933-59cf-41af-a0aa-6990c1fe5a08', 0, 'trial', NULL, NULL, 3, '2021-12-19 10:35:06', '2021-12-16 17:35:06', '2021-12-16 17:35:06'),
(41, '70f7f454-2575-437e-8ca9-445bf04fa243', '0e4bf93f-763f-4f76-81a5-4c988b565bd6', 0, 'trial', NULL, NULL, 3, '2021-12-19 11:06:58', '2021-12-16 18:06:58', '2021-12-16 18:06:58'),
(42, '516e67a3-e08a-4134-869e-7323a6ff9f61', 'ce377ae4-ede9-4f10-b741-0d36ff050593', 0, 'trial', NULL, NULL, 3, '2021-12-19 11:09:27', '2021-12-16 18:09:27', '2021-12-16 18:09:27'),
(43, 'a565f3d4-1d15-4675-bf57-d34b6ea901eb', '9e816eb8-be52-4038-8229-2536469c33be', 0, 'trial', NULL, NULL, 3, '2021-12-19 11:14:22', '2021-12-16 18:14:22', '2021-12-16 18:14:22'),
(44, '88d24b6b-a60f-474e-9467-54248a79ad5d', 'd3bb7079-7379-4468-91dc-4460a43ee80b', 0, 'trial', NULL, NULL, 3, '2021-12-22 09:55:37', '2021-12-22 16:55:37', '2021-12-22 16:55:37'),
(45, 'fc7490d0-7b57-4122-b970-b39ff57976d4', '5d2fb2ac-7895-4e93-8e44-8882ccaa28e7', 0, 'trial', NULL, NULL, 3, '2021-12-26 04:33:55', '2021-12-23 11:33:55', '2021-12-23 11:33:55'),
(46, 'fee5bf10-1a7f-4568-85dd-e1d2735388eb', 'd3bb7079-7379-4468-91dc-4460a43ee80b', 4.99, 'pending', NULL, NULL, 30, '2022-01-22 09:41:14', '2021-12-23 16:41:14', '2021-12-23 16:41:14'),
(47, 'f14ffd93-3fab-4a23-b0d1-fa069f6d723e', '43f1d3b0-9d23-41b6-9acc-8756f8e69bc2', 0, 'trial', NULL, NULL, 3, '2021-12-26 09:57:47', '2021-12-23 16:57:47', '2021-12-23 16:57:47'),
(48, '95b7f433-02c9-47d4-929b-010188d50273', 'caaeb23d-a83b-42d4-b913-0c87fc49fb40', 0, 'trial', NULL, NULL, 3, '2021-12-26 16:51:38', '2021-12-23 23:51:38', '2021-12-23 23:51:38'),
(49, 'ba5f34d1-2b6c-4c5a-a90d-b8b0c1014b04', '2767bb62-d43d-46fa-b5f7-be8bb7c372ed', 0, 'trial', NULL, NULL, 3, '2021-12-25 15:34:00', '2021-12-25 22:34:00', '2021-12-25 22:34:00'),
(50, 'b89ef355-17da-4f37-b376-012db162bda5', 'b4665684-c07a-40a4-b026-d6a82dd06791', 0, 'trial', NULL, NULL, 3, '2021-12-25 15:34:41', '2021-12-25 22:34:41', '2021-12-25 22:34:41'),
(51, 'f3335518-5808-4f67-9754-75149c98921b', 'be9fd82b-3840-44a9-bffb-93918c7c2157', 0, 'trial', NULL, NULL, 3, '2021-12-30 09:41:05', '2021-12-27 16:41:05', '2021-12-27 16:41:05'),
(52, '487cf050-c2cb-482a-9c1a-7155a2653ee7', '6066f2b0-07d3-4c77-951b-340244253e30', 0, 'trial', NULL, NULL, 3, '2021-12-27 14:37:54', '2021-12-27 21:37:54', '2021-12-27 21:37:54'),
(53, 'f74a478d-dced-456f-a88f-669d8973d76a', '508f074b-9a41-4ab8-a982-83f180f86af1', 0, 'trial', NULL, NULL, 3, '2021-12-30 14:55:24', '2021-12-27 21:55:24', '2021-12-27 21:55:24'),
(54, '50b3a55f-f723-407c-9b04-04d6de91087b', '003c0093-675c-41f9-b0e9-09c1b830198b', 0, 'trial', NULL, NULL, 3, '2021-12-26 23:20:06', '2021-12-28 01:14:08', '2021-12-28 06:20:06'),
(55, '241a44e0-8107-4f4e-80f0-a8d73373e280', '129b33c7-73f6-4e4d-9095-51b451fbcd7a', 0, 'trial', NULL, NULL, 3, '2021-12-27 19:13:29', '2021-12-28 02:13:29', '2021-12-28 02:13:29'),
(56, 'ae8433a4-bb8a-4e1f-9f8a-0f01e1017164', '129b33c7-73f6-4e4d-9095-51b451fbcd7a', 49.99, 'pending', NULL, NULL, 365, '2022-12-27 19:14:01', '2021-12-28 02:14:01', '2021-12-28 02:14:01'),
(57, 'd4f4cb7d-18fc-4725-a5b7-4e1d9c204011', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 4.99, 'pending', NULL, NULL, 30, '2022-01-26 19:28:36', '2021-12-28 02:28:36', '2021-12-28 02:28:36'),
(58, '9c812541-ce3e-4b55-aa85-9d7742dcd07f', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 0, 'trial', NULL, NULL, 3, '2021-12-26 22:58:35', '2021-12-28 02:30:47', '2021-12-28 05:58:35'),
(59, 'ccd2153d-ab63-4de4-98a5-4d311fa56d2a', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 0, 'trial', NULL, NULL, 3, '2021-12-30 22:43:22', '2021-12-28 05:43:22', '2021-12-28 05:43:22'),
(60, '5ec4137e-14a9-4cfb-bf09-1e053bd53967', '3a9c094b-1ebc-4dfb-9a0c-deb6f2b29323', 49.99, 'pending', NULL, NULL, 365, '2022-12-27 22:59:03', '2021-12-28 05:59:03', '2021-12-28 05:59:03'),
(61, '3168f935-5bad-44d8-9590-ada3428a40dd', '003c0093-675c-41f9-b0e9-09c1b830198b', 4.99, 'pending', NULL, NULL, 30, '2022-01-26 23:23:49', '2021-12-28 06:23:49', '2021-12-28 06:23:49'),
(62, '4ca41d34-5936-4534-aec9-016e87e1d11a', 'cf68a928-6479-40c2-91c4-921024dc9c37', 0, 'trial', NULL, NULL, 3, '2021-12-30 23:56:37', '2021-12-28 06:56:37', '2021-12-28 06:56:37'),
(63, '8b80a1d3-2315-4f07-98a4-7ee6af1e8923', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 0, 'trial', NULL, NULL, 3, '2021-12-31 00:20:12', '2021-12-28 07:20:12', '2021-12-28 07:20:12'),
(64, 'b7897abd-5230-41db-b262-d4dc74b64198', 'e6fbf4cc-a14e-4104-b300-fdeb5d8cd54e', 0, 'trial', NULL, NULL, 3, '2021-12-28 15:43:56', '2021-12-28 22:43:56', '2021-12-28 22:43:56'),
(65, 'bb9ac7b0-2fbe-47d4-93ce-9ed2e460c8f1', 'e6fbf4cc-a14e-4104-b300-fdeb5d8cd54e', 4.99, 'pending', NULL, NULL, 30, '2022-01-27 15:49:04', '2021-12-28 22:49:04', '2021-12-28 22:49:04'),
(66, 'fbb26079-a5f4-4fae-9aff-21729556f77b', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 0, 'trial', NULL, NULL, 3, '2021-12-31 19:35:27', '2021-12-29 02:35:27', '2021-12-29 02:35:27'),
(67, '5d535080-01b1-4acd-89fa-efc9ddf43d19', '05c2ba3e-d7b2-4ed1-af10-6b1769ed4b3c', 0, 'trial', NULL, NULL, 3, '2022-01-01 09:20:39', '2021-12-29 16:20:39', '2021-12-29 16:20:39'),
(68, 'ad6d2fb5-0d94-4b27-a63a-d6371322f307', 'b3aa3a02-c4e7-4d21-843e-a538d8a6ac9b', 0, 'trial', NULL, NULL, 3, '2022-01-01 12:08:55', '2021-12-29 19:08:55', '2021-12-29 19:08:55'),
(69, '77cde0d4-229b-465f-b410-19fc3f4580c6', '3f0e74e3-90cc-4826-b1cf-35f83f053816', 0, 'trial', NULL, NULL, 3, '2022-01-02 07:50:03', '2021-12-30 14:50:03', '2021-12-30 14:50:03'),
(70, '7a5c7ffb-916f-4f16-bce0-a79f03d9e1e5', 'c234fb07-e63b-47ca-bc6a-ee840fd2e6df', 0, 'trial', NULL, NULL, 3, '2022-01-02 07:54:26', '2021-12-30 14:54:26', '2021-12-30 14:54:26'),
(71, '604ad4f6-a273-4bb7-92d1-f2e7a89b7ca4', 'f83da394-2076-43b7-8845-e25511f93233', 0, 'trial', NULL, NULL, 3, '2022-01-02 07:56:47', '2021-12-30 14:56:47', '2021-12-30 14:56:47'),
(72, '8e785645-dde3-463a-9c87-068ffb215d57', 'cf68a928-6479-40c2-91c4-921024dc9c37', 4.99, 'pending', NULL, NULL, 30, '2022-01-30 00:32:08', '2021-12-31 07:32:08', '2021-12-31 07:32:08'),
(73, '6038fc74-37f9-4a6b-a0ac-17ae6c85d3c3', '213d5d65-a887-41eb-88f8-604e12acc266', 0, 'trial', NULL, NULL, 3, '2022-01-03 00:33:26', '2021-12-31 07:33:26', '2021-12-31 07:33:26'),
(74, '236d5f59-54f0-49aa-9859-347d647c2af9', 'f99a3837-d15d-4348-9fd4-12500122250f', 0, 'trial', NULL, NULL, 3, '2022-01-03 00:41:11', '2021-12-31 07:41:11', '2021-12-31 07:41:11'),
(75, 'd6c6d087-1d11-4f6b-b55f-d5fffb90b99e', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 0, 'trial', NULL, NULL, 3, '2022-01-03 03:18:45', '2021-12-31 10:18:45', '2021-12-31 10:18:45'),
(76, '2ff17b8a-eab3-4a9b-8904-a89378beb06e', '3aaec9e8-bfb4-4b81-bbe8-5abca169e7a6', 0, 'trial', NULL, NULL, 3, '2022-01-04 10:10:23', '2022-01-01 17:10:23', '2022-01-01 17:10:23'),
(77, 'f50bb2ca-83a8-41d6-9efd-6a659d3c3eaa', '1db97194-3ab7-4e37-a08d-6aa9f5e5a94b', 0, 'trial', NULL, NULL, 3, '2022-01-04 10:10:35', '2022-01-01 17:10:35', '2022-01-01 17:10:35'),
(78, '683debd4-9ec9-4591-9364-e08566f009ba', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 0, 'trial', NULL, NULL, 3, '2022-01-04 10:38:50', '2022-01-01 17:38:50', '2022-01-01 17:38:50'),
(79, 'e2e9f328-e358-43c8-b2c7-2714a24b89d2', '44d4d510-7070-4857-8625-a93453c25a0e', 0, 'trial', NULL, NULL, 3, '2022-01-04 13:03:27', '2022-01-01 20:03:27', '2022-01-01 20:03:27'),
(80, 'dd38f6a3-b233-4739-b854-0d7abdf2ac6e', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 0, 'trial', NULL, NULL, 3, '2022-01-04 22:52:51', '2022-01-02 05:52:51', '2022-01-02 05:52:51'),
(81, '033ff4f2-cb0c-4a83-bbd8-bbde5740869c', '7c577412-cdbd-46f0-b832-5316b068b2b4', 0, 'trial', NULL, NULL, 3, '2022-01-07 11:52:04', '2022-01-04 18:52:04', '2022-01-04 18:52:04'),
(82, 'e4d90801-3915-4824-aec6-781c9330a94b', '0af8cc95-9921-4b73-a245-52342b742e84', 0, 'trial', NULL, NULL, 3, '2022-01-09 08:44:14', '2022-01-06 15:44:14', '2022-01-06 15:44:14'),
(83, '12381092-213b-473c-856f-f8b48e99eb87', 'b17315f9-c372-409b-bb9b-16240925bafe', 0, 'trial', NULL, NULL, 3, '2022-01-09 08:44:47', '2022-01-06 15:44:47', '2022-01-06 15:44:47'),
(84, 'de2fa8fa-0670-41c3-a028-60f6608f6920', '71732563-d868-48e3-ae21-4b11e525ade4', 0, 'trial', NULL, NULL, 3, '2022-01-09 08:47:18', '2022-01-06 15:47:18', '2022-01-06 15:47:18'),
(85, 'b4c25114-064e-4d76-b51e-38579758d0b3', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 0, 'trial', NULL, NULL, 3, '2022-01-09 22:39:22', '2022-01-07 05:39:22', '2022-01-07 05:39:22'),
(86, '026d4088-ce67-4713-a9a6-442c480b9ab1', 'a00a83f9-bfa2-465e-a5e9-44b702f224a3', 0, 'trial', NULL, NULL, 3, '2022-01-10 15:53:16', '2022-01-07 22:53:16', '2022-01-07 22:53:16'),
(87, '60380419-5d6c-49f6-8981-12faaa5c7038', 'babeaefa-10db-41c8-b8ca-474d2d8bbd71', 0, 'trial', NULL, NULL, 3, '2022-01-10 16:26:36', '2022-01-07 23:26:36', '2022-01-07 23:26:36'),
(88, 'd3849362-274a-4be2-89a9-b3f6b714c891', '49cff6b9-d9c0-42c6-870b-b19cfe92df11', 0, 'trial', NULL, NULL, 3, '2022-01-10 16:30:48', '2022-01-07 23:30:48', '2022-01-07 23:30:48'),
(89, 'a0ed32fd-8c22-47c3-9db1-c8d6b3a00f77', '2a9079b2-30c8-4774-bcd3-a7916f78ca49', 4.99, 'pending', NULL, NULL, 30, '2022-02-06 20:09:59', '2022-01-08 03:09:59', '2022-01-08 03:09:59'),
(90, '624ac62e-9fcf-4b7f-9a23-80396e0b332e', '77ef87e7-a42f-4bab-b15c-e468a16cc706', 0, 'trial', NULL, NULL, 3, '2022-01-11 06:53:50', '2022-01-08 13:53:50', '2022-01-08 13:53:50'),
(91, '8ee81cf0-28cd-4e4e-819c-eddadb628916', 'c2af7023-e62f-4641-8e1a-461cc70cb262', 0, 'trial', NULL, NULL, 3, '2022-01-11 07:34:08', '2022-01-08 14:34:08', '2022-01-08 14:34:08'),
(92, 'ec3732d6-9f1d-4c55-85ba-59550ac40731', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 49.99, 'pending', NULL, NULL, 365, '2023-01-10 14:32:43', '2022-01-10 21:32:43', '2022-01-10 21:32:43'),
(93, '616fb5cd-a7c3-460b-ba22-0654a2ebc9fa', '5134231b-ae8c-457a-a987-e468ee55754f', 0, 'trial', NULL, NULL, 3, '2022-01-13 14:34:02', '2022-01-10 21:34:02', '2022-01-10 21:34:02'),
(94, '53854ec7-4416-41f6-bd11-7ede1df840c7', 'f88bccc2-8218-4e01-b7e9-ece986466870', 0, 'trial', NULL, NULL, 3, '2022-01-13 16:06:48', '2022-01-10 23:06:48', '2022-01-10 23:06:48'),
(95, '09e9bfb5-e855-4310-b95a-7248413c4145', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 0, 'trial', NULL, NULL, 3, '2022-01-13 16:07:02', '2022-01-10 23:07:02', '2022-01-10 23:07:02'),
(96, '070bd23b-f858-4496-b609-5997ebee27a0', '8f675b9d-652a-4294-a170-77859236089c', 0, 'trial', NULL, NULL, 3, '2022-01-13 16:14:50', '2022-01-10 23:14:50', '2022-01-10 23:14:50'),
(97, 'b447683c-6506-4999-b0bd-21b5f1c7e0bd', '1cf15667-4bf7-405a-8ed5-b98b632608c6', NULL, 'trial', NULL, 'WT2022', 365000, '3022-01-11 00:17:21', '2022-01-11 07:17:21', '2022-01-11 07:17:21'),
(98, '8339476c-28a8-4294-aec9-50777201cb6c', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 0, 'trial', NULL, NULL, 3, '2022-01-15 08:41:47', '2022-01-12 15:41:47', '2022-01-12 15:41:47'),
(99, '1635b588-c529-4990-9942-8cbdd85ae235', '93d47f95-420d-4001-bb25-53f99c9ce00b', 0, 'trial', NULL, NULL, 3, '2022-01-15 08:51:40', '2022-01-12 15:51:40', '2022-01-12 15:51:40'),
(100, '2edda51c-6f7e-4e7f-967d-a2896df48474', '6254a35f-9137-4725-b466-d8a680491d78', 0, 'trial', NULL, NULL, 3, '2022-01-15 09:15:27', '2022-01-12 16:15:27', '2022-01-12 16:15:27'),
(101, '355176b0-62d5-467e-8ac3-5b365363dfa9', 'ddb65e98-772d-4d50-be1c-640ff9d03360', 0, 'trial', NULL, NULL, 3, '2022-01-15 22:27:04', '2022-01-13 05:27:04', '2022-01-13 05:27:04'),
(102, '781727b8-fcb8-4f36-8304-b13dfbe79da4', '5fa5150c-3c7a-4c25-83ae-01fd8a2ce385', 0, 'trial', NULL, NULL, 3, '2022-01-16 05:05:00', '2022-01-13 12:05:00', '2022-01-13 12:05:00'),
(103, 'c76f4e0e-2e31-484c-bc3d-8902478d8468', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 4.99, 'pending', 'AP-5W769133KB9245629', NULL, 30, '2022-02-16 09:39:56', '2022-01-17 16:39:56', '2022-01-24 15:57:12'),
(104, '0148924c-2b9d-4433-b3e3-45d9eea40f45', '26dc0063-470b-453d-847c-dc71f16aa358', 0, 'trial', NULL, NULL, 3, '2022-01-17 09:41:54', '2022-01-17 16:41:54', '2022-01-17 16:41:54'),
(105, 'd0ffc5a1-2a19-4e48-8227-4ac30ed38cb3', '26dc0063-470b-453d-847c-dc71f16aa358', 4.99, 'pending', NULL, NULL, 30, '2022-02-16 09:42:00', '2022-01-17 16:42:00', '2022-01-17 16:42:00'),
(106, '41fe0d9a-6fa1-41ab-b4d1-682fe10b8967', 'c61db8fa-f01d-4b36-be91-508a65e1c0b5', NULL, 'trial', NULL, 'WT2022', 365000, '3022-01-17 18:54:57', '2022-01-18 01:54:57', '2022-01-18 01:54:57'),
(107, 'df97e2a2-ca8e-4959-b842-da1df7e0f56b', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', 0, 'trial', NULL, NULL, 3, '2022-01-20 20:55:28', '2022-01-18 03:55:28', '2022-01-18 03:55:28'),
(108, 'f8c94eb1-a170-4020-a5c3-2342fc22d531', '48868441-b03e-42a5-ade0-d291c6a284ba', 0, 'trial', NULL, NULL, 3, '2022-01-21 08:18:21', '2022-01-18 15:18:21', '2022-01-18 15:18:21'),
(109, 'b655becf-897f-4ab8-a1b1-133b353e6a14', '7c6521e3-72a1-4329-89fe-8ef071b44c2e', NULL, 'trial', NULL, 'WT2022', 365000, '2021-01-18 08:55:27', '2022-01-18 15:55:27', '2022-01-18 15:55:27'),
(110, 'bcf9e6eb-812f-4813-9afb-924a8c797dbf', '7c6521e3-72a1-4329-89fe-8ef071b44c2e', 4.99, 'pending', 'AP-06H4462546896490H', NULL, 30, '2022-02-17 09:53:42', '2022-01-18 16:53:42', '2022-01-18 17:00:12'),
(111, 'd064ede3-5b7f-46bd-bacc-50830732a58c', 'cdd69661-41da-4976-8225-be9584e55d05', NULL, 'trial', NULL, 'q0k8Be', 365, '2023-01-21 07:09:58', '2022-01-21 14:09:58', '2022-01-21 14:09:58'),
(112, 'db896c93-b1bc-4dd5-b274-baeed1874e97', 'ad81baa7-c62a-4be4-81db-3d97ec0d9bc1', 0, 'trial', NULL, 'Adam22', 3, '2022-01-24 19:30:11', '2022-01-22 02:30:11', '2022-01-22 02:30:11'),
(113, '4405b7a8-fadb-4d6d-b07a-4964feff7c16', 'a5d3853f-b227-425b-a66a-bf7aebf54868', 0, 'trial', NULL, 'Adam22', 3, '2022-01-24 19:35:44', '2022-01-22 02:35:44', '2022-01-22 02:35:44'),
(114, 'ab2ba8c6-e2ca-4059-8166-ef4f4b64bef4', '94fe9d1c-8246-42ac-8791-e103e2639aa2', 0, 'trial', NULL, 'Alee22', 3, '2022-01-24 21:36:59', '2022-01-22 04:36:59', '2022-01-22 04:36:59'),
(115, 'ab35a01b-12db-4096-9ac2-ab90c2ecd83a', '741be022-e61e-4ba8-9e14-1a0dddea94c4', 0, 'trial', NULL, NULL, 3, '2022-01-24 21:39:48', '2022-01-22 04:39:48', '2022-01-22 04:39:48'),
(116, 'dcd4082b-4700-4dfe-a248-c960fb0d8b0a', '1fa5a314-583e-475b-ba7e-86b7618f5004', 0, 'trial', NULL, NULL, 3, '2022-01-24 22:10:38', '2022-01-22 05:10:38', '2022-01-22 05:10:38'),
(117, '28579b32-9eaa-41d8-823d-aebef72b0ad3', '836579be-6efd-484b-bb32-60aaff95f67a', 0, 'trial', NULL, NULL, 3, '2022-01-25 18:54:32', '2022-01-23 01:54:32', '2022-01-23 01:54:32'),
(118, '96057ab1-9464-47d9-8dca-da9c0809a114', '5bd62c88-a2a4-466d-85b1-76b1c5145545', 0, 'trial', NULL, NULL, 3, '2022-01-28 09:57:24', '2022-01-25 16:57:24', '2022-01-25 16:57:24'),
(119, 'a38dff92-16c1-4a7e-bf47-89785e833936', 'c3016cf9-e094-49a6-90a2-05dfe122a88b', 0, 'trial', NULL, NULL, 3, '2022-01-28 20:53:21', '2022-01-26 03:53:21', '2022-01-26 03:53:21'),
(120, '7336b387-a7ba-4ad2-9c16-d06cbca77686', '32acb1d3-3196-4199-b8db-db5de28648d0', 0, 'trial', NULL, NULL, 3, '2022-02-05 21:41:44', '2022-02-03 04:41:44', '2022-02-03 04:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_views`
--

CREATE TABLE `user_profile_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_profile_views`
--

INSERT INTO `user_profile_views` (`id`, `uuid`, `ip_address`, `user_id`, `agent`, `view`, `status`, `created_at`, `updated_at`) VALUES
(1, '4a77d954-4f45-4c0b-a11d-1ab6246e1106', '72.255.40.124', '5e0158ef-45b1-459e-8cc7-bff48fb56b05', 'okhttp/3.14.9', NULL, NULL, '2022-01-01 19:59:30', '2022-01-01 19:59:30'),
(2, 'b15b8c16-f3bb-4cec-a9b8-f52307357a6b', '72.255.40.124', '44d4d510-7070-4857-8625-a93453c25a0e', 'okhttp/3.14.9', NULL, NULL, '2022-01-01 20:07:04', '2022-01-01 20:07:04'),
(3, '5a300a31-7b96-4e61-b537-cbcfd65d9ae6', '76.178.165.0', '1c574f81-ba2b-4af5-8baa-83b94dac738e', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-02 06:26:16', '2022-01-02 06:26:16'),
(4, 'f7f58bb5-c529-46bb-9ced-947fb3e97084', '72.69.29.51', '95c25fd1-3ee7-4f50-bd92-7aee87dd3c3a', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36 Edg/96.0.1054.62', NULL, NULL, '2022-01-03 12:16:05', '2022-01-03 12:16:05'),
(5, 'ba853fff-d568-4705-abde-cec7dbcad0f4', '76.178.165.0', '38920e2f-2cee-4679-a1d4-497ecbc3a3d8', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-03 21:55:29', '2022-01-03 21:55:29'),
(6, '69ed0ae1-2745-48e7-92c1-acb66c8a4baf', '76.178.165.0', '506500ef-9929-4a0c-903d-93cbea7bf733', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-03 21:55:53', '2022-01-03 21:55:53'),
(7, '7a9c8222-8861-4c00-8d51-65fd02e318af', '76.178.165.0', '57d5686a-2dd1-4eca-b3ba-02e8c96b1d44', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-03 21:56:15', '2022-01-03 21:56:15'),
(8, '6eee3540-1327-4477-9599-0750d87354e3', '43.245.10.57', '7c8e80e4-bd89-4e99-b2e2-f09232907d7a', 'okhttp/3.14.9', NULL, NULL, '2022-01-05 19:32:21', '2022-01-05 19:32:21'),
(9, 'c479e140-56f8-4604-b0c2-cede858af901', '72.255.40.124', '7c577412-cdbd-46f0-b832-5316b068b2b4', 'okhttp/3.14.9', NULL, NULL, '2022-01-06 15:57:59', '2022-01-06 15:57:59'),
(10, '806939bd-f4c6-4259-9de6-2019a59508f7', '72.255.40.124', 'af10af2d-c62f-4564-a702-cfcda4eb9461', 'okhttp/3.14.9', NULL, NULL, '2022-01-06 23:40:30', '2022-01-06 23:40:30'),
(11, '4e6fa09d-275a-4abb-b3c6-327fca159376', '76.178.165.0', '444775e0-6404-4992-b4cf-7fc0b62d5b7b', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-07 06:12:07', '2022-01-07 06:12:07'),
(12, 'b3c17d7e-5514-4e77-9838-52ceed31ac06', '76.178.165.0', '8ac92d8e-3842-4aa8-a2a5-020d6460bc69', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-07 07:10:33', '2022-01-07 07:10:33'),
(13, '79092198-5faf-47aa-a6b2-f1a758f70719', '76.178.165.0', '174e0b1c-cab2-475b-a04d-ea02ebf148c6', 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-07 13:50:07', '2022-01-07 13:50:07'),
(14, '13bb6a08-7e70-465a-a81a-a6739ffa0e06', '76.178.165.0', '1aecfb2b-eeba-4f99-b9c2-729b1234b19c', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', NULL, NULL, '2022-01-08 05:05:16', '2022-01-08 05:05:16'),
(15, '0a86a4ae-f83d-4210-85b8-f242a1180cd6', '76.178.165.0', '99a5e1e7-f4a7-48d5-bf04-3bd8aed1cb64', 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-08 08:13:49', '2022-01-08 08:13:49'),
(16, '6a9e1b0c-dbf2-4482-93bc-28afd5dd7947', '171.61.28.192', '213d5d65-a887-41eb-88f8-604e12acc266', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', NULL, NULL, '2022-01-08 13:55:41', '2022-01-08 13:55:41'),
(17, '7a51df41-ce99-4c9f-bda2-5471291724ae', '76.178.165.0', 'fa32bdd0-4c5f-491e-92ee-af49f025b379', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-09 02:24:17', '2022-01-09 02:24:17'),
(18, '4ba2c0a2-6954-4062-a50b-2943c865eb93', '76.178.165.0', 'b8803fc3-0d2c-4f09-a4c7-c7c03cf49fa4', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-09 02:24:17', '2022-01-09 02:24:17'),
(19, 'ba1e2974-3075-4751-a0ee-bceadda384af', '72.255.40.124', 'c2af7023-e62f-4641-8e1a-461cc70cb262', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', NULL, NULL, '2022-01-10 20:49:21', '2022-01-10 20:49:21'),
(20, '21100135-9bbc-4711-a341-e050762207cf', '72.255.40.124', '7efe18bc-2f07-4b41-a5da-3cb251db3398', 'okhttp/3.14.9', NULL, NULL, '2022-01-12 15:46:09', '2022-01-12 15:46:09'),
(21, '74ca6850-fb3b-47dd-8e27-7ee3c71dfee9', '72.255.40.124', '5b06c57f-7717-4f7d-bfa0-15aaebac1072', 'okhttp/3.14.9', NULL, NULL, '2022-01-12 15:53:36', '2022-01-12 15:53:36'),
(22, '1078e01d-5aa2-4cd1-942f-8b36b9ffc156', '72.255.40.124', '6254a35f-9137-4725-b466-d8a680491d78', 'okhttp/3.14.9', NULL, NULL, '2022-01-12 16:17:19', '2022-01-12 16:17:19'),
(23, '00aacf9a-73a4-4109-ab76-927d03401ea1', '76.178.132.167', '7742c325-a2d5-45fd-bd09-91f6e0cd608a', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-18 03:58:13', '2022-01-18 03:58:13'),
(24, '3fc2d182-a383-4778-8cfe-8b615b508dc5', '76.178.165.0', '8bab8021-40e6-4bf2-8a00-7ee08f92a72f', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-19 07:42:37', '2022-01-19 07:42:37'),
(25, '27ad7a84-60f8-4636-981f-60d4ae312608', '129.45.68.17', '63370a83-3a80-483d-9d30-85a438eca10b', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36', NULL, NULL, '2022-01-22 05:41:23', '2022-01-22 05:41:23'),
(26, 'bf7cfd50-32df-4a1b-8ca4-cb24ac217f3a', '129.45.68.17', 'b4665684-c07a-40a4-b026-d6a82dd06791', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36', NULL, NULL, '2022-01-22 05:41:43', '2022-01-22 05:41:43'),
(27, '39f5f2d6-dce2-4283-833c-c5f6f94afd92', '118.179.207.109', 'ad81baa7-c62a-4be4-81db-3d97ec0d9bc1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36', NULL, NULL, '2022-01-22 20:13:23', '2022-01-22 20:13:23'),
(28, '9aba9ea6-d6a2-41bb-a1e9-e8e01c517a76', '76.178.165.0', 'a5d3853f-b227-425b-a66a-bf7aebf54868', 'Expo/2.23.2.1013698 CFNetwork/1325.0.1 Darwin/21.1.0', NULL, NULL, '2022-01-25 06:51:06', '2022-01-25 06:51:06'),
(29, '170c0c60-789b-4d76-8460-ad4d75631fe9', '76.178.165.0', 'b17315f9-c372-409b-bb9b-16240925bafe', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', NULL, NULL, '2022-01-25 14:29:41', '2022-01-25 14:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `videoable_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videoable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_posts`
--
ALTER TABLE `admin_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_post_tags`
--
ALTER TABLE `admin_post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_tips`
--
ALTER TABLE `admin_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_conversations`
--
ALTER TABLE `group_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_users`
--
ALTER TABLE `group_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highlights`
--
ALTER TABLE `highlights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highlight_genres`
--
ALTER TABLE `highlight_genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highlight_hashtags`
--
ALTER TABLE `highlight_hashtags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highlight_ratings`
--
ALTER TABLE `highlight_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highlight_views`
--
ALTER TABLE `highlight_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `penpals`
--
ALTER TABLE `penpals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quicks`
--
ALTER TABLE `quicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quicks_text`
--
ALTER TABLE `quicks_text`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_codes`
--
ALTER TABLE `referral_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_connections`
--
ALTER TABLE `user_connections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_messages`
--
ALTER TABLE `user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payments`
--
ALTER TABLE `user_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile_views`
--
ALTER TABLE `user_profile_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_posts`
--
ALTER TABLE `admin_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `admin_post_tags`
--
ALTER TABLE `admin_post_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `admin_tips`
--
ALTER TABLE `admin_tips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `group_conversations`
--
ALTER TABLE `group_conversations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `group_users`
--
ALTER TABLE `group_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `highlights`
--
ALTER TABLE `highlights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `highlight_genres`
--
ALTER TABLE `highlight_genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `highlight_hashtags`
--
ALTER TABLE `highlight_hashtags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `highlight_ratings`
--
ALTER TABLE `highlight_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `highlight_views`
--
ALTER TABLE `highlight_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276;

--
-- AUTO_INCREMENT for table `penpals`
--
ALTER TABLE `penpals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=811;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `quicks`
--
ALTER TABLE `quicks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `quicks_text`
--
ALTER TABLE `quicks_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_codes`
--
ALTER TABLE `referral_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `user_connections`
--
ALTER TABLE `user_connections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_messages`
--
ALTER TABLE `user_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `user_payments`
--
ALTER TABLE `user_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `user_profile_views`
--
ALTER TABLE `user_profile_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
